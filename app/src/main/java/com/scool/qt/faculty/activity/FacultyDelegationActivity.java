package com.scool.qt.faculty.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.FacultyFragmentRecyclerAdapter;
import com.scool.qt.model.DelegationItem;
import com.scool.qt.model.FacultiesItem;
import com.scool.qt.model.FacultyInfoResponse;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.SchoolFacultiesResp;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;
import com.scool.qt.widgets.ProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Creative Developers
 */
public class FacultyDelegationActivity extends BaseActivity implements NetworkView, FacultyFragmentRecyclerAdapter.IItemClickListener {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.applayout)
    AppBarLayout applayout;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.rv_delegation_list)
    RecyclerView rvDelegationList;
    NetworkPresenter presenter;
    FacultyFragmentRecyclerAdapter facultyDelegationAdapter;
    @BindView(R.id.iv_nointernet)
    ImageView ivNointernet;
    @BindView(R.id.rl_nointernet)
    RelativeLayout rlNointernet;
    @BindView(R.id.mainlayout)
    ConstraintLayout mainlayout;
    private LinearLayoutManager mLayoutManager;
    List<FacultiesItem> mFacultiesItem = new ArrayList<>();
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faculty_delegate_activity);
        ButterKnife.bind(this);
        presenter = new NetworkPresenter(this, new NetworkInteractor());
        rvDelegationList.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        rvDelegationList.setLayoutManager(mLayoutManager);
        facultyDelegationAdapter = new FacultyFragmentRecyclerAdapter(this, "withlogin", this);
        rvDelegationList.setAdapter(facultyDelegationAdapter);
        tvSchoolname.setText(R.string.kselect_faculty_to_delegate);
        toolbarTitle.setText(R.string.kFacultyText);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(view -> finish());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utils.isNetworkAvailable(this)) {
            if (progressDialog == null)
                progressDialog = new ProgressDialog(this, false, "");
            progressDialog.show();
            schoolFaultyData();
        } else {
            noInternetConnection();
        }

    }

    private void fetchFacultyInfo(FacultyLoginResponse facultyLoginResponse) {
        presenter.methodToFetchFacultyBasicInfo(this, Constant.UNIQUE_SCHOOL_ID, facultyLoginResponse.getFacultyId(), facultyLoginResponse.getToken());
    }

    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", this);
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
        rlNointernet.setVisibility(View.VISIBLE);
        mainlayout.setVisibility(View.GONE);
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case school_faculty:
                progressDialog.dismiss();
                SchoolFacultiesResp schoolFacultiesResp = (SchoolFacultiesResp) object;
                mFacultiesItem = schoolFacultiesResp.getFaculties();

                mFacultiesItem.removeIf(item -> item.getId() == loadFacultyData().getFacultyId());
                facultyDelegationAdapter.updateList(mFacultiesItem);
                break;
            case faculty_info:
                Glide.with(this)
                        .load(Constant.IMAGE_BASE_URL + ((FacultyInfoResponse) object).getPhoto())
                        .thumbnail(0.5f)
                        .into(imgSchool);
                break;
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
     progressDialog.dismiss();
    }


    private void schoolFaultyData() {
        if (Utils.isNetworkAvailable(this)) {
            FacultyLoginResponse facultyLoginResponse = loadFacultyData();
            fetchFacultyInfo(facultyLoginResponse);
            presenter.methodToGetSchoolFacultiesData(this, Constant.UNIQUE_SCHOOL_ID);
        } else {
            // TODO
        }
    }

    @Override
    public void itemClick(View view, int position, FacultiesItem mFacultiesItem) {
        Intent i = new Intent(this, DelegationActivity.class);
        i.putExtra("facultyName", mFacultiesItem.getName());
        i.putExtra("facultyID", String.valueOf(mFacultiesItem.getId()));
        DelegationItem item = getIntent().getParcelableExtra("delegationItem");
        i.putExtra("delegationItem", item);
        startActivity(i);
        finish();
    }
}
