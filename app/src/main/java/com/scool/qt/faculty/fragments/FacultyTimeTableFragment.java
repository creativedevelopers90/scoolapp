package com.scool.qt.faculty.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.FacultyTimeTableAdapter;
import com.scool.qt.model.ClassTimeTableResp;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.TimeTablesByClassId;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FacultyTimeTableFragment extends Fragment implements NetworkView {
    @BindView(R.id.rv_faculty)
    RecyclerView rvFaculty;
    Unbinder unbinder;
    NetworkPresenter mNetworkPresenter;
    FacultyTimeTableAdapter facultyTimeTableAdapter;
    List<TimeTablesByClassId> mTimeTableItem = new ArrayList<>();
    @BindView(R.id.tv_class_id)
    TextView tvClassId;
    @BindView(R.id.tv_language)
    TextView tvLanguage;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.constraintLayout2)
    RelativeLayout constraintLayout2;
    @BindView(R.id.iv_nointernet)
    ImageView ivNointernet;
    @BindView(R.id.rl_nointernet)
    RelativeLayout rlNointernet;
    @BindView(R.id.rl_maincontent)
    RelativeLayout rlMaincontent;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_facultytimetable, container, false);
        unbinder = ButterKnife.bind(this, view);
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        if (mTimeTableItem != null)
            facultyTimeTableAdapter = new FacultyTimeTableAdapter(mTimeTableItem, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvFaculty.setLayoutManager(mLayoutManager);
        rvFaculty.setItemAnimator(new DefaultItemAnimator());
        rvFaculty.setAdapter(facultyTimeTableAdapter);
        getTimeTable();
        return view;
    }

    private void getTimeTable() {
        tvClassId.setText("Grade - " + Constant.ACTIVE_CLASS.getName());
        tvLanguage.setText(getString(R.string.kWeekDayTitle));
        tvTime.setText(getString(R.string.kTimeTitle));
        FacultyLoginResponse facultyData = loadFacultyData();
        mNetworkPresenter.methodToFetchTimeTable(getActivity(),
                Constant.ACTIVE_CLASS.getId().intValue(),
                Constant.UNIQUE_SCHOOL_ID, facultyData.getFacultyId(), facultyData.getToken());
    }

    /**
     * Method to load the faculty data
     *
     * @return
     */
    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
        rlNointernet.setVisibility(View.VISIBLE);
        rlMaincontent.setVisibility(View.GONE);
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case faculy_get_class_time_table:
                ClassTimeTableResp mTimeTableItem = (ClassTimeTableResp) object;
                facultyTimeTableAdapter = new FacultyTimeTableAdapter(mTimeTableItem.getTimeTables(), getActivity());
                rvFaculty.setAdapter(facultyTimeTableAdapter);
                break;
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case faculy_get_class_time_table:
                Utils.showSnackBar(getActivity(), message);
                break;
        }
    }
}
