package com.scool.qt.faculty.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.util.CollectionUtils;
import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.FacultyAttendanceHomeAdapter;
import com.scool.qt.model.ClassTimeTableResp;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.TimeTablesByClassId;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;
import com.scool.qt.widgets.CalendarSliderView;
import com.scool.qt.widgets.ProgressDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FacultyAttendanceHomeFragment extends Fragment implements NetworkView, FacultyAttendanceHomeAdapter.IItemClickListener, CalendarSliderView.CalenderSliderViewListener {
    @BindView(R.id.attendance_calendar_view)
    CalendarSliderView attendanceCalendarView;
    @BindView(R.id.attendance_list_rv)
    RecyclerView attendanceListRv;
    @BindView(R.id.iv_nointernet)
    ImageView ivNointernet;
    @BindView(R.id.rl_nointernet)
    RelativeLayout rlNointernet;
    @BindView(R.id.tv_nointernet)
    TextView tvNointernet;
    private View root;
    private NetworkPresenter mNetworkPresenter;
    private LinearLayoutManager mLayoutManager;
    FacultyAttendanceHomeAdapter facultyAttendanceHomeAdapter;
    FacultyBaseFragment.ClickInterface clickInterface;
    private List<TimeTablesByClassId> mTimeTableList;
    Date calendar;
    private String pattern = "dd-MM-yyyy";
    public ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_faculty_attendance, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        initViews();
        fetchFaultyTimeTable();
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void initViews() {
        attendanceCalendarView.setOnDateChangeListener(this, getString(R.string.kTitleTextAttendance));
        attendanceListRv.setHasFixedSize(true);
        facultyAttendanceHomeAdapter = new FacultyAttendanceHomeAdapter(getActivity(), FacultyAttendanceHomeFragment.this);
        mLayoutManager = new LinearLayoutManager(getActivity());
        attendanceListRv.setLayoutManager(mLayoutManager);
        attendanceListRv.setItemAnimator(new DefaultItemAnimator());
        attendanceListRv.setAdapter(facultyAttendanceHomeAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.clickInterface = (FacultyBaseFragment.ClickInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement HeadlineListener");
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void showProgress() {
        if(progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity(), false, "");
        }
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void noInternetConnection() {
        attendanceCalendarView.setVisibility(View.GONE);
        rlNointernet.setVisibility(View.VISIBLE);
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case faculy_get_class_time_table:
                ClassTimeTableResp mTimeTableItem = (ClassTimeTableResp) object;
                mTimeTableList = mTimeTableItem.getTimeTables();
                List<TimeTablesByClassId> mList = mTimeTableList.stream().filter(item ->
                        item.getDay().equalsIgnoreCase(dayOfWeekCompare(Calendar.getInstance()))).collect(Collectors.toList());
                if (CollectionUtils.isEmpty(mList)) {
                    rlNointernet.setVisibility(View.VISIBLE);
                    tvNointernet.setText(getString(R.string.kNoAttendancefor) + " " + dayOfWeek(Calendar.getInstance()));
                    ivNointernet.setImageDrawable(getActivity().getDrawable(R.mipmap.iv_note_pen));
                } else {
                    rlNointernet.setVisibility(View.GONE);
                    facultyAttendanceHomeAdapter.setList(mList);
                }

                break;
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
    }


    private void fetchFaultyTimeTable() {
        calendar = Calendar.getInstance().getTime();
        FacultyLoginResponse facultyData = loadFacultyData();
        mNetworkPresenter.methodToFetchTimeTable(getActivity(),
                Constant.ACTIVE_CLASS.getId().intValue(),
                Constant.UNIQUE_SCHOOL_ID, facultyData.getFacultyId(), facultyData.getToken());
    }

    /**
     * Method to load the faculty data
     *
     * @return
     */
    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }

    @Override
    public void itemClick(View view, int position, TimeTablesByClassId mTimeTablesItem) {
        clickInterface.buttonClicked(0);
        FacultyAttendanceFragment facultyAttendanceFragment = new FacultyAttendanceFragment();
        Bundle args = new Bundle();
        args.putParcelable("timeTableItem", mTimeTablesItem);
        args.putString("Date", new SimpleDateFormat(pattern).format(calendar));
        facultyAttendanceFragment.setArguments(args);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.onboard_fragment,
                facultyAttendanceFragment, getString(R.string.kTitleTextAttendance))
                .addToBackStack(getString(R.string.kTitleTextAttendance))
                .commit();
    }

    @Override
    public void onDateChanged(Calendar cal) {
        List<TimeTablesByClassId> mList = mTimeTableList.stream().filter(item ->
                item.getDay().equalsIgnoreCase(dayOfWeekCompare(cal))).collect(Collectors.toList());
        calendar = cal.getTime();
        if (CollectionUtils.isEmpty(mList)) {
            rlNointernet.setVisibility(View.VISIBLE);
            tvNointernet.setText(getString(R.string.kNoAttendancefor) + " " + dayOfWeek(cal));
            ivNointernet.setImageDrawable(getActivity().getDrawable(R.mipmap.iv_note_pen));
        } else {
            rlNointernet.setVisibility(View.GONE);

        }
        facultyAttendanceHomeAdapter.setList(mList);
    }


    private String dayOfWeek(Calendar instance) {
        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
        Calendar cal = instance;
        Date dateRepresentation = cal.getTime();
        return simpledateformat.format(dateRepresentation);
    }

    private String dayOfWeekCompare(Calendar instance) {
        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE", Locale.ENGLISH);
        Calendar cal = instance;
        Date dateRepresentation = cal.getTime();
        return simpledateformat.format(dateRepresentation);
    }
}
