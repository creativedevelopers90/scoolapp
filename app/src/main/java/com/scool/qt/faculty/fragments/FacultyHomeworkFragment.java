package com.scool.qt.faculty.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.activity.HomeworkActivity;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.FacultyStudentListResponse;
import com.scool.qt.model.StudentDetailsResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FacultyHomeworkFragment extends Fragment implements NetworkView {

    @BindView(R.id.select_student_tv)
    TextView selectStudentTV;
    @BindView(R.id.student_list_rv)
    RecyclerView studentListRV;
    @BindView(R.id.rl_nointernet)
    RelativeLayout rlNoInternet;
    @BindView(R.id.cl_maincontent)
    ConstraintLayout clMainContent;

    private View root;
    private LinearLayoutManager mLayoutManager;
    private NetworkPresenter mNetworkPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_faculty_homework, container, false);
        ButterKnife.bind(this, root);
        selectStudentTV.setText(getResources().getText(R.string.kSelectStudents) + " - " + Constant.ACTIVE_CLASS.getName());
        studentListRV.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        studentListRV.setLayoutManager(mLayoutManager);
        fetchFacultyStudentList();
        return root;
    }


    public void fetchFacultyStudentList() {
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        FacultyLoginResponse facultyLoginResponse = loadFacultyData();
        mNetworkPresenter.methodToFetchFacultyStudentList(getActivity(), Constant.UNIQUE_SCHOOL_ID,
                facultyLoginResponse.getFacultyId(), Constant.ACTIVE_CLASS.getId().intValue(), facultyLoginResponse.getToken());
    }

    /**
     * Method to load the faculty data
     *
     * @return
     */
    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
        rlNoInternet.setVisibility(View.VISIBLE);
        clMainContent.setVisibility(View.GONE);
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        ReportRecyclerAdapter mAdapter = new ReportRecyclerAdapter(((FacultyStudentListResponse) object).getStudents());
        studentListRV.setAdapter(mAdapter);

    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        Log.e("== " + endpointType + " ==>", message);
    }



    class ReportRecyclerAdapter extends RecyclerView.Adapter<ReportRecyclerAdapter.ReportRecyclerViewHolder> {
        private final List<StudentDetailsResponse> students;
        public ReportRecyclerAdapter(List<StudentDetailsResponse> students) {
            this.students = students;
        }

        @NonNull
        @Override
        public ReportRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ReportRecyclerViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.faculty_student_list_item, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ReportRecyclerViewHolder holder, int position) {
            holder.root.setOnClickListener(v -> {
                Bundle bundle = new Bundle();
                if (position == 0) {
                    bundle.putString("header", "All Students - " + Constant.ACTIVE_CLASS.getName());
                } else {
                    bundle.putString("header", students.get(position - 1).getName().trim() + " - " + Constant.ACTIVE_CLASS.getName());
                    bundle.putSerializable("selected_student", students.get(position - 1));
                }
                Intent intent = new Intent(getActivity(), HomeworkActivity.class);
                intent.putExtra("select_record", bundle);
                startActivity(intent);
            });

            if (position == 0) {
                holder.studentName.setText(R.string.kAllStudentsText);
                holder.studentImage.setImageResource(R.mipmap.iv_all);
            } else {
                holder.studentName.setText(students.get(position - 1).getName().trim());
                Glide.with(getActivity())
                        .load(Constant.IMAGE_BASE_URL + students.get(position - 1).getPhoto())
                        .thumbnail(0.3f)
                        .into(holder.studentImage);
            }
        }

        @Override
        public int getItemCount() {
            return students.size() + 1;
        }

        class ReportRecyclerViewHolder extends RecyclerView.ViewHolder {
            ConstraintLayout root;
            TextView studentName;
            RecyclingImageView studentImage;

            public ReportRecyclerViewHolder(View itemView) {
                super(itemView);
                root = itemView.findViewById(R.id.student_item_root);
                studentName = itemView.findViewById(R.id.faculty_student_name);
                studentImage = itemView.findViewById(R.id.faculty_student_image);
            }
        }
    }
}
