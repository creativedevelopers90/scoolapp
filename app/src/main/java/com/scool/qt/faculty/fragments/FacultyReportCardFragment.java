package com.scool.qt.faculty.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.ReportCardListAdapter;
import com.scool.qt.base.fragment.BaseFragment;
import com.scool.qt.model.ExamListResponse;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;
import com.scool.qt.widgets.ProgressDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FacultyReportCardFragment extends BaseFragment implements NetworkView {
    @BindView(R.id.exam_list_rv)
    RecyclerView examListRV;
    @BindView(R.id.rl_nointernet)
    RelativeLayout rlNointernet;
    @BindView(R.id.cl_maincontent)
    ConstraintLayout clMainCOntent;

    private View root;
    private NetworkPresenter mPresenter;
    private LinearLayoutManager mLayoutManager;

    ReportCardListAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_faculty_report_card, container, false);
        ButterKnife.bind(this, root);

        mPresenter = new NetworkPresenter(this, new NetworkInteractor());
        mAdapter = new ReportCardListAdapter(getActivity());

        examListRV.setHasFixedSize(true);
        examListRV.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(getActivity());
        examListRV.setLayoutManager(mLayoutManager);
        fetchExamList();

        return root;
    }

    /**
     * Method to fetch exam list
     */
    public void fetchExamList() {
        mPresenter.methodToFetchFacultyExamList(getActivity(), Constant.ACTIVE_CLASS.getId().intValue(), loadFacultyData().getFacultyId(), Constant.UNIQUE_SCHOOL_ID, loadFacultyData().getToken());
    }

    /**
     * Method to load the faculty data
     *
     * @return
     */
    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }

    @Override
    public void showProgress() {
                if (progressDialog == null)
            progressDialog = new ProgressDialog(getActivity(), false, "");
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void noInternetConnection() {
        rlNointernet.setVisibility(View.VISIBLE);
        clMainCOntent.setVisibility(View.GONE);
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        if (endpointType == EndpointEnums.exam_list) {
            ExamListResponse examResponse = (ExamListResponse) object;
            mAdapter.addItemsInList(examResponse.getExams());
        }

    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        if (endpointType == EndpointEnums.exam_list) {
            Log.e("== FACULTY EXAM RES ==>", "onEndpointFailure: " + endpointType + " : " + message);
        }
    }
}
