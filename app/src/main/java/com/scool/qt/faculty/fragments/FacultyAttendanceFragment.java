package com.scool.qt.faculty.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.AttendanceListRecyclerAdapter;
import com.scool.qt.base.fragment.BaseFragment;
import com.scool.qt.model.Attendance;
import com.scool.qt.model.ClassAttendanceResponse;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.TimeTablesByClassId;
import com.scool.qt.model.UpdateAttendanceResponse;
import com.scool.qt.model.UpdateMultipleAttendanceResponse;
import com.scool.qt.model.request.AttendanceRequest;
import com.scool.qt.model.request.MultipleAttendanceRequest;
import com.scool.qt.model.request.UpdateAttendanceRequest;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;
import com.scool.qt.widgets.CalendarSliderView;
import com.scool.qt.widgets.ProgressDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.scool.qt.utils.Constant.ATTENDANCE_TYPE_ABSENT;
import static com.scool.qt.utils.Constant.ATTENDANCE_TYPE_LEAVE;
import static com.scool.qt.utils.Constant.ATTENDANCE_TYPE_PRESENT;
import static com.scool.qt.utils.Constant.ATTENDANCE_TYPE_UPDATE;

public class FacultyAttendanceFragment extends BaseFragment implements NetworkView, AttendanceListRecyclerAdapter.ItemClickListener {
    public static boolean ENABLE_EDIT_ATTENDANCE = true;
    public MultipleAttendanceRequest multipleAttendance;
    @BindView(R.id.attendance_calendar_view)
    CalendarSliderView calendarView;
    @BindView(R.id.attendance_list_rv)
    RecyclerView attendanceListRV;
    private View root;
    private NetworkPresenter mNetworkPresenter;
    private LinearLayoutManager mLayoutManager;
    TimeTablesByClassId timeTableItem;
    private Boolean isDataSaved = false;
    private CallBackListener callBackListener;
    AttendanceListRecyclerAdapter mAdapter;
    private int selectItem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_faculty_attendance, container, false);
        ButterKnife.bind(this, root);
        calendarView.setVisibility(View.GONE);
        attendanceListRV.setHasFixedSize(true);

        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        if (getActivity() instanceof CallBackListener)
            callBackListener = (CallBackListener) getActivity();

        return root;
    }

    /**
     * Method to fetch attendance
     *
     * @param calendar
     */
    public void fetchAttendance(Calendar calendar) {
        //Retrieve the value
        if(getArguments() != null) {
            timeTableItem = getArguments().getParcelable("timeTableItem");
            mNetworkPresenter.methodToFetchClassAttendanceByFaculty(getActivity(), Constant.UNIQUE_SCHOOL_ID,
                    timeTableItem.getClassListId(), loadFacultyData().getFacultyId(),
                    calendar.getTime(), loadFacultyData().getToken(), timeTableItem.getId().toString());
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        if(new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime()).equals(getArguments().getString("Date"))){
//            isDataSaved = true;
//            if(callBackListener != null)
//                callBackListener.onCallBack();
//        }
        mLayoutManager = new LinearLayoutManager(getActivity());
        attendanceListRV.setLayoutManager(mLayoutManager);
        mAdapter = new AttendanceListRecyclerAdapter(getActivity(), this, isDataSaved);
        attendanceListRV.setAdapter(mAdapter);
        fetchAttendance(Calendar.getInstance());
    }

    /**
     * Method to load the faculty data
     *
     * @return
     */
    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }

    /**
     * Method invoked from the activity when user clicked on save attendance button
     */
    public void onSaveAttendanceClicked() {
        multipleAttendance.setTime_table_id(timeTableItem.getId().toString());
        multipleAttendance.setSchoolId(Constant.UNIQUE_SCHOOL_ID);

            Optional<AttendanceRequest> multisOptional = multipleAttendance.getAttendances().stream()
                    .filter(multipleAttendanceItem ->
                    Objects.isNull(multipleAttendanceItem.getStatus())
                            || multipleAttendanceItem.getStatus() == Constant.ATTENDANCE_TYPE_NO_STATUS_MARKED).findAny();
            if(!multisOptional.isPresent()) {
                try {
                    Utils.showSnackBar(getActivity(), "PERFECT");
                    JSONObject jsonRequest = new JSONObject(new Gson().toJson(multipleAttendance));
                    mNetworkPresenter.methodToUpdateMultipleAttendanceByFaculty(getActivity(), RequestBody.create(MediaType.parse("application/json"),
                            jsonRequest.toString()), loadFacultyData().getToken());
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.hideSoftKeyboard(getActivity());
                Utils.showSnackBar(getActivity(), getString(R.string.str_mark_all_student_attendance));
            }
    }

    /**
     * Method to create a attendance object to be updated when the user change change students attendance status
     *
     * @param initialAttendance
     */
    private void initializeUpdateAttendanceObject(ClassAttendanceResponse initialAttendance) {
        multipleAttendance = new MultipleAttendanceRequest();
        List<AttendanceRequest> updateAttendanceList = new ArrayList<>();
        initialAttendance.getAttendances().stream().forEach(currentAttendance -> {
            AttendanceRequest updateAttendance = new AttendanceRequest();
            updateAttendance.setDate(currentAttendance.getDate());
            updateAttendance.setStudentId(currentAttendance.getStudentId());
            updateAttendance.setTimeTableId(timeTableItem.getId().longValue());
            updateAttendance.setStatus(currentAttendance.getAttendanceStatus());
            updateAttendanceList.add(updateAttendance);
        });
        multipleAttendance.setAttendances(updateAttendanceList);
       Optional<Attendance> optionalAttendance =  initialAttendance.getAttendances().stream().filter(item -> item.getAttendanceStatus() == ATTENDANCE_TYPE_PRESENT
                || item.getAttendanceStatus() == ATTENDANCE_TYPE_LEAVE
                || item.getAttendanceStatus() == ATTENDANCE_TYPE_ABSENT).findAny();
       if(!optionalAttendance.isPresent() && new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime()).equals(getArguments().getString("Date"))){
           isDataSaved = true;
           mAdapter.setIsDataSaved(isDataSaved);
           if(callBackListener != null)
               callBackListener.onCallBack();
       }
    }


    @Override
    public void showProgress() {
            if(progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity(), false, "");
                progressDialog.show();
            }
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void noInternetConnection() {
           if(getView() != null){
               hideProgress();
               Utils.hideSoftKeyboard(getActivity());
               Utils.showSnackBar(getActivity(), getString(R.string.check_internet));
           }
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case faculty_class_attendance:
                initializeUpdateAttendanceObject((ClassAttendanceResponse) object);
                mAdapter.setElements(((ClassAttendanceResponse) object).getAttendances());
                break;
            case faculty_update_multiple_attendance:
                Utils.showSnackBar(getActivity(), ((UpdateMultipleAttendanceResponse) object).getMessage());
                break;
            case update_attendance:
                UpdateAttendanceResponse attendanceResponse = (UpdateAttendanceResponse) object;
                mAdapter.findItemById(attendanceResponse.getId(), Long.valueOf(attendanceResponse.getStatus()));
                Utils.showSnackBar(getActivity(), "Attendance update successfully");
                break;
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        Utils.showSnackBar(getActivity(), message);
    }

    /***
     *
     * @param mAttendance
     * @param attendanceUpdatedStatus
     * adapter itemClick Listener
     */
    @Override
    public void click(Attendance mAttendance, long attendanceUpdatedStatus, int position) {
         if(attendanceUpdatedStatus != ATTENDANCE_TYPE_UPDATE) {
             Optional<AttendanceRequest> item =  multipleAttendance.getAttendances().stream().
                     filter(student -> student.getStudentId() == mAttendance.getStudentId()).findFirst();
             if(item.isPresent()) {
                 item.get().setStatus(attendanceUpdatedStatus);
             }
         } else {
             // setup the alert builder
             if(mAttendance != null && mAttendance.getAttendanceStatus() != 4) {
                 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                 builder.setTitle("Choose an option");// add a radio button list
                 String[] item = {"Absent", "Present", "Leave"};
                 int checkedItem = mAttendance.getAttendanceStatus().intValue();
                 builder.setSingleChoiceItems(item, checkedItem, (dialog, which) -> {
                      selectItem = which;
                     // user checked an item

                 });
                 builder.setPositiveButton(getString(R.string.kokay), (dialog, which) -> {
                     if(selectItem != mAttendance.getAttendanceStatus().intValue()) {
                         mNetworkPresenter.methodToUpdateAttendance(getActivity(),
                                 new UpdateAttendanceRequest(Constant.UNIQUE_SCHOOL_ID, Math.toIntExact(mAttendance.getId()),
                                         selectItem), loadFacultyData().getToken());
                     }
                 });
                 builder.setNegativeButton(getString(R.string.kCancel), null);// create and show the alert dialog
                 AlertDialog dialog = builder.create();
                 dialog.show();
             }
         }
    }

    public interface CallBackListener {
        void onCallBack();
    }
}
