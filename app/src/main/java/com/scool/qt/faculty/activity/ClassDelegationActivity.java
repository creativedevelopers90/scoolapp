package com.scool.qt.faculty.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.FacultyDelegationAdapter;
import com.scool.qt.model.ClassListFrDelegation;
import com.scool.qt.model.DelegationItem;
import com.scool.qt.model.FacultyInfoResponse;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 */
public class ClassDelegationActivity  extends BaseActivity implements NetworkView, FacultyDelegationAdapter.IItemClickListener {
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.applayout)
    AppBarLayout applayout;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.rv_delegation_list)
    RecyclerView rvDelegationList;
    NetworkPresenter presenter;
    FacultyDelegationAdapter facultyDelegationAdapter;
    private LinearLayoutManager mLayoutManager;
    private List<DelegationItem> list = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faculty_delegate_activity);
        ButterKnife.bind(this);
        presenter = new NetworkPresenter(this, new NetworkInteractor());
        rvDelegationList.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        rvDelegationList.setLayoutManager(mLayoutManager);
        facultyDelegationAdapter = new FacultyDelegationAdapter(list, this);
        rvDelegationList.setAdapter(facultyDelegationAdapter);

        tvSchoolname.setText(R.string.kselect_class_to_delegate);
        toolbarTitle.setText(R.string.kFacultyText);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(view -> finish());
    }

    @Override
    protected void onResume() {
        getDelegationList();
        super.onResume();
    }

    private void fetchFacultyInfo(FacultyLoginResponse facultyLoginResponse) {
        presenter.methodToFetchFacultyBasicInfo(this, Constant.UNIQUE_SCHOOL_ID, facultyLoginResponse.getFacultyId(), facultyLoginResponse.getToken());
    }

    private  void getDelegationList() {
            FacultyLoginResponse facultyLoginResponse = loadFacultyData();
            fetchFacultyInfo(facultyLoginResponse);
            presenter.methodToFetchDelegationClassList(ClassDelegationActivity.this,
                    Constant.UNIQUE_SCHOOL_ID, facultyLoginResponse.getFacultyId(), facultyLoginResponse.getToken());
    }

    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", this);
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
        Utils.showSnackBar(this, getString(R.string.check_internet));
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        if (endpointType == EndpointEnums.class_lists_for_Delegation) {
            list.clear();
            ClassListFrDelegation classListFrDelegation = (ClassListFrDelegation) object;
            list.addAll(classListFrDelegation.getClassListsForDelegation());
            facultyDelegationAdapter.updateList(list);
        } else if (endpointType == EndpointEnums.faculty_info) {
            Glide.with(this)
                    .load(Constant.IMAGE_BASE_URL + ((FacultyInfoResponse) object).getPhoto())
                    .thumbnail(0.5f)
                    .into(imgSchool);
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {

    }

    @Override
    public void itemClick(View view, int position, DelegationItem mTimeTablesItem) {
        if(mTimeTablesItem.getDelegated()) {
            Intent i = new Intent(this, DelegationActivity.class);
            i.putExtra("facultyName", mTimeTablesItem.getName());
            i.putExtra("facultyID", String.valueOf(mTimeTablesItem.getDelegatedFacultyId()));
            DelegationItem delegationItem = new DelegationItem();
            delegationItem.setFromdate(mTimeTablesItem.getFromdate());
            delegationItem.setDelegatedFacultyId(mTimeTablesItem.getDelegatedFacultyId());
            delegationItem.setAttdelegate(mTimeTablesItem.getAttdelegate());
            delegationItem.setTodate(mTimeTablesItem.getTodate());
            delegationItem.setName(mTimeTablesItem.getName());
            delegationItem.setId(mTimeTablesItem.getId());
            delegationItem.setDelegated(mTimeTablesItem.getDelegated());
            delegationItem.setMarkdelegate(mTimeTablesItem.getMarkdelegate());
            delegationItem.setHwdelegate(mTimeTablesItem.getHwdelegate());
            delegationItem.setAttdelegate(mTimeTablesItem.getAttdelegate());
            delegationItem.setDiarydelegate(mTimeTablesItem.getDiarydelegate());
            i.putExtra("delegationItem", delegationItem);
            startActivity(i);
        } else {
            Intent i = new Intent(this, FacultyDelegationActivity.class);
            i.putExtra("delegationItem", mTimeTablesItem);
            startActivity(i);
        }

    }
}

