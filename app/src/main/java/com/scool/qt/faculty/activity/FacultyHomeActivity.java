package com.scool.qt.faculty.activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.appbar.AppBarLayout;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.activity.FacultyAllClassTimeTableActivity;
import com.scool.qt.base.activity.HomeActivity;
import com.scool.qt.faculty.fragments.FacultyAttendanceFragment;
import com.scool.qt.faculty.fragments.FacultyBaseFragment;
import com.scool.qt.helper.PrefData;
import com.scool.qt.model.LogoutResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FacultyHomeActivity extends BaseActivity implements NetworkView, FacultyBaseFragment.ClickInterface, FacultyAttendanceFragment.CallBackListener {
    @BindView(R.id.diary_toolbar_title)
    TextView diaryToolbarTitle;
    @BindView(R.id.tv_all)
    TextView tvAll;
    @BindView(R.id.save_attendance_iv)
    ImageView saveAttendanceIv;
    @BindView(R.id.diary_toolbar)
    Toolbar diaryToolbar;
    @BindView(R.id.applayout)
    AppBarLayout applayout;
    @BindView(R.id.onboard_fragment)
    FrameLayout onboardFragment;
    @BindView(R.id.iv_nointernet)
    ImageView ivNointernet;
    @BindView(R.id.rl_nointernet)
    RelativeLayout rlNointernet;
    @BindView(R.id.feed_frame_layout)
    ConstraintLayout feedFrameLayout;
    NetworkPresenter mNetworkPresenter = null;
    int type = -1;
    Resources resources;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        resources = getLanguageType();
        setContentView(R.layout.activity_faculty_home_login);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {
        saveAttendanceIv.setVisibility(View.GONE);
        tvAll.setText(getResources().getString(R.string.kLogout));
        setSupportActionBar(diaryToolbar);
        diaryToolbar.setNavigationOnClickListener(view -> onBackPressed());

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        diaryToolbarTitle.setText(resources.getString(R.string.kFacultyHomeText));
        getSupportFragmentManager().beginTransaction().replace(R.id.onboard_fragment, new FacultyBaseFragment(), getString(R.string.kFacultyHomeText)).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @OnClick(R.id.tv_all)
    void setTvAllClick() {
        if(this.type != 3) {
            logoutAlertDialog(resources.getString(R.string.kLogout), resources.getString(R.string.kLogOutConfirmationText));
        }else {
            if(Utils.isNetworkAvailable(FacultyHomeActivity.this)){
                Intent i = new Intent(this, FacultyAllClassTimeTableActivity.class);
                startActivity(i);
            }else{
                Utils.showSnackBar(this,resources.getString(R.string.check_internet));
            }
        }
    }

    @OnClick(R.id.save_attendance_iv)
    public void onSaveAttendanceClicked() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setTitle(resources.getString(R.string.kTitleTextAttendance));
        builder.setMessage(resources.getString(R.string.save_attendance_alert));
        String positiveText = resources.getString(R.string.kokay);
        builder.setPositiveButton(positiveText,
                (dialog, which) -> {
                    if (getVisibleFragment() instanceof FacultyAttendanceFragment)
                        ((FacultyAttendanceFragment) getVisibleFragment()).onSaveAttendanceClicked();
                });

        String negativeText = resources.getString(R.string.kCancel);
        builder.setNegativeButton(negativeText,
                (dialog, which) -> dialog.dismiss());

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().findFragmentByTag(getString(R.string.kFacultyHomeText)) != null) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(getString(R.string.kTitleTextAttendance));
            if(fragment instanceof FacultyAttendanceFragment){
                diaryToolbarTitle.setText(getString(R.string.kTitleTextAttendance));
                tvAll.setVisibility(View.GONE);
                saveAttendanceIv.setVisibility(View.GONE);
            } else {
                tvAll.setVisibility(View.VISIBLE);
                tvAll.setText(resources.getString(R.string.kLogout));
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                saveAttendanceIv.setVisibility(View.GONE);
                this.type = -1;
                diaryToolbarTitle.setText(resources.getString(R.string.kFacultyHomeText));
            }
        }
        super.onBackPressed();
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        Constant.ACTIVE_STUDENT = null;
        Utils.showSnackBar(this, ((LogoutResponse) object).getMessage());
        Utils.clearPreferences(Constant.IS_LOGGED_IN, this);
        Utils.clearPreferences(Constant.PREF_LOGGED_IN_DATA, FacultyHomeActivity.this);
        new PrefData(FacultyHomeActivity.this).clearLoginPreference();
        Intent intent = new Intent(FacultyHomeActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
    }


    /**
     * Method to fetch the current visible fragment from the fragment stack
     *
     * @return
     */
    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = FacultyHomeActivity.this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }

    @Override
    public void buttonClicked(int type) {
        this.type = type;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        switch (type) {
            case 0:
                diaryToolbarTitle.setText(resources.getString(R.string.kTitleTextAttendance));
                tvAll.setVisibility(View.GONE);
                saveAttendanceIv.setVisibility(View.GONE);
                break;
            case 1:
                diaryToolbarTitle.setText(resources.getString(R.string.kHomeworkText));
                tvAll.setVisibility(View.GONE);
                saveAttendanceIv.setVisibility(View.GONE);
                break;
            case 2:
                diaryToolbarTitle.setText(resources.getString(R.string.kDiaryText));
                tvAll.setVisibility(View.GONE);
                saveAttendanceIv.setVisibility(View.GONE);
                break;
            case 3:
                diaryToolbarTitle.setText(resources.getString(R.string.kTimeTableText));
                tvAll.setVisibility(View.VISIBLE);
                tvAll.setText(resources.getString(R.string.kAllText));
                saveAttendanceIv.setVisibility(View.GONE);
                break;
            case 4:
                diaryToolbarTitle.setText(resources.getString(R.string.kMarksText));
                tvAll.setVisibility(View.GONE);
                saveAttendanceIv.setVisibility(View.GONE);
                break;
            case 5:
                diaryToolbarTitle.setText(resources.getString(R.string.kGalleryText));
                tvAll.setVisibility(View.GONE);
                saveAttendanceIv.setVisibility(View.GONE);
                break;
            case 6:
                diaryToolbarTitle.setText(resources.getString(R.string.kEventsText));
                tvAll.setVisibility(View.GONE);
                saveAttendanceIv.setVisibility(View.GONE);
                break;
            case 7:
                diaryToolbarTitle.setText(resources.getString(R.string.kChangeClass));
                tvAll.setVisibility(View.GONE);
                saveAttendanceIv.setVisibility(View.GONE);
                break;
            case 8:
                diaryToolbarTitle.setText(resources.getString(R.string.kSettingsText));
                tvAll.setVisibility(View.GONE);
                saveAttendanceIv.setVisibility(View.GONE);
                break;
            default:
                tvAll.setVisibility(View.GONE);
                saveAttendanceIv.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onCallBack() {
        saveAttendanceIv.setVisibility(View.VISIBLE);
    }
}
