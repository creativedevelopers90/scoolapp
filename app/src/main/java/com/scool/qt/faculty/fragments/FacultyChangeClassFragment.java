package com.scool.qt.faculty.fragments;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.model.ClassList;
import com.scool.qt.model.FacultyClassResponse;
import com.scool.qt.model.FacultyInfoResponse;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FacultyChangeClassFragment extends Fragment implements NetworkView {
    @BindView(R.id.faculty_name)
    TextView facultyNameTV;
    @BindView(R.id.faculty_image)
    ImageView facultyImageIV;
    @BindView(R.id.class_list_recycler_view)
    RecyclerView classListRV;
    @BindView(R.id.ll_main_content)
    LinearLayout llMainContent;
    @BindView(R.id.rl_nointernet)
    RelativeLayout rlNoInternet;
    @BindView(R.id.iv_nointernet)
    ImageView ivNointernet;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private View root;
    private Unbinder unbinder;
    private NetworkPresenter mNetworkPresenter;
    private LinearLayoutManager mLayoutManager;
    private MyRecyclerAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_faculty_home, container, false);
        unbinder = ButterKnife.bind(this, root);
        classListRV.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        classListRV.setLayoutManager(mLayoutManager);
        fetchFacultyInfo();
        return root;
    }

    /**
     * Method to fetch faculty information
     */
    private void fetchFacultyInfo() {
        progressBar.setVisibility(View.VISIBLE);
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        FacultyLoginResponse facultyLoginResponse = loadFacultyData();
        mNetworkPresenter.methodToFetchFacultyBasicInfo(getActivity(), Constant.UNIQUE_SCHOOL_ID, facultyLoginResponse.getFacultyId(), facultyLoginResponse.getToken());
        mNetworkPresenter.methodToFetchFacultyClassList(getActivity(), Constant.UNIQUE_SCHOOL_ID, facultyLoginResponse.getFacultyId(), facultyLoginResponse.getToken());
    }

    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
//        Utils.showSnackBar(getActivity(),getString(R.string.check_internet));
        rlNoInternet.setVisibility(View.VISIBLE);
        llMainContent.setVisibility(View.GONE);
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        if (endpointType == EndpointEnums.faculty_info) {
            facultyNameTV.setText(((FacultyInfoResponse) object).getName() + " - " + ((FacultyInfoResponse) object).getDepartment());
            Glide.with(getActivity())
                    .load(Constant.IMAGE_BASE_URL + ((FacultyInfoResponse) object).getPhoto())
                    .thumbnail(0.5f)
                    .into(facultyImageIV);
        } else if (endpointType == EndpointEnums.faculty_class) {
            if (getView() == null) return;
            progressBar.setVisibility(View.GONE);
            mAdapter = new MyRecyclerAdapter(((FacultyClassResponse) object).getClassList());
            classListRV.setAdapter(mAdapter);
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        if (getView() == null) return;
        progressBar.setVisibility(View.GONE);
        Utils.showSnackBar(getActivity(), message);
    }

    private class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyViewHolder> {
        private List<ClassList> classList;

        public MyRecyclerAdapter(List<ClassList> classList) {
            this.classList = classList;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.faculty_class_list_item, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            holder.classNameTV.setText(classList.get(position).getName());
            holder.classNameTV.setGravity(getResources().getConfiguration().getLayoutDirection() == View.LAYOUT_DIRECTION_RTL ? Gravity.RIGHT : Gravity.LEFT);
            holder.root.setOnClickListener(v -> {
                holder.activeClassIV.setVisibility(View.VISIBLE);
                holder.inactiveClassIV.setVisibility(View.GONE);
                Constant.ACTIVE_CLASS = classList.get(position);
                MyRecyclerAdapter.this.notifyDataSetChanged();
            });
            if (Constant.ACTIVE_CLASS == null) {
                if (position == 0) {
                    holder.activeClassIV.setVisibility(View.VISIBLE);
                    holder.inactiveClassIV.setVisibility(View.GONE);
                    Constant.ACTIVE_CLASS = classList.get(0);
                }
            } else if (Constant.ACTIVE_CLASS.getName().equals(classList.get(position).getName())) {
                holder.activeClassIV.setVisibility(View.VISIBLE);
                holder.inactiveClassIV.setVisibility(View.GONE);
            } else {
                holder.activeClassIV.setVisibility(View.GONE);
                holder.inactiveClassIV.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public int getItemCount() {
            return classList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView classNameTV;
            public ImageView activeClassIV, inactiveClassIV;
            public View root;

            public MyViewHolder(View itemView) {
                super(itemView);
                root = itemView;
                classNameTV = itemView.findViewById(R.id.faculty_class_name);
                activeClassIV = itemView.findViewById(R.id.faculty_class_active);
                inactiveClassIV = itemView.findViewById(R.id.faculty_class_inactive);
            }
        }
    }
}
