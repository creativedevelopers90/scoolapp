package com.scool.qt.faculty.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.ScoolQtApplication;
import com.scool.qt.adapter.FacultyHomeScreenAdapter;
import com.scool.qt.base.activity.HomeActivity;
import com.scool.qt.base.fragment.BaseFragment;
import com.scool.qt.base.fragment.EventsFragment;
import com.scool.qt.base.fragment.GalleryFragment;
import com.scool.qt.fragments.LoggedInSettingsFragment;
import com.scool.qt.helper.ThemeHelper;
import com.scool.qt.model.FacultyClassResponse;
import com.scool.qt.model.FacultyHomeScreenObj;
import com.scool.qt.model.FacultyInfoResponse;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FacultyBaseFragment extends BaseFragment implements NetworkView, FacultyHomeScreenAdapter.IMessageListener {
    Unbinder unbinder;
    @BindView(R.id.tv_class_id)
    TextView tvClassId;
    @BindView(R.id.top_view)
    View topView;
    @BindView(R.id.feed_listview)
    RecyclerView feedListview;
    @BindView(R.id.view_botton)
    View viewBotton;
    @BindView(R.id.setting_footer)
    AppCompatTextView settingFooter;
    @BindView(R.id.rl_maincontent)
    ConstraintLayout rlMaincontent;
    @BindView(R.id.feed_frame_layout)
    ConstraintLayout feedFrameLayout;
    private View root;
    ArrayList<FacultyHomeScreenObj> mFacultyList;
    private int NUM_OF_COLUMNS = 3;

    FacultyHomeScreenAdapter mFacultyAdapter;
    private NetworkPresenter mNetworkPresenter;
    ClickInterface clickInterface;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        resources = ScoolQtApplication.getInstance().initAppLanguage(getActivity());
        root = inflater.inflate(R.layout.fragment_faculty_first_screen, container, false);
        unbinder = ButterKnife.bind(this, root);
        initViews();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.clickInterface = (ClickInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement HeadlineListener");
        }
    }

    private void initViews() {
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        mFacultyList = new ArrayList<>();
        feedListview.setLayoutManager(new GridLayoutManager(getActivity(), NUM_OF_COLUMNS));
        mFacultyAdapter = new FacultyHomeScreenAdapter(getActivity(), FacultyBaseFragment.this);
        feedListview.setAdapter(mFacultyAdapter);
        if (Constant.ACTIVE_CLASS == null) {
            fetchFacultyInfo();
        } else {
            defaultGridLayoutList("blue");
            Constant.ACTIVE_CLASS.getId().intValue();
            tvClassId.setText("Grade - " + Constant.ACTIVE_CLASS.getName());
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        String themeSelect = ThemeHelper.getPrefCurrentTheme(getActivity());
        if (themeSelect.equalsIgnoreCase("blue")) {
            defaultGridLayoutList("blue");
        } else if(themeSelect.equalsIgnoreCase("red")) {
            defaultGridLayoutList("red");
        }
    }

    private void defaultGridLayoutList(String themeColor) {
        mFacultyList.clear();
        if(themeColor.equalsIgnoreCase("blue")) {
            mFacultyList.add(new FacultyHomeScreenObj(
                    getResources().getString(R.string.kTitleTextAttendance),
                    R.drawable.ic_atten_blue));
            mFacultyList.add(new FacultyHomeScreenObj(resources.getString(R.string.kHomeworkText),
                    R.drawable.ic_blue_home));
            mFacultyList.add(new FacultyHomeScreenObj(resources.getString(R.string.kDiaryText),
                    R.drawable.ic_blue_diary));
            mFacultyList.add(new FacultyHomeScreenObj(resources.getString(R.string.kTimeTableText),
                    R.drawable.ic_blue_tb));
            mFacultyList.add(new FacultyHomeScreenObj(getResources().getString(R.string.kMarksText),
                    R.drawable.ic_blue_report));
            mFacultyList.add(new FacultyHomeScreenObj(getResources().getString(R.string.kGalleryText),
                    R.drawable.ic_blue_gallery));
            mFacultyList.add(new FacultyHomeScreenObj(getResources().getString(R.string.kEventsText),
                    R.drawable.ic_blue_event));
            mFacultyList.add(new FacultyHomeScreenObj(getResources().getString(R.string.kChangeClass),
                    R.drawable.ic_chn_class));
            mFacultyList.add(new FacultyHomeScreenObj(getResources().getString(R.string.kFacultySettingsText),
                    R.drawable.ic_blue_setting));
        } else if(themeColor.equalsIgnoreCase("red")) {
            mFacultyList.add(new FacultyHomeScreenObj(
                    getResources().getString(R.string.kTitleTextAttendance),
                    R.drawable.ic_atten_blue));
            mFacultyList.add(new FacultyHomeScreenObj(getResources().getString(R.string.kHomeworkText),
                    R.drawable.ic_blue_home));
            mFacultyList.add(new FacultyHomeScreenObj(getResources().getString(R.string.kDiaryText),
                    R.drawable.ic_blue_diary));
            mFacultyList.add(new FacultyHomeScreenObj(getResources().getString(R.string.kTimeTableText),
                    R.drawable.ic_blue_tb));
            mFacultyList.add(new FacultyHomeScreenObj(getResources().getString(R.string.kMarksText),
                    R.drawable.ic_blue_report));
            mFacultyList.add(new FacultyHomeScreenObj(getResources().getString(R.string.kGalleryText),
                    R.drawable.ic_blue_gallery));
            mFacultyList.add(new FacultyHomeScreenObj(getResources().getString(R.string.kEventsText),
                    R.drawable.ic_blue_event));
            mFacultyList.add(new FacultyHomeScreenObj(getResources().getString(R.string.kChangeClass),
                    R.drawable.ic_chn_class));
            mFacultyList.add(new FacultyHomeScreenObj(getResources().getString(R.string.kFacultySettingsText),
                    R.drawable.ic_red_setting));
        }
        mFacultyAdapter.addItems(mFacultyList);

    }


    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
        // rlNointernet.setVisibility(View.VISIBLE);
        //rlMaincontent.setVisibility(View.GONE);
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        if (endpointType == EndpointEnums.faculty_class) {
            defaultGridLayoutList("blue");
            tvClassId.setText("Grade - " + ((FacultyClassResponse) object).getClassList().get(0).getName());
            Constant.ACTIVE_CLASS = ((FacultyClassResponse) object).getClassList().get(0);
        }
        if (endpointType == EndpointEnums.faculty_info) {
            Constant.facultyData = (FacultyInfoResponse) object;
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        if(message.equalsIgnoreCase("Internal Server Error")){
        }else {
            Utils.clearPreferences(Constant.PREF_LOGGED_IN_DATA, getActivity());
            prefData.clearLoginPreference();
            Intent intent = new Intent(getActivity(), HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            Utils.showSnackBar(getActivity(), message);
        }

    }

    /**
     * Method to fetch faculty information
     */
    private void fetchFacultyInfo() {
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        FacultyLoginResponse facultyLoginResponse = loadFacultyData();
        mNetworkPresenter.methodToFetchFacultyClassList(getActivity(), Constant.UNIQUE_SCHOOL_ID,
                facultyLoginResponse.getFacultyId(), facultyLoginResponse.getToken());
        mNetworkPresenter.methodToFetchFacultyBasicInfo(getActivity(), Constant.UNIQUE_SCHOOL_ID, facultyLoginResponse.getFacultyId(), facultyLoginResponse.getToken());
    }

    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }

    @Override
    public void itemClick(int position, String data) {
        switch (position) {
            case 0:
                if (Constant.ACTIVE_CLASS != null) {
                    clickInterface.buttonClicked(0);
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.onboard_fragment,
                            new FacultyAttendanceHomeFragment())
                            .addToBackStack(getString(R.string.kTitleTextAttendance))
                            .commit();
                } else {
                    Utils.showSnackBar(getActivity(), "Please select a class to proceed.");
                }
                break;
            case 1:
                if (Constant.ACTIVE_CLASS != null) {
                    clickInterface.buttonClicked(1);
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.onboard_fragment,
                            new FacultyHomeworkFragment())
                            .addToBackStack(getString(R.string.kHomeworkText))
                            .commit();
                } else {
                    Utils.showSnackBar(getActivity(), "Please select a class to proceed.");
                }
                break;

            case 2:
                if (Constant.ACTIVE_CLASS != null) {
                    clickInterface.buttonClicked(2);
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.onboard_fragment,
                            new FacultyDiaryFragment())
                            .addToBackStack(getString(R.string.kDiaryText))
                            .commit();
                } else {
                    Utils.showSnackBar(getActivity(), "Please select a class to proceed.");
                }
                break;

            case 3:
                if (Constant.ACTIVE_CLASS != null) {
                    //  saveAttendanceIv.setVisibility(View.VISIBLE);
                    //   diaryToolbarTitle.setText(getString(R.string.kTimeTableText));
                    clickInterface.buttonClicked(3);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.onboard_fragment,
                                    new FacultyTimeTableFragment(), getString(R.string.kTimeTableText))
                            .addToBackStack(getString(R.string.kTimeTableText))
                            .commit();
                } else {
                    //fragmentClass = FacultyChangeClassFragment.class;
                    //  diaryToolbarTitle.setText(getString(R.string.kTimeTableText));
                    Utils.showSnackBar(getActivity(), "Please select a class to proceed.");
                }
                break;

            case 4:
                if (Constant.ACTIVE_CLASS != null) {
                    //   saveAttendanceIv.setVisibility(View.VISIBLE);
                    //   diaryToolbarTitle.setText(getString(R.string.kReportCardText));
                    clickInterface.buttonClicked(4);
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.onboard_fragment,
                            new FacultyReportCardFragment())
                            .addToBackStack(getString(R.string.kMarksText))
                            .commit();
                } else {
                    //fragmentClass = FacultyChangeClassFragment.class;
                    //  diaryToolbarTitle.setText(getString(R.string.kReportCardText));
                    Utils.showSnackBar(getActivity(), "Please select a class to proceed.");
                }
                break;
            case 5:
                if (Constant.ACTIVE_CLASS != null) {
                    //  saveAttendanceIv.setVisibility(View.VISIBLE);
                    //  diaryToolbarTitle.setText(getString(R.string.kGalleryText));
                    clickInterface.buttonClicked(5);
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.onboard_fragment,
                            new GalleryFragment())
                            .addToBackStack(getString(R.string.kGalleryText))
                            .commit();
                } else {
                    //fragmentClass = FacultyChangeClassFragment.class;
                    //  diaryToolbarTitle.setText(getString(R.string.kGalleryText));
                    Utils.showSnackBar(getActivity(), "Please select a class to proceed.");
                }
                break;
            case 6:
                if (Constant.ACTIVE_CLASS != null) {
                    //   saveAttendanceIv.setVisibility(View.VISIBLE);
                    //   diaryToolbarTitle.setText(getString(R.string.kEventsText));
                    clickInterface.buttonClicked(6);
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.onboard_fragment,
                            new EventsFragment())
                            .addToBackStack(getString(R.string.kEventsText))
                            .commit();
                } else {
                    //fragmentClass = FacultyChangeClassFragment.class;
                    //   diaryToolbarTitle.setText(getString(R.string.kEventsText));
                    Utils.showSnackBar(getActivity(), "Please select a class to proceed.");
                }
                break;

            case 7:
                if (Constant.ACTIVE_CLASS != null) {
                    //  saveAttendanceIv.setVisibility(View.VISIBLE);
                    //   diaryToolbarTitle.setText(getString(R.string.kChangeClass));
                    clickInterface.buttonClicked(7);
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.onboard_fragment,
                            new FacultyChangeClassFragment())
                            .addToBackStack(getString(R.string.kChangeClass))
                            .commit();
                } else {
                    //fragmentClass = FacultyChangeClassFragment.class;
                    //   diaryToolbarTitle.setText(getString(R.string.kChangeClass));
                    Utils.showSnackBar(getActivity(), "Please select a class to proceed.");
                }
                break;

            case 8:
                if (Constant.ACTIVE_CLASS != null) {
                    //   saveAttendanceIv.setVisibility(View.VISIBLE);
                    //   diaryToolbarTitle.setText(getString(R.string.kSettingsText));
                    clickInterface.buttonClicked(8);
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.onboard_fragment,
                            new LoggedInSettingsFragment())
                            .addToBackStack(getString(R.string.kSettingsText))
                            .commit();
                } else {
                    //fragmentClass = FacultyChangeClassFragment.class;
                    //   diaryToolbarTitle.setText(getString(R.string.kSettingsText));
                    Utils.showSnackBar(getActivity(), "Please select a class to proceed.");
                }
                break;
        }

    }

    public interface ClickInterface {
        public void buttonClicked(int type);
    }
}
