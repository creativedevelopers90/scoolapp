package com.scool.qt.faculty.activity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.model.DelegationItem;
import com.scool.qt.model.FacultyInfoResponse;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.request.AddDelegationRequest;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;
import com.scool.qt.widgets.ThemeButton;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DelegationActivity extends BaseActivity implements NetworkView {
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.applayout)
    AppBarLayout applayout;
    @BindView(R.id.tv_attendance)
    AppCompatTextView tvAttendance;
    @BindView(R.id.switch_attendance)
    SwitchCompat switchAttendance;
    @BindView(R.id.tv_diary)
    AppCompatTextView tvDiary;
    @BindView(R.id.switch_diary)
    SwitchCompat switchDiary;
    @BindView(R.id.tv_homework)
    AppCompatTextView tvHomework;
    @BindView(R.id.switch_homework)
    SwitchCompat switchHomework;
    @BindView(R.id.tv_marks)
    AppCompatTextView tvMarks;
    @BindView(R.id.switch_marks)
    SwitchCompat switchMarks;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.btn_submit)
    ThemeButton btnSubmit;
    @BindView(R.id.tv_class)
    AppCompatTextView tvClass;
    @BindView(R.id.tv_class_name)
    AppCompatTextView tvClassName;
    @BindView(R.id.cns_classname)
    ConstraintLayout cnsClassname;
    @BindView(R.id.tv_fromdate)
    AppCompatTextView tvFromdate;
    @BindView(R.id.from_dt)
    AppCompatTextView fromDt;
    @BindView(R.id.cns_fromdate)
    ConstraintLayout cnsFromdate;
    @BindView(R.id.tv_todate)
    AppCompatTextView tvTodate;
    @BindView(R.id.to_dt)
    AppCompatTextView toDt;
    @BindView(R.id.cns_todate)
    ConstraintLayout cnsTodate;
    private int mYear, mMonth, mDay;
    DelegationItem item;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delegation);
        ButterKnife.bind(this);
        tvSchoolname.setText("Delegate to - " + getIntent().getStringExtra("facultyName"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        btnSubmit.setBackground(getDrawable(buttonThemeId));
        toolbar.setNavigationOnClickListener(view -> finish());
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        fromDt.setText(mDay + "/" + (mMonth+1) + "/" + mYear);
        toDt.setText(mDay + "/" + (mMonth+1) + "/" + mYear);
        if(getIntent() != null) {
            item =  getIntent().getParcelableExtra("delegationItem");
           tvClassName.setText(item.getName());
            if(item.getDelegated()) {
                fromDt.setText(item.getFromdate());
                toDt.setText(item.getTodate());
            }
            if(!item.getMarkdelegate().equalsIgnoreCase("") && !item.getMarkdelegate().equalsIgnoreCase("0")) {
                switchMarks.setChecked(Boolean.parseBoolean(item.getMarkdelegate()));
            }

            if(!item.getHwdelegate().equalsIgnoreCase("") && !item.getHwdelegate().equalsIgnoreCase("0")) {
                switchHomework.setChecked(Boolean.parseBoolean(item.getHwdelegate()));
            }

            if(!item.getDiarydelegate().equalsIgnoreCase("") && !item.getDiarydelegate().equalsIgnoreCase("0")) {
                switchDiary.setChecked(Boolean.parseBoolean(item.getDiarydelegate()));
            }

            if(!item.getAttdelegate().equalsIgnoreCase("") && !item.getAttdelegate().equalsIgnoreCase("0")) {
                switchAttendance.setChecked(Boolean.parseBoolean(item.getAttdelegate()));
            }
        }
        fetchFacultyInfo(loadFacultyData());
    }

    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", this);
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }

    private void fetchFacultyInfo(FacultyLoginResponse facultyLoginResponse) {
        mNetworkPresenter.methodToFetchFacultyBasicInfo(this, Constant.UNIQUE_SCHOOL_ID, facultyLoginResponse.getFacultyId(), facultyLoginResponse.getToken());
    }

    @OnClick(R.id.btn_submit)
    public void setBtnSubmit() {
        if(!item.getDelegated()) {
            mNetworkPresenter.methodToAddDelegation(this, new AddDelegationRequest(Constant.UNIQUE_SCHOOL_ID, loadFacultyData().getFacultyId(),
                    item.getId().toString(), getIntent().getStringExtra("facultyID"),
                    String.valueOf(switchAttendance.isChecked()), String.valueOf(switchMarks.isChecked()), String.valueOf(switchDiary.isChecked()),
                    String.valueOf(switchHomework.isChecked()), fromDt.getText().toString(), toDt.getText().toString()), loadFacultyData().getToken());
        } else {
            mNetworkPresenter.methodToUpdateDelegationRequest(this, new AddDelegationRequest(Constant.UNIQUE_SCHOOL_ID, loadFacultyData().getFacultyId(),
                    item.getId().toString(), getIntent().getStringExtra("facultyID"),
                    String.valueOf(switchAttendance.isChecked()), String.valueOf(switchMarks.isChecked()), String.valueOf(switchDiary.isChecked()),
                    String.valueOf(switchHomework.isChecked()), fromDt.getText().toString(), toDt.getText().toString(), String.valueOf(item.getId())), loadFacultyData().getToken());
        }

    }

    @OnClick(R.id.from_dt)
    public void fromDt() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> fromDt.setText(dayOfMonth + "/" + (monthOfYear+1) + "/" + year), mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    @OnClick(R.id.to_dt)
    public void toDt() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> toDt.setText(dayOfMonth + "/" + (monthOfYear+1) + "/" + year), mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        if(endpointType.equals(EndpointEnums.add_delegation) || endpointType.equals(EndpointEnums.update_delegation)) {
            finish();
        } else if (endpointType == EndpointEnums.faculty_info) {
            Glide.with(this)
                    .load(Constant.IMAGE_BASE_URL + ((FacultyInfoResponse) object).getPhoto())
                    .thumbnail(0.5f)
                    .into(imgSchool);
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        if(endpointType.equals(EndpointEnums.add_delegation)) {

        }
    }
}
