package com.scool.qt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.model.parent.TimeTable;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParentTimeTableRecyclerAdapter extends RecyclerView.Adapter<ParentTimeTableRecyclerAdapter.MyViewHolder> {
    private List<TimeTable> mTimeTableList;
    private Context mContext;

    public ParentTimeTableRecyclerAdapter(Context mContext) {
        this.mTimeTableList = new ArrayList<>();
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder
    onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_parent_time_table_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TimeTable mSchedule = mTimeTableList.get(position);
        holder.bindItems(mSchedule);
    }

    @Override
    public int getItemCount() {
        return mTimeTableList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_subject_name)
        TextView subjectName;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.item_time)
        TextView timeRange;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindItems(TimeTable mSchedule) {
            subjectName.setText(mSchedule.getSubjectName());
            timeRange.setText(mSchedule.getTime());
        }
    }

    public void addItemInList(List<TimeTable> mTimeTableList) {
        this.mTimeTableList = mTimeTableList;
        notifyDataSetChanged();
    }

    public void clearList() {
        mTimeTableList.clear();
        notifyDataSetChanged();
    }
}
