package com.scool.qt.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.activity.FacultyMarksListReportActivity;
import com.scool.qt.activity.ParentHomeActivity;
import com.scool.qt.activity.ParentReportCardStatusActivity;
import com.scool.qt.model.Exam;

import java.util.ArrayList;
import java.util.List;

public class ReportCardListAdapter extends RecyclerView.Adapter<ReportCardListAdapter.MyExamListViewHolder> {
    private final Context mContext;
    List<Exam> examList;

    public ReportCardListAdapter(Context mContext) {
        this.examList = new ArrayList<>();
        this.mContext = mContext;
    }
    @NonNull
    @Override
    public MyExamListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyExamListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.exam_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyExamListViewHolder holder, int position) {
        holder.examNameTV.setText(examList.get(position).getSubjectName() != null ? examList.get(position).getName() + "-" + examList.get(position).getSubjectName(): examList.get(position).getName());
        holder.rootItem.setOnClickListener(v -> {
            if (mContext instanceof ParentHomeActivity) {
                Intent intent = new Intent(mContext, ParentReportCardStatusActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("selected_exam", examList.get(position));
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            } else {
                Intent intent = new Intent(mContext, FacultyMarksListReportActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("selected_exam", examList.get(position));
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return examList.size();
    }

    class MyExamListViewHolder extends RecyclerView.ViewHolder {

        View rootItem;
        TextView examNameTV;

        public MyExamListViewHolder(View itemView) {
            super(itemView);
            rootItem = itemView.findViewById(R.id.exam_list_root);
            examNameTV = itemView.findViewById(R.id.exam_name);
        }
    }

    public void addItemsInList(List<Exam> examList) {
        this.examList = examList;
        notifyDataSetChanged();
    }
}
