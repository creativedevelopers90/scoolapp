package com.scool.qt.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.helper.LocaleHelper;
import com.scool.qt.model.TimeTableObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParentAttendanceListRecyclerAdapter extends RecyclerView.Adapter<ParentAttendanceListRecyclerAdapter.ParentAttendanceListRecyclerViewHolder> {

    public List<TimeTableObject> attendanceList;
    private final Context mContext;
    private Resources resources;

    public ParentAttendanceListRecyclerAdapter(Context mContext) {
        this.mContext = mContext;
        this.attendanceList = new ArrayList<>();
        resources = getLanguageType();
    }

    @NonNull
    @Override
    public ParentAttendanceListRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ParentAttendanceListRecyclerViewHolder(LayoutInflater.from(mContext).inflate(R.layout.layout_parent_time_table_row, parent, false));
    }

    public void addItemInList(List<TimeTableObject> attendanceList) {
        this.attendanceList = attendanceList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull ParentAttendanceListRecyclerViewHolder holder, int position) {
        TimeTableObject mSchedule = attendanceList.get(position);
        holder.bindItems(mSchedule);

    }

    @Override
    public int getItemCount() {
        return attendanceList.size();
    }

    class ParentAttendanceListRecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_subject_name)
        TextView subjectName;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.item_time)
        TextView timeRange;
        @BindView(R.id.ivstudent_attendance)
        ImageView ivstudent_attendance;
        @BindView(R.id.attendance_leave_layout)
        RelativeLayout attendance_leave_layout;
        @BindView(R.id.tv_student_type)
        TextView tv_student_type;

        public ParentAttendanceListRecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            attendance_leave_layout.setVisibility(View.VISIBLE);
        }

        public void bindItems(TimeTableObject mSchedule) {
            subjectName.setText(mSchedule.getTime());
            timeRange.setText(mSchedule.getSubjectName());
            switch (mSchedule.getStatus()){
                case 0:
                    tv_student_type.setText("A");
                    ivstudent_attendance.setImageDrawable(mContext.getDrawable(R.drawable.attendance_red_circle));
                    break;
                case 1:
                    tv_student_type.setText("P");
                    ivstudent_attendance.setImageDrawable(mContext.getDrawable(R.drawable.attendance_green_circle));
                    break;
                case 2:
                    tv_student_type.setText("L");
                    ivstudent_attendance.setImageDrawable(mContext.getDrawable(R.drawable.attendance_yellow_circle));
                    break;
                case 3:
                    tv_student_type.setText("");
                    ivstudent_attendance.setImageDrawable(mContext.getDrawable(R.drawable.attendance_grey_circle));
                    break;
            }
        }
    }

    public Resources getLanguageType() {
        Context context = LocaleHelper.setLocale(mContext, LocaleHelper.getLanguage(mContext));
        return context.getResources();
    }
}
