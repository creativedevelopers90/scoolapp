package com.scool.qt.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.helper.ThemeHelper;
import com.scool.qt.model.FacultyHomeScreenObj;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FacultyHomeScreenAdapter  extends RecyclerView.Adapter<FacultyHomeScreenAdapter.MyViewHolder> {
    private Context mContext;
    ArrayList<FacultyHomeScreenObj> mFacultyList;
    IMessageListener messageListener;
    private String themeSelect;


    @NonNull
    @Override
    public FacultyHomeScreenAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.faculty_home_row_item, parent, false);
        return new FacultyHomeScreenAdapter.MyViewHolder(itemView);
    }

    public FacultyHomeScreenAdapter(Context mContext, IMessageListener messageListener) {
        this.mContext = mContext;
        this.messageListener = messageListener;
        themeSelect = ThemeHelper.getPrefCurrentTheme(mContext);
    }

    @Override
    public void onBindViewHolder(@NonNull FacultyHomeScreenAdapter.MyViewHolder holder, int position) {
        FacultyHomeScreenObj mFacultiesItem = mFacultyList.get(position);
        holder.bindItems(mFacultiesItem);
    }

    public void addItems(ArrayList<FacultyHomeScreenObj> mFacultyList) {
        this.mFacultyList = mFacultyList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mFacultyList != null ? mFacultyList.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_header)
        ImageView img_header;

        @BindView(R.id.tv_heading)
        TextView tv_heading;

        @BindView(R.id.layout_grid_item_click)
        CoordinatorLayout layout_grid_item_click;

        @BindView(R.id.img_layout)
        RelativeLayout img_layout;

        @BindView(R.id.tv_badge)
        TextView tv_badge;



        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindItems(FacultyHomeScreenObj mFacultiesItem) {
            if(themeSelect.equalsIgnoreCase("blue")) {
                  img_header.setColorFilter(ContextCompat.getColor(mContext, R.color.color_blue));
                img_layout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_screen_item_bg_blue));
            } else if(themeSelect.equalsIgnoreCase("red")) {
                img_layout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_screen_item_bg_red));
                img_header.setColorFilter(ContextCompat.getColor(mContext, R.color.color_red_accent));
            } else if(themeSelect.equalsIgnoreCase("purple")) {
                img_layout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_screen_item_bg_purple));
                img_header.setColorFilter(ContextCompat.getColor(mContext, R.color.color_purple_accent));
            } else if(themeSelect.equalsIgnoreCase("black")) {
                img_layout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_screen_item_bg_black));
                img_header.setColorFilter(ContextCompat.getColor(mContext, R.color.color_black_accent));
            } else if(themeSelect.equalsIgnoreCase("brown")) {
                img_layout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_screen_item_bg_brown));
                img_header.setColorFilter(ContextCompat.getColor(mContext, R.color.color_brown_primary_accent));
            } else if(themeSelect.equalsIgnoreCase("green")) {
                img_layout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_screen_item_bg_green));
                img_header.setColorFilter(ContextCompat.getColor(mContext, R.color.color_green));
            }
            img_header.setImageResource(mFacultiesItem.getId());
            tv_badge.setVisibility(View.GONE);
            tv_heading.setText(mFacultiesItem.getName());
            layout_grid_item_click.setOnClickListener(v -> {
                Log.d("FACULTY",String.valueOf(getItemId()));
                if(mFacultiesItem.getName().equalsIgnoreCase(mContext.getString(R.string.kTitleTextAttendance))){
                   messageListener.itemClick(0, mContext.getString(R.string.kTitleTextAttendance));
                }else if(mFacultiesItem.getName().equalsIgnoreCase(mContext.getString(R.string.kHomeworkText))) {
                    messageListener.itemClick(1, mContext.getString(R.string.kHomeworkText));
                }else if(mFacultiesItem.getName().equalsIgnoreCase(mContext.getString(R.string.kDiaryText))) {
                    messageListener.itemClick(2, mContext.getString(R.string.kDiaryText));
                }else if(mFacultiesItem.getName().equalsIgnoreCase(mContext.getString(R.string.kTimeTableText))) {
                    messageListener.itemClick(3, mContext.getString(R.string.kTimeTableText));
                }else if(mFacultiesItem.getName().equalsIgnoreCase(mContext.getString(R.string.kMarksText))) {
                    messageListener.itemClick(4, mContext.getString(R.string.kMarksText));
                }else if(mFacultiesItem.getName().equalsIgnoreCase(mContext.getString(R.string.kGalleryText))) {
                    messageListener.itemClick(5, mContext.getString(R.string.kGalleryText));
                }else if(mFacultiesItem.getName().equalsIgnoreCase(mContext.getString(R.string.kEventsText))) {
                    messageListener.itemClick(6, mContext.getString(R.string.kEventsText));
                }else if(mFacultiesItem.getName().equalsIgnoreCase(mContext.getString(R.string.kChangeClass))) {
                    messageListener.itemClick(7, mContext.getString(R.string.kChangeClass));
                }else {
                        messageListener.itemClick(8, mContext.getString(R.string.kSettingsText));
                    }
            });
        }
    }
    public interface IMessageListener {
        void itemClick(int position, String data);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
}
