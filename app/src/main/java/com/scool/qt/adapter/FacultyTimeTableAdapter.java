package com.scool.qt.adapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.model.TimeTablesByClassId;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FacultyTimeTableAdapter extends RecyclerView.Adapter<FacultyTimeTableAdapter.MyViewHolder> {
    private List<TimeTablesByClassId> mTimeTableItem;
    private Context mContext;

    public FacultyTimeTableAdapter(List<TimeTablesByClassId> mTimeTableItem, FragmentActivity activity) {
        this.mTimeTableItem = mTimeTableItem;
        this.mContext = activity;
    }

    @NonNull
    @Override
    public MyViewHolder
    onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_faculty_time_table_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TimeTablesByClassId mFacultiesItem = mTimeTableItem.get(position);
        holder.bindItems(mFacultiesItem);
    }

    @Override
    public int getItemCount() {
        return mTimeTableItem.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_language)
        TextView tvLanguage;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.constraintLayout2)
        RelativeLayout constraintLayout2;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindItems(TimeTablesByClassId mTimeTablesItem) {
            tvTime.setText(mTimeTablesItem.getTime());
            tvLanguage.setText(mTimeTablesItem.getDay());
        }
    }
}
