package com.scool.qt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.model.TimeTablesByClassId;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FacultyAttendanceHomeAdapter extends RecyclerView.Adapter<FacultyAttendanceHomeAdapter.MyViewHolder> {
    private List<TimeTablesByClassId> mTimeTableItem;
    private Context mContext;
    private IItemClickListener itemClickListener;

    public FacultyAttendanceHomeAdapter(FragmentActivity activity, IItemClickListener itemClickListener) {
        this.mTimeTableItem = new ArrayList<>();
        this.mContext = activity;
        this.itemClickListener = itemClickListener;
    }



    @NonNull
    @Override
    public FacultyAttendanceHomeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_faculty_attendance_home_row, parent, false);
        return new FacultyAttendanceHomeAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FacultyAttendanceHomeAdapter.MyViewHolder holder, int position) {
        TimeTablesByClassId mFacultiesItem = mTimeTableItem.get(position);
        holder.bindItems(mFacultiesItem, position);
    }

    @Override
    public int getItemCount() {
        return mTimeTableItem.size();
    }

    public void setList(List<TimeTablesByClassId> mTimeTableItem) {
        this.mTimeTableItem = mTimeTableItem;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_day)
        AppCompatTextView tv_day;

        @BindView(R.id.tv_subject)
        AppCompatTextView tv_subject;
        @BindView(R.id.tv_time)
        AppCompatTextView tv_time;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindItems(TimeTablesByClassId mTimeTablesItem, int position) {
            tv_time.setText(mTimeTablesItem.getTime());
            tv_day.setText(mTimeTablesItem.getDay());
            tv_subject.setText(mTimeTablesItem.getSubject_name());
            itemView.setOnClickListener(v -> itemClickListener.itemClick(itemView, position, mTimeTablesItem));
        }
    }

    public interface IItemClickListener {
        void itemClick(View view, int position, TimeTablesByClassId mTimeTablesItem);
    }

}
