package com.scool.qt.adapter;
import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scool.qt.R;
import com.scool.qt.activity.ParentReplyDiaryActivity;
import com.scool.qt.model.parent.Diary;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ParentDiaryListAdapter extends RecyclerView.Adapter<ParentDiaryListAdapter.ParentDiaryListAdapterViewHolder> {
    private final Activity mContext;
    private final List<Diary> mDiaryList;

    /**
     *
     * @param mContext
     * @param mDiaryList
     */
    public ParentDiaryListAdapter(Activity mContext, List<Diary> mDiaryList) {
        this.mContext = mContext;
        this.mDiaryList = mDiaryList;
    }

    @NonNull
    @Override
    public ParentDiaryListAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ParentDiaryListAdapterViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_parent_diary_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ParentDiaryListAdapterViewHolder holder, int position) {
        Diary diary = mDiaryList.get(position);
        holder.setDiaryData(diary);

    }

    @Override
    public int getItemCount() {
        return mDiaryList.size();
    }

    class ParentDiaryListAdapterViewHolder extends RecyclerView.ViewHolder {
        View root;
        @BindView(R.id.diary_faculty_tv)
        TextView diaryFacultyTv;
        @BindView(R.id.diary_description_tv)
        TextView diaryDescriptionTv;
        @BindView(R.id.tv_reply)
        TextView tvReply;
        @BindView(R.id.diary_reply_tv)
        TextView diaryReplyTv;

        @OnClick(R.id.diary_reply_tv)
        void setDiaryReplyTv() {
            Intent intent = new Intent(mContext, ParentReplyDiaryActivity.class);
            intent.putExtra("diary", mDiaryList.get(getAdapterPosition()));
            mContext.startActivityForResult(intent, 2000);
        }

        public void setDiaryData(Diary diary) {
            diaryFacultyTv.setText(diary.getFacultyName());
            diaryDescriptionTv.setText(diary.getDiary());
            tvReply.setText(diary.getParentReply());
            if (diary.getStudentId() == 0) {
                    diaryReplyTv.setVisibility(View.GONE);
            }else {
                if(diary.getParentReply() == null)
                diaryReplyTv.setVisibility(View.VISIBLE);
            }
        }


        public ParentDiaryListAdapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            root = itemView;
        }
    }
}
