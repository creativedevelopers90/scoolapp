package com.scool.qt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.model.CircularsItem;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventsFragmentAdapter extends RecyclerView.Adapter<EventsFragmentAdapter.MyViewHolder> {
    private EventFragmentAdapterListener mListener;
    private List<CircularsItem> mEventList;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.faculty_image)
        ImageView faculty_image;
        @BindView(R.id.tv_date)
        TextView tv_date;
        @BindView(R.id.tv_targetdt)
        TextView tv_targetdt;
        @BindView(R.id.tv_circularinfo)
        TextView tv_circularinfo;
        @BindView(R.id.cns_layout)
        ConstraintLayout cns_layout;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindItems(CircularsItem mFacultiesItem) {
            tv_date.setText(Utils.convertDtFormat(mFacultiesItem.getDate().trim()));
            tv_targetdt.setText(Utils.convertDtFormat(mFacultiesItem.getTargetDate().trim()));
            tv_circularinfo.setText(mFacultiesItem.getCircular().trim());
            Picasso.get()
                    .load(Constant.IMAGE_BASE_URL+ mFacultiesItem.getPhoto())
                    .placeholder(R.mipmap.app_icon)
                    .error(R.mipmap.app_icon)
                    .into(faculty_image);
            cns_layout.setOnClickListener(v -> mListener.onItemSelected(mFacultiesItem));
        }
    }


    public EventsFragmentAdapter(List<CircularsItem> mEventList, Context mContext, EventFragmentAdapterListener mListener) {
        this.mEventList = mEventList;
        this.mContext = mContext;
        this.mListener = mListener;
    }

    @Override
    public EventsFragmentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.events_frag_item_row, parent, false);
        return new EventsFragmentAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EventsFragmentAdapter.MyViewHolder holder, int position) {
        CircularsItem mFacultiesItem = mEventList.get(position);
        holder.bindItems(mFacultiesItem);
    }

    @Override
    public int getItemCount() {
        return mEventList.size();
    }

    public interface EventFragmentAdapterListener {
        void onItemSelected(CircularsItem mItem);
    }

    public   void updateList(List<CircularsItem> mFacultiesList) {
        this.mEventList = mFacultiesList;
        notifyDataSetChanged();
    }

    void notifySingleItem(int pos,  CircularsItem mItem) {
        this.mEventList.set(pos, mItem);
        notifyItemChanged(pos);
    }
}

