package com.scool.qt.adapter;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.scool.qt.R;
import com.scool.qt.model.parent.Student;
import com.scool.qt.utils.Constant;

import java.util.List;

public class ParentStudentListAdapter extends RecyclerView.Adapter<ParentStudentListAdapter.ParentStudentListHolder> {

    private final List<Student> studentList;
    private final Context mContext;

    public ParentStudentListAdapter(Context mContext, List<Student> studentList) {
        this.mContext = mContext;
        this.studentList = studentList;
    }

    @NonNull
    @Override
    public ParentStudentListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ParentStudentListHolder(LayoutInflater.from(mContext).inflate(R.layout.item_parent_student_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ParentStudentListHolder holder, int position) {
        Student student = studentList.get(position);
        holder.studentDetailsLayout.setGravity(mContext.getResources().getConfiguration().getLayoutDirection() == View.LAYOUT_DIRECTION_RTL ? Gravity.RIGHT : Gravity.LEFT);
        Glide.with(mContext)
                .load(Constant.IMAGE_BASE_URL + student.getImage())
                .thumbnail(0.5f)
                .into(holder.studentIV);
        holder.studentDetailsTV.setText(student.getName() + ", " + student.getClassName());
        holder.root.setOnClickListener(v -> {
            holder.activeStudentIV.setVisibility(View.VISIBLE);
            holder.inactiveStudentIV.setVisibility(View.GONE);
            Constant.ACTIVE_STUDENT = student;
            notifyDataSetChanged();
        });
        if (Constant.ACTIVE_STUDENT == null) {
            if (position == 0) {
                holder.activeStudentIV.setVisibility(View.VISIBLE);
                holder.inactiveStudentIV.setVisibility(View.GONE);
                Constant.ACTIVE_STUDENT = student;
            }
        } else if (Constant.ACTIVE_STUDENT.getStudentId().equals(student.getStudentId())) {
            holder.activeStudentIV.setVisibility(View.VISIBLE);
            holder.inactiveStudentIV.setVisibility(View.GONE);
        } else {
            holder.activeStudentIV.setVisibility(View.GONE);
            holder.inactiveStudentIV.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    class ParentStudentListHolder extends RecyclerView.ViewHolder {

        View root;
        LinearLayout studentDetailsLayout;
        ImageView studentIV, activeStudentIV, inactiveStudentIV;
        TextView studentDetailsTV;

        public ParentStudentListHolder(View itemView) {
            super(itemView);

            root = itemView.findViewById(R.id.parent_student_list_root);
            studentDetailsLayout = itemView.findViewById(R.id.student_detail_layout);
            studentDetailsTV = itemView.findViewById(R.id.student_detail_tv);
            studentIV = itemView.findViewById(R.id.student_image);
            activeStudentIV = itemView.findViewById(R.id.parent_student_active);
            inactiveStudentIV = itemView.findViewById(R.id.parent_student_inactive);
        }
    }
}
