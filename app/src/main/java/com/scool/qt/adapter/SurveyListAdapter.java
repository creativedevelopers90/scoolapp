package com.scool.qt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.model.SurveyObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SurveyListAdapter extends RecyclerView.Adapter<SurveyListAdapter.MyMarkListViewHolder> {

    private List<SurveyObject> mSurveyList;
    private Context mContext;
    public  ItemClick itemClick;

    public SurveyListAdapter(FragmentActivity activity, ItemClick itemClick) {
        this.mSurveyList = new ArrayList<>();
        this.mContext = activity;
        this.itemClick = itemClick;
    }

    public void addItemsInList(List<SurveyObject> mSurveyList) {
        this.mSurveyList = mSurveyList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyMarkListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyMarkListViewHolder(LayoutInflater.from(mContext).inflate(R.layout.survey_list_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyMarkListViewHolder holder, int position) {
        if(holder instanceof MyMarkListViewHolder) {
            holder.bindItems(mSurveyList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mSurveyList.size();
    }

    class MyMarkListViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title)
        AppCompatTextView tvTitle;

        @BindView(R.id.tv_sub_title)
        AppCompatTextView tv_sub_title;

        public MyMarkListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindItems(SurveyObject surveyObject) {
            tvTitle.setText(surveyObject.getName());
            tv_sub_title.setText(surveyObject.getDescription());
            itemView.setOnClickListener(v -> itemClick.onItemSelected(surveyObject));
        }
    }

    public interface ItemClick {
        void onItemSelected(SurveyObject mItem);
    }
}


