package com.scool.qt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.helper.ThemeHelper;
import com.scool.qt.model.parent.ParentMenuModel;

import java.util.ArrayList;

public class ParentMenuRecyclerAdapter extends RecyclerView.Adapter<ParentMenuRecyclerAdapter.ParentMenuRecyclerViewHolder> {
    private ArrayList<ParentMenuModel> mParentMenuList;
    private final Context mContext;
    private final ParentMenuRecyclerAdapterListener mInteractor;
    private String themeSelect;

    public ParentMenuRecyclerAdapter(ArrayList<ParentMenuModel> mParentMenuList, Context mContext, ParentMenuRecyclerAdapterListener mInteractor) {
        this.mParentMenuList = mParentMenuList;
        this.mContext = mContext;
        this.mInteractor = mInteractor;
        themeSelect = ThemeHelper.getPrefCurrentTheme(mContext);
    }

    @NonNull
    @Override
    public ParentMenuRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ParentMenuRecyclerViewHolder(LayoutInflater.from(mContext).inflate(R.layout.faculty_home_row_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ParentMenuRecyclerViewHolder holder, int position) {
        if(themeSelect.equalsIgnoreCase("blue")) {
            holder.img_layout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_screen_item_bg_blue));
            holder.menuImage.setColorFilter(ContextCompat.getColor(mContext, R.color.color_blue));
        } else if(themeSelect.equalsIgnoreCase("red")) {
            holder.img_layout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_screen_item_bg_red));
            holder.menuImage.setColorFilter(ContextCompat.getColor(mContext, R.color.color_red_accent));
        } else if(themeSelect.equalsIgnoreCase("purple")) {
            holder.img_layout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_screen_item_bg_purple));
            holder.menuImage.setColorFilter(ContextCompat.getColor(mContext, R.color.color_purple_accent));
        } else if(themeSelect.equalsIgnoreCase("black")) {
            holder.img_layout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_screen_item_bg_black));
            holder.menuImage.setColorFilter(ContextCompat.getColor(mContext, R.color.color_black_accent));
        } else if(themeSelect.equalsIgnoreCase("brown")) {
            holder.img_layout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_screen_item_bg_brown));
            holder.menuImage.setColorFilter(ContextCompat.getColor(mContext, R.color.color_brown_primary_accent));
        } else if(themeSelect.equalsIgnoreCase("green")) {
            holder.img_layout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_screen_item_bg_green));
            holder.menuImage.setColorFilter(ContextCompat.getColor(mContext, R.color.color_green));
        }
        if(mParentMenuList.get(position).getBadgeNum() != null && mParentMenuList.get(position).getBadgeNum() != ""){
            holder.tv_badge.setVisibility(View.VISIBLE);
            holder.tv_badge.setText(mParentMenuList.get(position).getBadgeNum());
        } else {
            holder.tv_badge.setVisibility(View.GONE);
        }

        holder.menuImage.setImageResource(mParentMenuList.get(position).getId());
        holder.menuText.setText(mParentMenuList.get(position).getName());
        holder.root.setOnClickListener(v -> mInteractor.onMenuSelected(mParentMenuList.get(position)));
    }

    @Override
    public int getItemCount() {
        return mParentMenuList.size();
    }

    public interface ParentMenuRecyclerAdapterListener {
        void onMenuSelected(ParentMenuModel mSelectedMenu);
    }

    class ParentMenuRecyclerViewHolder extends RecyclerView.ViewHolder {
        private final ImageView menuImage;
        private final TextView menuText;
        private final View root;
        private final RelativeLayout img_layout;
        private final TextView tv_badge;

        public ParentMenuRecyclerViewHolder(View itemView) {
            super(itemView);

            root = itemView;
            tv_badge = itemView.findViewById(R.id.tv_badge);
            img_layout = itemView.findViewById(R.id.img_layout);
            menuImage = itemView.findViewById(R.id.img_header);
            menuText = itemView.findViewById(R.id.tv_heading);
        }
    }

  public void setList(ArrayList<ParentMenuModel> mList) {
        this.mParentMenuList = mList;
        notifyDataSetChanged();
  }
}
