package com.scool.qt.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.dao.DatabaseClient;
import com.scool.qt.model.parent.HomeWork;
import com.scool.qt.model.room.Homework;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ParentHomeworkListAdapter extends RecyclerView.Adapter<ParentHomeworkListAdapter.ParentHomeworkListAdapterViewHolder> {
    private final Context mContext;
    private List<HomeWork> mHomeworkList;
    List<Homework> allHomeItem;
    public IItemClick iItemClick;

    public ParentHomeworkListAdapter(Context mContext, List<HomeWork> mHomeworkList, List<Homework> allHomeItem, IItemClick itemClick) {
        this.mContext = mContext;
        this.iItemClick = itemClick;
        this.mHomeworkList = mHomeworkList;
        this.allHomeItem = allHomeItem;
    }

    @NonNull
    @Override
    public ParentHomeworkListAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ParentHomeworkListAdapterViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_parent_homework_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ParentHomeworkListAdapterViewHolder holder, int position) {
        HomeWork homework = mHomeworkList.get(position);
        holder.homeworkSubject.setText(homework.getSubjectName());
        holder.homeworkFinishBefore.setText(mContext.getString(R.string.kfinish_before) + homework.getTargetDate());
        holder.homeworkDescription.setText(homework.getHomeWork());

       if(homework.isChecked()) {
           //homework.setChecked(true);
           holder.homeworkInactiveCheck.setBackground(mContext.getDrawable(R.drawable.ic_active_circle_tick));
       } else {
          // homework.setChecked(false);
           holder.homeworkInactiveCheck.setBackground(mContext.getDrawable(R.drawable.ic_inactive_circle_tick));
       }
        Homework homework1 = new Homework(homework.getHomeworkId(), homework.getDate(), homework.getHomeWork());
        holder.homeworkInactiveCheck.setOnClickListener(v -> {
            if(!homework.isChecked()) {
                insertItem(homework1);
                homework.setChecked(true);
               // iItemClick.itemClick(homework1, position, "insert");
            } else {
                deleteItem(homework1);
                homework.setChecked(false);
               // iItemClick.itemClick(homework1, position, "delete");
            }
            notifyItemChanged(position);
        });

    }

    public void insertItem(Homework homework) {
        Executor myExecutor = Executors.newSingleThreadExecutor();
        myExecutor.execute(() -> DatabaseClient.getInstance(mContext).getAppDatabase().homeworkDao().insertAll(homework));
    }

    public void updateItem(List<HomeWork> homeworkList, int position, String operation) {
        if(operation.equalsIgnoreCase("insert")) {
           this.mHomeworkList = homeworkList;
        } else {
            this.mHomeworkList.remove(position);
        }
        notifyItemChanged(position);
    }

    public void deleteItem(Homework homework) {
        AsyncTask.execute(() -> {
            DatabaseClient.getInstance(mContext).getAppDatabase().homeworkDao().delete(homework);
        });

    }




    @Override
    public int getItemCount() {
        return mHomeworkList.size();
    }

    class ParentHomeworkListAdapterViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout root;
        TextView homeworkSubject, homeworkFinishBefore, homeworkDescription;
        ImageView homeworkInactiveCheck;

        public ParentHomeworkListAdapterViewHolder(View itemView) {
            super(itemView);
            root = itemView.findViewById(R.id.homework_item_root_layout);
            homeworkSubject = itemView.findViewById(R.id.homework_subject_tv);
            homeworkFinishBefore = itemView.findViewById(R.id.homework_finish_before_tv);
            homeworkDescription = itemView.findViewById(R.id.homework_description_tv);
            homeworkInactiveCheck = itemView.findViewById(R.id.homework_inactive_check_iv);
        }
    }

    public interface IItemClick {
        void itemClick(Homework homework, int position, String operation);
    }
}
