package com.scool.qt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.model.FacultiesItem;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FacultyFragmentRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<FacultiesItem> mFacultiesList;
    private Context mContext;
    public String type;
    public IItemClickListener itemClickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.faculty_image)
        ImageView faculty_image;
        @BindView(R.id.faculty_name)
        TextView facultyName;
        @BindView(R.id.faculty_qualification)
        TextView facultyQualification;
        @BindView(R.id.deparment)
        TextView deparment;
        @BindView(R.id.faculty_info)
        TextView faculty_info;

        /**
         *
         * @param view
         */
        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindItems(FacultiesItem mFacultiesItem) {
            facultyName.setText(mFacultiesItem.getName());
            if(type.equalsIgnoreCase("withoutLogin")) {
                facultyQualification.setText(mFacultiesItem.getQualificationEducation());
                deparment.setText(mFacultiesItem.getDeparment() +" " +mContext.getString(R.string.kdepartment));
                faculty_info.setText(mFacultiesItem.getInfo());

                Picasso.get()
                        .load(Constant.IMAGE_BASE_URL+ mFacultiesItem.getPhoto())
                        .placeholder(R.mipmap.app_icon)
                        .error(R.mipmap.app_icon)
                        .into(faculty_image);
            } else {
                faculty_image.setVisibility(View.GONE);
                facultyQualification.setVisibility(View.GONE);
                deparment.setVisibility(View.GONE);
                faculty_info.setVisibility(View.GONE);
            }
        }
    }

    public class LoginViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.student_item_root)
        ConstraintLayout student_item_root;

        @BindView(R.id.faculty_student_name)
        TextView studentName;

        @BindView(R.id.faculty_student_image)
        RecyclingImageView studentImage;

        public LoginViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> itemClickListener.itemClick(itemView, getAdapterPosition(), mFacultiesList.get(getAdapterPosition())));
        }

        public void bindItems(FacultiesItem mFacultiesItem) {
            studentName.setText(mFacultiesItem.getName());
            studentImage.setVisibility(View.GONE);
        }
    }




    public FacultyFragmentRecyclerAdapter(Context mContext, String type, IItemClickListener itemClickListener) {
        this.mFacultiesList = new ArrayList<>();
        this.mContext = mContext;
        this.type = type;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(type.equalsIgnoreCase("withoutLogin")) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.frag_faculty_item_row, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.faculty_student_list_item, parent, false);
            return new LoginViewHolder(itemView);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof MyViewHolder) {
            FacultiesItem mFacultiesItem = mFacultiesList.get(position);
            ((MyViewHolder) holder).bindItems(mFacultiesItem);
        } else if(holder instanceof LoginViewHolder) {
            FacultiesItem mFacultiesItem = mFacultiesList.get(position);
            ((LoginViewHolder) holder).bindItems(mFacultiesItem);
        }
    }

    @Override
    public int getItemCount() {
        return mFacultiesList.size();
    }

    public   void updateList(List<FacultiesItem> mFacultiesList) {
        this.mFacultiesList = mFacultiesList;
        notifyDataSetChanged();
    }

    void notifySingleItem(int pos,  FacultiesItem mItem) {
       this.mFacultiesList.set(pos, mItem);
       notifyItemChanged(pos);
    }

    public interface IItemClickListener {
        void itemClick(View view, int position, FacultiesItem mFacultiesItem);
    }
}
