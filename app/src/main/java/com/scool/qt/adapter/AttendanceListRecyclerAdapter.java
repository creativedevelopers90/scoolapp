package com.scool.qt.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.faculty.fragments.FacultyAttendanceFragment;
import com.scool.qt.helper.LocaleHelper;
import com.scool.qt.model.Attendance;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.scool.qt.utils.Constant.ATTENDANCE_TYPE_UPDATE;

public class AttendanceListRecyclerAdapter extends RecyclerView.Adapter<AttendanceListRecyclerAdapter.MyViewHolder> {

    private List<Attendance> attendanceList;
    private final Context mContext;
    private Resources resources;
    private  ItemClickListener itemClickListener;
    private int attendanceUpdatedStatus = Constant.ATTENDANCE_TYPE_NO_STATUS_MARKED;
    private Boolean isDataSaved;

    public AttendanceListRecyclerAdapter(Context mContext, ItemClickListener itemClickListener, Boolean isDataSaved) {
        this.attendanceList = new ArrayList<>();
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
        resources = getLanguageType();
        this.isDataSaved = isDataSaved;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.faculty_attendance_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.studentNameTV.setText(attendanceList.get(position).getStudentName());
        if (FacultyAttendanceFragment.ENABLE_EDIT_ATTENDANCE)
            manageClickListener(holder, attendanceList.get(position), position);
        showAttendance(holder, attendanceList.get(position).getAttendanceStatus().intValue());
    }

    public void findItemById(int id, Long attendanceUpdatedStatus) {
        Optional<Attendance> attendanceOptional = attendanceList.stream().filter(item -> item.getId() == id).findFirst();
        if(attendanceOptional.isPresent()) {
            attendanceOptional.get().setAttendanceStatus(attendanceUpdatedStatus);
            notifyDataSetChanged();
        }
    }

    public void setElements(List<Attendance> attendanceList) {
        this.attendanceList = attendanceList;
        notifyDataSetChanged();;
    }

    public void setIsDataSaved(boolean isDataSaved) {
        this.isDataSaved = isDataSaved;
    }

    /**
     * Method to manage click event on the layout
     */
    private void manageClickListener(final MyViewHolder holder, final Attendance mAttendance, int position) {
        holder.itemView.setOnLongClickListener(v -> {
            itemClickListener.click(mAttendance, ATTENDANCE_TYPE_UPDATE, position);
            return false;
        });
        holder.attendanceLeaveLayout.setOnClickListener(v -> {
            if(isDataSaved) {
                holder.attendancePresentIV.setImageDrawable(resources.getDrawable(R.drawable.attendance_grey_circle));
                holder.attendanceAbsentIV.setImageDrawable(resources.getDrawable(R.drawable.attendance_grey_circle));
                holder.attendanceLeaveIV.setImageDrawable(resources.getDrawable(R.drawable.attendance_yellow_circle));
                holder.attendancePresentTV.setVisibility(View.GONE);
                holder.attendanceAbsentTV.setVisibility(View.GONE);
                holder.attendanceLeaveTV.setVisibility(View.VISIBLE);
                attendanceUpdatedStatus = Constant.ATTENDANCE_TYPE_LEAVE;
                itemClickListener.click(mAttendance, attendanceUpdatedStatus, position);
            }else {
                Toast.makeText(mContext, "Previous date's attendance can not be edited", Toast.LENGTH_SHORT).show();
            }

        });

        holder.attendanceAbsentLayout.setOnClickListener(v -> {
                    if(isDataSaved) {
                        holder.attendancePresentIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.attendance_grey_circle));
                        holder.attendanceAbsentIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.attendance_red_circle));
                        holder.attendanceLeaveIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.attendance_grey_circle));

                        holder.attendancePresentTV.setVisibility(View.GONE);
                        holder.attendanceAbsentTV.setVisibility(View.VISIBLE);
                        holder.attendanceLeaveTV.setVisibility(View.GONE);

                        attendanceUpdatedStatus = Constant.ATTENDANCE_TYPE_ABSENT;
                        itemClickListener.click(mAttendance, attendanceUpdatedStatus, position);
                    } else {
                        Toast.makeText(mContext, "Previous date's attendance can not be edited", Toast.LENGTH_SHORT).show();
                    }

        });

       holder.attendancePresentLayout.setOnClickListener( v -> {
                   if(isDataSaved) {
                       holder.attendancePresentIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.attendance_green_circle));
                       holder.attendanceAbsentIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.attendance_grey_circle));
                       holder.attendanceLeaveIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.attendance_grey_circle));

                       holder.attendancePresentTV.setVisibility(View.VISIBLE);
                       holder.attendanceAbsentTV.setVisibility(View.GONE);
                       holder.attendanceLeaveTV.setVisibility(View.GONE);

                       attendanceUpdatedStatus = Constant.ATTENDANCE_TYPE_PRESENT;
                       itemClickListener.click(mAttendance, attendanceUpdatedStatus, position);
                   } else {
                       Toast.makeText(mContext, "Previous date's attendance can not be edited", Toast.LENGTH_SHORT).show();
                   }

       });
    }


    /**
     * method to show attendance in the UI
     *
     * @param holder
     * @param attendanceStatus
     */
    public void showAttendance(final MyViewHolder holder, final int attendanceStatus) {
        switch (attendanceStatus) {
            case Constant.ATTENDANCE_TYPE_NO_STATUS_MARKED:
                holder.attendancePresentIV.setImageDrawable(resources.getDrawable(R.drawable.attendance_grey_circle));
                holder.attendanceAbsentIV.setImageDrawable(resources.getDrawable(R.drawable.attendance_grey_circle));
                holder.attendanceLeaveIV.setImageDrawable(resources.getDrawable(R.drawable.attendance_grey_circle));

                holder.attendancePresentTV.setVisibility(View.GONE);
                holder.attendanceAbsentTV.setVisibility(View.GONE);
                holder.attendanceLeaveTV.setVisibility(View.GONE);
                break;
            case Constant.ATTENDANCE_TYPE_LEAVE:
                holder.attendancePresentIV.setImageDrawable(resources.getDrawable(R.drawable.attendance_grey_circle));
                holder.attendanceAbsentIV.setImageDrawable(resources.getDrawable(R.drawable.attendance_grey_circle));
                holder.attendanceLeaveIV.setImageDrawable(resources.getDrawable(R.drawable.attendance_yellow_circle));

                holder.attendancePresentTV.setVisibility(View.GONE);
                holder.attendanceAbsentTV.setVisibility(View.GONE);
                holder.attendanceLeaveTV.setVisibility(View.VISIBLE);
                break;
            case Constant.ATTENDANCE_TYPE_ABSENT:
                holder.attendancePresentIV.setImageDrawable(resources.getDrawable(R.drawable.attendance_grey_circle));
                holder.attendanceAbsentIV.setImageDrawable(resources.getDrawable(R.drawable.attendance_red_circle));
                holder.attendanceLeaveIV.setImageDrawable(resources.getDrawable(R.drawable.attendance_grey_circle));

                holder.attendancePresentTV.setVisibility(View.GONE);
                holder.attendanceAbsentTV.setVisibility(View.VISIBLE);
                holder.attendanceLeaveTV.setVisibility(View.GONE);
                break;
            case Constant.ATTENDANCE_TYPE_PRESENT:
                holder.attendancePresentIV.setImageDrawable(resources.getDrawable(R.drawable.attendance_green_circle));
                holder.attendanceAbsentIV.setImageDrawable(resources.getDrawable(R.drawable.attendance_grey_circle));
                holder.attendanceLeaveIV.setImageDrawable(resources.getDrawable(R.drawable.attendance_grey_circle));

                holder.attendancePresentTV.setVisibility(View.VISIBLE);
                holder.attendanceAbsentTV.setVisibility(View.GONE);
                holder.attendanceLeaveTV.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return attendanceList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private final View root;
        private final TextView studentNameTV, attendanceLeaveTV, attendanceAbsentTV, attendancePresentTV;
        private final ImageView attendanceLeaveIV, attendanceAbsentIV, attendancePresentIV;
        private final RelativeLayout attendanceLeaveLayout, attendanceAbsentLayout, attendancePresentLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            root = itemView.findViewById(R.id.attendance_item_root);
            studentNameTV = itemView.findViewById(R.id.faculty_student_name);

            attendanceLeaveTV = itemView.findViewById(R.id.attendance_leave_tv);
            attendanceAbsentTV = itemView.findViewById(R.id.attendance_absent_tv);
            attendancePresentTV = itemView.findViewById(R.id.attendance_present_tv);

            attendanceLeaveIV = itemView.findViewById(R.id.attendance_leave_iv);
            attendanceAbsentIV = itemView.findViewById(R.id.attendance_absent_iv);
            attendancePresentIV = itemView.findViewById(R.id.attendance_present_iv);

            attendanceLeaveLayout = itemView.findViewById(R.id.attendance_leave_layout);
            attendanceAbsentLayout = itemView.findViewById(R.id.attendance_absent_layout);
            attendancePresentLayout = itemView.findViewById(R.id.attendance_present_layout);
        }
    }

    public Resources getLanguageType() {
        Context context = LocaleHelper.setLocale(mContext, LocaleHelper.getLanguage(mContext));
        return context.getResources();
    }

    public interface ItemClickListener {
         void click(final Attendance mAttendance, final long attendanceUpdatedStatus , final int position);
    }
}
