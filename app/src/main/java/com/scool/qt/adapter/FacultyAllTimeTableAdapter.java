package com.scool.qt.adapter;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.model.AllClassTimetableList;
import com.scool.qt.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FacultyAllTimeTableAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = FacultyAllTimeTableAdapter.class.getSimpleName();

    private int HEADER_VIEW = 0;
    private int LIST_VIEw = 1;
    public Context mContext;
    private List<AllClassTimetableList> mAllClassTimeTable = null;

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_header)
        TextView tvHeader;
        @BindView(R.id.tv_language)
        TextView tvLanguage;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.constraintLayout2)
        RelativeLayout constraintLayout2;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindItems(Object mTimeTablesItem) {
            AllClassTimetableList allClassTimeTableResp = (AllClassTimetableList) mTimeTablesItem;
            tvHeader.setText(allClassTimeTableResp.getHeaderName());
            tvHeader.setTextColor(Utils.getColor(mContext, R.color.colorPrimaryDark));
            tvLanguage.setBackgroundColor(ContextCompat.getColor(mContext, R.color.grey));
            tvLanguage.setText("Subject - " + mContext.getString(R.string.kclass));
            tvLanguage.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            tvTime.setBackgroundColor(ContextCompat.getColor(mContext, R.color.grey));
            tvTime.setText(mContext.getString(R.string.kTimeTitle));
            tvTime.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        }
    }

    class ListViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_header)
        TextView tvHeader;
        @BindView(R.id.tv_language)
        TextView tvLanguage;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.constraintLayout2)
        RelativeLayout constraintLayout2;

        public ListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindItems(Object mTimeTablesItem) {
            AllClassTimetableList allClassTimeTableResp = (AllClassTimetableList) mTimeTablesItem;
            tvHeader.setVisibility(View.GONE);
            tvTime.setText(allClassTimeTableResp.getTime());
            view.setVisibility(View.VISIBLE);
            tvLanguage.setText(allClassTimeTableResp.getSubjectName() + "-" + allClassTimeTableResp.getClassName());
            Log.d("TAG", "onBindViewHolder: TIME" + allClassTimeTableResp.getTime() +
                    "SUBJECT NAME ==>" + allClassTimeTableResp.getSubjectName());
        }

    }

    public FacultyAllTimeTableAdapter(Context mContext, List<AllClassTimetableList> mAllClassTimeTable) {
        this.mContext = mContext;
        this.mAllClassTimeTable = mAllClassTimeTable;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == HEADER_VIEW) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.header_layout_row, null);
            return new HeaderViewHolder(v);
        } else if (viewType == LIST_VIEw) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.header_layout_row, null);
            return new ListViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderViewHolder) {
            ((HeaderViewHolder) holder).bindItems(mAllClassTimeTable.get(position));
        } else if (holder instanceof ListViewHolder) {
            ((ListViewHolder) holder).bindItems(mAllClassTimeTable.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mAllClassTimeTable.size();
    }

    private Boolean isHeaderData(int pos) {
        return mAllClassTimeTable.get(pos).isHeader();
    }

    private Boolean isListData(int pos) {
        return mAllClassTimeTable.get(pos).isHeader();
    }

    /**
     * @param position the position
     * @return Method will Return Item View Type
     */
    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                if (isHeaderData(position)) {
                    return HEADER_VIEW;
                }
                break;
            case 1:
                if (!isListData(position)) {
                    return LIST_VIEw;
                }
                break;
            default:
                if (!isListData(position)) {
                    return LIST_VIEw;
                }
                break;
        }
        return super.getItemViewType(position);
    }

    public void updateList(List<AllClassTimetableList> mAllClassTimeTableList) {
        this.mAllClassTimeTable = mAllClassTimeTableList;
        notifyDataSetChanged();
    }

    void notifySingleItem(int pos, AllClassTimetableList mItem) {
        this.mAllClassTimeTable.set(pos, mItem);
        notifyItemChanged(pos);
    }
}



