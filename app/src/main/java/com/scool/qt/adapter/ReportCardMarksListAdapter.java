package com.scool.qt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.model.MarksList;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ReportCardMarksListAdapter extends RecyclerView.Adapter<ReportCardMarksListAdapter.MyMarkListViewHolder> {

    private final Context mContext;
    private List<MarksList> marksList;
    private final AdapterButtonClickListener mListener;

    public ReportCardMarksListAdapter(Context mContext,  AdapterButtonClickListener mListener) {
        this.mContext = mContext;
        this.marksList = new ArrayList<>();
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public MyMarkListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyMarkListViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_report_card_mark_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyMarkListViewHolder holder, int position) {
        holder.studentName.setText(marksList.get(position).getStudentName());
        if (!marksList.get(position).getMarks().isEmpty())
            holder.studentMarks.setText(marksList.get(position).getMarks());
        holder.saveMarksBtn.setOnClickListener(v -> {
            Utils.hideSoftKeyboard(mContext);
            if(Constant.MAXIMUM_MARKS >= Float.parseFloat(holder.studentMarks.getText().toString())){
                mListener.onSaveMarksSaveClicked(marksList.get(position), Float.parseFloat(holder.studentMarks.getText().toString()));
            } else {
                mListener.unknownMarks();
                holder.studentMarks.setText("");
            }

        });
    }

    @Override
    public int getItemCount() {
        return marksList.size();
    }

    public interface AdapterButtonClickListener {
        void onSaveMarksSaveClicked(MarksList marks, float updatedMarks);
        void unknownMarks();
    }

    class MyMarkListViewHolder extends RecyclerView.ViewHolder {
        TextView studentName;
        EditText studentMarks;
        ImageView saveMarksBtn;

        public MyMarkListViewHolder(View itemView) {
            super(itemView);
            studentName = itemView.findViewById(R.id.student_name);
            studentMarks = itemView.findViewById(R.id.student_marks);
            saveMarksBtn = itemView.findViewById(R.id.save_marks_btn);
        }
    }

    public void addItemsInList(List<MarksList> marksList) {
        this.marksList = marksList;
        notifyDataSetChanged();
    }
}
