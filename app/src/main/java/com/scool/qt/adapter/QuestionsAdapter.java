package com.scool.qt.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.model.QuestionObject;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class QuestionsAdapter  extends RecyclerView.Adapter<QuestionsAdapter.MyMarkListViewHolder>{
    public static final String TAG = QuestionsAdapter.class.getSimpleName();
    private List<QuestionObject> mQuestionlist;
    private Context mContext;
    public QuestionsAdapter.ItemClick itemClick;

    public QuestionsAdapter(FragmentActivity activity, ItemClick itemClick) {
        this.mQuestionlist = new ArrayList<>();
        this.mContext = activity;
        this.itemClick = itemClick;
    }

    public void addItemsInList(List<QuestionObject> mSurveyList) {
        this.mQuestionlist = mSurveyList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public QuestionsAdapter.MyMarkListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new QuestionsAdapter.MyMarkListViewHolder(LayoutInflater.from(mContext).inflate(R.layout.question_list_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionsAdapter.MyMarkListViewHolder holder, int position) {
        if(holder instanceof QuestionsAdapter.MyMarkListViewHolder) {
            holder.bindItems(mQuestionlist.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mQuestionlist.size();
    }

    class MyMarkListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_question)
        AppCompatTextView tvQuestion;

        @BindView(R.id.edt_answer)
        AppCompatEditText edt_answer;

        public MyMarkListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindItems(QuestionObject questionObject) {
            tvQuestion.setText(questionObject.getText());
            if(questionObject.getAnswered()) {
                edt_answer.setText(questionObject.getAns());
                edt_answer.setEnabled(false);
                edt_answer.setFocusable(false);
                edt_answer.setFocusableInTouchMode(false);
                edt_answer.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.faculty_selector_grey));
              //  edt_answer.setBackgroundColor(Utils.getColor(mContext, R.color.colorInactive));
            } else {
                if(questionObject.getType().equalsIgnoreCase(Constant.RAPID_QUESTION_SELECT)) {
                    String sampleString = questionObject.getAnswerOptions();
                    String[] items = sampleString.split("\\r?\\n");
                    List<String> itemList = new ArrayList<>();
                    for (String item : items) {
                        itemList.add(item);
                    }

                    edt_answer.setFocusableInTouchMode(false);
                    edt_answer.setOnClickListener(v -> {
                        Utils.hideSoftKeyboard(mContext);
                        Timber.d("<<<Get Text>>"+edt_answer.getText().toString());
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setTitle("Choose an option");// add a radio button list
                        int checkedItem = 0;
                        if(edt_answer.getText().toString().equalsIgnoreCase("")) {
                            checkedItem = 0;
                        } else {
                            checkedItem =  IntStream.range(0, items.length)
                                    .filter(i -> edt_answer.getText().toString().equalsIgnoreCase(items[i]))
                                    .findFirst()
                                    .orElse(-1);
                        }
                        builder.setSingleChoiceItems(items, checkedItem, (dialog, which) -> {
                            edt_answer.setText(items[which]);

                        });
                        builder.setPositiveButton("OK", (dialog1, which) -> {
                            QuestionObject findObject = mQuestionlist.stream().filter(v1 -> v1.getId() == questionObject.getId()).findAny().orElse(null);
                            findObject.setAns(edt_answer.getText().toString());
                            itemClick.onItemSelected(findObject);
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    });
                } else {
                    edt_answer.setHint(questionObject.getPlaceholder());
                    edt_answer.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            Log.d(TAG, "onTextChanged" + s.toString());
                            QuestionObject findObject = mQuestionlist.stream().filter(v1 -> v1.getId() == questionObject.getId()).findAny().orElse(null);
                            findObject.setAns(edt_answer.getText().toString());
                            itemClick.onItemSelected(findObject);
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }
                    });
                }
            }
        }
    }

    public interface ItemClick {
        public void onItemSelected(QuestionObject mQuestionlist);
    }
}
