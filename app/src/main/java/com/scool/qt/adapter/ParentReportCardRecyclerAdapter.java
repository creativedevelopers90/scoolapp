package com.scool.qt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.model.parent.Mark;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParentReportCardRecyclerAdapter extends RecyclerView.Adapter<ParentReportCardRecyclerAdapter.MyViewHolder> {
    private List<Mark> mMarkList;
    private final Context mContext;

    public ParentReportCardRecyclerAdapter(Context mContext) {
        this.mMarkList = new ArrayList<>();
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder
    onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_parent_report_card_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Mark mMark = mMarkList.get(position);
        holder.bindItems(mMark);
    }

    @Override
    public int getItemCount() {
        return mMarkList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_subject_name)
        TextView subjectName;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.item_marks_count)
        TextView marksCount;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindItems(Mark mMarks) {
            subjectName.setText(mMarks.getmSubjectName());
            marksCount.setText(mMarks.getMarks());
        }
    }

    public void addItemsInList(List<Mark> mMarkList) {
        this.mMarkList = mMarkList;
        notifyDataSetChanged();
    }
}
