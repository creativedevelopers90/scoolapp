package com.scool.qt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.scool.qt.R;
import com.scool.qt.model.DelegationItem;
import com.scool.qt.utils.RecyclingImageView;

import java.util.List;
public  class FacultyDelegationAdapter extends RecyclerView.Adapter<FacultyDelegationAdapter.ReportRecyclerViewHolder> {
    private List<DelegationItem> students;
    private Context mContext;
    private IItemClickListener iItemClickListener;

    public FacultyDelegationAdapter(List<DelegationItem> students, IItemClickListener iItemClickListener) {
        this.students = students;
        this.mContext = mContext;
        this.iItemClickListener = iItemClickListener;
    }

    @NonNull
    @Override
    public FacultyDelegationAdapter.ReportRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FacultyDelegationAdapter.ReportRecyclerViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.faculty_student_list_item, parent, false));
    }


    public void updateList(List<DelegationItem> students) {
        this.students = students;
        notifyDataSetChanged();;
    }

    @Override
    public void onBindViewHolder(@NonNull FacultyDelegationAdapter.ReportRecyclerViewHolder holder, int position) {
        holder.root.setOnClickListener(v -> iItemClickListener.itemClick(holder.root, position, students.get(position)));
        if(students.get(position).getDelegated()) {
            holder.studentName.setText(students.get(position).getName() + " Delegation - " + students.get(position).getDelegatedTo());
        } else {
            holder.studentName.setText(students.get(position).getName());
        }
    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    class ReportRecyclerViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout root;
        TextView studentName;
        RecyclingImageView studentImage;

        public ReportRecyclerViewHolder(View itemView) {
            super(itemView);
            root = itemView.findViewById(R.id.student_item_root);
            studentName = itemView.findViewById(R.id.faculty_student_name);
            studentImage = itemView.findViewById(R.id.faculty_student_image);
            studentImage.setVisibility(View.GONE);
        }
    }

    public interface IItemClickListener {
        void itemClick(View view, int position, DelegationItem mTimeTablesItem);
    }
}
