package com.scool.qt.network;

import com.scool.qt.EndpointEnums;

public interface NetworkView {
    void showProgress();
    void hideProgress();
    void noInternetConnection();
    void onEndpointSuccess(Object object, String message, EndpointEnums endpointType);
    void onEndpointFailure(String message, EndpointEnums endpointType);
}
