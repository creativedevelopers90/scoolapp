package com.scool.qt.network;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.model.AboutUsResp;
import com.scool.qt.model.AddDiaryResponse;
import com.scool.qt.model.AddHomeworkResponse;
import com.scool.qt.model.AddMarksResponse;
import com.scool.qt.model.AllClassTimeTableResp;
import com.scool.qt.model.ChangePasswordResponse;
import com.scool.qt.model.ClassAttendanceResponse;
import com.scool.qt.model.ClassListFrDelegation;
import com.scool.qt.model.ClassTimeTableResp;
import com.scool.qt.model.ContactUsResp;
import com.scool.qt.model.DeleteDiaryResponse;
import com.scool.qt.model.DeleteHomeworkResponse;
import com.scool.qt.model.ExamListResponse;
import com.scool.qt.model.FacultyClassResponse;
import com.scool.qt.model.FacultyDiaryListResponse;
import com.scool.qt.model.FacultyHomeWorkListResponse;
import com.scool.qt.model.FacultyInfoResponse;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.FacultyStudentListResponse;
import com.scool.qt.model.LogoutResponse;
import com.scool.qt.model.MarksListResponse;
import com.scool.qt.model.ParentDiaryReplyResponse;
import com.scool.qt.model.ParentLoginResponse;
import com.scool.qt.model.QuestionListResponse;
import com.scool.qt.model.SchoolCircularResponse;
import com.scool.qt.model.SchoolFacultiesResp;
import com.scool.qt.model.StartResponse;
import com.scool.qt.model.StudentDetailsResponse;
import com.scool.qt.model.StudentLoginResponse;
import com.scool.qt.model.SubjectListResponse;
import com.scool.qt.model.SurveyListResponse;
import com.scool.qt.model.TimetableList;
import com.scool.qt.model.UpdateAttendanceResponse;
import com.scool.qt.model.UpdateDiaryResponse;
import com.scool.qt.model.UpdateHomeworkResponse;
import com.scool.qt.model.UpdateMarksResponse;
import com.scool.qt.model.UpdateMultipleAttendanceResponse;
import com.scool.qt.model.UpdateSurveyResponse;
import com.scool.qt.model.parent.AttendanceDateObject;
import com.scool.qt.model.parent.ParentDetailResponse;
import com.scool.qt.model.parent.ParentDiaryResponse;
import com.scool.qt.model.parent.ParentHomeworkResponse;
import com.scool.qt.model.parent.ParentReportCardDetailResponse;
import com.scool.qt.model.parent.ParentTimeTableResponse;
import com.scool.qt.model.parent.request.ReplyDiaryRequest;
import com.scool.qt.model.request.AddDelegationRequest;
import com.scool.qt.model.request.AddDiaryRequest;
import com.scool.qt.model.request.AddHomeworkRequest;
import com.scool.qt.model.request.AddMarksRequest;
import com.scool.qt.model.request.ChangePasswordRequest;
import com.scool.qt.model.request.LoginUserRequest;
import com.scool.qt.model.request.SurveyRequest;
import com.scool.qt.model.request.UpdateAttendanceRequest;
import com.scool.qt.model.request.UpdateDiaryRequest;
import com.scool.qt.model.request.UpdateHomeworkRequest;
import com.scool.qt.model.request.UpdateMarksRequest;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.util.Date;
import java.util.Objects;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.scool.qt.EndpointEnums.add_delegation;
import static com.scool.qt.EndpointEnums.class_lists_for_Delegation;
import static com.scool.qt.EndpointEnums.exam_list;
import static com.scool.qt.EndpointEnums.faculty_add_diary;
import static com.scool.qt.EndpointEnums.faculty_change_password;
import static com.scool.qt.EndpointEnums.faculty_class;
import static com.scool.qt.EndpointEnums.faculty_class_attendance;
import static com.scool.qt.EndpointEnums.faculty_delete_diary;
import static com.scool.qt.EndpointEnums.faculty_diary_list;
import static com.scool.qt.EndpointEnums.faculty_info;
import static com.scool.qt.EndpointEnums.faculty_save_marks;
import static com.scool.qt.EndpointEnums.faculty_update_multiple_attendance;
import static com.scool.qt.EndpointEnums.faculy_get_class_time_table;
import static com.scool.qt.EndpointEnums.login;
import static com.scool.qt.EndpointEnums.parent_details;
import static com.scool.qt.EndpointEnums.parent_diary_details;
import static com.scool.qt.EndpointEnums.parent_diary_reply;
import static com.scool.qt.EndpointEnums.parent_report_card_status;
import static com.scool.qt.EndpointEnums.student_details;
import static com.scool.qt.EndpointEnums.update_delegation;

public class NetworkInteractor {
    public static final String TAG = NetworkInteractor.class.getSimpleName();
    private NetworkService mNetworkService;

    public NetworkInteractor() {
        if (this.mNetworkService == null) {
            this.mNetworkService = new NetworkService();
        }
    }

    /**
     * @param mOnFinishedListener
     * @param schoolUniqueID
     */
    public void getSchoolAboutUsData(OnFinishedListener mOnFinishedListener, String schoolUniqueID) {
        mNetworkService.getAPI().getSchoolAboutUsData(schoolUniqueID)
                .enqueue(new Callback<AboutUsResp>() {
                    @Override
                    public void onResponse(Call<AboutUsResp> call, Response<AboutUsResp> response) {
                        if (response.isSuccessful() && response.code() == 200) {
                            mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.about_us);
                        } else {
                            try {
                                mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                        response.message()), EndpointEnums.about_us);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AboutUsResp> call, Throwable t) {
                        try {
                            mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.about_us);
                            throw new InterruptedException("Something went wrong!");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }



    /**
     * @param schoolUniqueID the school unique ID
     */
    public void getSchoolHomeScreenData(OnFinishedListener mOnFinishedListener, String schoolUniqueID) {
        mNetworkService.getAPI().getSchoolHomePageData(schoolUniqueID)
                .enqueue(new Callback<StartResponse>() {
                    @Override
                    public void onResponse(Call<StartResponse> call, Response<StartResponse> response) {
                        if (response.isSuccessful() && response.code() == 200) {
                            mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.start);
                        } else {
                            try {
                                mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                        response.message()), EndpointEnums.start);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<StartResponse> call, Throwable t) {
                        try {
                            mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.start);
                            throw new InterruptedException("Something went wrong!");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    /**
     * @param mOnFinishedListener
     * @param schoolUniqueID
     */
    public void getSchoolFaultyData(OnFinishedListener mOnFinishedListener, String schoolUniqueID) {
        mNetworkService.getAPI().getSchoolFacultiesList(schoolUniqueID)
                .enqueue(new Callback<SchoolFacultiesResp>() {
                    @Override
                    public void onResponse(Call<SchoolFacultiesResp> call, Response<SchoolFacultiesResp> response) {
                        if (response.isSuccessful() && response.code() == 200) {
                            mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.school_faculty);
                        } else {
                            try {
                                if(response.code() != 500) {
                                    mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                            response.message()), EndpointEnums.school_faculty);
                                } else {
                                    mOnFinishedListener.onEndpointFailure("Something went wrong!", EndpointEnums.school_faculty);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SchoolFacultiesResp> call, Throwable t) {
                        try {
                            mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.school_faculty);
                            throw new InterruptedException("Something went wrong!");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    /**
     * @param mOnFinishedListener the on finished listener
     * @param schoolUniqueID      the school unique id
     */
    public void getSchoolCircularList(OnFinishedListener mOnFinishedListener, String schoolUniqueID) {
        mNetworkService.getAPI().getSchoolCircularlist(schoolUniqueID)
                .enqueue(new Callback<SchoolCircularResponse>() {
                    @Override
                    public void onResponse(Call<SchoolCircularResponse> call, Response<SchoolCircularResponse> response) {
                        if (response.isSuccessful() && response.code() == 200) {
                            mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.school_circular);
                        } else {
                            try {
                                mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                        response.message()), EndpointEnums.school_circular);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SchoolCircularResponse> call, Throwable t) {
                        try {
                            mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.school_circular);
                            throw new InterruptedException("Something went wrong!");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    /**
     * @param mOnFinishedListener
     * @param schoolUniqueID
     */
    public void getSchoolContactUsData(OnFinishedListener mOnFinishedListener, String schoolUniqueID) {
        mNetworkService.getAPI().getSchoolContactUsData(schoolUniqueID)
                .enqueue(new Callback<ContactUsResp>() {
                    @Override
                    public void onResponse(Call<ContactUsResp> call, Response<ContactUsResp> response) {
                        if (response.isSuccessful() && response.code() == 200) {
                            mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.contact_us);
                        } else {
                            try {
                                mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                        response.message()), EndpointEnums.contact_us);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ContactUsResp> call, Throwable t) {
                        try {
                            mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.contact_us);
                            throw new InterruptedException("Something went wrong!");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    /**
     * @param mOnFinishedListener
     * @param mLoginUserReq
     * @param mContext
     */
    public void loginAsParent(OnFinishedListener mOnFinishedListener, LoginUserRequest mLoginUserReq, Context mContext) {
        mNetworkService.getAPI().loginAsParent(mLoginUserReq).enqueue(new Callback<ParentLoginResponse>() {
            @Override
            public void onResponse(Call<ParentLoginResponse> call, Response<ParentLoginResponse> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());
                    Utils.savePreferences(Constant.IS_LOGGED_IN, "true", mContext);
                    Utils.savePreferences(Constant.PREF_LOGGED_IN_DATA, json, mContext);
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.login);
                } else {
                    try {
                        Utils.savePreferences(Constant.IS_LOGGED_IN, "false", mContext);
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(), response.message()), EndpointEnums.login);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ParentLoginResponse> call, Throwable t) {
                try {
                    Utils.savePreferences(Constant.IS_LOGGED_IN, "false", mContext);
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.login);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * @param mOnFinishedListener
     * @param mLoginUserReq
     */
    public void loginAsFaculty(OnFinishedListener mOnFinishedListener, LoginUserRequest mLoginUserReq, Context mContext) {
        mNetworkService.getAPI().loginAsFaculty(mLoginUserReq).enqueue(new Callback<FacultyLoginResponse>() {
            @Override
            public void onResponse(Call<FacultyLoginResponse> call, Response<FacultyLoginResponse> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());
                    Utils.savePreferences(Constant.IS_LOGGED_IN, "true", mContext);
                    Utils.savePreferences(Constant.PREF_LOGGED_IN_DATA, json, mContext);
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), login);
                } else {
                    try {
                        Utils.savePreferences(Constant.IS_LOGGED_IN, "false", mContext);
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.login);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<FacultyLoginResponse> call, Throwable t) {
                try {
                    Utils.savePreferences(Constant.IS_LOGGED_IN, "false", mContext);
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.login);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * @param mOnFinishedListener
     * @param mLoginUserReq
     */
    public void loginAsStudent(OnFinishedListener mOnFinishedListener, LoginUserRequest mLoginUserReq, Context mContext) {
        mNetworkService.getAPI().loginAsStudent(mLoginUserReq).enqueue(new Callback<StudentLoginResponse>() {
            @Override
            public void onResponse(Call<StudentLoginResponse> call, Response<StudentLoginResponse> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());
                    Utils.savePreferences(Constant.IS_LOGGED_IN, "true", mContext);
                    Utils.savePreferences(Constant.PREF_LOGGED_IN_DATA, json, mContext);
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), login);
                } else {
                    try {
                        Utils.savePreferences(Constant.IS_LOGGED_IN, "false", mContext);
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.login);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<StudentLoginResponse> call, Throwable t) {
                try {
                    Utils.savePreferences(Constant.IS_LOGGED_IN, "false", mContext);
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), login);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to change password for the user as faculty
     *
     * @param mOnFinishedListener
     * @param mRequest
     * @param token
     */
    public void changeFacultyPassword(OnFinishedListener mOnFinishedListener, ChangePasswordRequest mRequest, String token) {
        mNetworkService.getAPI().changeFacultyPassword(mRequest, token).enqueue(new Callback<ChangePasswordResponse>() {
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 201) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), faculty_change_password);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.faculty_change_password);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), faculty_change_password);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     *
     *
     * @param mOnFinishedListener
     * @param mRequest
     * @param token
     */
    public void updateDelegationRequest(OnFinishedListener mOnFinishedListener, AddDelegationRequest mRequest, String token) {
        mNetworkService.getAPI().updateDelegation(mRequest, token).enqueue(new Callback<ChangePasswordResponse>() {
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), update_delegation);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.update_delegation);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), update_delegation);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to change password for the user as faculty
     *
     * @param mOnFinishedListener
     * @param mRequest
     * @param token
     */
    public void addDelegationRequest(OnFinishedListener mOnFinishedListener, AddDelegationRequest mRequest, String token) {
        mNetworkService.getAPI().addDelegation(mRequest, token).enqueue(new Callback<ChangePasswordResponse>() {
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), add_delegation);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.add_delegation);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), add_delegation);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * @param mOnFinishedListener
     * @param schoolID
     * @param facultyID
     * @param token
     */
    public void fetchFacultyBasicInfo(OnFinishedListener mOnFinishedListener, String schoolID, int facultyID, String token) {
        mNetworkService.getAPI().fetchFacultyInfo(schoolID, facultyID, token).enqueue(new Callback<FacultyInfoResponse>() {
            @Override
            public void onResponse(Call<FacultyInfoResponse> call, Response<FacultyInfoResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), faculty_info);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.faculty_info);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<FacultyInfoResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), faculty_info);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * @param mOnFinishedListener
     * @param schoolID
     * @param facultyID
     * @param token
     */
    public void fetchFacultyClassList(OnFinishedListener mOnFinishedListener, String schoolID, int facultyID, String token) {
        mNetworkService.getAPI().fetchFacultyClassList(schoolID, facultyID, token).enqueue(new Callback<FacultyClassResponse>() {
            @Override
            public void onResponse(Call<FacultyClassResponse> call, Response<FacultyClassResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.faculty_class);
                } else if(response.code() == 500) {
                        mOnFinishedListener.onEndpointFailure("Invalid token", EndpointEnums.faculty_class);
                }
                else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.faculty_class);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<FacultyClassResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), faculty_class);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     * @param mOnFinishedListener
     * @param schoolID
     * @param facultyID
     * @param token
     */
    public void delegationClassList(OnFinishedListener mOnFinishedListener, String schoolID, int facultyID, String token) {
        mNetworkService.getAPI().classListForDelegation(schoolID, facultyID, token).enqueue(new Callback<ClassListFrDelegation>() {
            @Override
            public void onResponse(Call<ClassListFrDelegation> call, Response<ClassListFrDelegation> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), class_lists_for_Delegation);
                } else if(response.code() == 500) {
                    mOnFinishedListener.onEndpointFailure("Invalid token", class_lists_for_Delegation);
                }
                else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), class_lists_for_Delegation);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ClassListFrDelegation> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), class_lists_for_Delegation);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to fetch the list of students for a particular faculty
     *
     * @param onFinishedListener
     * @param schoolID
     * @param facultyID
     * @param studentListID
     * @param token
     */
    public void fetchFacultyStudentList(OnFinishedListener onFinishedListener, String schoolID, int facultyID, int studentListID, String token) {
        mNetworkService.getAPI().facultyStudentList(schoolID, facultyID, studentListID, token).enqueue(new Callback<FacultyStudentListResponse>() {
            @Override
            public void onResponse(Call<FacultyStudentListResponse> call, Response<FacultyStudentListResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    onFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.faculty_student);
                } else {
                    try {
                        onFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.faculty_student);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<FacultyStudentListResponse> call, Throwable t) {
                try {
                    onFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.faculty_student);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to fetch the list of diary for a particular faculty based on the selected student
     *
     * @param onFinishedListener
     * @param schoolID
     * @param facultyID
     * @param classListID
     * @param studentID
     * @param token
     */
    public void fetchFacultyDiaryList(OnFinishedListener onFinishedListener, String schoolID, int facultyID, int classListID, int studentID, String token) {
        mNetworkService.getAPI().facultyDiaryList(schoolID, facultyID, classListID, studentID, token).enqueue(new Callback<FacultyDiaryListResponse>() {
            @Override
            public void onResponse(Call<FacultyDiaryListResponse> call, Response<FacultyDiaryListResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    onFinishedListener.onEndpointSuccess(response.body(), response.message(), faculty_diary_list);
                } else {
                    try {
                        onFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.faculty_diary_list);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<FacultyDiaryListResponse> call, Throwable t) {
                try {
                    onFinishedListener.onEndpointFailure(t.getMessage(), faculty_diary_list);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to allow faculty to add a diary
     *
     * @param mOnFinishedListener
     * @param mRequest
     * @param token
     */
    public void addFacultyDiary(OnFinishedListener mOnFinishedListener, AddDiaryRequest mRequest, String token) {
        mNetworkService.getAPI().addFacultyDiary(mRequest, token).enqueue(new Callback<AddDiaryResponse>() {
            @Override
            public void onResponse(Call<AddDiaryResponse> call, Response<AddDiaryResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 201) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), faculty_add_diary);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.faculty_add_diary);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<AddDiaryResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), faculty_add_diary);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to allow faculty to delete a diary
     *
     * @param mOnFinishedListener
     * @param schoolID
     * @param diaryID
     * @param token
     */
    public void deleteFacultyDiary(OnFinishedListener mOnFinishedListener, String schoolID, int diaryID, String token) {
        mNetworkService.getAPI().deleteFacultyDiary(schoolID, diaryID, token).enqueue(new Callback<DeleteDiaryResponse>() {
            @Override
            public void onResponse(Call<DeleteDiaryResponse> call, Response<DeleteDiaryResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), faculty_delete_diary);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.faculty_delete_diary);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<DeleteDiaryResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), faculty_delete_diary);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     * Method to allow faculty to update a diary
     *
     * @param mOnFinishedListener
     * @param mRequest
     * @param token
     */
    public void updateFacultyAttendanceRequest(OnFinishedListener mOnFinishedListener, UpdateAttendanceRequest mRequest, String token) {
        mNetworkService.getAPI().updateAttendance(mRequest, token).enqueue(new Callback<UpdateAttendanceResponse>() {
            @Override
            public void onResponse(Call<UpdateAttendanceResponse> call, Response<UpdateAttendanceResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.update_attendance);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.update_attendance);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateAttendanceResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.update_attendance);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to allow faculty to update a diary
     *
     * @param mOnFinishedListener
     * @param mRequest
     * @param token
     */
    public void updateDiaryRequest(OnFinishedListener mOnFinishedListener, UpdateDiaryRequest mRequest, String token) {
        mNetworkService.getAPI().updateFacultyDiary(mRequest, token).enqueue(new Callback<UpdateDiaryResponse>() {
            @Override
            public void onResponse(Call<UpdateDiaryResponse> call, Response<UpdateDiaryResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.faculty_update_diary);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.faculty_update_diary);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateDiaryResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.faculty_update_diary);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to fetch the list of homework for a particular selected student
     *
     * @param onFinishedListener
     * @param schoolID
     * @param facultyID
     * @param classListID
     * @param studentID
     * @param token
     */
    public void fetchFacultyHomeWorkList(OnFinishedListener onFinishedListener, String schoolID, int facultyID, int classListID, int studentID, String token) {
        mNetworkService.getAPI().facultyHomeWorkList(schoolID, facultyID, classListID, studentID, token).enqueue(new Callback<FacultyHomeWorkListResponse>() {
            @Override
            public void onResponse(Call<FacultyHomeWorkListResponse> call, Response<FacultyHomeWorkListResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    onFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.faculty_homework_list);
                } else {
                    try {
                        onFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.faculty_homework_list);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<FacultyHomeWorkListResponse> call, Throwable t) {
                try {
                    onFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.faculty_homework_list);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to allow faculty to add a homework
     *
     * @param mOnFinishedListener
     * @param mRequest
     * @param token
     */
    public void addFacultyHomework(OnFinishedListener mOnFinishedListener, AddHomeworkRequest mRequest, String token) {
        mNetworkService.getAPI().addFacultyHomework(mRequest, token).enqueue(new Callback<AddHomeworkResponse>() {
            @Override
            public void onResponse(Call<AddHomeworkResponse> call, Response<AddHomeworkResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 201) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.faculty_add_homework);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.faculty_add_homework);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<AddHomeworkResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.faculty_add_homework);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to allow faculty to update homework
     *
     * @param mOnFinishedListener
     * @param mRequest
     * @param token
     */
    public void updateFacultyHomework(OnFinishedListener mOnFinishedListener, UpdateHomeworkRequest mRequest, String token) {
        mNetworkService.getAPI().updateFacultyHomework(mRequest, token).enqueue(new Callback<UpdateHomeworkResponse>() {
            @Override
            public void onResponse(Call<UpdateHomeworkResponse> call, Response<UpdateHomeworkResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.faculty_update_homework);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.faculty_update_homework);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateHomeworkResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.faculty_update_homework);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to allow faculty to delete a diary
     *
     * @param mOnFinishedListener
     * @param schoolID
     * @param homeworkID
     * @param token
     */
    public void deleteFacultyHomework(OnFinishedListener mOnFinishedListener, String schoolID, int homeworkID, String token) {
        mNetworkService.getAPI().deleteFacultyHomework(schoolID, homeworkID, token).enqueue(new Callback<DeleteHomeworkResponse>() {
            @Override
            public void onResponse(Call<DeleteHomeworkResponse> call, Response<DeleteHomeworkResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.faculty_delete_homework);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.faculty_delete_homework);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<DeleteHomeworkResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.faculty_delete_homework);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * @param mOnFinishedListener
     * @param schoolID
     * @param facultyID
     * @param token
     */
    public void getFacultySubjects(OnFinishedListener mOnFinishedListener, int facultyID, String schoolID, String token) {
        mNetworkService.getAPI().getFacultySubjects(facultyID, schoolID, token).enqueue(new Callback<SubjectListResponse>() {
            @Override
            public void onResponse(Call<SubjectListResponse> call, Response<SubjectListResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.faculty_subject_list);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.faculty_subject_list);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SubjectListResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.faculty_subject_list);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     * Method to logout user as faculty
     *
     * @param mOnFinishedListener
     * @param token
     */
    public void logoutFaculty(OnFinishedListener mOnFinishedListener, String token) {
        mNetworkService.getAPI().facultyLogout(token).enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.faculty_logout);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.faculty_logout);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.faculty_logout);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }



    /**
     * Method to logout user as faculty
     *
     * @param mOnFinishedListener
     * @param token
     */
    public void logoutStudent(OnFinishedListener mOnFinishedListener, String token) {
        mNetworkService.getAPI().studentLogout(token).enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.students_logout);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.students_logout);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.faculty_logout);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     *
     * @param mOnFinishedListener
     * @param classID
     * @param schoolID
     * @param token
     */
    public void getExamList(OnFinishedListener mOnFinishedListener, int classID, String schoolID, String token) {
        mNetworkService.getAPI().getExamList(schoolID, classID, token).enqueue(new Callback<ExamListResponse>() {
            @Override
            public void onResponse(Call<ExamListResponse> call, Response<ExamListResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), exam_list);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.exam_list);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ExamListResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), exam_list);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     *
     * @param mOnFinishedListener
     * @param classID
     * @param schoolID
     * @param token
     */
    public void facultyExamList(OnFinishedListener mOnFinishedListener, int classID, int facultyId,  String schoolID, String token) {
        mNetworkService.getAPI().facultyExamList(schoolID, facultyId, classID, token).enqueue(new Callback<ExamListResponse>() {
            @Override
            public void onResponse(Call<ExamListResponse> call, Response<ExamListResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), exam_list);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.exam_list);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ExamListResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), exam_list);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     * Method to fetch class time table
     *
     * @param mOnFinishedListener
     * @param activeClassId
     * @param schoolId
     * @param facultyID
     * @param token
     */
	    public void getClassTimeTable(OnFinishedListener mOnFinishedListener, int activeClassId, String schoolId, int facultyID, String token) {
        mNetworkService.getAPI().getClassTimeTable(schoolId,String.valueOf(facultyID), String.valueOf(activeClassId), token)
                .enqueue(new Callback<ClassTimeTableResp>() {
            @Override
            public void onResponse(Call<ClassTimeTableResp> call, Response<ClassTimeTableResp> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), faculy_get_class_time_table);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(), response.message()), EndpointEnums.faculy_get_class_time_table);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ClassTimeTableResp> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), faculy_get_class_time_table);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void getAllClassTimeTable(OnFinishedListener mOnFinishedListener, String schoolId, int facultyID, String token) {
        mNetworkService.getAPI().getAllClassTimeTableList(schoolId,String.valueOf(facultyID), token)
                .enqueue(new Callback<AllClassTimeTableResp>() {
                    @Override
                    public void onResponse(Call<AllClassTimeTableResp> call, Response<AllClassTimeTableResp> response) {
                        if (response.isSuccessful() && response.code() == 200) {
                            mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), faculy_get_class_time_table);
                        } else {
                            try {
                                mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(), response.message()), EndpointEnums.faculy_get_class_time_table);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AllClassTimeTableResp> call, Throwable t) {
                        try {
                            mOnFinishedListener.onEndpointFailure(t.getMessage(), faculy_get_class_time_table);
                            throw new InterruptedException("Something went wrong!");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }



    /**
     * Method to add marks of the student by the faculty
     *
     * @param mOnFinishedListener
     * @param mRequest
     * @param token
     */
    public void addMarks(OnFinishedListener mOnFinishedListener, AddMarksRequest mRequest, String token) {
        mNetworkService.getAPI().addMarks(mRequest, token).enqueue(new Callback<AddMarksResponse>() {
            @Override
            public void onResponse(Call<AddMarksResponse> call, Response<AddMarksResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && (response.code() == 200 || response.code() == 201)) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), faculty_save_marks);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(), response.message()), EndpointEnums.faculty_save_marks);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<AddMarksResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), faculty_save_marks);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     * Method to update marks of the student by the faculty
     *
     * @param mOnFinishedListener
     * @param mRequest
     * @param token
     */
    public void updateMarks(OnFinishedListener mOnFinishedListener, UpdateMarksRequest mRequest, String token) {
        mNetworkService.getAPI().updateMarks(mRequest, token).enqueue(new Callback<UpdateMarksResponse>() {
            @Override
            public void onResponse(Call<UpdateMarksResponse> call, Response<UpdateMarksResponse> response) {
                if (response.isSuccessful() && (response.code() == 200)) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.faculty_update_marks);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(), response.message()), EndpointEnums.faculty_update_marks);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateMarksResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.faculty_update_marks);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private String getErrorMessage(ResponseBody errorBody, String message) throws JSONException {
        String errorMessage = null;
        try {
            errorMessage = Objects.isNull(errorBody) ? message : errorBody.string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject json= (JSONObject) new JSONTokener(errorMessage).nextValue();
        return (String) json.get("message");
    }


    /**
     *
     * @param mOnFinishedListener
     * @param schoolID
     * @param studentId
     * @param exam_name
     * @param token
     */
    public void getMarksList2(OnFinishedListener mOnFinishedListener, String schoolID, int studentId, String exam_name, String token) {
        mNetworkService.getAPI().getMarksList2(schoolID, studentId, exam_name, token).enqueue(new Callback<MarksListResponse>() {
            @Override
            public void onResponse(Call<MarksListResponse> call, Response<MarksListResponse> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.marks_list);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.marks_list);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<MarksListResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.marks_list);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     *
     * @param mOnFinishedListener
     * @param schoolID
     * @param classListId
     * @param examId
     * @param facultyId
     * @param token
     */
    public void getFacultyMarksList(OnFinishedListener mOnFinishedListener, String schoolID, int classListId,int examId, int facultyId, String token) {
        mNetworkService.getAPI().getMarksList(schoolID, classListId, examId, facultyId, token).enqueue(new Callback<MarksListResponse>() {
            @Override
            public void onResponse(Call<MarksListResponse> call, Response<MarksListResponse> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.marks_list);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.marks_list);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<MarksListResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.marks_list);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     * Method to get class attendance
     *
     * @param mOnFinishedListener
     * @param schoolID
     * @param classListID
     * @param facultyID
     * @param selectedDate
     * @param token
     */
    public void getClassAttendanceList(OnFinishedListener mOnFinishedListener, String schoolID, int classListID, int facultyID, Date selectedDate, String token, String timeTableID) {
        mNetworkService.getAPI().getClassAttendance(schoolID, classListID, timeTableID, facultyID, selectedDate, token).enqueue(new Callback<ClassAttendanceResponse>() {
            @Override
            public void onResponse(Call<ClassAttendanceResponse> call, Response<ClassAttendanceResponse> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), faculty_class_attendance);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.faculty_class_attendance);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ClassAttendanceResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), faculty_class_attendance);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     * Method to update attendance of the student by the faculty
     *
     * @param mOnFinishedListener
     * @param mRequest
     * @param token
     */
    public void updateMultipleAttendance(OnFinishedListener mOnFinishedListener, RequestBody mRequest, String token) {
        mNetworkService.getAPI().updateClassMultipleAttendance(mRequest, token).enqueue(new Callback<UpdateMultipleAttendanceResponse>() {
            @Override
            public void onResponse(Call<UpdateMultipleAttendanceResponse> call, Response<UpdateMultipleAttendanceResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && (response.code() == 200)) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), faculty_update_multiple_attendance);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.faculty_update_multiple_attendance);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateMultipleAttendanceResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), faculty_update_multiple_attendance);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to fetch parent details
     *
     * @param mOnFinishedListener
     * @param schoolID
     * @param parentID
     * @param token
     */
    public void getParentDetails(OnFinishedListener mOnFinishedListener, String schoolID, int parentID, String token) {
        mNetworkService.getAPI().getParentDetails(schoolID, parentID, token).enqueue(new Callback<ParentDetailResponse>() {
            @Override
            public void onResponse(Call<ParentDetailResponse> call, Response<ParentDetailResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), parent_details);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.parent_details);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ParentDetailResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), parent_details);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to fetch parent details
     *
     * @param mOnFinishedListener
     * @param schoolID
     * @param parentID
     * @param token
     */
    public void getStudentDetails(OnFinishedListener mOnFinishedListener, String schoolID, int parentID, String token) {
        mNetworkService.getAPI().getStudentDetails(schoolID, parentID, token).enqueue(new Callback<StudentDetailsResponse>() {
            @Override
            public void onResponse(Call<StudentDetailsResponse> call, Response<StudentDetailsResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), student_details);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.student_details);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<StudentDetailsResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), student_details);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to fetch parent homework details
     *
     * @param mOnFinishedListener
     * @param schoolID
     * @param studentID
     * @param date
     * @param token
     */
    public void getParentHomeworkDetails(OnFinishedListener mOnFinishedListener, String schoolID, long studentID, Date date, String token) {
        mNetworkService.getAPI().getParentHomeworkDetails(schoolID, (int) studentID, date, token).enqueue(new Callback<ParentHomeworkResponse>() {
            @Override
            public void onResponse(Call<ParentHomeworkResponse> call, Response<ParentHomeworkResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(),
                            EndpointEnums.parent_homework_details);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.parent_homework_details);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ParentHomeworkResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.parent_homework_details);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void parentDiaryReply(OnFinishedListener mOnFinishedListener, String schoolId, int studentID, int diaryId, String token, int facultyId, Date date, String parentReply) {
        mNetworkService.getAPI().parentDiaryReplyRequest(new ReplyDiaryRequest(schoolId, studentID, facultyId, diaryId, date, parentReply), token).enqueue(new Callback<ParentDiaryReplyResponse>() {
            @Override
            public void onResponse(Call<ParentDiaryReplyResponse> call, Response<ParentDiaryReplyResponse> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), parent_diary_reply);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.parent_diary_reply);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ParentDiaryReplyResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.parent_homework_details);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to fetch parent diary details
     *
     * @param mOnFinishedListener
     * @param schoolID
     * @param studentID
     * @param date
     * @param token
     */
    public void getParentDiaryDetails(OnFinishedListener mOnFinishedListener, String schoolID, long studentID, Date date, String token) {
        mNetworkService.getAPI().getParentDiaryDetails(schoolID, (int) studentID, date, token).enqueue(new Callback<ParentDiaryResponse>() {
            @Override
            public void onResponse(Call<ParentDiaryResponse> call, Response<ParentDiaryResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), parent_diary_details);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.parent_diary_details);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ParentDiaryResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), parent_diary_details);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     *
     * @param mOnFinishedListener
     * @param schoolID
     * @param studentID
     * @param examName
     * @param token
     */
    public void getParentReportStatusDetails(OnFinishedListener mOnFinishedListener, String schoolID, int studentID, String examName, String token) {
        mNetworkService.getAPI().getParentReportStatus(schoolID, studentID, examName, token).enqueue(new Callback<ParentReportCardDetailResponse>() {
            @Override
            public void onResponse(Call<ParentReportCardDetailResponse> call, Response<ParentReportCardDetailResponse> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), parent_report_card_status);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.parent_report_card_status);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ParentReportCardDetailResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), parent_report_card_status);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to fetch parent timetable status
     *
     * @param mOnFinishedListener
     * @param schoolID
     * @param classID
     * @param day
     * @param token
     */
    public void getParentTimeTableDetails(OnFinishedListener mOnFinishedListener, String schoolID, String classID, String day, String token) {
        mNetworkService.getAPI().getParentTimeTableDetails(schoolID, classID, day, token).enqueue(new Callback<ParentTimeTableResponse>() {
            @Override
            public void onResponse(Call<ParentTimeTableResponse> call, Response<ParentTimeTableResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.parent_time_table_details);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.parent_time_table_details);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ParentTimeTableResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.parent_time_table_details);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Method to fetch attendance details between dates
     *
     * @param mOnFinishedListener
     * @param schoolID
     * @param studentID
     * @param startDate
     * @param endDate
     * @param token
     */
    public void getParentAttendanceDetailsBtnDates(OnFinishedListener mOnFinishedListener, String schoolID, int studentID, Date startDate, Date endDate, String token) {
        mNetworkService.getAPI().getParentAttendanceDetailsBtnDates(schoolID, studentID, startDate, endDate, token).enqueue(new Callback<AttendanceDateObject>() {
            @Override
            public void onResponse(Call<AttendanceDateObject> call, Response<AttendanceDateObject> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.parent_attendance_details);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.parent_attendance_details);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<AttendanceDateObject> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.parent_attendance_details);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void getAttendanceAllPeriods(OnFinishedListener mOnFinishedListener,
                                        String school_id, int class_list_id, String day, int student_id, Date date, String token) {
       mNetworkService.getAPI().getAttendanceAllPeriods(school_id, class_list_id, day, student_id, date, token).enqueue(new Callback<TimetableList>() {
           @Override
           public void onResponse(Call<TimetableList> call, Response<TimetableList> response) {
               mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.attendance_all_periods);
           }

           @Override
           public void onFailure(Call<TimetableList> call, Throwable t) {
               try {
                   mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.attendance_all_periods);
                   throw new InterruptedException("Something went wrong!");
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
           }
       });
    }

    /**
     * Method to change password for the user as parent
     *
     * @param mOnFinishedListener
     * @param mRequest
     * @param token
     */
    public void changeParentPassword(OnFinishedListener mOnFinishedListener, ChangePasswordRequest mRequest, String token) {
        mNetworkService.getAPI().changeParentPassword(mRequest, token).enqueue(new Callback<ChangePasswordResponse>() {
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 201) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.parent_change_password);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.parent_change_password);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.parent_change_password);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     * Method to logout user as faculty
     *
     * @param mOnFinishedListener
     * @param token
     */
    public void logoutParent(OnFinishedListener mOnFinishedListener, String token) {
        mNetworkService.getAPI().parentLogout(token).enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.parent_logout);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.parent_logout);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.parent_logout);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     *
     * @param mOnFinishedListener
     * @param schoolID
     * @param token
     */
    public void getSurveyList(OnFinishedListener mOnFinishedListener, String schoolID, String token) {
        mNetworkService.getAPI().getSurveyList(schoolID, token).enqueue(new Callback<SurveyListResponse>() {
            @Override
            public void onResponse(Call<SurveyListResponse> call, Response<SurveyListResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.survey_list);
                } else {
                    try {
                        mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                response.message()), EndpointEnums.survey_list);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SurveyListResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.survey_list);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     *
     * @param mOnFinishedListener
     * @param schoolID
     * @param token
     */
    public void getQuestionList(OnFinishedListener mOnFinishedListener, String schoolID, String surveyId, int parentId, String token) {
        mNetworkService.getAPI().getQuestionList(schoolID, surveyId, String.valueOf(parentId),  token).enqueue(new Callback<QuestionListResponse>() {
            @Override
            public void onResponse(Call<QuestionListResponse> call, Response<QuestionListResponse> response) {
                Log.d("NETWORK", response.message());
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.question_list);
                } else {
                    if(response.code() == 500) {
                        try {
                            mOnFinishedListener.onEndpointFailure("Something went wrong!", EndpointEnums.question_list);
                            throw new InterruptedException("Something went wrong!");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                    response.message()), EndpointEnums.question_list);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<QuestionListResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.question_list);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    /**
     *
     * @param mOnFinishedListener
     * @param mRequest
     * @param token
     */
    public void updateSurveyObject(OnFinishedListener mOnFinishedListener, SurveyRequest mRequest, String token) {
        mNetworkService.getAPI().updateSurvey(mRequest, token).enqueue(new Callback<UpdateSurveyResponse>() {
            @Override
            public void onResponse(Call<UpdateSurveyResponse> call, Response<UpdateSurveyResponse> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mOnFinishedListener.onEndpointSuccess(response.body(), response.message(), EndpointEnums.update_survey);
                } else {
                    if(response.code() != 500) {
                        try {
                            mOnFinishedListener.onEndpointFailure(getErrorMessage(response.errorBody(),
                                    response.message()), EndpointEnums.update_survey);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        mOnFinishedListener.onEndpointFailure("Something went wrong!", EndpointEnums.update_survey);
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateSurveyResponse> call, Throwable t) {
                try {
                    mOnFinishedListener.onEndpointFailure(t.getMessage(), EndpointEnums.update_survey);
                    throw new InterruptedException("Something went wrong!");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
    }


    public interface OnFinishedListener {
        void onEndpointSuccess(Object object, String message, EndpointEnums endpointType);

        void onEndpointFailure(String message, EndpointEnums endpointType);
    }

}
