package com.scool.qt.network;

import android.content.Context;

import com.scool.qt.EndpointEnums;
import com.scool.qt.model.request.AddDelegationRequest;
import com.scool.qt.model.request.AddDiaryRequest;
import com.scool.qt.model.request.AddHomeworkRequest;
import com.scool.qt.model.request.AddMarksRequest;
import com.scool.qt.model.request.ChangePasswordRequest;
import com.scool.qt.model.request.LoginUserRequest;
import com.scool.qt.model.request.SurveyRequest;
import com.scool.qt.model.request.UpdateAttendanceRequest;
import com.scool.qt.model.request.UpdateDiaryRequest;
import com.scool.qt.model.request.UpdateHomeworkRequest;
import com.scool.qt.model.request.UpdateMarksRequest;
import com.scool.qt.utils.Utils;

import java.util.Date;

import okhttp3.RequestBody;

public class NetworkPresenter implements NetworkInteractor.OnFinishedListener {
    public static final String TAG = NetworkPresenter.class.getSimpleName();
    private NetworkView mNetworkView;
    private NetworkInteractor mNetworkInteractor;

    /**
     * @param mNetworkView      the network view object
     * @param networkInteractor the network Interactor Object
     */
    public NetworkPresenter(NetworkView mNetworkView, NetworkInteractor networkInteractor) {
        this.mNetworkView = mNetworkView;
        this.mNetworkInteractor = networkInteractor;
    }

    /**
     * @param mContext
     * @param schoolUniqueID
     */
    public void methodToGetAboutUsSchool(Context mContext, String schoolUniqueID) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getSchoolAboutUsData(this, schoolUniqueID);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * @param mContext
     * @param schoolUniqueID
     */
    public void methodToGetSchoolHomeData(Context mContext, String schoolUniqueID) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getSchoolHomeScreenData(this, schoolUniqueID);
        } else
            mNetworkView.noInternetConnection();
    }

    public void methodToGetSchoolFacultiesData(Context mContext, String schoolUniqueID) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getSchoolFaultyData(this, schoolUniqueID);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * @param mContext
     * @param schoolUniqueID
     */
    public void methodToGetSchoolCircularData(Context mContext, String schoolUniqueID) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getSchoolCircularList(this, schoolUniqueID);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * @param mContext
     * @param schoolUniqueID
     */
    public void methodToGetContactUsData(Context mContext, String schoolUniqueID) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getSchoolContactUsData(this, schoolUniqueID);
        } else
            mNetworkView.noInternetConnection();
    }

    public void methodToLoginAsParent(Context mContext, LoginUserRequest loginUserRequest) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.loginAsParent(this, loginUserRequest, mContext);
        } else
            mNetworkView.noInternetConnection();
    }

    public void methodToLoginAsFaculty(Context mContext, LoginUserRequest loginUserRequest) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.loginAsFaculty(this, loginUserRequest, mContext);
        } else
            mNetworkView.noInternetConnection();
    }

    public void methodToLoginAsStudent(Context mContext, LoginUserRequest loginUserRequest) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.loginAsStudent(this, loginUserRequest, mContext);
        } else
            mNetworkView.noInternetConnection();
    }

    public void methodToFetchAllTimeTable(Context mContext, String schoolId,
                                       int facultyID, String token){
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkInteractor.getAllClassTimeTable(this, schoolId, facultyID, token);
        } else
            mNetworkView.noInternetConnection();
    }

    public void methodToFetchTimeTable(Context mContext,
                                       int activeClassId, String schoolId,
                                       int facultyID, String token){
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getClassTimeTable(this,activeClassId, schoolId, facultyID, token);
        } else{
            mNetworkView.hideProgress();
            mNetworkView.noInternetConnection();
        }
    }

    /**
     * Method to fetch faculty information
     *  @param mContext
     * @param schoolID
     * @param facultyID
     * @param token
     */
    public void methodToFetchFacultyBasicInfo(Context mContext, String schoolID, int facultyID, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.fetchFacultyBasicInfo(this, schoolID, facultyID, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to fetch faculty class list
     *
     * @param mContext
     * @param schoolID
     * @param facultyID
     * @param token
     */
    public void methodToFetchFacultyClassList(Context mContext, String schoolID, int facultyID, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.fetchFacultyClassList(this, schoolID, facultyID, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to fetch faculty class list
     *
     * @param mContext
     * @param schoolID
     * @param facultyID
     * @param token
     */
    public void methodToFetchDelegationClassList(Context mContext, String schoolID, int facultyID, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.delegationClassList(this, schoolID, facultyID, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to change password for faculty
     *
     * @param mContext
     * @param mRequest
     * @param token
     */
    public void methodToChangePasswordForFaculty(Context mContext, ChangePasswordRequest mRequest, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.changeFacultyPassword(this, mRequest, token);
        } else
            mNetworkView.noInternetConnection();
    }


    /**
     * Method to change password for faculty
     *
     * @param mContext
     * @param mRequest
     * @param token
     */
    public void methodToAddDelegation(Context mContext, AddDelegationRequest mRequest, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.addDelegationRequest(this, mRequest, token);
        } else
            mNetworkView.noInternetConnection();
    }


    /***
     *
     * @param mContext
     * @param mRequest
     * @param token
     */
    public void methodToUpdateDelegationRequest(Context mContext, AddDelegationRequest mRequest, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.updateDelegationRequest(this, mRequest, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to fetch student list for the faculty
     *
     * @param mContext
     * @param schoolID
     * @param facultyID
     * @param studentListID
     * @param token
     */
    public void methodToFetchFacultyStudentList(Context mContext, String schoolID, int facultyID, int studentListID, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.fetchFacultyStudentList(this, schoolID, facultyID, studentListID, token);
        } else
            mNetworkView.noInternetConnection();
    }


    /**
     * Method to fetch the list of diaries for the selected students
     *
     * @param mContext
     * @param schoolID
     * @param facultyID
     * @param classListID
     * @param studentID
     * @param token
     */
    public void methodToFetchFacultyDiaryList(Context mContext, String schoolID, int facultyID, int classListID, int studentID, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.fetchFacultyDiaryList(this, schoolID, facultyID, classListID, studentID, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to add diary by faculty
     *
     * @param mContext
     * @param mRequest
     * @param token
     */
    public void methodToAddDiaryByFaculty(Context mContext, AddDiaryRequest mRequest, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.addFacultyDiary(this, mRequest, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to delete diary by faculty
     *
     * @param mContext
     * @param schoolID
     * @param diaryID
     * @param token
     */
    public void methodToDeleteDiaryByFaculty(Context mContext, String schoolID, int diaryID, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.deleteFacultyDiary(this, schoolID, diaryID, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Mrthod to update diary
     *
     * @param mContext
     * @param mRequest
     * @param token
     */
    public void methodToUpdateDiaryByFaculty(Context mContext, UpdateDiaryRequest mRequest, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.updateDiaryRequest(this, mRequest, token);
        } else
            mNetworkView.noInternetConnection();
    }


    /****
     * Method to update attendance
     * @param mContext
     * @param mRequest
     * @param token
     */
    public void methodToUpdateAttendance(Context mContext, UpdateAttendanceRequest mRequest, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.updateFacultyAttendanceRequest(this, mRequest, token);
        } else
            mNetworkView.noInternetConnection();
    }


    /**
     * Method to fetch the list of homework for the selected students
     *
     * @param mContext
     * @param schoolID
     * @param facultyID
     * @param classListID
     * @param studentID
     * @param token
     */
    public void methodToFetchFacultyHomeworkList(Context mContext, String schoolID, int facultyID, int classListID, int studentID, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.fetchFacultyHomeWorkList(this, schoolID, facultyID, classListID, studentID, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to add homework by faculty
     *
     * @param mContext
     * @param mRequest
     * @param token
     */
    public void methodToAddHomeworkByFaculty(Context mContext, AddHomeworkRequest mRequest, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.addFacultyHomework(this, mRequest, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Mrthod to update homework
     *
     * @param mContext
     * @param mRequest
     * @param token
     */
    public void methodToUpdateHomeworkByFaculty(Context mContext, UpdateHomeworkRequest mRequest, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.updateFacultyHomework(this, mRequest, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to delete homework by faculty
     *
     * @param mContext
     * @param schoolID
     * @param homeworkID
     * @param token
     */
    public void methodToDeleteHomeworkByFaculty(Context mContext, String schoolID, int homeworkID, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.deleteFacultyHomework(this, schoolID, homeworkID, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to get subject list
     *
     * @param mContext
     * @param schoolID
     * @param facultyID
     * @param token
     */
    public void methodToGetSubjectListByFaculty(Context mContext, int facultyID, String schoolID, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getFacultySubjects(this, facultyID, schoolID, token);
        } else
            mNetworkView.noInternetConnection();
    }


    /**
     * Method to logout as faculty
     *
     * @param mContext
     * @param token
     */
    public void methodToLogoutAsFaculty(Context mContext, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.logoutFaculty(this, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to logout as faculty
     *
     * @param mContext
     * @param token
     */
    public void methodToLogoutAsStudent(Context mContext, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.logoutStudent(this, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to get exam list
     *  @param mContext
     * @param classID
     * @param schoolID
     * @param token
     * @param
     */
    public void methodToFetchExamlist(Context mContext, int classID, String schoolID, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getExamList(this, classID, schoolID, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to get exam list
     *  @param mContext
     * @param classID
     * @param schoolID
     * @param token
     * @param
     */
    public void methodToFetchFacultyExamList(Context mContext, int classID, int facultyID, String schoolID, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.facultyExamList(this, classID,facultyID, schoolID, token);
        } else
            mNetworkView.noInternetConnection();
    }


    /**
     *
     * @param mContext
     * @param schoolID
     * @param studentId
     * @param examName
     * @param token
     */
    public void methodToFetchMarksListForParent(Context mContext, String schoolID, int studentId, String examName, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getMarksList2(this, schoolID, studentId, examName, token);
        } else
            mNetworkView.noInternetConnection();
    }


    /**
     *
     * @param mContext
     * @param schoolID
     * @param classListID
     * @param examId
     * @param facultyId
     * @param token
     */
    public void methodToFetchMarksListForFaculty(Context mContext, String schoolID, int classListID, int examId, int facultyId, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getFacultyMarksList(this, schoolID, classListID, examId, facultyId, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to add marks by the faculty
     *
     * @param mContext
     * @param mRequest
     * @param token
     */
    public void methodToAddMarksByFaculty(Context mContext, AddMarksRequest mRequest, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.addMarks(this, mRequest, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to update marks by the faculty
     *
     * @param mContext
     * @param mRequest
     * @param token
     */
    public void methodToUpdateMarksByFaculty(Context mContext, UpdateMarksRequest mRequest, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.updateMarks(this, mRequest, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to get marks list
     *
     * @param mContext
     * @param schoolID
     * @param classID
     * @param token
     */
    public void methodToFetchClassAttendanceByFaculty(Context mContext, String schoolID, int classID,
                                                      int facultyID, Date selectedDate, String token, String timeTableID) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getClassAttendanceList(this, schoolID, classID, facultyID, selectedDate, token, timeTableID);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to update multiple attendance by the faculty
     *
     * @param mContext
     * @param mRequest
     * @param token
     */
    public void methodToUpdateMultipleAttendanceByFaculty(Context mContext, RequestBody mRequest, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.updateMultipleAttendance(this, mRequest, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to fetch parent details
     *
     * @param mContext
     * @param schoolID
     * @param parentID
     * @param token
     */
    public void methodToFetchParentDetails(Context mContext, String schoolID, int parentID, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getParentDetails(this, schoolID, parentID, token);
        } else
            mNetworkView.noInternetConnection();
    }


    /**
     * Method to fetch parent details
     *
     * @param mContext
     * @param schoolID
     * @param
     * @param token
     */
    public void methodToFetchStudentDetails(Context mContext, String schoolID, int studentId, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getStudentDetails(this, schoolID, studentId, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to fetch parent homework details
     *
     * @param mContext
     * @param schoolID
     * @param studentID
     * @param homeworkDate
     * @param token
     */
    public void methodToFetchParentHomeworkDetails(Context mContext, String schoolID, long studentID, Date homeworkDate, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getParentHomeworkDetails(this, schoolID, studentID, homeworkDate, token);
        } else
            mNetworkView.noInternetConnection();
    }

    public void parentDiaryReplyRequest(Context mContext, String schoolID, int studentID, String homeworkDate,
                                        String token, int facultyID, int diaryId, String parentReply) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.parentDiaryReply(this, schoolID, studentID,
                    diaryId,token, facultyID, Utils.convertStringToDate(homeworkDate), parentReply);
        } else
            mNetworkView.noInternetConnection();
    }




    /**
     * Method to fetch parent diary details
     *
     * @param mContext
     * @param schoolID
     * @param studentID
     * @param homeworkDate
     * @param token
     */
    public void methodToFetchParentDiaryDetails(Context mContext, String schoolID, long studentID, Date homeworkDate, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getParentDiaryDetails(this, schoolID, studentID, homeworkDate, token);
        } else
            mNetworkView.noInternetConnection();
    }


    /**
     *
     * @param mContext
     * @param schoolID
     * @param studentID
     * @param examName
     * @param token
     */
    public void methodToFetchParentReportStatusDetails(Context mContext, String schoolID, int studentID, String examName, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getParentReportStatusDetails(this, schoolID, studentID, examName, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to fetch parent time table details
     *  @param mContext
     * @param schoolID
     * @param classID
     * @param day
     * @param token
     */
    public void methodToFetchParentTimeTableDetails(Context mContext, String schoolID, String classID, String day, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getParentTimeTableDetails(this, schoolID, classID, day, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to fetch attendance details between dates
     *
     * @param mContext
     * @param schoolID
     * @param studentID
     * @param startDate
     * @param endDate
     * @param token
     */
    public void methodToFetchAttendanceDetailsBetweenDates(Context mContext, String schoolID, int studentID, Date startDate, Date endDate, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getParentAttendanceDetailsBtnDates(this, schoolID, studentID, startDate, endDate, token);
        } else
            mNetworkView.noInternetConnection();
    }

    public void methodToGetAttendanceAllPeriods(Context mContext, String schoolID, int class_list_id, String day, int student_id, Date endDate, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getAttendanceAllPeriods(this, schoolID, class_list_id, day, student_id, endDate, token);
        } else
            mNetworkView.noInternetConnection();
    }

    /**
     * Method to change password for parent
     *
     * @param mContext
     * @param mRequest
     * @param token
     */
    public void methodToChangePasswordForParent(Context mContext, ChangePasswordRequest mRequest, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.changeParentPassword(this, mRequest, token);
        } else
            mNetworkView.noInternetConnection();
    }


    /**
     * Method to logout as parent
     *
     * @param mContext
     * @param token
     */
    public void methodToLogoutAsParent(Context mContext, String token) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.logoutParent(this, token);
        } else
            mNetworkView.noInternetConnection();
    }


    /**
     *
     * @param mContext
     * @param token
     * @param schoolId
     */
    public void methodToGetSurveyList(Context mContext, String token, String schoolId) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getSurveyList(this, schoolId, token);
        } else{
            mNetworkView.noInternetConnection();
        }

    }

    /**
     *
     * @param mContext
     * @param token
     * @param schoolId
     */
    public void methodToGetQuestionList(Context mContext, String token, String schoolId, String surveyId, int parentId) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.getQuestionList(this, schoolId, surveyId, parentId, token);
        } else
            mNetworkView.noInternetConnection();
    }


    /**
     *
     * @param mContext
     * @param token
     * @param mSurveyRequest
     */
    public void methodToUpdateSurvey(Context mContext, String token, SurveyRequest mSurveyRequest) {
        if (Utils.isNetworkAvailable(mContext)) {
            mNetworkView.showProgress();
            mNetworkInteractor.updateSurveyObject(this, mSurveyRequest, token);
        } else
            mNetworkView.noInternetConnection();
    }


    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        mNetworkView.hideProgress();
        mNetworkView.onEndpointSuccess(object, message, endpointType);
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        mNetworkView.hideProgress();
        mNetworkView.onEndpointFailure(message, endpointType);
    }
}
