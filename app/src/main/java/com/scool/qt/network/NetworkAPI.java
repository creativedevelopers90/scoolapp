package com.scool.qt.network;

import com.scool.qt.model.AboutUsResp;
import com.scool.qt.model.AddDiaryResponse;
import com.scool.qt.model.AddHomeworkResponse;
import com.scool.qt.model.AddMarksResponse;
import com.scool.qt.model.AllClassTimeTableResp;
import com.scool.qt.model.ChangePasswordResponse;
import com.scool.qt.model.ClassAttendanceResponse;
import com.scool.qt.model.ClassListFrDelegation;
import com.scool.qt.model.ClassTimeTableResp;
import com.scool.qt.model.ContactUsResp;
import com.scool.qt.model.DeleteDiaryResponse;
import com.scool.qt.model.DeleteHomeworkResponse;
import com.scool.qt.model.ExamListResponse;
import com.scool.qt.model.FacultyClassResponse;
import com.scool.qt.model.FacultyDiaryListResponse;
import com.scool.qt.model.FacultyHomeWorkListResponse;
import com.scool.qt.model.FacultyInfoResponse;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.FacultyStudentListResponse;
import com.scool.qt.model.LogoutResponse;
import com.scool.qt.model.MarksListResponse;
import com.scool.qt.model.ParentDiaryReplyResponse;
import com.scool.qt.model.ParentLoginResponse;
import com.scool.qt.model.QuestionListResponse;
import com.scool.qt.model.SchoolCircularResponse;
import com.scool.qt.model.SchoolFacultiesResp;
import com.scool.qt.model.StartResponse;
import com.scool.qt.model.StudentDetailsResponse;
import com.scool.qt.model.StudentLoginResponse;
import com.scool.qt.model.SubjectListResponse;
import com.scool.qt.model.SurveyListResponse;
import com.scool.qt.model.TimetableList;
import com.scool.qt.model.UpdateAttendanceResponse;
import com.scool.qt.model.UpdateDiaryResponse;
import com.scool.qt.model.UpdateHomeworkResponse;
import com.scool.qt.model.UpdateMarksResponse;
import com.scool.qt.model.UpdateMultipleAttendanceResponse;
import com.scool.qt.model.UpdateSurveyResponse;
import com.scool.qt.model.parent.AttendanceDateObject;
import com.scool.qt.model.parent.ParentDetailResponse;
import com.scool.qt.model.parent.ParentDiaryResponse;
import com.scool.qt.model.parent.ParentHomeworkResponse;
import com.scool.qt.model.parent.ParentReportCardDetailResponse;
import com.scool.qt.model.parent.ParentTimeTableResponse;
import com.scool.qt.model.parent.request.ReplyDiaryRequest;
import com.scool.qt.model.request.AddDelegationRequest;
import com.scool.qt.model.request.AddDiaryRequest;
import com.scool.qt.model.request.AddHomeworkRequest;
import com.scool.qt.model.request.AddMarksRequest;
import com.scool.qt.model.request.ChangePasswordRequest;
import com.scool.qt.model.request.LoginUserRequest;
import com.scool.qt.model.request.SurveyRequest;
import com.scool.qt.model.request.UpdateAttendanceRequest;
import com.scool.qt.model.request.UpdateDiaryRequest;
import com.scool.qt.model.request.UpdateHomeworkRequest;
import com.scool.qt.model.request.UpdateMarksRequest;

import java.util.Date;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface NetworkAPI {
    /**
     * @param school_id
     * @return Method to Get About us data through school unique ID
     */
    @GET("schools/about_us")
    Call<AboutUsResp> getSchoolAboutUsData(@Query("school_id") String school_id);

    /**
     * @param school_id
     * @return Method to Get School Home Page Data
     */
    @GET("schools/start")
    Call<StartResponse> getSchoolHomePageData(@Query("school_id") String school_id);

    /**
     * @param school_id
     * @return Method to Get School Home Page Data
     */
    @GET("schools/faculties")
    Call<SchoolFacultiesResp> getSchoolFacultiesList(@Query("school_id") String school_id);

    /**
     * @param school_id
     * @return Method to Get School Home Page Data
     */
    @GET("schools/circulars")
    Call<SchoolCircularResponse> getSchoolCircularlist(@Query("school_id") String school_id);

    /**
     * @param school_id
     * @return Method to Get School Home Page Data
     */
    @GET("schools/contact_us")
    Call<ContactUsResp> getSchoolContactUsData(@Query("school_id") String school_id);

    /**
     * Parent ENDPOINTS
     */
    @POST("parents/login")
    Call<ParentLoginResponse> loginAsParent(@Body LoginUserRequest loginUser);

    /**
     * Faculty ENDPOINTS
     */
    @POST("faculties/login")
    Call<FacultyLoginResponse> loginAsFaculty(@Body LoginUserRequest loginUser);

    @PUT("faculties/update_attendance")
    Call<UpdateAttendanceResponse> updateAttendance(@Body UpdateAttendanceRequest updateAttendanceRequest, @Header("Authorization") String token);

    /**
     * Faculty ENDPOINTS
     */
    @POST("students/login")
    Call<StudentLoginResponse> loginAsStudent(@Body LoginUserRequest loginUser);

    /**
     * Method to logout parent
     */
    @DELETE("students/logout")
    Call<LogoutResponse> studentLogout(@Header("Authorization") String token);


    /**
     * Faculty ENDPOINTS
     */
    @GET("faculties")
    Call<FacultyInfoResponse> fetchFacultyInfo(@Query("school_id") String schoolID, @Query("id") int facultyID,
                                               @Header("Authorization") String token);


    /**
     * Faculty ENDPOINTS
     */
    @GET("faculties/class_lists")
    Call<FacultyClassResponse> fetchFacultyClassList(@Query("school_id") String schoolID, @Query("faculty_id") int facultyID,
                                                     @Header("Authorization") String token);


    /**
     * Faculty ENDPOINTS
     */
    @GET("faculties/class_lists_for_Delegation")
    Call<ClassListFrDelegation> classListForDelegation(@Query("school_id") String schoolID, @Query("faculty_id") int facultyID,
                                                       @Header("Authorization") String token);


    /***
     *
     * @param addDelegationRequest
     * @param token
     * @return
     */
    @POST("faculties/add_delegation")
    Call<ChangePasswordResponse> addDelegation(@Body AddDelegationRequest addDelegationRequest,
                                                       @Header("Authorization") String token);


    /***
     *
     * @param school_id
     * @param faculty_id
     * @param delegate_faculty_id
     * @param class_list_id
     * @param attdelegate
     * @param markdelegate
     * @param diarydelegate
     * @param hwdelegate
     * @param fromdate
     * @param todate
     * @param id
     * @param token
     * @return
     */
    @PUT("faculties/update_delegation")
    Call<ChangePasswordResponse> updateDelegation(@Body AddDelegationRequest addDelegationRequest,
                                                  @Header("Authorization") String token);

    /**
     * Faculty ENDPOINTS
     */
    @POST("faculties/change_password")
    Call<ChangePasswordResponse> changeFacultyPassword(@Body ChangePasswordRequest mChangePasswordRequest,
                                                       @Header("Authorization") String token);


    /**
     * Faculty ENDPOINTS
     */
    @GET("faculties/student_list")
    Call<FacultyStudentListResponse> facultyStudentList(@Query("school_id") String schoolID, @Query("faculty_id") int facultyID,
                                                        @Query("class_list_id") int classListID, @Header("Authorization") String token);


    /**
     * Faculty ENDPOINTS
     */
    @GET("faculties/diary_list")
    Call<FacultyDiaryListResponse> facultyDiaryList(@Query("school_id") String schoolID, @Query("faculty_id") int facultyID,
                                                    @Query("class_list_id") int classListID, @Query("student_id") int studentID,
                                                    @Header("Authorization") String token);


    /**
     * Faculty ENDPOINTS
     */
    @POST("faculties/add_diary")
    Call<AddDiaryResponse> addFacultyDiary(@Body AddDiaryRequest mAddDiaryRequest, @Header("Authorization") String token);

    /**
     * Faculty ENDPOINTS
     */
    @DELETE("faculties/delete_diary")
    Call<DeleteDiaryResponse> deleteFacultyDiary(@Query("school_id") String schoolID, @Query("id") int diaryID,
                                                 @Header("Authorization") String token);

    /**
     * Faculty ENDPOINTS
     */
    @PUT("faculties/update_diary")
    Call<UpdateDiaryResponse> updateFacultyDiary(@Body UpdateDiaryRequest mUpdateDiaryRequest, @Header("Authorization") String token);

    /**
     * Faculty ENDPOINTS
     */
    @GET("faculties/homework_list")
    Call<FacultyHomeWorkListResponse> facultyHomeWorkList(@Query("school_id") String schoolID, @Query("faculty_id") int facultyID,
                                                          @Query("class_list_id") int classListID, @Query("student_id") int studentID,
                                                          @Header("Authorization") String token);


    /**
     * Faculty ENDPOINTS
     */
    @POST("faculties/add_homework")
    Call<AddHomeworkResponse> addFacultyHomework(@Body AddHomeworkRequest mAddHomeworkRequest, @Header("Authorization") String token);

    /**
     * Faculty ENDPOINTS
     */
    @PUT("faculties/update_homework")
    Call<UpdateHomeworkResponse> updateFacultyHomework(@Body UpdateHomeworkRequest mUpdateDiaryRequest, @Header("Authorization") String token);

    /**
     * Faculty ENDPOINTS
     */
    @DELETE("faculties/delete_home_work")
    Call<DeleteHomeworkResponse> deleteFacultyHomework(@Query("school_id") String schoolID, @Query("id") int homeworkID,
                                                       @Header("Authorization") String token);

    /**
     * Faculty ENDPOINTS
     */
    @GET("faculties/subjects")
    Call<SubjectListResponse> getFacultySubjects(@Query("faculty_id") int facultyID, @Query("school_id") String schoolID,
                                                 @Header("Authorization") String token);

    /**
     * Faculty ENDPOINTS
     */
    @DELETE("faculties/logout")
    Call<LogoutResponse> facultyLogout(@Header("Authorization") String token);

    /**
     * Parent ENDPOINTS
     */
    @GET("parents/exam_name_list")
    Call<ExamListResponse> getExamList(@Query("school_id") String schoolID, @Query("class_id") int classID, @Header("Authorization") String token);


    /**
     * Parent ENDPOINTS
     */
    @GET("parents/exam_list")
    Call<ExamListResponse> facultyExamList(@Query("school_id") String schoolID, @Query("faculty_id") int faculty_id, @Query("class_id") int classID, @Header("Authorization") String token);


    /**
     * @param school_id
     * @param faculty_id
     * @param class_list_id
     * @param token
     * @return
     */
    @GET("faculties/time_tables")
    Call<ClassTimeTableResp> getClassTimeTable(@Query("school_id") String school_id,
                                               @Query("faculty_id") String faculty_id,
                                               @Query("class_list_id") String class_list_id,
                                               @Header("Authorization") String token);

    /**
     * @param school_id
     * @param faculty_id
     * @param token
     * @return
     */
    @GET("faculties/time_table_list")
    Call<AllClassTimeTableResp> getAllClassTimeTableList(@Query("school_id") String school_id,
                                                         @Query("faculty_id") String faculty_id,
                                                         @Header("Authorization") String token);

    /**
     * Parent ENDPOINTS
     *
     * @param schoolID
     * @param classListID
     * @param examID
     * @param token
     * @return
     */
    @GET("parents/parent_exam_marks")
    Call<MarksListResponse> getMarksList(@Query("school_id") String schoolID,
                                         @Query("class_list_id") int classListID,
                                         @Query("exam_id") int examID,
                                         @Query("faculty_id") int faculty_id,
                                         @Header("Authorization") String token);


    @GET("parents/marks_list2")
    Call<MarksListResponse> getMarksList2(@Query("school_id") String schoolID,
                                         @Query("student_id") int classListID,
                                         @Query("exam_name") String examID,
                                         @Header("Authorization") String token);

    /**
     * Method to add marks by the faulty
     *
     * @param mRequest
     * @param token
     * @return
     */
    @POST("faculties/add_mark")
    Call<AddMarksResponse> addMarks(@Body AddMarksRequest mRequest, @Header("Authorization") String token);

    /**
     * Method to update marks by the faulty
     *
     * @param mRequest
     * @param token
     * @return
     */
    @PUT("faculties/update_mark")
    Call<UpdateMarksResponse> updateMarks(@Body UpdateMarksRequest mRequest, @Header("Authorization") String token);

    /**
     * Method to get class attendance
     *
     * @param schoolID
     * @param classID
     * @param facultyID
     * @param date
     * @param token
     * @return
     */
    @GET("faculties/class_attendance")
    Call<ClassAttendanceResponse> getClassAttendance(@Query("school_id") String schoolID, @Query("class_list_id") int classID,
                                                     @Query("time_table_id") String time_table_id,
                                                     @Query("faculty_id") int facultyID, @Query("date") Date date,
                                                     @Header("Authorization") String token);


    /**
     * Method to update multiple attendance by the faulty
     *
     * @param mRequest
     * @param token
     * @return
     */
    @POST("faculties/create_multiple_attendance")
    Call<UpdateMultipleAttendanceResponse> updateClassMultipleAttendance(@Body RequestBody mRequest, @Header("Authorization") String token);


    /**
     * Method to fetch parent details
     *
     * @param school_id
     * @param parent_id
     * @param token
     * @return
     */
    @GET("parents")
    Call<ParentDetailResponse> getParentDetails(@Query("school_id") String school_id,
                                                @Query("id") int parent_id,
                                                @Header("Authorization") String token);


    /**
     * Method to fetch parent details
     *
     * @param school_id
     * @param parent_id
     * @param token
     * @return
     */
    @GET("students")
    Call<StudentDetailsResponse> getStudentDetails(@Query("school_id") String school_id,
                                                   @Query("id") int parent_id,
                                                   @Header("Authorization") String token);




    /**
     * Method to fetch parent homework details
     *
     * @param schoolId
     * @param studentId
     * @param date
     * @param token
     * @return
     */
    @GET("parents/home_work")
    Call<ParentHomeworkResponse> getParentHomeworkDetails(@Query("school_id") String schoolId,
                                                          @Query("student_id") int studentId,
                                                          @Query("date") Date date,
                                                          @Header("Authorization") String token);

    @PUT("parents/diary_reply")
    Call<ParentDiaryReplyResponse> parentDiaryReplyRequest(@Body ReplyDiaryRequest mUpdateDiaryRequest, @Header("Authorization") String token);

    /**
     * Method to fetch parent diary details
     *
     * @param schoolId
     * @param studentId
     * @param date
     * @param token
     * @return
     */
    @GET("parents/diary")
    Call<ParentDiaryResponse> getParentDiaryDetails(@Query("school_id") String schoolId,
                                                    @Query("student_id") int studentId,
                                                    @Query("date") Date date,
                                                    @Header("Authorization") String token);


    /***
     *
     * @param schoolId
     * @param studenttID
     * @param examName
     * @param token
     * @return
     */
    @GET("parents/marks_list2")
    Call<ParentReportCardDetailResponse> getParentReportStatus(@Query("school_id") String schoolId,
                                                               @Query("student_id") int studenttID,
                                                               @Query("exam_name") String examName,
                                                               @Header("Authorization") String token);


    /**
     * Method to fetch parent timetable details
     *
     * @param schoolId
     * @param classID
     * @param day
     * @param token
     * @return
     */
    @GET("parents/time_table_list")
    Call<ParentTimeTableResponse> getParentTimeTableDetails(@Query("school_id") String schoolId,
                                                            @Query("class_id") String classID,
                                                            @Query("day") String day,
                                                            @Header("Authorization") String token);

    /**
     * Method to fetch attendance details between dates
     *
     * @param schoolId
     * @param studentID
     * @param startDate
     * @param endDate
     * @param token
     * @return
     */
    @GET("parents/attendance_details_between_date")
    Call<AttendanceDateObject> getParentAttendanceDetailsBtnDates(@Query("school_id") String schoolId,
                                                                  @Query("student_id") int studentID,
                                                                  @Query("start_date") Date startDate,
                                                                  @Query("end_date") Date endDate,
                                                                  @Header("Authorization") String token);


    /***
     *
     * @param schoolId
     * @param class_list_id
     * @param day
     * @param student_id
     * @param date
     * @param token
     * @return
     */
    @GET("parents/attendance_all_periods")
    Call<TimetableList> getAttendanceAllPeriods(@Query("school_id") String schoolId,
                                                @Query("class_list_id") int class_list_id,
                                                @Query("day") String day,
                                                @Query("student_id") int student_id,
                                                @Query("date") Date date,
                                                @Header("Authorization") String token);



    /**
     * Method to change password for parent
     */
    @POST("parents/change_password")
    Call<ChangePasswordResponse> changeParentPassword(@Body ChangePasswordRequest mChangePasswordRequest,
                                                      @Header("Authorization") String token);

    /**
     * Method to logout parent
     */
    @DELETE("parents/logout")
    Call<LogoutResponse> parentLogout(@Header("Authorization") String token);

    /**
     * Get Survey List
     */
    @GET("parents/survey_list")
    Call<SurveyListResponse> getSurveyList(@Query("school_id") String schoolId,
                                           @Header("Authorization") String token);


    /**
     * Get Survey List
     */
    @GET("parents/question_list")
    Call<QuestionListResponse> getQuestionList(@Query("school_id") String schoolId,
                                               @Query("survey_id") String surveyId,
                                               @Query("parent_id") String parentId,
                                               @Header("Authorization") String token);

    /**
     * Method to change password for parent
     */
    @PUT("parents/update_survey")
    Call<UpdateSurveyResponse> updateSurvey(@Body SurveyRequest mSurveyRequest,
                                            @Header("Authorization") String token);
}
