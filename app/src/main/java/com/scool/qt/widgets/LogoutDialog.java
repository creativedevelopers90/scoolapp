package com.scool.qt.widgets;

import android.app.Dialog;
import android.content.Context;
import androidx.annotation.NonNull;

import com.scool.qt.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LogoutDialog extends Dialog {
    private LogoutDialogListener mListener;

    public LogoutDialog(@NonNull Context context) {
        super(context);

        setContentView(R.layout.dialog_logout);
        ButterKnife.bind(this);
    }

    public LogoutDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);

        setContentView(R.layout.dialog_logout);
        ButterKnife.bind(this);
    }

    public LogoutDialog setListener(LogoutDialogListener mListener) {
        this.mListener = mListener;
        return LogoutDialog.this;
    }

    @OnClick(R.id.confirm_logout_btn)
    public void onLogoutConfirmClicked() {
        dismiss();
        mListener.onLogoutConfirmClicked();
    }

    @OnClick(R.id.deny_logout_btn)
    public void onLogoutDenyClicked() {
        dismiss();
        mListener.onLogoutDenyClicked();
    }

    public interface LogoutDialogListener {
        public void onLogoutConfirmClicked();

        public void onLogoutDenyClicked();
    }
}
