package com.scool.qt.widgets;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

import com.scool.qt.R;
import com.scool.qt.helper.ThemeHelper;

public class ThemeButton extends AppCompatButton {
    public String theme;
    public ThemeButton(Context context) {
        super(context);
        theme = ThemeHelper.getPrefCurrentTheme(context);
        switch (theme) {
            case "blue":
                setBackground(getResources().getDrawable(R.drawable.btn_rounded_cornor_less));
                break;
            case "red":
                setBackground(getResources().getDrawable(R.drawable.btn_rounded_red_cornor));
                break;
        }

    }

    public ThemeButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBackground(getResources().getDrawable(R.drawable.btn_rounded_cornor_less));
    }

    public ThemeButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setBackground(getResources().getDrawable(R.drawable.btn_rounded_cornor_less));
    }
}
