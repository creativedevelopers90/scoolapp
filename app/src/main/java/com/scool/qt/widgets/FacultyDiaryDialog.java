package com.scool.qt.widgets;

import android.app.Dialog;
import android.content.Context;
import androidx.annotation.NonNull;

import com.scool.qt.R;
import com.scool.qt.model.DiaryList;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class FacultyDiaryDialog extends Dialog {

    private FacultyDiaryDialogListener mListener;
    private DiaryList diary;

    public FacultyDiaryDialog(@NonNull Context context) {
        super(context);
        setContentView(R.layout.dialog_faculty_diary);
        ButterKnife.bind(this);
    }

    public FacultyDiaryDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        setContentView(R.layout.dialog_faculty_diary);
        ButterKnife.bind(this);
    }

    public void setListener(FacultyDiaryDialogListener mListener) {
        this.mListener = mListener;
    }

    public void setSelectedDiary(DiaryList diary) {
        this.diary = diary;
    }

    @OnClick(R.id.edit_diary)
    public void onEditDiaryClicked() {
        dismiss();
        mListener.onEditDiaryClicked(diary);
    }

    @OnClick(R.id.delete_diary)
    public void onDeleteDiaryClicked() {
        dismiss();
        mListener.onDeleteDiaryClicked(diary);
    }

    public interface FacultyDiaryDialogListener {
        public void onEditDiaryClicked(DiaryList diary);

        public void onDeleteDiaryClicked(DiaryList diary);
    }
}
