package com.scool.qt.widgets;

import android.app.Dialog;
import android.content.Context;

import androidx.annotation.NonNull;

import com.scool.qt.R;

public class ProgressDialog extends Dialog {
    public ProgressDialog(@NonNull Context context, Boolean isCancelable, String label) {
        super(context);

        initView(isCancelable, label);
    }

    private void initView(Boolean isCancelable, String label) {
        setContentView(R.layout.dialog_progress);
        setCancelable(isCancelable);
    }
}
