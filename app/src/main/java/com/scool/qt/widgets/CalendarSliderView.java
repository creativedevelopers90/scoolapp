package com.scool.qt.widgets;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;

import com.scool.qt.R;
import com.scool.qt.helper.LocaleHelper;
import com.scool.qt.helper.ThemeHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalendarSliderView extends LinearLayout implements View.OnClickListener {

    private final Context mContext;
    private Calendar cal;
    private LayoutInflater layoutInflater;
    private int mYear, mMonth, mDay;

    private TextView currentDateTV;
    private TextView textView;
    private AppCompatImageView backCalendarBtn, forwardCalendarBtn;
    private CalenderSliderViewListener mListener;
    private String themeSelect;

    public CalendarSliderView(Context context) {
        super(context);
        this.mContext = context;
        themeSelect = ThemeHelper.getPrefCurrentTheme(mContext);
        inflate();
        bindViews();
        initializeCalender();
    }

    public CalendarSliderView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        LocaleHelper.getLanguage(mContext);
        themeSelect = ThemeHelper.getPrefCurrentTheme(mContext);
        inflate();
        bindViews();
        initializeCalender();
    }

    /**
     * Method to initialize calendar
     */
    private void initializeCalender() {
        cal = Calendar.getInstance();
        mYear = cal.get(Calendar.YEAR);
        mMonth = cal.get(Calendar.MONTH);
        mDay = cal.get(Calendar.DAY_OF_MONTH);
        if(mDay > 9){
            currentDateTV.setText(mDay + "-" + (mMonth + 1) + "-" + mYear+ ", " + dayOfWeek(mYear, mMonth, mDay));
        }else {
            currentDateTV.setText("0"+mDay + "-" + (mMonth + 1) + "-" + mYear+ ", " + dayOfWeek(mYear, mMonth, mDay));
        }

    }

    /**
     * Method to inflate the layout
     */
    private void inflate() {
        layoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.layout_calender_slider, this, true);
    }

    /**
     * Method to bind layout views
     */
    private void bindViews() {
        // bind all views here
        currentDateTV = findViewById(R.id.calender_date_text);
        backCalendarBtn = findViewById(R.id.backward_calendar_btn);
        forwardCalendarBtn = findViewById(R.id.forward_calendar_btn);
        textView = findViewById(R.id.textView);
        currentDateTV.setOnClickListener(this);
        backCalendarBtn.setOnClickListener(this);
        forwardCalendarBtn.setOnClickListener(this);
        setTheme();
        setDisableTheme();
    }



    /**
     * Method to set the listener, every the selected date will be change this listener will get invoked
     *
     * @param mListener
     * @return
     */
    public CalendarSliderView setOnDateChangeListener(CalenderSliderViewListener mListener, String viewType) {
        this.mListener = mListener;
      //  this.viewType = viewType;
        this.textView.setText(viewType);
        return this;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.calender_date_text:
                DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,
                        (view, year, monthOfYear, dayOfMonth) -> {
                            currentDateTV.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year + ", " + dayOfWeek(year, monthOfYear, dayOfMonth));
                            mYear = year;
                            mMonth = monthOfYear;
                            mDay = dayOfMonth;
                            cal.set(mYear, mMonth, mDay);
                            callbackDateChange(cal);

                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
                break;
            case R.id.backward_calendar_btn:
                cal.set(mYear, mMonth, mDay);
                cal.add(Calendar.DAY_OF_MONTH, -1);
                mYear = cal.get(Calendar.YEAR);
                mMonth = cal.get(Calendar.MONTH);
                mDay = cal.get(Calendar.DAY_OF_MONTH);
                if(mDay > 9){
                    currentDateTV.setText(mDay + "-" + (mMonth + 1) + "-" + mYear + ", " + dayOfWeek(mYear, mMonth, mDay));
                }else {
                    currentDateTV.setText("0"+mDay + "-" + (mMonth + 1) + "-" + mYear + ", " + dayOfWeek(mYear, mMonth, mDay));
                }
                callbackDateChange(cal);
                break;
            case R.id.forward_calendar_btn:
                cal.set(mYear, mMonth, mDay);
                Calendar systemCalendar = Calendar.getInstance();
                systemCalendar.setTimeInMillis(System.currentTimeMillis());

                if (cal.get(Calendar.YEAR) == systemCalendar.get(Calendar.YEAR)) {
                    if (cal.get(Calendar.MONTH) == systemCalendar.get(Calendar.MONTH)) {
                        if (cal.get(Calendar.DAY_OF_MONTH) < systemCalendar.get(Calendar.DAY_OF_MONTH)) {
                            dayIncrementByOne();
                        }
                    } else if (cal.get(Calendar.MONTH) < systemCalendar.get(Calendar.MONTH)) {
                        dayIncrementByOne();
                    }
                } else if (cal.get(Calendar.YEAR) < systemCalendar.get(Calendar.YEAR)) {
                    dayIncrementByOne();
                }
                break;
        }
    }

    /**
     * Method to increase day value by one
     */
    public void dayIncrementByOne() {
        cal.add(Calendar.DAY_OF_MONTH, 1);
        mYear = cal.get(Calendar.YEAR);
        mMonth = cal.get(Calendar.MONTH);
        mDay = cal.get(Calendar.DAY_OF_MONTH);
        if(mDay > 9) {
            currentDateTV.setText(mDay + "-" + (mMonth + 1) + "-" + mYear + ", " + dayOfWeek(mYear, mMonth, mDay));
        }else {
            currentDateTV.setText("0"+mDay + "-" + (mMonth + 1) + "-" + mYear + ", " + dayOfWeek(mYear, mMonth, mDay));
        }

        callbackDateChange(cal);
    }

    /**
     * Method to manage forward arrow color and send callback when the date is changed
     *
     * @param cal
     */
    private void callbackDateChange(Calendar cal) {
        Calendar systemCalendar = Calendar.getInstance();
        systemCalendar.setTimeInMillis(System.currentTimeMillis());

        if (cal.get(Calendar.YEAR) == systemCalendar.get(Calendar.YEAR)) {
            if (cal.get(Calendar.MONTH) == systemCalendar.get(Calendar.MONTH)) {
                if (cal.get(Calendar.DAY_OF_MONTH) < systemCalendar.get(Calendar.DAY_OF_MONTH)) {
                   setEnableTheme();
                } else
                    setDisableTheme();
            } else
                setEnableTheme();
        } else
            setEnableTheme();

        mListener.onDateChanged(cal);
    }

    public interface CalenderSliderViewListener {
        void onDateChanged(Calendar cal);
    }

    private void setTheme() {
        if(themeSelect.equalsIgnoreCase("blue")) {
            backCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.color_blue));
        } else if(themeSelect.equalsIgnoreCase("red")) {
            backCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.color_red_accent));
        } else if(themeSelect.equalsIgnoreCase("purple")) {
            backCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.color_purple_accent));
        } else if(themeSelect.equalsIgnoreCase("black")) {
            backCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.color_black_accent));
        } else if(themeSelect.equalsIgnoreCase("brown")) {
            backCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.color_brown_primary_accent));
        } else if(themeSelect.equalsIgnoreCase("green")) {
              backCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.color_green));
        }
    }

    private void setEnableTheme() {
        if(themeSelect.equalsIgnoreCase("blue")) {
            forwardCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.color_blue));
        } else if(themeSelect.equalsIgnoreCase("red")) {
            forwardCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.color_red_accent));
        } else if(themeSelect.equalsIgnoreCase("purple")) {
            forwardCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.color_purple_accent));
        } else if(themeSelect.equalsIgnoreCase("black")) {
            forwardCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.color_black_accent));
        } else if(themeSelect.equalsIgnoreCase("brown")) {
            forwardCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.color_brown_primary_accent));
        } else if(themeSelect.equalsIgnoreCase("green")) {
            forwardCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.color_green));
        }
    }

    private void setDisableTheme() {
        if(themeSelect.equalsIgnoreCase("blue")) {
            forwardCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.color_blue));
        } else if(themeSelect.equalsIgnoreCase("red")) {
            forwardCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.light_red_color));
        } else if(themeSelect.equalsIgnoreCase("purple")) {
            forwardCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.light_purple));
        } else if(themeSelect.equalsIgnoreCase("black")) {
            forwardCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.color_black_accent));
        } else if(themeSelect.equalsIgnoreCase("brown")) {
            forwardCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.light_color_brown));
        } else if(themeSelect.equalsIgnoreCase("green")) {
            forwardCalendarBtn.setColorFilter(ContextCompat.getColor(mContext, R.color.light_color_green));
        }
    }

    private String dayOfWeek(int year, int monthOfYear, int dayOfMonth) {
        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        Date dateRepresentation = cal.getTime();
        return simpledateformat.format(dateRepresentation);
    }

    public ImageView getBackCalendarBtn() {
        return backCalendarBtn;
    }

    public void setBackCalendarBtn() {
        backCalendarBtn.setEnabled(false);
    }


    public void setForwardCalendarBtn() {
        forwardCalendarBtn.setEnabled(false);
    }

    public Resources getLanguageType() {
        Context context = LocaleHelper.setLocale(mContext, LocaleHelper.getLanguage(mContext));
        return context.getResources();
    }
}
