package com.scool.qt.widgets;

import android.content.Context;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.scool.qt.R;

public class ThemeTextView extends AppCompatTextView {
    public ThemeTextView(Context context) {
        super(context);
        setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    public ThemeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    public ThemeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTextColor(getResources().getColor(R.color.colorPrimary));
    }
}
