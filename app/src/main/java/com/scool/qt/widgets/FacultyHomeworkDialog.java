package com.scool.qt.widgets;

import android.app.Dialog;
import android.content.Context;
import androidx.annotation.NonNull;

import com.scool.qt.R;
import com.scool.qt.model.HomeworkList;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class FacultyHomeworkDialog extends Dialog {

    private FacultyHomeworkDialogListener mListener;
    private HomeworkList homework;

    public FacultyHomeworkDialog(@NonNull Context context) {
        super(context);
        setContentView(R.layout.dialog_faculty_homework);
        ButterKnife.bind(this);
    }

    public FacultyHomeworkDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        setContentView(R.layout.dialog_faculty_diary);
        ButterKnife.bind(this);
    }

    public void setListener(FacultyHomeworkDialogListener mListener) {
        this.mListener = mListener;
    }

    public void setSelectedDiary(HomeworkList homework) {
        this.homework = homework;
    }

    @OnClick(R.id.edit_homework)
    public void onEditDiaryClicked() {
        dismiss();
        mListener.onEditHomeworkClicked(homework);
    }

    @OnClick(R.id.delete_homework)
    public void onDeleteDiaryClicked() {
        dismiss();
        mListener.onDeleteHomeworkClicked(homework);
    }

    public interface FacultyHomeworkDialogListener {
        public void onEditHomeworkClicked(HomeworkList homeworkList);

        public void onDeleteHomeworkClicked(HomeworkList homeworkList);
    }
}
