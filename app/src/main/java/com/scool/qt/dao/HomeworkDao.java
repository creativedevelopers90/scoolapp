package com.scool.qt.dao;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.scool.qt.model.room.Homework;

import java.util.List;

@Dao
public interface HomeworkDao {
    @Query("SELECT * FROM homework")
    LiveData<List<Homework>> getAll();

    @Query("SELECT * FROM homework WHERE homework_id IN (:userIds)")
    List<Homework> loadAllByIds(int[] userIds);

    @Insert
    void insertAll(Homework homework);

    @Delete
    void delete(Homework homework);



}
