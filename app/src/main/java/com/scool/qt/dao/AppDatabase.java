package com.scool.qt.dao;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.scool.qt.model.room.Homework;

@Database(entities = {Homework.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase  {
    public abstract HomeworkDao homeworkDao();
}
