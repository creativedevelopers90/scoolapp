package com.scool.qt.parent.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.ParentStudentListAdapter;
import com.scool.qt.model.ParentLoginResponse;
import com.scool.qt.model.StartResponse;
import com.scool.qt.model.parent.ParentDetailResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParentHomeFragment extends Fragment implements NetworkView {
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;
    @BindView(R.id.tv_school_slogan)
    TextView tvSchoolSlogan;
    @BindView(R.id.student_list_rv)
    RecyclerView studentListRV;
    private View root;
    private NetworkPresenter mNetworkPresenter;
    private LinearLayoutManager mLayoutManager;
    private ParentStudentListAdapter mAdapter;
    private StartResponse schoolData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_parent_home, container, false);
        ButterKnife.bind(this, root);
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        schoolData = Utils.loadSchoolData(getActivity());

        initSchoolDetails();
        mLayoutManager = new LinearLayoutManager(getActivity());
        studentListRV.setLayoutManager(mLayoutManager);
        fetchParentDetails();
        return root;
    }

    private void initSchoolDetails() {
        if (schoolData != null) {
            Glide.with(getActivity())
                    .load(Constant.IMAGE_BASE_URL + schoolData.getLogo())
                    .thumbnail(0.5f)
                    .into(imgSchool);
            tvSchoolname.setText(schoolData.getName());
            tvSchoolSlogan.setText(schoolData.getSlogan());
        }
    }

    /**
     * Method to fetch parent details
     */
    private void fetchParentDetails() {
        if (mNetworkPresenter != null) {
            mNetworkPresenter.methodToFetchParentDetails(getActivity(), Constant.UNIQUE_SCHOOL_ID, loadParentData().getParentId(), loadParentData().getToken());
        }
    }



    /**
     * Method to load the parent data
     *
     * @return
     */
    private ParentLoginResponse loadParentData() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson.fromJson(json, ParentLoginResponse.class);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case parent_details:
                mAdapter = new ParentStudentListAdapter(getActivity(), ((ParentDetailResponse) object).getParent().getStudents());
                studentListRV.setAdapter(mAdapter);
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
    }
}
