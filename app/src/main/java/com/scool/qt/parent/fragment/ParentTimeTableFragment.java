package com.scool.qt.parent.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.ParentTimeTableRecyclerAdapter;
import com.scool.qt.model.ParentLoginResponse;
import com.scool.qt.model.parent.ParentTimeTableResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;
import com.scool.qt.widgets.CalendarSliderView;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.scool.qt.EndpointEnums.parent_time_table_details;

public class ParentTimeTableFragment extends Fragment implements CalendarSliderView.CalenderSliderViewListener, NetworkView {

    @BindView(R.id.parent_time_table_calendar)
    CalendarSliderView timetableCalendarCV;
    @BindView(R.id.time_subject_list_rv)
    RecyclerView timeSubjectRV;
    @BindView(R.id.timetable_header)
    TextView timetableHeader;
    @BindView(R.id.time_column_header)
    TextView timeColumnHeader;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.subject_column_header)
    TextView subjectColumnHeader;
    @BindView(R.id.constraintLayout2)
    RelativeLayout constraintLayout2;
    @BindView(R.id.parent_time_table_root)
    RelativeLayout parentTimeTableRoot;
    @BindView(R.id.no_diary_iv)
    ImageView noDiaryIv;
    @BindView(R.id.no_diary_tv)
    TextView noDiaryTv;
    @BindView(R.id.no_diary_view)
    RelativeLayout noDiaryView;
    private View root;
    private NetworkPresenter mNetworkPresenter;
    ParentTimeTableRecyclerAdapter mAdapter;
    private String dayOfWeek;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_parent_time_table, container, false);
        ButterKnife.bind(this, root);
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());

        initView();

        return root;
    }

    /**
     * Method to initialize view
     */
    private void initView() {
        timetableCalendarCV.setOnDateChangeListener(this, getString(R.string.kTimeTableText));
        timeSubjectRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        timeSubjectRV.setHasFixedSize(true);
        mAdapter = new ParentTimeTableRecyclerAdapter(getActivity());
        timeSubjectRV.setAdapter(mAdapter);
        fetchTimeTableDetails(Calendar.getInstance());
    }

    /**
     * Method to fetch time table details
     *
     * @param calendar
     */
    private void fetchTimeTableDetails(Calendar calendar) {
            if(Constant.ACTIVE_STUDENT != null)
            mNetworkPresenter.methodToFetchParentTimeTableDetails(getContext(), Constant.UNIQUE_SCHOOL_ID,
                    Constant.ACTIVE_STUDENT.getClassId().toString(), "" + getDayOfWeek(calendar.get(Calendar.DAY_OF_WEEK)), loadParentData().getToken());
    }

    /**
     * method to get string for the selected day
     *
     * @param dayOfWeek
     * @return
     */
    private String getDayOfWeek(int dayOfWeek) {
        switch (dayOfWeek) {
            case 1:
                return "Sunday";
            case 2:
                return "Monday";
            case 3:
                return "Tuesday";
            case 4:
                return "Wednesday";
            case 5:
                return "Thursday";
            case 6:
                return "Friday";
            case 7:
                return "Saturday";
        }
        return null;
    }

    /**
     * Method to load the parent data
     *
     * @return
     */
    private ParentLoginResponse loadParentData() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson.fromJson(json, ParentLoginResponse.class);
    }

    @Override
    public void onDateChanged(Calendar cal) {
        dayOfWeek = getDayOfWeek(cal.get(Calendar.DAY_OF_WEEK));
        fetchTimeTableDetails(cal);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case parent_time_table_details:
                if (((ParentTimeTableResponse) object).getTimeTable().size() > 0) {
                    noDiaryView.setVisibility(View.GONE);
                    constraintLayout2.setVisibility(View.VISIBLE);
                    mAdapter.addItemInList(((ParentTimeTableResponse) object).getTimeTable());
                } else {
                    mAdapter.clearList();
                    constraintLayout2.setVisibility(View.GONE);
                    noDiaryView.setVisibility(View.VISIBLE);
                    noDiaryTv.setText(getString(R.string.no_time_table) + " " +  dayOfWeek);
                }
                break;
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        if (endpointType ==  parent_time_table_details) {
            Utils.showSnackBar(getActivity(), message);
            mAdapter.clearList();
            constraintLayout2.setVisibility(View.GONE);
            noDiaryView.setVisibility(View.VISIBLE);
            noDiaryTv.setText(getString(R.string.no_time_table) + dayOfWeek);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
