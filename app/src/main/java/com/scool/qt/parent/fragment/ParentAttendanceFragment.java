package com.scool.qt.parent.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.ParentAttendanceListRecyclerAdapter;
import com.scool.qt.base.fragment.BaseFragment;
import com.scool.qt.model.ParentLoginResponse;
import com.scool.qt.model.StudentLoginResponse;
import com.scool.qt.model.TimetableList;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;
import com.scool.qt.widgets.CalendarSliderView;
import com.scool.qt.widgets.ProgressDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParentAttendanceFragment extends BaseFragment implements NetworkView, CalendarSliderView.CalenderSliderViewListener {

    private View root;
    private NetworkPresenter mPresenter;
    private ParentAttendanceListRecyclerAdapter mAttendanceAdapter;

    @BindView(R.id.timetable_header)
    TextView timetableHeader;
    @BindView(R.id.parent_time_table_calendar)
    CalendarSliderView parentTimeTableCalendar;
    @BindView(R.id.time_column_header)
    TextView timeColumnHeader;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.subject_column_header)
    TextView subjectColumnHeader;
    @BindView(R.id.constraintLayout2)
    RelativeLayout constraintLayout2;
    @BindView(R.id.time_subject_list_rv)
    RecyclerView timeSubjectListRv;
    @BindView(R.id.parent_time_table_root)
    RelativeLayout parentTimeTableRoot;
    @BindView(R.id.no_diary_iv)
    ImageView noDiaryIv;
    @BindView(R.id.no_diary_tv)
    TextView noDiaryTv;
    @BindView(R.id.no_diary_view)
    RelativeLayout noDiaryView;
    private Calendar attendanceDate;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_parent_attendance, container, false);
        ButterKnife.bind(this, root);
        mPresenter = new NetworkPresenter(this, new NetworkInteractor());
        initView();
        return root;
    }

    /**
     * Method to initialize the view
     */
    private void initView() {
        parentTimeTableCalendar.setOnDateChangeListener(this, getString(R.string.kTitleTextAttendance));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        timeSubjectListRv.setLayoutManager(layoutManager);
        timeSubjectListRv.setHasFixedSize(true);
        mAttendanceAdapter = new ParentAttendanceListRecyclerAdapter(getActivity());
        timeSubjectListRv.setAdapter(mAttendanceAdapter);
        fetchAttendanceDetails(attendanceDate = Calendar.getInstance());
    }

    /**
     * Method to fetch attendance details
     *
     * @param cal
     */
    private void fetchAttendanceDetails(Calendar cal) {
        if (mPresenter != null) {
            if (Constant.ACTIVE_STUDENT != null) {
                mPresenter.methodToGetAttendanceAllPeriods(getActivity(), Constant.UNIQUE_SCHOOL_ID, Constant.ACTIVE_STUDENT.getClassId().intValue(),
                        dayOfWeek(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)),
                        Constant.ACTIVE_STUDENT.getStudentId().intValue(), cal.getTime(), loadParentData().getToken());
            }
        }
    }

    /**
     * Method to load the parent data
     *
     * @return
     */
    private ParentLoginResponse loadParentData() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson.fromJson(json, ParentLoginResponse.class);
    }

    /**
     * Method to load the parent data
     *
     * @return
     */
    private StudentLoginResponse loadStudentData() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson.fromJson(json, StudentLoginResponse.class);
    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity(), true, "");
            progressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case attendance_all_periods:
                if (getView() == null) return;
                progressDialog.dismiss();
                TimetableList attendanceBtwnDatesResponse = (TimetableList) object;
                if (attendanceBtwnDatesResponse != null && attendanceBtwnDatesResponse.getTimeTable() != null && attendanceBtwnDatesResponse.getTimeTable().size() > 0) {
                    constraintLayout2.setVisibility(View.VISIBLE);
                    timeSubjectListRv.setVisibility(View.VISIBLE);
                    noDiaryView.setVisibility(View.GONE);
                    mAttendanceAdapter.addItemInList(attendanceBtwnDatesResponse.getTimeTable());
                } else {
                    mAttendanceAdapter.addItemInList(attendanceBtwnDatesResponse.getTimeTable());
                    constraintLayout2.setVisibility(View.GONE);
                    noDiaryView.setVisibility(View.VISIBLE);
                    noDiaryTv.setText(getString(R.string.kNoAttendancefor) + " " + Constant.ACTIVE_STUDENT.getName());
                }
                break;
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case parent_attendance_details:
                if (getView() == null) return;
                progressDialog.dismiss();
                noDiaryView.setVisibility(View.VISIBLE);
                noDiaryTv.setText(getString(R.string.kNoAttendancefor) + " " + Constant.ACTIVE_STUDENT.getName());
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDateChanged(Calendar cal) {
        attendanceDate = cal;
        fetchAttendanceDetails(cal);
    }

    private String dayOfWeek(int year, int monthOfYear, int dayOfMonth) {
        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        Date dateRepresentation = cal.getTime();
        return simpledateformat.format(dateRepresentation);
    }
}
