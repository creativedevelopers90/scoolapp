package com.scool.qt.parent.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.QuestionsAdapter;
import com.scool.qt.base.fragment.BaseFragment;
import com.scool.qt.model.ParentLoginResponse;
import com.scool.qt.model.QuestionListResponse;
import com.scool.qt.model.QuestionObject;
import com.scool.qt.model.request.AnswerDataObject;
import com.scool.qt.model.request.SurveyRequest;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class QuestionsFragment extends BaseFragment implements NetworkView, QuestionsAdapter.ItemClick {
    Unbinder unbinder;
    @BindView(R.id.attendance_list_tv)
    TextView attendanceListTv;
    @BindView(R.id.attendance_list_header)
    RelativeLayout attendanceListHeader;
    @BindView(R.id.attendance_list_rv)
    RecyclerView attendanceListRv;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.cl_maincontent)
    ConstraintLayout clMaincontent;
    @BindView(R.id.iv_nointernet)
    ImageView ivNointernet;
    @BindView(R.id.rl_nointernet)
    RelativeLayout rlNointernet;
    @BindView(R.id.image_view)
    ImageView imageView;
    @BindView(R.id.textview)
    TextView textview;
    @BindView(R.id.rl_view)
    RelativeLayout rlView;
    NetworkPresenter mPresenter;
    @BindView(R.id.btn_done)
    AppCompatButton btn_done;
    private  QuestionsAdapter mAdapter;
    private List<QuestionObject> mQuestionlist = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_survey, container, false);
        unbinder = ButterKnife.bind(this, root);
        mPresenter = new NetworkPresenter(this, new NetworkInteractor());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        attendanceListRv.setLayoutManager(layoutManager);
        attendanceListRv.setHasFixedSize(true);
        mAdapter = new QuestionsAdapter(getActivity(), QuestionsFragment.this);
        attendanceListRv.setAdapter(mAdapter);
        attendanceListTv.setText(getString(R.string.kenterdetails));
        getQuestionList();
        return root;
    }

    public void getQuestionList() {
        if(Utils.isNetworkAvailable(getActivity())){
            progressBar.setVisibility(View.VISIBLE);
            mPresenter.methodToGetQuestionList(getActivity(), loadParentData().getToken(), Constant.UNIQUE_SCHOOL_ID, getArguments().getString("surveyId"),
                    loadParentData().getParentId());
        }
    }

    @OnClick(R.id.btn_done)
    void btnDone() {
        SurveyRequest mSurveyReq = new SurveyRequest();
        mSurveyReq.setSchoolId(Constant.UNIQUE_SCHOOL_ID);
        mSurveyReq.setParentId(loadParentData().getParentId());
        mSurveyReq.setSurveyId(Integer.parseInt(getArguments().getString("surveyId")));
        List<AnswerDataObject> answerDataObjectList = new ArrayList<>();

       Optional<QuestionObject> questionObject =
               mQuestionlist.stream().filter(item -> item.getValidationRules().getPresence().equalsIgnoreCase("1")
                       && item.getAns().equalsIgnoreCase("")).findAny();

       if(questionObject.isPresent()) {
           Utils.showSnackBar(getActivity(), "Please answer all mandatory question");
       } else {
           mQuestionlist.stream().forEach(item -> {
               AnswerDataObject object = new AnswerDataObject();
               object.setAnswerText(item.getAns());
               object.setQuestionId(item.getId());
               answerDataObjectList.add(object);
           });
           mSurveyReq.setAnswers(answerDataObjectList);
           mPresenter.methodToUpdateSurvey(getActivity(), loadParentData().getToken(), mSurveyReq);
       }
    }

    /**
     * Method to load the parent data
     *
     * @return
     */
    private ParentLoginResponse loadParentData() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson.fromJson(json, ParentLoginResponse.class);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        if(endpointType == EndpointEnums.question_list) {
            if(getView() != null) {
                progressBar.setVisibility(View.GONE);
                QuestionListResponse listresponse = (QuestionListResponse)object;
                mQuestionlist.addAll(listresponse.getQuestions());
                mQuestionlist.stream().forEach(item -> {
                    if(item.getValidationRules().getPresence().equalsIgnoreCase("1")){
                       item.setText("*" + item.getText());
                    }
                });
                progressBar.setVisibility(View.GONE);
                mAdapter.addItemsInList(listresponse.getQuestions());
               Optional<QuestionObject> itemObject =  mQuestionlist.stream().filter(item -> !item.getAnswered()).findAny();
               if(itemObject.isPresent()) {
                   attendanceListTv.setText(getString(R.string.kenterdetails));
               } else {
                   btn_done.setVisibility(View.GONE);
                   attendanceListTv.setText(getString(R.string.ksurveycompleted));
               }
            }
        }else if(endpointType == EndpointEnums.update_survey) {
            Utils.hideSoftKeyboard(getActivity());
            Utils.showSnackBar(getActivity(),getString(R.string.ksurvey_saved));
            new Handler().postDelayed(() -> getActivity().onBackPressed(), 1500);
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        if(endpointType == EndpointEnums.question_list) {
            if(getView() != null) {
                btn_done.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                Utils.showSnackBar(getActivity(),message);
            }
        } else if(endpointType == EndpointEnums.update_survey) {

        }
    }


    @Override
    public void onItemSelected(QuestionObject object) {
        QuestionObject mItemObject = mQuestionlist.stream().filter( v1 -> v1.getId() == object.getId()).findAny().orElse(null);
        mItemObject.setAns(object.getAns());
    }
}
