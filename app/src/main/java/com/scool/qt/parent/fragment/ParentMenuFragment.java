package com.scool.qt.parent.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.activity.ParentHomeActivity;
import com.scool.qt.adapter.ParentMenuRecyclerAdapter;
import com.scool.qt.base.fragment.EventsFragment;
import com.scool.qt.event.PushnotificationEvent;
import com.scool.qt.fragments.LoggedInSettingsFragment;
import com.scool.qt.helper.PrefData;
import com.scool.qt.model.ParentLoginResponse;
import com.scool.qt.model.StudentDetailsResponse;
import com.scool.qt.model.StudentLoginResponse;
import com.scool.qt.model.parent.ParentDetailResponse;
import com.scool.qt.model.parent.ParentMenuModel;
import com.scool.qt.model.parent.Student;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;
public class ParentMenuFragment extends Fragment implements ParentMenuRecyclerAdapter.ParentMenuRecyclerAdapterListener, NetworkView {
    @BindView(R.id.parent_menu_rv)
    RecyclerView parentMenuRV;
    @BindView(R.id.parent_menu_header_tv)
    TextView parentHeaderTV;
    @BindView(R.id.parent_footer)
    AppCompatTextView parentFooter;
    private View root;
    private NetworkPresenter mNetworkPresenter;
    PrefData prefData = null;
    ParentMenuRecyclerAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_parent_menu, container, false);
        ButterKnife.bind(this, root);
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        prefData = new PrefData(getActivity());
        initView();
        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (Constant.ACTIVE_STUDENT == null)
           fetchDetails();
        else
            parentHeaderTV.setText(Constant.ACTIVE_STUDENT.getName());
    }

    private void fetchDetails() {
      if(prefData.getLoginType() == Constant.LOGIN_TYPE_STUDENT) {
          fetchStudentDetails();
      }else if(prefData.getLoginType() == Constant.LOGIN_TYPE_PARENT) {
          fetchParentDetails();
      }
    }

    /**
     * Method to initialize view
     */
    private void initView() {
        parentMenuRV.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        parentMenuRV.setHasFixedSize(true);
        mAdapter = new ParentMenuRecyclerAdapter(generateMenuList(), getContext(), ParentMenuFragment.this);
        parentMenuRV.setAdapter(mAdapter);
        if(prefData.getLoginType() == Constant.LOGIN_TYPE_PARENT) {
            parentFooter.setText(getActivity().getResources().getString(R.string.kFooterNotesTextForParent));
        }else if(prefData.getLoginType() == Constant.LOGIN_TYPE_STUDENT) {
            parentFooter.setText(getResources().getString(R.string.kFooterNotesTextForStudent));
        }
    }

    /**
     * Method to generate menu list
     *
     * @return
     */
    private ArrayList<ParentMenuModel> generateMenuList() {
        ArrayList<ParentMenuModel> parentList = new ArrayList<>();
        parentList.add(new ParentMenuModel(getActivity().getResources().getString(R.string.kTitleTextAttendance),
                R.drawable.ic_atten_blue, (getArguments() != null && getArguments().getString("abadge") != null)? getArguments().getString("abadge"): null, PARENT_MENU.ATTENDANCE));
        parentList.add(new ParentMenuModel(getActivity().getResources().getString(R.string.kHomeworkText),
                R.drawable.ic_blue_home, (getArguments() != null && getArguments().getString("hwbadge") != null)? getArguments().getString("hwbadge"): null, PARENT_MENU.HOMEWORK));
        parentList.add(new ParentMenuModel(getResources().getString(R.string.kDiaryText),
                R.drawable.ic_blue_diary,(getArguments() != null && getArguments().getString("dbadge") != null)? getArguments().getString("dbadge"): null, PARENT_MENU.DIARY));
        parentList.add(new ParentMenuModel(getActivity().getResources().getString(R.string.kTimeTableText),
                R.drawable.ic_blue_tb, PARENT_MENU.TIME_TABLE));
        parentList.add(new ParentMenuModel(getResources().getString(R.string.kMarksText),
                R.drawable.ic_blue_report, (getArguments() != null && getArguments().getString("mbadge") != null)? getArguments().getString("mbadge"): null,PARENT_MENU.REPORT_CARD));
        parentList.add(new ParentMenuModel(getResources().getString(R.string.ksurveyText),
                R.drawable.ic_blue_gallery, (getArguments() != null && getArguments().getString("sbadge") != null)? getArguments().getString("sbadge"): null, PARENT_MENU.SURVEY));
        parentList.add(new ParentMenuModel(getResources().getString(R.string.kEventsText),
                R.drawable.ic_blue_event, (getArguments() != null && getArguments().getString("ebadge") != null)? getArguments().getString("ebadge"): null, PARENT_MENU.CIRCULARS));
        if (new PrefData(getActivity()).getLoginType() == Constant.LOGIN_TYPE_PARENT) {
            parentList.add(new ParentMenuModel(getResources().getString(R.string.kChangeKid),
                    R.drawable.ic_chn_class, PARENT_MENU.CHANGE_CLASS));
        }
        parentList.add(new ParentMenuModel(getResources().getString(R.string.kFacultySettingsText),
                R.drawable.ic_blue_setting, PARENT_MENU.SETTINGS));

        return parentList;
    }

    /**
     * Method invoked when a menu is selected
     *
     * @param mSelectedMenu
     */
    @Override
    public void onMenuSelected(ParentMenuModel mSelectedMenu) {
        String toolbar = Constant.ACTIVE_STUDENT != null && Constant.ACTIVE_STUDENT.getClassName() != null?
                Constant.ACTIVE_STUDENT.getName()+", "+ Constant.ACTIVE_STUDENT.getClassName(): Constant.ACTIVE_STUDENT != null ?
                Constant.ACTIVE_STUDENT.getName(): "";
        switch (mSelectedMenu.getMenuId()) {
            case ATTENDANCE:
                if (Constant.ACTIVE_STUDENT != null) {
                    ((ParentHomeActivity) getActivity()).addFragment(new ParentAttendanceFragment(), getString(R.string.kTitleTextAttendance));
                    ((ParentHomeActivity) getActivity()).updateToolbar(toolbar, true);
                } else {
                    ((ParentHomeActivity) getActivity()).addFragment(new ParentAttendanceFragment(), getString(R.string.kTitleTextAttendance));
                    ((ParentHomeActivity) getActivity()).updateToolbar("", true);
                }
                break;
            case HOMEWORK:
                if (Constant.ACTIVE_STUDENT != null) {
                    ((ParentHomeActivity) getActivity()).addFragment(new ParentHomeworkFragment(), getString(R.string.kHomeworkText));
                    ((ParentHomeActivity) getActivity()).updateToolbar(toolbar, false);
                } else {
                    ((ParentHomeActivity) getActivity()).addFragment(new ParentHomeworkFragment(), getString(R.string.kHomeworkText));
                    ((ParentHomeActivity) getActivity()).updateToolbar("", false);
                }
                break;
            case DIARY:
                if (Constant.ACTIVE_STUDENT != null) {
                    ((ParentHomeActivity) getActivity()).addFragment(new ParentDiaryFragment(), getString(R.string.kDiaryText));
//                ((ParentHomeActivity) getActivity()).updateToolbar(getString(R.string.kDiaryText), false);
                    ((ParentHomeActivity) getActivity()).updateToolbar(toolbar, false);
                } else {
                    ((ParentHomeActivity) getActivity()).addFragment(new ParentDiaryFragment(), getString(R.string.kDiaryText));
//                ((ParentHomeActivity) getActivity()).updateToolbar(getString(R.string.kDiaryText), false);
                    ((ParentHomeActivity) getActivity()).updateToolbar("", false);
                }

                break;
            case TIME_TABLE:
                if (Constant.ACTIVE_STUDENT != null) {
                    ((ParentHomeActivity) getActivity()).addFragment(new ParentTimeTableFragment(), getString(R.string.kTimeTableText));
//                ((ParentHomeActivity) getActivity()).updateToolbar(getString(R.string.kTimeTableText), false);
                    ((ParentHomeActivity) getActivity()).updateToolbar(toolbar, false);
                } else {
                    ((ParentHomeActivity) getActivity()).addFragment(new ParentTimeTableFragment(), getString(R.string.kTimeTableText));
//                ((ParentHomeActivity) getActivity()).updateToolbar(getString(R.string.kTimeTableText), false);
                    ((ParentHomeActivity) getActivity()).updateToolbar("", false);
                }

                break;
            case REPORT_CARD:
                if (Constant.ACTIVE_STUDENT != null) {
                    ((ParentHomeActivity) getActivity()).addFragment(new ParentReportCardFragment(), getString(R.string.kMarksText));
                    ((ParentHomeActivity) getActivity()).updateToolbar(toolbar, false);
                } else {
                    ((ParentHomeActivity) getActivity()).addFragment(new ParentReportCardFragment(), getString(R.string.kMarksText));
                    ((ParentHomeActivity) getActivity()).updateToolbar("", false);
                }
                break;
            case CIRCULARS:
                Bundle bundle = new Bundle();
                bundle.putBoolean("isParent", true);
                EventsFragment eventsFragment = new EventsFragment();
                eventsFragment.setArguments(bundle);
                if (Constant.ACTIVE_STUDENT != null) {
                    ((ParentHomeActivity) getActivity()).addFragment(eventsFragment, getString(R.string.kEventsText));
//                ((ParentHomeActivity) getActivity()).updateToolbar(getString(R.string.kEventsText), false);
                    ((ParentHomeActivity) getActivity()).updateToolbar(toolbar, false);
                } else {
                    ((ParentHomeActivity) getActivity()).addFragment(eventsFragment, getString(R.string.kEventsText));
//                ((ParentHomeActivity) getActivity()).updateToolbar(getString(R.string.kEventsText), false);
                    ((ParentHomeActivity) getActivity()).updateToolbar("", false);
                }

                break;
            case CHANGE_CLASS:
                ((ParentHomeActivity) getActivity()).addFragment(new ParentHomeFragment(), getString(R.string.kParentHomeText));
                ((ParentHomeActivity) getActivity()).updateToolbar("Change Kid", false);
                break;
            case SURVEY:
                ((ParentHomeActivity) getActivity()).addFragment(new SurveyFragment(), getString(R.string.ksurveyText));
                ((ParentHomeActivity) getActivity()).updateToolbar(getString(R.string.ksurveyText), false);
                break;
            case SETTINGS:
                if (Constant.ACTIVE_STUDENT != null) {
                    ((ParentHomeActivity) getActivity()).addFragment(new LoggedInSettingsFragment(), getString(R.string.kSettingsText));
                    ((ParentHomeActivity) getActivity()).updateToolbar(toolbar, false);
                } else {
                    ((ParentHomeActivity) getActivity()).addFragment(new LoggedInSettingsFragment(), getString(R.string.kSettingsText));
                    ((ParentHomeActivity) getActivity()).updateToolbar("", false);
                }
                break;
        }
    }

    /**
     * Method to fetch parent details
     */
    private void fetchParentDetails() {
        if (mNetworkPresenter != null) {
            mNetworkPresenter.methodToFetchParentDetails(getActivity(), Constant.UNIQUE_SCHOOL_ID, loadParentData().getParentId(), loadParentData().getToken());
        }
    }

    private void fetchStudentDetails() {
        if (mNetworkPresenter != null) {
            mNetworkPresenter.methodToFetchStudentDetails(getActivity(), Constant.UNIQUE_SCHOOL_ID, loadStudentData().getStudentId(), loadStudentData().getToken());
        }
    }

    /**
     * Method to load the parent data
     *
     * @return
     */
    private ParentLoginResponse loadParentData() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson.fromJson(json, ParentLoginResponse.class);
    }

    /**
     * Method to load the parent data
     *
     * @return
     */
    private StudentLoginResponse loadStudentData() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson.fromJson(json, StudentLoginResponse.class);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Timber.d("Parent Diary Fragment");
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case parent_details:
                parentHeaderTV.setText(((ParentDetailResponse) object).getParent().getStudents().get(0).getName());
                Constant.parentData = (ParentDetailResponse) object;
                Constant.ACTIVE_STUDENT = ((ParentDetailResponse) object).getParent().getStudents().get(0);
                break;
            case student_details:
                parentHeaderTV.setText(((StudentDetailsResponse) object).getName());
                StudentDetailsResponse response = (StudentDetailsResponse) object;
                Student student = new Student();
                student.setName(response.getName());
                student.setStudentId(Long.valueOf(response.getId()));
                student.setClassId(Long.valueOf(response.getClassListId()));
                student.setImage(response.getPhoto());
                Constant.ACTIVE_STUDENT = student;
                break;
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        Log.e("== " + endpointType + " ==>", message);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void notificationReceived(PushnotificationEvent event){
        ArrayList<ParentMenuModel> parentList = new ArrayList<>();
        parentList.add(new ParentMenuModel(getResources().getString(R.string.kTitleTextAttendance),
                R.drawable.ic_atten_blue, event.getAbadge(), PARENT_MENU.ATTENDANCE));
        parentList.add(new ParentMenuModel(getResources().getString(R.string.kHomeworkText),
                R.drawable.ic_blue_home, event.getHwbadge(), PARENT_MENU.HOMEWORK));
        parentList.add(new ParentMenuModel(getResources().getString(R.string.kDiaryText),
                R.drawable.ic_blue_diary, event.getDbadge(), PARENT_MENU.DIARY));
        parentList.add(new ParentMenuModel(getResources().getString(R.string.kTimeTableText),
                R.drawable.ic_blue_tb, PARENT_MENU.TIME_TABLE));
        parentList.add(new ParentMenuModel(getResources().getString(R.string.kMarksText),
                R.drawable.ic_blue_report, PARENT_MENU.REPORT_CARD));
        parentList.add(new ParentMenuModel(getResources().getString(R.string.ksurveyText),
                R.drawable.ic_blue_gallery, event.getSbadge(), PARENT_MENU.SURVEY));
        parentList.add(new ParentMenuModel(getResources().getString(R.string.kEventsText),
                R.drawable.ic_blue_event, event.getEbadge(), PARENT_MENU.CIRCULARS));
        if (new PrefData(getActivity()).getLoginType() == Constant.LOGIN_TYPE_PARENT) {
            parentList.add(new ParentMenuModel(getResources().getString(R.string.kChangeKid),
                    R.drawable.ic_chn_class, PARENT_MENU.CHANGE_CLASS));
        }
        parentList.add(new ParentMenuModel(getResources().getString(R.string.kFacultySettingsText),
                R.drawable.ic_blue_setting, PARENT_MENU.SETTINGS));
        mAdapter.setList(parentList);
    }

    public enum PARENT_MENU {ATTENDANCE, HOMEWORK, DIARY, TIME_TABLE, REPORT_CARD, CIRCULARS, CHANGE_CLASS, SETTINGS, SURVEY}
}
