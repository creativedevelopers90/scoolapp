package com.scool.qt.parent.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.ParentHomeworkListAdapter;
import com.scool.qt.dao.DatabaseClient;
import com.scool.qt.helper.PrefData;
import com.scool.qt.model.ParentLoginResponse;
import com.scool.qt.model.StudentLoginResponse;
import com.scool.qt.model.parent.HomeWork;
import com.scool.qt.model.parent.ParentHomeworkResponse;
import com.scool.qt.model.room.Homework;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;
import com.scool.qt.widgets.CalendarSliderView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParentHomeworkFragment extends Fragment implements CalendarSliderView.CalenderSliderViewListener, NetworkView, ParentHomeworkListAdapter.IItemClick {
    @BindView(R.id.parent_homework_calender_view)
    CalendarSliderView homeworkCalendarCV;
    @BindView(R.id.no_homework_view)
    RelativeLayout noHomeworkView;
    @BindView(R.id.parent_homework_rv)
    RecyclerView homeworkRV;
    @BindView(R.id.no_homework_tv)
    TextView noHomeworkTV;
    private View root;
    private Calendar homeworkDate;
    private NetworkPresenter mNetworkPresenter;
    LiveData<List<Homework>> databaseClient;

    List<Homework> homeworkList = new ArrayList<>();
    ParentHomeworkListAdapter mAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_parent_homework, container, false);
        ButterKnife.bind(this, root);
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        initView();
         databaseClient =  DatabaseClient.getInstance(getActivity()).getAppDatabase().homeworkDao().getAll();
         databaseClient.observe(getViewLifecycleOwner(), homework -> {
             Log.d("ITEM" , homework.toString());
             if(homework != null) {
                 this.homeworkList = homework;
             } else {
                 this.homeworkList = new ArrayList<>();
             }
         });
        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    /**
     * Method to initialize view
     */
    private void initView() {
        homeworkCalendarCV.setOnDateChangeListener(this, getString(R.string.kHomeworkText));
        homeworkRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        homeworkRV.setHasFixedSize(true);
        noHomeworkTV.setText("No Home Work on " + Calendar.getInstance().get(Calendar.DAY_OF_MONTH) +
                "-" + (Calendar.getInstance().get(Calendar.MONTH) + 1) + "-" + Calendar.getInstance().get(Calendar.YEAR));

        fetchHomeworkDetails(homeworkDate = Calendar.getInstance());
    }

    /**
     * Method to fetch homework details
     */
    private void fetchHomeworkDetails(Calendar calendar) {
        if (mNetworkPresenter != null) {
            if(new PrefData(getActivity()).getLoginType() == Constant.LOGIN_TYPE_PARENT){
                if(Constant.ACTIVE_STUDENT != null)
                mNetworkPresenter.methodToFetchParentHomeworkDetails(getActivity(), Constant.UNIQUE_SCHOOL_ID,
                        Constant.ACTIVE_STUDENT.getStudentId(), calendar.getTime(), loadParentData().getToken());
            }else if(new PrefData(getActivity()).getLoginType() == Constant.LOGIN_TYPE_STUDENT){
                if(Constant.ACTIVE_STUDENT != null)
                mNetworkPresenter.methodToFetchParentHomeworkDetails(getActivity(), Constant.UNIQUE_SCHOOL_ID,
                       loadStudentData().getStudentId(), calendar.getTime(), loadStudentData().getToken());
            }

        }
    }

    /**
     * Method to load the parent data
     *
     * @return
     */
    private ParentLoginResponse loadParentData() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson.fromJson(json, ParentLoginResponse.class);
    }

    /**
     * Method to load the parent data
     *
     * @return
     */
    private StudentLoginResponse loadStudentData() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson.fromJson(json, StudentLoginResponse.class);
    }

    @Override
    public void onDateChanged(Calendar cal) {
        homeworkDate = cal;
        fetchHomeworkDetails(cal);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        Log.e("== " + endpointType + " ==>", message + " : " + object.toString());
        switch (endpointType) {
            case parent_homework_details:
                ParentHomeworkResponse homework = (ParentHomeworkResponse) object;
                if (homework.getHomeWork().size() > 0) {
                    homework.setHomeworkDate(homework.getHomeWork().get(0).getDate());
                }
                if (homework.getHomeWork().isEmpty()) {
                    noHomeworkView.setVisibility(View.VISIBLE);
                    homeworkRV.setVisibility(View.GONE);
                    noHomeworkTV.setText("No Home Work on " + homeworkDate.get(Calendar.DAY_OF_MONTH) +
                            "-" + (homeworkDate.get(Calendar.MONTH) + 1) + "-" + homeworkDate.get(Calendar.YEAR));
                } else {
                    noHomeworkView.setVisibility(View.GONE);
                    homeworkRV.setVisibility(View.VISIBLE);
                    List<HomeWork> listOfHomework;
                    if(homeworkList.size() > 0) {
                        listOfHomework = homework.getHomeWork().stream()
                                .filter(two -> homeworkList.stream()
                                        .anyMatch(one -> one.getHomework_id().equals(two.getHomeworkId())))
                                .collect(Collectors.toList());
                        listOfHomework.stream().forEach(item -> item.setChecked(true));
                    } else {
                        listOfHomework = homework.getHomeWork();
                    }

                    mAdapter = new ParentHomeworkListAdapter(getActivity(),  listOfHomework, homeworkList, this);
                    homeworkRV.setAdapter(mAdapter);
                }
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        Log.e("== " + endpointType + " ==>", message);
    }

    @Override
    public void itemClick(Homework homework, int position, String operation) {


    }
}
