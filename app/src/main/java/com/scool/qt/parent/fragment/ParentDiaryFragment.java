package com.scool.qt.parent.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.ParentDiaryListAdapter;
import com.scool.qt.model.ParentLoginResponse;
import com.scool.qt.model.StudentLoginResponse;
import com.scool.qt.model.parent.ParentDiaryResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;
import com.scool.qt.widgets.CalendarSliderView;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class ParentDiaryFragment extends Fragment implements CalendarSliderView.CalenderSliderViewListener, NetworkView {
    @BindView(R.id.parent_diary_calender_view)
    CalendarSliderView diaryCalendarCV;
    @BindView(R.id.no_diary_view)
    RelativeLayout noDiaryView;
    @BindView(R.id.parent_diary_rv)
    RecyclerView diaryRV;
    @BindView(R.id.no_diary_tv)
    TextView noDiaryTV;
    private View root;
    private Calendar diaryDate;
    private NetworkPresenter mNetworkPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_parent_diary, container, false);
        ButterKnife.bind(this, root);
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        initView();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchDiaryDetails(diaryDate = Calendar.getInstance());
        Timber.d("Parent Diary Fragment On Resume");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Timber.d("Parent Diary Fragment onAttach");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Timber.d("Parent Diary Fragment onDetach");
    }

    /**
     * Method to initialize view
     */
    private void initView() {
        diaryCalendarCV.setOnDateChangeListener(this, getString(R.string.kDiaryText));
        diaryRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        diaryRV.setHasFixedSize(true);
        noDiaryTV.setText(getString(R.string.kNoDiaryfor) + Calendar.getInstance().get(Calendar.DAY_OF_MONTH) +
                "-" + (Calendar.getInstance().get(Calendar.MONTH) + 1) + "-" + Calendar.getInstance().get(Calendar.YEAR));
        fetchDiaryDetails(diaryDate = Calendar.getInstance());
    }

    /**
     * Method to fetch diary details
     */
    private void fetchDiaryDetails(Calendar calendar) {
        if (mNetworkPresenter != null) {
            if (Constant.ACTIVE_STUDENT != null) {
                mNetworkPresenter.methodToFetchParentDiaryDetails(getActivity(), Constant.UNIQUE_SCHOOL_ID,
                        Constant.ACTIVE_STUDENT.getStudentId(), calendar.getTime(), loadParentData().getToken());
            } else {
                mNetworkPresenter.methodToFetchParentDiaryDetails(getActivity(), Constant.UNIQUE_SCHOOL_ID,
                        loadStudentData().getStudentId(), calendar.getTime(), loadStudentData().getToken());
            }

        }
    }

    /**
     * Method to load the parent data
     *
     * @return
     */
    private ParentLoginResponse loadParentData() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson.fromJson(json, ParentLoginResponse.class);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Timber.d("Parent Diary Fragment");
    }

    /**
     * Method to load the parent data
     *
     * @return
     */
    private StudentLoginResponse loadStudentData() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson.fromJson(json, StudentLoginResponse.class);
    }

    @Override
    public void onDateChanged(Calendar cal) {
        diaryDate = cal;
        fetchDiaryDetails(cal);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {

    }


    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case parent_diary_details:
                ParentDiaryResponse diary = (ParentDiaryResponse) object;
                if (diary.getDiaries().isEmpty()) {
                    noDiaryView.setVisibility(View.VISIBLE);
                    diaryRV.setVisibility(View.GONE);
                    noDiaryTV.setText(getString(R.string.kNoDiaryfor) + diaryDate.get(Calendar.DAY_OF_MONTH) +
                            "-" + (diaryDate.get(Calendar.MONTH) + 1) + "-" + diaryDate.get(Calendar.YEAR));
                } else {
                    noDiaryView.setVisibility(View.GONE);
                    diaryRV.setVisibility(View.VISIBLE);
                    ParentDiaryListAdapter mAdapter = new ParentDiaryListAdapter(getActivity(), diary.getDiaries());
                    diaryRV.setAdapter(mAdapter);
                }
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
    }

}
