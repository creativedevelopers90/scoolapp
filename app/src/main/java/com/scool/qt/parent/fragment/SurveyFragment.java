package com.scool.qt.parent.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.activity.ParentHomeActivity;
import com.scool.qt.adapter.SurveyListAdapter;
import com.scool.qt.base.fragment.BaseFragment;
import com.scool.qt.model.ParentLoginResponse;
import com.scool.qt.model.SurveyListResponse;
import com.scool.qt.model.SurveyObject;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;
import com.scool.qt.widgets.ProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SurveyFragment extends BaseFragment implements NetworkView, SurveyListAdapter.ItemClick {
    @BindView(R.id.attendance_list_tv)
    TextView attendanceListTv;
    @BindView(R.id.attendance_list_header)
    RelativeLayout attendanceListHeader;
    @BindView(R.id.attendance_list_rv)
    RecyclerView attendanceListRv;
    @BindView(R.id.cl_maincontent)
    ConstraintLayout clMaincontent;
    @BindView(R.id.iv_nointernet)
    ImageView ivNointernet;
    @BindView(R.id.rl_nointernet)
    RelativeLayout rlNointernet;
    @BindView(R.id.image_view)
    ImageView imageView;
    @BindView(R.id.textview)
    TextView textview;
    @BindView(R.id.rl_view)
    RelativeLayout rlView;
    @BindView(R.id.btn_done)
    AppCompatButton btnDone;
    private NetworkPresenter mPresenter;
    Unbinder unbinder;
    List<SurveyObject> mSurveyListResponse = new ArrayList<>();
    private SurveyListAdapter mAdapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_survey, container, false);
        unbinder = ButterKnife.bind(this, root);
        mPresenter = new NetworkPresenter(this, new NetworkInteractor());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        attendanceListRv.setLayoutManager(layoutManager);
        attendanceListRv.setHasFixedSize(true);
        mAdapter = new SurveyListAdapter(getActivity(), SurveyFragment.this);
        attendanceListRv.setAdapter(mAdapter);
        btnDone.setVisibility(View.GONE);
        getSurveyList();

        return root;
    }

    private void getSurveyList() {
            mPresenter.methodToGetSurveyList(getActivity(), loadParentData().getToken(), Constant.UNIQUE_SCHOOL_ID);
    }

    /**
     * Method to load the parent data
     *
     * @return
     */
    private ParentLoginResponse loadParentData() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson.fromJson(json, ParentLoginResponse.class);
    }

    @Override
    public void showProgress() {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(getActivity(), false, "");
        progressDialog.show();
    }

    @Override
    public void hideProgress() {


    }

    @Override
    public void noInternetConnection() {
            rlNointernet.setVisibility(View.VISIBLE);
            rlView.setVisibility(View.GONE);
            clMaincontent.setVisibility(View.GONE);

    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        if (endpointType == EndpointEnums.survey_list) {
            progressDialog.dismiss();
            SurveyListResponse listresponse = (SurveyListResponse) object;
            if(listresponse.getSurveys().size() > 0) {
                this.mSurveyListResponse = listresponse.getSurveys();
                mAdapter.addItemsInList(this.mSurveyListResponse);
            } else {
                rlView.setVisibility(View.VISIBLE);
                textview.setText(getString(R.string.knosurveysdisplay));
                attendanceListTv.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        if (endpointType == EndpointEnums.survey_list) {
            progressDialog.dismiss();
            rlView.setVisibility(View.VISIBLE);
            textview.setText(getString(R.string.knosurveysdisplay));
            attendanceListTv.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemSelected(SurveyObject mItem) {
        QuestionsFragment nextFrag = new QuestionsFragment();
        Bundle args = new Bundle();
        ((ParentHomeActivity) getActivity()).updateToolbar(mItem.getName(), false);
        args.putString("surveyId", String.valueOf(mItem.getId()));
        nextFrag.setArguments(args);
//        getActivity().getSupportFragmentManager().beginTransaction()
//                .replace(R.id.content_frame, nextFrag)
//                .addToBackStack(getString(R.string.ksurveyText))
//                .commit();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, nextFrag,
                nextFrag.getTag()).addToBackStack(getString(R.string.ksurveyText)).commit();
    }
}
