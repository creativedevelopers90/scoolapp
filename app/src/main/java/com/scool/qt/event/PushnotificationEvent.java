package com.scool.qt.event;

public class PushnotificationEvent {
    private String hwbadge;
    private String sbadge;
    private String dbadge;
    private String abadge;
    private String ebadge;
    private String mbadge;
    private String badge;

    /**
     *
     * @param hwbadge
     * @param sbadge
     * @param dbadge
     * @param abadge
     * @param ebadge
     * @param mbadge
     */
    public PushnotificationEvent(String hwbadge, String sbadge, String dbadge, String abadge, String ebadge, String mbadge) {
        this.hwbadge = hwbadge;
        this.sbadge = sbadge;
        this.dbadge = dbadge;
        this.abadge = abadge;
        this.ebadge = ebadge;
        this.mbadge = mbadge;
    }

    public PushnotificationEvent(String hwbadge, String dbadge) {
        this.hwbadge = hwbadge;
        this.dbadge = dbadge;
    }

    public String getHwbadge() {
        return hwbadge;
    }

    public void setHwbadge(String hwbadge) {
        this.hwbadge = hwbadge;
    }

    public String getSbadge() {
        return sbadge;
    }

    public void setSbadge(String sbadge) {
        this.sbadge = sbadge;
    }

    public String getDbadge() {
        return dbadge;
    }

    public void setDbadge(String dbadge) {
        this.dbadge = dbadge;
    }

    public String getAbadge() {
        return abadge;
    }

    public void setAbadge(String abadge) {
        this.abadge = abadge;
    }

    public String getEbadge() {
        return ebadge;
    }

    public void setEbadge(String ebadge) {
        this.ebadge = ebadge;
    }

    public String getMbadge() {
        return mbadge;
    }

    public void setMbadge(String mbadge) {
        this.mbadge = mbadge;
    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }
}
