package com.scool.qt;

public enum EndpointEnums {
    about_us, contact_us, login, start, school_faculty, school_circular,
    faculty_info, faculty_class, faculty_change_password, faculty_student,
    faculty_diary_list, faculty_add_diary, faculty_delete_diary, faculty_update_diary,
    faculty_homework_list, faculty_add_homework, faculty_update_homework, faculty_delete_homework,
    faculty_subject_list, exam_list, marks_list, faculty_save_marks, faculty_update_marks, faculty_class_attendance,
    faculty_update_multiple_attendance, faculy_get_class_time_table, faculty_logout, parent_details,
    student_details, parent_homework_details, parent_diary_reply, parent_diary_details, parent_report_card_status,
    parent_time_table_details, parent_attendance_details, parent_change_password, parent_logout, class_lists_for_Delegation,
    add_delegation, survey_list, question_list, students_logout, attendance_all_periods, update_survey, update_attendance, update_delegation;

    EndpointEnums() {
    }


}
