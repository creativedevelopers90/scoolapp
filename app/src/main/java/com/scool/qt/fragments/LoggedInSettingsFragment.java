package com.scool.qt.fragments;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.base.activity.ChangeLanguageActivity;
import com.scool.qt.base.activity.ChangePasswordActivity;
import com.scool.qt.base.activity.ChangeThemeActivity;
import com.scool.qt.base.activity.HomeActivity;
import com.scool.qt.base.fragment.BaseFragment;
import com.scool.qt.faculty.activity.ClassDelegationActivity;
import com.scool.qt.helper.PrefData;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.LogoutResponse;
import com.scool.qt.model.ParentLoginResponse;
import com.scool.qt.model.StudentLoginResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;
import com.scool.qt.widgets.LogoutDialog;
import com.scool.qt.widgets.ThemeButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
public class LoggedInSettingsFragment extends BaseFragment implements NetworkView, LogoutDialog.LogoutDialogListener {
    Unbinder unbinder;
    PrefData prefData;
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;
    @BindView(R.id.img_changelang)
    RecyclingImageView imgChangelang;
    @BindView(R.id.tv_changelang)
    TextView tvChangelang;
    @BindView(R.id.view_changelang)
    View viewChangelang;
    @BindView(R.id.change_language)
    ConstraintLayout changeLanguage;
    @BindView(R.id.img_chngethmem)
    RecyclingImageView imgChngethmem;
    @BindView(R.id.tv_chngtheme)
    TextView tvChngtheme;
    @BindView(R.id.view_thme)
    View viewThme;
    @BindView(R.id.change_theme)
    ConstraintLayout changeTheme;
    @BindView(R.id.setting_footer)
    AppCompatTextView settingFooter;
    @BindView(R.id.faculty_logout_btn)
    ThemeButton facultyLogoutBtn;
    @BindView(R.id.cns_deletegation)
    ConstraintLayout cnsDeletegation;
    private View root;
    private NetworkPresenter mNetworkPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_logged_in_settings, container, false);
        unbinder = ButterKnife.bind(this, root);
        prefData = new PrefData(getActivity());
        switch (prefData.getLoginType()) {
            case Constant.LOGIN_TYPE_FACULTY:
                cnsDeletegation.setVisibility(View.VISIBLE);
                break;
            case Constant.LOGIN_TYPE_PARENT:
            case Constant.LOGIN_TYPE_STUDENT:
                cnsDeletegation.setVisibility(View.GONE);
                break;
        }
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        tvSchoolname.setText(Utils.loadSchoolData(getActivity()).getName());
        Glide.with(getActivity())
                .load(Constant.IMAGE_BASE_URL + Utils.loadSchoolData(getActivity()).getLogo())
                .thumbnail(0.5f)
                .into(imgSchool);
        settingFooter.setText(getActivity().getString(R.string.kCopyrightText) + "\nApp Version - 1.0");
        facultyLogoutBtn.setBackground(getActivity().getDrawable(buttonThemeId));
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.change_password)
    void onPasswordChange() {
        Intent i = new Intent(getActivity(), ChangePasswordActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.change_language)
    void onLanguageBtnClick() {
        Intent i = new Intent(getActivity(), ChangeLanguageActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.change_theme)
    void onChangeTheme() {
        Intent i = new Intent(getActivity(), ChangeThemeActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.faculty_logout_btn)
    void onLogoutButtonClicked() {
        if (Utils.isNetworkAvailable(getActivity())) {
            logoutAlertDialog(getString(R.string.kLogout), getString(R.string.kLogOutConfirmationText));
        } else {
            alertDialog(getString(R.string.kalert), getString(R.string.no_internet_connection));
        }

    }

    @OnClick(R.id.cns_deletegation)
    void onDeleteDelegation() {
        Intent i = new Intent(getActivity(), ClassDelegationActivity.class);
        startActivity(i);
    }

    /**
     * Method to load the faculty data
     *
     * @return
     */
    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }

    /**
     * Method to load the parent data
     *
     * @return
     */
    private ParentLoginResponse loadParentData() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson.fromJson(json, ParentLoginResponse.class);
    }


    /**
     * Method to load the parent data
     *
     * @return
     */
    private StudentLoginResponse loadStudentData() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", getActivity());
        return gson.fromJson(json, StudentLoginResponse.class);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        Utils.showSnackBar(getActivity(), ((LogoutResponse) object).getMessage());
        Utils.clearPreferences(Constant.PREF_LOGGED_IN_DATA, getActivity());
        new PrefData(getActivity()).clearLoginPreference();
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        Utils.showSnackBar(getActivity(), message);
    }

    @Override
    public void onLogoutConfirmClicked() {
        switch (prefData.getLoginType()) {
            case Constant.LOGIN_TYPE_FACULTY:
                FacultyLoginResponse facultyLoginResponse = loadFacultyData();
                mNetworkPresenter.methodToLogoutAsFaculty(getActivity(), facultyLoginResponse.getToken());
                break;
            case Constant.LOGIN_TYPE_PARENT:
                mNetworkPresenter.methodToLogoutAsParent(getActivity(), loadParentData().getToken());
                break;

            case Constant.LOGIN_TYPE_STUDENT:
                mNetworkPresenter.methodToLogoutAsStudent(getActivity(), loadStudentData().getToken());
                break;
        }
    }

    @Override
    public void onLogoutDenyClicked() {
    }

    public void logoutAlertDialog(String title, String message) {
        // create a dialog with AlertDialog builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        builder.setTitle(title);
        builder.setMessage(message);
        String positiveText = getString(R.string.kokay);
        builder.setPositiveButton(positiveText,
                (dialog, which) -> {
                    switch (prefData.getLoginType()) {
                        case Constant.LOGIN_TYPE_FACULTY:
                            FacultyLoginResponse facultyLoginResponse = loadFacultyData();
                            mNetworkPresenter.methodToLogoutAsFaculty(getActivity(),
                                    facultyLoginResponse.getToken());
                            break;
                        case Constant.LOGIN_TYPE_PARENT:
                            ParentLoginResponse parentLoginResponse = loadParentData();
                            mNetworkPresenter.methodToLogoutAsParent(getActivity(), parentLoginResponse.getToken());
                            dialog.dismiss();
                            break;
                        case Constant.LOGIN_TYPE_STUDENT:
                            StudentLoginResponse studentLoginResponse = loadStudentData();
                            mNetworkPresenter.methodToLogoutAsStudent(getActivity(), studentLoginResponse.getToken());
                            dialog.dismiss();
                            break;
                    }

                });

        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                (dialog, which) -> {
                    dialog.dismiss();
                    // dismiss dialog, start counter again
                });

        AlertDialog dialog = builder.create();
// display dialog
        dialog.show();
    }
}
