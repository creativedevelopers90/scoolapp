package com.scool.qt.fragments;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.HomePageAdapter;
import com.scool.qt.base.activity.HomeActivity;
import com.scool.qt.custom.AutoScrollViewPager;
import com.scool.qt.model.StartResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 *
 */
public class HomeFragment extends Fragment implements NetworkView, ViewPager.OnPageChangeListener {
    private static final int AUTO_SCROLL_THRESHOLD_IN_MILLI = 1000;
    Unbinder unbinder;

    HomePageAdapter mHomeAdapter;
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;
    @BindView(R.id.tv_school_slogan)
    TextView tvSchoolSlogan;
    @BindView(R.id.layout_top)
    ConstraintLayout layoutTop;
    @BindView(R.id.view_pager)
    AutoScrollViewPager viewPager;
    @BindView(R.id.viewPagerCountDots)
    LinearLayout viewPagerCountDots;
    HomeActivity homeActivity;
    private NetworkPresenter mApiPresenter;
    private List<String> images;
    private int dotsCount;
    private ImageView[] dots;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mApiPresenter = new NetworkPresenter(this, new NetworkInteractor());
        homeActivity = (HomeActivity) getActivity();
        getStartScreenRecord();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        images = new ArrayList<>();
        mHomeAdapter = new HomePageAdapter(getActivity(), images);
        viewPager.setAdapter(mHomeAdapter);
        viewPager.addOnPageChangeListener(this);
        viewPager.setCurrentItem(0);
        dotsCount = images.size();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if (getView() != null && viewPager != null) {
            viewPager.stopAutoScroll();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
        Utils.showSnackBar(getActivity(), getString(R.string.check_internet));
    }

    @Override
    public void onResume() {
        super.onResume();
        // start auto scroll when onResume
    }

    @Override
    public void onStop() {
        super.onStop();
        if (getView() != null) {
            viewPager.stopAutoScroll();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getView() != null) {
            viewPager.stopAutoScroll();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu();
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case start:
                if (getView() != null) {
                    StartResponse startResponse = (StartResponse) object;
                    Utils.saveSchoolData(getActivity(), startResponse);
                    images = startResponse.getImages();
                    tvSchoolname.setText(startResponse.getName());
                    tvSchoolSlogan.setText(startResponse.getSlogan());
                    homeActivity.update("updateActivity");
                    Glide.with(getActivity())
                            .load(Constant.IMAGE_BASE_URL + Utils.loadSchoolData(getActivity()).getLogo())
                            .thumbnail(0.5f)
                            .into(imgSchool);
                    dotsCount = images.size();
                    setUiPageViewController(viewPagerCountDots);
                    mHomeAdapter.setElements(images);
                    viewPager.startAutoScroll();
                    viewPager.setInterval(AUTO_SCROLL_THRESHOLD_IN_MILLI);
                    viewPager.setCycle(true);
                }
                break;
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case start:
                if (getView() != null)
                    Utils.showSnackBar(getActivity(), message);
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (getView() != null) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageDrawable(getActivity().getResources().getDrawable(R.drawable.non_selected_item_dot));
            }
            dots[position].setImageDrawable(getActivity().getResources().getDrawable(R.drawable.selected_item_dot));
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
    }

    public void getStartScreenRecord() {
        mApiPresenter.methodToGetSchoolHomeData(getActivity(), Constant.UNIQUE_SCHOOL_ID);
    }

    /**
     * Method to Set UI View PAger Controller
     *
     * @param viewPagerCountDots
     */
    private void setUiPageViewController(LinearLayout viewPagerCountDots) {
        dots = new ImageView[0];
        viewPagerCountDots.removeAllViews();
        dots = new ImageView[dotsCount];
        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(getActivity().getResources().getDrawable(R.drawable.non_selected_item_dot));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(15, 0, 15, 0);
            viewPagerCountDots.addView(dots[i], params);
        }
        dots[0].setImageDrawable(getActivity().getResources().getDrawable(R.drawable.selected_item_dot));
    }

    public interface IFragmentToActivity {
        void update(String msg);
    }

}
