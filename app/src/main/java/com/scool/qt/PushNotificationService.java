package com.scool.qt;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.scool.qt.activity.ParentHomeActivity;
import com.scool.qt.base.activity.HomeActivity;
import com.scool.qt.event.PushnotificationEvent;
import com.scool.qt.helper.PrefData;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Random;

import timber.log.Timber;

public class PushNotificationService extends FirebaseMessagingService {
    private NotificationManager notificationManager;
    private  String NOTIFICATION_ID_EXTRA = "notificationId";
    private  String IMAGE_URL_EXTRA = "imageUrl";
    private  String ADMIN_CHANNEL_ID = "101";
    public PushNotificationService() {
    }

    @Override
    public void onNewToken(String token) {
        Timber.d("Registration token" + token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
      //  Timber.d("PUSH MESSAGE RECEIVED" +  remoteMessage.getNotification().getBody());
        Intent notificationIntent;

        Map<String, String> params = remoteMessage.getData();

        JSONObject jsonNotification = null;
        JSONObject jsonDataObject = null;
        String title = "";
        String message = "";
        try {
            String  jsonObject = params.get("notification");
            jsonNotification = new JSONObject(jsonObject);
            jsonDataObject = new JSONObject(params.get("data"));
            title = jsonNotification.get("title").toString();
            message = jsonNotification.get("body").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(new PrefData(getApplicationContext()).getLoginStatus()){
            notificationIntent = new Intent(this, ParentHomeActivity.class);
        }else{
            notificationIntent = new Intent(this, HomeActivity.class);
        }
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        try {
            EventBus.getDefault().post(new PushnotificationEvent(jsonDataObject.get("hwbadge").toString(),
                    jsonDataObject.get("sbadge").toString(),
                    jsonDataObject.get("dbadge").toString(), jsonDataObject.get("abadge").toString(),
                    jsonDataObject.get("ebadge").toString(), jsonDataObject.get("mbadge").toString()));
            notificationIntent.putExtra("fromNotification", true);
            notificationIntent.putExtra("hwbadge", jsonDataObject.get("hwbadge").toString());
            notificationIntent.putExtra("sbadge", jsonDataObject.get("sbadge").toString());
            notificationIntent.putExtra("dbadge", jsonDataObject.get("dbadge").toString());
            notificationIntent.putExtra("abadge", jsonDataObject.get("abadge").toString());
            notificationIntent.putExtra("ebadge", jsonDataObject.get("ebadge").toString());
            notificationIntent.putExtra("mbadge", jsonDataObject.get("mbadge").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_ONE_SHOT);
        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        //Setting up Notification channels for android O and above
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setupChannels();
        }
        int notificationId = new Random().nextInt(60000);

        Timber.d("PUSH MESSAGE RECEIVED >"+ params.get("notification"));
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setSmallIcon(R.drawable.default_notif_icon)  //a resource for your custom small icon
                .setContentTitle(title) //the "title" value you sent in your notification
                .setContentText(message) //ditto
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.app_icon))
                .setAutoCancel(true)  //dismisses the notification on click
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels(){
        CharSequence adminChannelName = getString(R.string.notifications_admin_channel_name);
        String adminChannelDescription = getString(R.string.notifications_admin_channel_description);

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_DEFAULT);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }
}
