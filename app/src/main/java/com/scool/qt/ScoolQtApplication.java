package com.scool.qt;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.multidex.BuildConfig;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import com.google.firebase.messaging.FirebaseMessaging;
import com.scool.qt.helper.LocaleHelper;
import com.scool.qt.utils.FakeCrashLibrary;

import timber.log.Timber;

public class ScoolQtApplication extends MultiDexApplication {
    private static ScoolQtApplication applicationInstance;
    LocaleHelper localeHelper = new LocaleHelper();


    public static synchronized ScoolQtApplication getInstance() {
        if(applicationInstance == null) {
            applicationInstance = new ScoolQtApplication();
        }
        return applicationInstance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(localeHelper.onAttach(base, "en"));
        MultiDex.install(this);
    }

    public Resources initAppLanguage(Context context) {
        LocaleHelper.setLocale(context, LocaleHelper.getLanguage(context));
        return context.getResources();
       // LocaleHelper.initialize(context, LocaleUtils.getSelectedLanguageId());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseMessaging.getInstance().isAutoInitEnabled();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashReportingTree());
        }


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /** A tree which logs important information for crash reporting. */
    private static class CrashReportingTree extends Timber.Tree {
        @Override protected void log(int priority, String tag, @NonNull String message, Throwable t) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return;
            }

            FakeCrashLibrary.log(priority, tag, message);

            if (t != null) {
                if (priority == Log.ERROR) {
                    FakeCrashLibrary.logError(t);
                } else if (priority == Log.WARN) {
                    FakeCrashLibrary.logWarning(t);
                }
            }
        }
    }
}
