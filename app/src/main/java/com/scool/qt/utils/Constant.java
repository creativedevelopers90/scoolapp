package com.scool.qt.utils;

import com.scool.qt.model.ClassList;
import com.scool.qt.model.FacultyInfoResponse;
import com.scool.qt.model.StudentDetailsResponse;
import com.scool.qt.model.parent.ParentDetailResponse;
import com.scool.qt.model.parent.Student;

public class Constant {
    public static final String UNIQUE_SCHOOL_ID = "1234567890ABCD";
    public static final Long UNIQUE_TIMETABLE_ID = 40l;
    public static final Double MAXIMUM_MARKS = 100.0;
    public static final String API_BASE_URL = "https://www.schoolsqatar.com/api/v1/";
    public static final String IMAGE_BASE_URL = "https://www.schoolsqatar.com";
    public static final boolean DEBUG = Boolean.parseBoolean("true");

    // https://www.schoolsqatar.com/api/v1/faculties/class_lists_for_Delegation

    // Exclusive data to Save School data in Shared Prefrences
    public static final String SCHOOL_DATA = "school_data";


    public static final String PREF_NAME = "preference_data";
    public static final String PREF_LOGGED_IN_DATA = "loggedInData";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String PREF_LOGIN_STATUS = "is_login";
    public static final String PREF_LOGIN_TYPE = "login_type";
    public static final String PREF_FACULTY_ID = "facultyID";
    public static final String PREF_AUTH_TOKEN = "authorizationToken";


    // LOGIN_TYPE
    public static String LOGIN_TYPE = "login_type";
    public static final int LOGIN_TYPE_FACULTY = 2;
    public static final int LOGIN_TYPE_STUDENT = 1;
    public static final int LOGIN_TYPE_PARENT = 0;

    // ATTENDANCE_TYPE
    public static final int ATTENDANCE_TYPE_ABSENT = 0;
    public static final int ATTENDANCE_TYPE_PRESENT = 1;
    public static final int ATTENDANCE_TYPE_LEAVE = 2;
    public static final int ATTENDANCE_TYPE_NO_STATUS_MARKED = 4;
    public static final int ATTENDANCE_TYPE_UPDATE = 5;
    public static final int ATTENDANCE_TYPE_SCHOOL_LEAVE = 9;

    //OTHER CONSTANTS
    public static ClassList ACTIVE_CLASS = null;
    public static FacultyInfoResponse facultyData = null;
    public static Student ACTIVE_STUDENT = null;
    public static ParentDetailResponse parentData;
    public static StudentDetailsResponse studentData;

    // QUESTIONS SURVEY
    public static final String RAPID_QUESTION_SELECT = "Rapidfire::Questions::Select";
    public static final String RAPID_QUESTION_SHORT = "Rapidfire::Questions::Short";

    enum ATTENDANCE_TYPE_SMALL {

    }



}
