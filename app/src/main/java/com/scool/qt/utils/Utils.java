package com.scool.qt.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.scool.qt.model.StartResponse;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class Utils {


    /**
     * Method to check Network is available or not
     * @param context the activity or fragment context
     * @return
     */
    public static boolean isNetworkAvailable(Context context){
        boolean isActiveNetwork = false;
        if(context != null){
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null) { // connected to the internet
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    // connected to wifi
                    isActiveNetwork = true;
                } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                    // connected to the mobile provider's data plan
                    isActiveNetwork = true;
                }
            } else {
                // not connected to the internet
            }
        }

        return isActiveNetwork;
    }

    /**
     * Method to Show Snack Bar
     * @param mActivity
     * @param message
     */
    public static void showSnackBar(Activity mActivity, String message){
        View rootView = mActivity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 2000).show();
    }

    /**
     * Metho to Show Toast
     * @param context
     * @param message
     */
    public void showToast(Context context, String message){
        new Handler(Looper.getMainLooper()).post(() -> Toast.makeText(context, message, Toast.LENGTH_SHORT).show());
    }

    public static int getColor(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

    /**
     * Hides the soft keyboard
     */
    public static void hideSoftKeyboard(Context context) {
        if (context != null && ((Activity) context).getCurrentFocus() != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     *
     * @param mActivity
     * @return
     */
    public static StartResponse loadSchoolData(Activity mActivity) {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.SCHOOL_DATA,
                "", mActivity);
        StartResponse userProfile = gson1.fromJson(json1, StartResponse.class);
        return userProfile;
    }

    public static void saveSchoolData(Activity mActivity, StartResponse me){
        Gson gson = new Gson();
        String json = gson.toJson(me);
        Utils.savePreferences(Constant.SCHOOL_DATA,
                json, mActivity);
    }

    /**
     * Save the value in preference
     *
     * @param key
     * @param value
     * @param activity
     */
    public static void savePreferences(String key, String value,
                                       Context activity) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(activity);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * Method to remove preference data
     *
     * @param key
     * @param mContext
     */
    public static void clearPreferences(String key, Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.remove(key);
        editor.apply();
        editor.commit();
    }

    /**
     * get the value from preference
     *
     * @param key
     * @param defaultStr
     * @param activity
     * @return
     */
    public static String loadPrefrence(String key, String defaultStr, Context activity) {
        if(activity != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
            return sharedPreferences.getString(key, defaultStr);
        } else {
            return "Empty";
        }
    }

    public static String convertDtFormat(String mFacultiesItem) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
        String inputDateStr = mFacultiesItem;
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputFormat.format(date);
    }

    public static Date convertStringToDate(String mFacultiesItem) {
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        String inputDateStr = mFacultiesItem;
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * Constant method to hide the keyboard
     *
     * @param activity
     */
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static LocalDate convertDateToLocalDate(long input) {
        java.util.Date safeDate = new Date(input);
        Instant instant = safeDate.toInstant();
        ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
        LocalDate date = zdt.toLocalDate();
        return date;
    }

}
