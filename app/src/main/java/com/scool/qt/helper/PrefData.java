package com.scool.qt.helper;
import android.content.Context;
import android.content.SharedPreferences;
import com.scool.qt.utils.Constant;
public class PrefData {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String FCM_TOKEN = "puchNotificationToken";

    public PrefData(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(Constant.PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public boolean getLoginStatus() {
        return pref.getBoolean(Constant.PREF_LOGIN_STATUS, false);
    }

    public void setLoginStatus(boolean isLogin) {
        editor.putBoolean(Constant.PREF_LOGIN_STATUS, isLogin);
        editor.commit();
    }

    public int getLoginType() {
        return pref.getInt(Constant.PREF_LOGIN_TYPE, -1);
    }

    public void setLoginType(int loginType) {
        editor.putInt(Constant.PREF_LOGIN_TYPE, loginType);
        editor.commit();
    }

    public int getFacultyID() {
        return pref.getInt(Constant.PREF_FACULTY_ID, -1);
    }

    public void setFacultyID(int facultyID) {
        editor.putInt(Constant.PREF_FACULTY_ID, facultyID);
        editor.commit();
    }

    public String getAuthorizationToken() {
        return pref.getString(Constant.PREF_AUTH_TOKEN, null);
    }

    public void setAuthorizationToken(String authorizationToken) {
        editor.putString(Constant.PREF_AUTH_TOKEN, authorizationToken);
        editor.commit();
    }

    public void clearLoginPreference() {
        editor.remove(Constant.PREF_LOGIN_TYPE);
        editor.remove(Constant.PREF_LOGIN_STATUS);
        editor.apply();
        editor.commit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public void setFcmToken(String  fcmToken) {
        editor.putString(FCM_TOKEN, fcmToken);
        editor.commit();
    }

    public String getFcmToken() {
        return pref.getString(FCM_TOKEN, "-1");
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }
}
