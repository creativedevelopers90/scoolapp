package com.scool.qt.helper;

import android.content.Context;
import android.content.SharedPreferences;

public class ThemeHelper {
    private static final String PREF_CURRENT_THEME = "Theme.Helper.Selected.Theme";


    public static String getPrefCurrentTheme(Context context) {
        return getPersistedData(context, "blue");
    }

    public static void setTheme(Context context, String color) {
        persist(context, color);
    }

    private static String getPersistedData(Context context, String defaultLanguage) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_CURRENT_THEME, Context.MODE_PRIVATE);
        return preferences.getString(PREF_CURRENT_THEME, defaultLanguage);
    }

    private static void persist(Context context, String language) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_CURRENT_THEME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_CURRENT_THEME, language);
        editor.apply();
    }

}
