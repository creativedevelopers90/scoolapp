package com.scool.qt;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.scool.qt.base.activity.HomeActivity;
import com.scool.qt.helper.LocaleHelper;
import com.scool.qt.helper.PrefData;
import com.scool.qt.helper.ThemeHelper;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.LogoutResponse;
import com.scool.qt.model.ParentLoginResponse;
import com.scool.qt.model.StudentLoginResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

public class BaseActivity extends AppCompatActivity implements NetworkView {
    public String theme;
    public PrefData prefData;
    public  NetworkPresenter mNetworkPresenter;
    public int buttonThemeId;
    public int sideNavId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        theme = ThemeHelper.getPrefCurrentTheme(this);
        prefData = new PrefData(BaseActivity.this);
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        if(theme.equalsIgnoreCase("blue")){
            setTheme(R.style.Theme_App_Blue);
            sideNavId = R.drawable.gd_side_nav_bar_blue;
            buttonThemeId = R.drawable.btn_rounded_cornor_less;
        } else if(theme.equalsIgnoreCase("red")) {
            setTheme(R.style.Theme_App_Red);
            sideNavId = R.drawable.gd_side_nav_bar_red;
            buttonThemeId = R.drawable.btn_rounded_red_cornor;
        } else if(theme.equalsIgnoreCase("green")) {
            sideNavId = R.drawable.gd_side_nav_bar_green;
            setTheme(R.style.Theme_App_Green);
            buttonThemeId = R.drawable.btn_rounded_green_cornor;
        } else if(theme.equalsIgnoreCase("purple")) {
            sideNavId = R.drawable.gd_side_nav_bar_purple;
            setTheme(R.style.Theme_App_Purple);
            buttonThemeId = R.drawable.btn_rounded_purple_cornor;
        }else if(theme.equalsIgnoreCase("black")) {
            sideNavId = R.drawable.gd_side_nav_bar_black;
            setTheme(R.style.Theme_App_Black);
            buttonThemeId = R.drawable.btn_rounded_black_cornor;
        }else if(theme.equalsIgnoreCase("brown")) {
            sideNavId = R.drawable.gd_side_nav_bar_brown;
            setTheme(R.style.Theme_App_Brown);
            buttonThemeId = R.drawable.btn_rounded_brown_cornor;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public  void logoutAlertDialog(String title, String message) {
        // create a dialog with AlertDialog builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        builder.setTitle(title);
        builder.setMessage(message);

        String positiveText = getString(R.string.kokay);
        builder.setPositiveButton(positiveText,
                (dialog, which) -> {
            switch (prefData.getLoginType()) {
                case Constant.LOGIN_TYPE_FACULTY:
                    FacultyLoginResponse facultyLoginResponse = loadFacultyData();
                    mNetworkPresenter.methodToLogoutAsFaculty(BaseActivity.this,
                            facultyLoginResponse.getToken());
                    break;
                case Constant.LOGIN_TYPE_PARENT:
                    ParentLoginResponse parentLoginResponse = loadParentData();
                    mNetworkPresenter.methodToLogoutAsParent(this, parentLoginResponse.getToken());
                    dialog.dismiss();
                    break;
                case Constant.LOGIN_TYPE_STUDENT:
                    StudentLoginResponse studentLoginResponse = loadStudentData();
                    mNetworkPresenter.methodToLogoutAsStudent(this, studentLoginResponse.getToken());
                    dialog.dismiss();
                    break;
            }

                });

        String negativeText = getResources().getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                (dialog, which) -> {
            dialog.dismiss();
                    // dismiss dialog, start counter again
                });

        AlertDialog dialog = builder.create();
// display dialog
        dialog.show();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        if (endpointType == EndpointEnums.parent_logout || endpointType == EndpointEnums.students_logout) {
            Constant.ACTIVE_STUDENT = null;
            Utils.showSnackBar(this, ((LogoutResponse) object).getMessage());
            Utils.clearPreferences(Constant.IS_LOGGED_IN, this);
            ThemeHelper.setTheme(this, ThemeHelper.getPrefCurrentTheme(this));
            Utils.clearPreferences(Constant.PREF_LOGGED_IN_DATA, BaseActivity.this);
            new PrefData(BaseActivity.this).clearLoginPreference();
            Intent intent = new Intent(BaseActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        Utils.showSnackBar(this, message);
    }

    /**
     * Method to load the faculty data
     *
     * @return
     */
    protected ParentLoginResponse loadParentData() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", this);
        return gson.fromJson(json, ParentLoginResponse.class);
    }

    /**
     * Method to load the parent data
     *
     * @return
     */
    private StudentLoginResponse loadStudentData() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", this);
        return gson.fromJson(json, StudentLoginResponse.class);
    }

    /**
     * Method to load the faculty data
     *
     * @return
     */
    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", this);
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }


    public Resources getLanguageType() {
        Context context = LocaleHelper.setLocale(this, LocaleHelper.getLanguage(this));
        return context.getResources();
    }
}
