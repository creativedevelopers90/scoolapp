package com.scool.qt.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddDelegationRequest {

    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("faculty_id")
    @Expose
    private Integer facultyId;

    @SerializedName("class_list_id")
    @Expose
    private String classListId;
    @SerializedName("delegate_faculty_id")
    @Expose
    private String delegateFacultyId;
    @SerializedName("attdelegate")
    @Expose
    private String attdelegate;
    @SerializedName("markdelegate")
    @Expose
    private String markdelegate;
    @SerializedName("diarydelegate")
    @Expose
    private String diarydelegate;
    @SerializedName("hwdelegate")
    @Expose
    private String hwdelegate;
    @SerializedName("fromdate")
    @Expose
    private String fromdate;
    @SerializedName("todate")
    @Expose
    private String todate;

    @SerializedName("id")
    @Expose
    private String id;

    /***
     *
     * @param schoolId
     * @param facultyId
     * @param classListId
     * @param delegateFacultyId
     * @param attdelegate
     * @param markdelegate
     * @param diarydelegate
     * @param hwdelegate
     * @param fromdate
     * @param todate
     */
    public AddDelegationRequest(String schoolId, Integer facultyId, String classListId,
                                String delegateFacultyId, String attdelegate, String markdelegate, String diarydelegate, String hwdelegate, String fromdate, String todate) {
        this.schoolId = schoolId;
        this.facultyId = facultyId;
        this.classListId = classListId;
        this.delegateFacultyId = delegateFacultyId;
        this.attdelegate = attdelegate;
        this.markdelegate = markdelegate;
        this.diarydelegate = diarydelegate;
        this.hwdelegate = hwdelegate;
        this.fromdate = fromdate;
        this.todate = todate;
    }

    public AddDelegationRequest(String schoolId, Integer facultyId, String classListId, String delegateFacultyId, String attdelegate, String markdelegate, String diarydelegate,
                                String hwdelegate, String fromdate, String todate, String id) {
        this.schoolId = schoolId;
        this.facultyId = facultyId;
        this.classListId = classListId;
        this.delegateFacultyId = delegateFacultyId;
        this.attdelegate = attdelegate;
        this.markdelegate = markdelegate;
        this.diarydelegate = diarydelegate;
        this.hwdelegate = hwdelegate;
        this.fromdate = fromdate;
        this.todate = todate;
        this.id = id;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Integer facultyId) {
        this.facultyId = facultyId;
    }

    public String getClassListId() {
        return classListId;
    }

    public void setClassListId(String classListId) {
        this.classListId = classListId;
    }

    public String getDelegateFacultyId() {
        return delegateFacultyId;
    }

    public void setDelegateFacultyId(String delegateFacultyId) {
        this.delegateFacultyId = delegateFacultyId;
    }

    public String getAttdelegate() {
        return attdelegate;
    }

    public void setAttdelegate(String attdelegate) {
        this.attdelegate = attdelegate;
    }

    public String getMarkdelegate() {
        return markdelegate;
    }

    public void setMarkdelegate(String markdelegate) {
        this.markdelegate = markdelegate;
    }

    public String getDiarydelegate() {
        return diarydelegate;
    }

    public void setDiarydelegate(String diarydelegate) {
        this.diarydelegate = diarydelegate;
    }

    public String getHwdelegate() {
        return hwdelegate;
    }

    public void setHwdelegate(String hwdelegate) {
        this.hwdelegate = hwdelegate;
    }

    public String getFromdate() {
        return fromdate;
    }

    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "AddDelegationRequest{" +
                "schoolId='" + schoolId + '\'' +
                ", facultyId=" + facultyId +
                ", classListId='" + classListId + '\'' +
                ", delegateFacultyId='" + delegateFacultyId + '\'' +
                ", attdelegate='" + attdelegate + '\'' +
                ", markdelegate='" + markdelegate + '\'' +
                ", diarydelegate='" + diarydelegate + '\'' +
                ", hwdelegate='" + hwdelegate + '\'' +
                ", fromdate='" + fromdate + '\'' +
                ", todate='" + todate + '\'' +
                '}';
    }
}
