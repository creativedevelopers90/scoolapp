
package com.scool.qt.model;

import com.google.gson.annotations.SerializedName;

public class AddDiaryResponse {

    @SerializedName("diary")
    private Diary mDiary;

    public Diary getDiary() {
        return mDiary;
    }

    public void setDiary(Diary diary) {
        mDiary = diary;
    }

}
