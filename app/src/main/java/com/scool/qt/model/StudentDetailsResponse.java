package com.scool.qt.model;
import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StudentDetailsResponse implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("class_list_id")
    @Expose
    private Integer classListId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Integer getClassListId() {
        return classListId;
    }

    public void setClassListId(Integer classListId) {
        this.classListId = classListId;
    }

}
