package com.scool.qt.model.request;

import com.google.gson.annotations.SerializedName;

public class UpdateMarksRequest {

    @SerializedName("school_id")
    String schoolID;

    @SerializedName("id")
    int marksID;

    @SerializedName("marks")
    float marks;

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public int getMarksID() {
        return marksID;
    }

    public void setMarksID(int marksID) {
        this.marksID = marksID;
    }

    public float getMarks() {
        return marks;
    }

    public void setMarks(float marks) {
        this.marks = marks;
    }
}
