
package com.scool.qt.model;

import com.google.gson.annotations.SerializedName;

public class MarksList {

    @SerializedName("mark_id")
    private Long mMarkId;
    @SerializedName("marks")
    private String mMarks;
    @SerializedName("student_id")
    private Long mStudentId;
    @SerializedName("student_name")
    private String mStudentName;

    public Long getMarkId() {
        return mMarkId;
    }

    public void setMarkId(Long markId) {
        mMarkId = markId;
    }

    public String getMarks() {
        return mMarks;
    }

    public void setMarks(String marks) {
        mMarks = marks;
    }

    public Long getStudentId() {
        return mStudentId;
    }

    public void setStudentId(Long studentId) {
        mStudentId = studentId;
    }

    public String getStudentName() {
        return mStudentName;
    }

    public void setStudentName(String studentName) {
        mStudentName = studentName;
    }

}
