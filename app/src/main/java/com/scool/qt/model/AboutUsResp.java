package com.scool.qt.model;

import com.google.gson.annotations.SerializedName;

public class AboutUsResp {
    @SerializedName("about_us")
    public String about_us;

    public String getAbout_us() {
        return about_us;
    }

    public void setAbout_us(String about_us) {
        this.about_us = about_us;
    }
}
