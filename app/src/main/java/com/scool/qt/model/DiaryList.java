
package com.scool.qt.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DiaryList implements Serializable {

    @SerializedName("class_list_id")
    private Long mClassListId;
    @SerializedName("created_at")
    private String mCreatedAt;
    @SerializedName("date")
    private String mDate;
    @SerializedName("diary")
    private String mDiary;
    @SerializedName("faculty_id")
    private Long mFacultyId;
    @SerializedName("id")
    private Long mId;
    @SerializedName("notified")
    private Boolean mNotified;
    @SerializedName("parent_reply")
    private Object mParentReply;
    @SerializedName("student_id")
    private Long mStudentId;
    @SerializedName("updated_at")
    private String mUpdatedAt;

    public Long getClassListId() {
        return mClassListId;
    }

    public void setClassListId(Long classListId) {
        mClassListId = classListId;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDiary() {
        return mDiary;
    }

    public void setDiary(String diary) {
        mDiary = diary;
    }

    public Long getFacultyId() {
        return mFacultyId;
    }

    public void setFacultyId(Long facultyId) {
        mFacultyId = facultyId;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Boolean getNotified() {
        return mNotified;
    }

    public void setNotified(Boolean notified) {
        mNotified = notified;
    }

    public Object getParentReply() {
        return mParentReply;
    }

    public void setParentReply(Object parentReply) {
        mParentReply = parentReply;
    }

    public Long getStudentId() {
        return mStudentId;
    }

    public void setStudentId(Long studentId) {
        mStudentId = studentId;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

}
