package com.scool.qt.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuestionObject {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("survey_id")
    @Expose
    private Integer surveyId;
    @SerializedName("default_text")
    @Expose
    private String defaultText;
    @SerializedName("placeholder")
    @Expose
    private String placeholder;
    @SerializedName("position")
    @Expose
    private Object position;
    @SerializedName("answer_options")
    @Expose
    private String answerOptions;
    @SerializedName("validation_rules")
    @Expose
    private ValidationRulesObject validationRules;
    @SerializedName("answered")
    @Expose
    private Boolean answered;
    @SerializedName("ans")
    @Expose
    private String ans;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public String getDefaultText() {
        return defaultText;
    }

    public void setDefaultText(String defaultText) {
        this.defaultText = defaultText;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public Object getPosition() {
        return position;
    }

    public void setPosition(Object position) {
        this.position = position;
    }

    public String getAnswerOptions() {
        return answerOptions;
    }

    public void setAnswerOptions(String answerOptions) {
        this.answerOptions = answerOptions;
    }

    public ValidationRulesObject getValidationRules() {
        return validationRules;
    }

    public void setValidationRules(ValidationRulesObject validationRules) {
        this.validationRules = validationRules;
    }

    public Boolean getAnswered() {
        return answered;
    }

    public void setAnswered(Boolean answered) {
        this.answered = answered;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }
}
