package com.scool.qt.model;
import java.util.List;
import com.google.gson.annotations.SerializedName;
public class AllClassTimeTableResp{

	@SerializedName("time_tables")
	private List<TimeTablesItem> timeTables;

	public void setTimeTables(List<TimeTablesItem> timeTables){
		this.timeTables = timeTables;
	}

	public List<TimeTablesItem> getTimeTables(){
		return timeTables;
	}

	@Override
 	public String toString(){
		return 
			"AllClassTimeTableResp{" + 
			"time_tables = '" + timeTables + '\'' + 
			"}";
		}
}