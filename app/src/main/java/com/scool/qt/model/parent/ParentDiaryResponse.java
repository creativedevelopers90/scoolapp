
package com.scool.qt.model.parent;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ParentDiaryResponse {

    @SerializedName("diaries")
    private List<Diary> mDiaries;

    public List<Diary> getDiaries() {
        return mDiaries;
    }

    public void setDiaries(List<Diary> diaries) {
        mDiaries = diaries;
    }

}
