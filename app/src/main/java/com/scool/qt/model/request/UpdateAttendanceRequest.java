package com.scool.qt.model.request;

import com.google.gson.annotations.SerializedName;

public class UpdateAttendanceRequest {

    @SerializedName("school_id")
    private String school_id;

    @SerializedName("id")
    private Integer id;

    @SerializedName("status")
    private Integer status;

    /***
     *
     * @param school_id
     * @param id
     * @param status
     */
    public UpdateAttendanceRequest(String school_id, Integer id, Integer status) {
        this.school_id = school_id;
        this.id = id;
        this.status = status;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "UpdateAttendanceRequest{" +
                "school_id='" + school_id + '\'' +
                ", id=" + id +
                ", status=" + status +
                '}';
    }
}
