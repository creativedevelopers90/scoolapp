package com.scool.qt.model.parent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;

public class AttendanceDateObject {

    @SerializedName("attendance_list")
    @Expose
    private HashMap<String, List<AttendanceList>> attendanceList = null;

    public HashMap<String, List<AttendanceList>> getAttendanceList() {
        return attendanceList;
    }

    public void setAttendanceList(HashMap<String, List<AttendanceList>> attendanceList) {
        this.attendanceList = attendanceList;
    }
}
