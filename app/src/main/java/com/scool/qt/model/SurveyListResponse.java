package com.scool.qt.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SurveyListResponse {

    @SerializedName("surveys")
    @Expose
    private List<SurveyObject> surveys = null;

    public List<SurveyObject> getSurveys() {
        return surveys;
    }

    public void setSurveys(List<SurveyObject> surveys) {
        this.surveys = surveys;
    }
}
