package com.scool.qt.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class UpdateHomeworkRequest {
    @SerializedName("school_id")
    private String schoolID;
    @SerializedName("id")
    private int homeworkID;
    @SerializedName("home_work")
    private String homeworkContent;
    @SerializedName("target_date")
    private Date targetDate;
    @SerializedName("notified")
    private Boolean notified;

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public int getHomeworkID() {
        return homeworkID;
    }

    public void setHomeworkID(int homeworkID) {
        this.homeworkID = homeworkID;
    }

    public String getHomeworkContent() {
        return homeworkContent;
    }

    public void setHomeworkContent(String homeworkContent) {
        this.homeworkContent = homeworkContent;
    }

    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    public Boolean getNotified() {
        return notified;
    }

    public void setNotified(Boolean notified) {
        this.notified = notified;
    }
}
