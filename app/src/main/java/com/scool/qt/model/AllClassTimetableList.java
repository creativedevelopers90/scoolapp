package com.scool.qt.model;

import android.os.Parcel;
import android.os.Parcelable;

public class AllClassTimetableList implements Parcelable {
    private String schoolId;
    private String subjectName;
    private String className;
    private int id;
    private String time;
    private int classListId;
    private String day;
    private boolean isHeader;
    private String headerName;

    public AllClassTimetableList(String schoolId, String subjectName, int id, String time,
                                 int classListId, String day, boolean isHeader, String headerName, String className) {
        this.schoolId = schoolId;
        this.subjectName = subjectName;
        this.id = id;
        this.time = time;
        this.classListId = classListId;
        this.day = day;
        this.isHeader = isHeader;
        this.headerName = headerName;
        this.className =className;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getClassListId() {
        return classListId;
    }

    public void setClassListId(int classListId) {
        this.classListId = classListId;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }

    public String getHeaderName() {
        return headerName;
    }

    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public String toString() {
        return "AllClassTimetableList{" +
                "schoolId='" + schoolId + '\'' +
                ", subjectName='" + subjectName + '\'' +
                ", id=" + id +
                ", time='" + time + '\'' +
                ", classListId=" + classListId +
                ", day='" + day + '\'' +
                ", isHeader=" + isHeader +
                ", headerName='" + headerName + '\'' +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.schoolId);
        dest.writeString(this.subjectName);
        dest.writeString(this.className);
        dest.writeInt(this.id);
        dest.writeString(this.time);
        dest.writeInt(this.classListId);
        dest.writeString(this.day);
        dest.writeByte(this.isHeader ? (byte) 1 : (byte) 0);
        dest.writeString(this.headerName);
    }

    protected AllClassTimetableList(Parcel in) {
        this.schoolId = in.readString();
        this.subjectName = in.readString();
        this.className = in.readString();
        this.id = in.readInt();
        this.time = in.readString();
        this.classListId = in.readInt();
        this.day = in.readString();
        this.isHeader = in.readByte() != 0;
        this.headerName = in.readString();
    }

    public static final Creator<AllClassTimetableList> CREATOR = new Creator<AllClassTimetableList>() {
        @Override
        public AllClassTimetableList createFromParcel(Parcel source) {
            return new AllClassTimetableList(source);
        }

        @Override
        public AllClassTimetableList[] newArray(int size) {
            return new AllClassTimetableList[size];
        }
    };
}
