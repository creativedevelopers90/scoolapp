
package com.scool.qt.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class FacultyStudentListResponse {

    @SerializedName("grade")
    private String mGrade;
    @SerializedName("id")
    private Long mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("section")
    private String mSection;
    @SerializedName("students")
    private List<StudentDetailsResponse> mStudents;
    @SerializedName("year")
    private String mYear;

    public String getGrade() {
        return mGrade;
    }

    public void setGrade(String grade) {
        mGrade = grade;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getSection() {
        return mSection;
    }

    public void setSection(String section) {
        mSection = section;
    }

    public List<StudentDetailsResponse> getStudents() {
        return mStudents;
    }

    public void setStudents(List<StudentDetailsResponse> students) {
        mStudents = students;
    }

    public String getYear() {
        return mYear;
    }

    public void setYear(String year) {
        mYear = year;
    }

}
