
package com.scool.qt.model.parent;

import com.google.gson.annotations.SerializedName;

public class AttendanceBtwnDatesResponse {

    @SerializedName("attendance_list")
    private AttendanceDateObject mAttendanceList;

    public AttendanceDateObject getAttendanceList() {
        return mAttendanceList;
    }

    public void setAttendanceList(AttendanceDateObject attendanceList) {
        mAttendanceList = attendanceList;
    }

}
