
package com.scool.qt.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ClassAttendanceResponse {

    @SerializedName("attendances")
    private List<Attendance> mAttendances;

    public List<Attendance> getAttendances() {
        return mAttendances;
    }

    public void setAttendances(List<Attendance> attendances) {
        mAttendances = attendances;
    }

}
