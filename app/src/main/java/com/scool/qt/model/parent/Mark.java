
package com.scool.qt.model.parent;

import com.google.gson.annotations.SerializedName;

public class Mark {

    @SerializedName("created_at")
    private String mCreatedAt;
    @SerializedName("exam_id")
    private Long mExamId;
    @SerializedName("id")
    private Long mId;
    @SerializedName("marks")
    private String mMarks;
    @SerializedName("notified")
    private Boolean mNotified;
    @SerializedName("student_id")
    private Long mStudentId;
    @SerializedName("subject_id")
    private Long mSubjectId;
    @SerializedName("updated_at")
    private String mUpdatedAt;
    @SerializedName("subject_name")
    private String mSubjectName;

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public Long getExamId() {
        return mExamId;
    }

    public void setExamId(Long examId) {
        mExamId = examId;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getMarks() {
        return mMarks;
    }

    public void setMarks(String marks) {
        mMarks = marks;
    }

    public Boolean getNotified() {
        return mNotified;
    }

    public void setNotified(Boolean notified) {
        mNotified = notified;
    }

    public Long getStudentId() {
        return mStudentId;
    }

    public void setStudentId(Long studentId) {
        mStudentId = studentId;
    }

    public Long getSubjectId() {
        return mSubjectId;
    }

    public void setSubjectId(Long subjectId) {
        mSubjectId = subjectId;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }


    public String getmSubjectName() {
        return mSubjectName;
    }

    public void setmSubjectName(String mSubjectName) {
        this.mSubjectName = mSubjectName;
    }
}
