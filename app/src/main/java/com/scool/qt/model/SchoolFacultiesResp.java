package com.scool.qt.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SchoolFacultiesResp{

	@SerializedName("faculties")
	private List<FacultiesItem> faculties;

	public void setFaculties(List<FacultiesItem> faculties){
		this.faculties = faculties;
	}

	public List<FacultiesItem> getFaculties(){
		return faculties;
	}

	@Override
 	public String toString(){
		return 
			"SchoolFacultiesResp{" + 
			"faculties = '" + faculties + '\'' + 
			"}";
		}
}