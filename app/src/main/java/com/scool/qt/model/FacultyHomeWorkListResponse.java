
package com.scool.qt.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class FacultyHomeWorkListResponse {

    @SerializedName("homework_list")
    private List<HomeworkList> mHomeworkList;

    public List<HomeworkList> getHomeworkList() {
        return mHomeworkList;
    }

    public void setHomeworkList(List<HomeworkList> homeworkList) {
        mHomeworkList = homeworkList;
    }

}
