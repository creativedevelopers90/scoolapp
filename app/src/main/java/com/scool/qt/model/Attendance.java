
package com.scool.qt.model;

import com.google.gson.annotations.SerializedName;

public class Attendance {

    @SerializedName("attendance_status")
    private Long mAttendanceStatus;
    @SerializedName("date")
    private String mDate;
    @SerializedName("id")
    private Long mId;
    @SerializedName("student_id")
    private Long mStudentId;
    @SerializedName("student_name")
    private String mStudentName;
    @SerializedName("time_table_id")
    private Long mTimeTableId;

    public Long getAttendanceStatus() {
        return mAttendanceStatus;
    }

    public void setAttendanceStatus(Long attendanceStatus) {
        mAttendanceStatus = attendanceStatus;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Long getStudentId() {
        return mStudentId;
    }

    public void setStudentId(Long studentId) {
        mStudentId = studentId;
    }

    public String getStudentName() {
        return mStudentName;
    }

    public void setStudentName(String studentName) {
        mStudentName = studentName;
    }

    public Long getTimeTableId() {
        return mTimeTableId;
    }

    public void setTimeTableId(Long timeTableId) {
        mTimeTableId = timeTableId;
    }

}
