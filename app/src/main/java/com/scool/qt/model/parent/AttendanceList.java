
package com.scool.qt.model.parent;

import com.google.gson.annotations.SerializedName;

public class AttendanceList {


    @SerializedName("id")
    private Long mId;
    @SerializedName("reason")
    private Boolean reason;
    @SerializedName("status")
    private Long mStatus;



    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Boolean getNotified() {
        return reason;
    }

    public void setNotified(String reason) {
        reason = reason;
    }

    public Long getStatus() {
        return mStatus;
    }

    public void setStatus(Long status) {
        mStatus = status;
    }



}
