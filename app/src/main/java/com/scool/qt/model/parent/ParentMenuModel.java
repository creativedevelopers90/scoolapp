package com.scool.qt.model.parent;

import com.scool.qt.parent.fragment.ParentMenuFragment;

public class ParentMenuModel {
    private String name;
    private int id;
    private String  badgeNum;
    private ParentMenuFragment.PARENT_MENU menuId;

    public ParentMenuModel(String name, int id, ParentMenuFragment.PARENT_MENU menuId) {
        this.name = name;
        this.id = id;
        this.menuId = menuId;
    }

    public ParentMenuModel(String name, int id, String badgeNum, ParentMenuFragment.PARENT_MENU menuId) {
        this.name = name;
        this.id = id;
        this.badgeNum = badgeNum;
        this.menuId = menuId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ParentMenuFragment.PARENT_MENU getMenuId() {
        return menuId;
    }

    public void setMenuId(ParentMenuFragment.PARENT_MENU menuId) {
        this.menuId = menuId;
    }

    public String getBadgeNum() {
        return badgeNum;
    }

    public void setBadgeNum(String badgeNum) {
        this.badgeNum = badgeNum;
    }
}
