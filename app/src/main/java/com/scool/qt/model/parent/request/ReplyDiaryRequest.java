package com.scool.qt.model.parent.request;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ReplyDiaryRequest {
    @SerializedName("school_id")
    private String schoolID;
    @SerializedName("student_id")
    private int studentID;
    @SerializedName("faculty_id")
    private int facultyID;
    @SerializedName("id")
    private int diaryID;
    @SerializedName("date")
    private Date date;
    @SerializedName("parent_reply")
    private String parentReply;

    public ReplyDiaryRequest(String schoolID, int studentID, int facultyID, int diaryID, Date date, String parentReply) {
        this.schoolID = schoolID;
        this.studentID = studentID;
        this.facultyID = facultyID;
        this.diaryID = diaryID;
        this.date = date;
        this.parentReply = parentReply;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public int getFacultyID() {
        return facultyID;
    }

    public void setFacultyID(int facultyID) {
        this.facultyID = facultyID;
    }

    public int getDiaryID() {
        return diaryID;
    }

    public void setDiaryID(int diaryID) {
        this.diaryID = diaryID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getParentReply() {
        return parentReply;
    }

    public void setParentReply(String parentReply) {
        this.parentReply = parentReply;
    }
}
