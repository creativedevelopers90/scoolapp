
package com.scool.qt.model.parent;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ParentDetailResponse {

    @SerializedName("parent")
    private Parent mParent;

    public Parent getParent() {
        return mParent;
    }

    public void setParent(Parent parent) {
        mParent = parent;
    }

}
