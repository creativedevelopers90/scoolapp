package com.scool.qt.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class AddHomeworkRequest {
    @SerializedName("school_id")
    private String schoolID;
    @SerializedName("faculty_id")
    private int facultyID;
    @SerializedName("class_list_id")
    private int classListID;
    @SerializedName("student_id")
    private int studentID;
    @SerializedName("subject_id")
    private int subjectID;
    @SerializedName("date")
    private Date date;
    @SerializedName("target_date")
    private Date targetDate;
    @SerializedName("home_work")
    private String homeworkContent;

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public int getFacultyID() {
        return facultyID;
    }

    public void setFacultyID(int facultyID) {
        this.facultyID = facultyID;
    }

    public int getClassListID() {
        return classListID;
    }

    public void setClassListID(int classListID) {
        this.classListID = classListID;
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public int getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(int subjectID) {
        this.subjectID = subjectID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    public String getHomeworkContent() {
        return homeworkContent;
    }

    public void setHomeworkContent(String homeworkContent) {
        this.homeworkContent = homeworkContent;
    }
}
