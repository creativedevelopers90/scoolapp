package com.scool.qt.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DelegationItem implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("delegated")
    @Expose
    private Boolean delegated;
    @SerializedName("delegated_to")
    @Expose
    private String delegatedTo;
    @SerializedName("delegation_id")
    @Expose
    private String delegationId;
    @SerializedName("delegated_faculty_id")
    @Expose
    private String delegatedFacultyId;
    @SerializedName("attdelegate")
    @Expose
    private String attdelegate;
    @SerializedName("hwdelegate")
    @Expose
    private String hwdelegate;
    @SerializedName("diarydelegate")
    @Expose
    private String diarydelegate;
    @SerializedName("markdelegate")
    @Expose
    private String markdelegate;
    @SerializedName("fromdate")
    @Expose
    private String fromdate;
    @SerializedName("todate")
    @Expose
    private String todate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDelegated() {
        return delegated;
    }

    public void setDelegated(Boolean delegated) {
        this.delegated = delegated;
    }

    public String getDelegatedTo() {
        return delegatedTo;
    }

    public void setDelegatedTo(String delegatedTo) {
        this.delegatedTo = delegatedTo;
    }

    public String getDelegationId() {
        return delegationId;
    }

    public void setDelegationId(String delegationId) {
        this.delegationId = delegationId;
    }

    public String getDelegatedFacultyId() {
        return delegatedFacultyId;
    }

    public void setDelegatedFacultyId(String delegatedFacultyId) {
        this.delegatedFacultyId = delegatedFacultyId;
    }

    public String getAttdelegate() {
        return attdelegate;
    }

    public void setAttdelegate(String attdelegate) {
        this.attdelegate = attdelegate;
    }

    public String getHwdelegate() {
        return hwdelegate;
    }

    public void setHwdelegate(String hwdelegate) {
        this.hwdelegate = hwdelegate;
    }

    public String getDiarydelegate() {
        return diarydelegate;
    }

    public void setDiarydelegate(String diarydelegate) {
        this.diarydelegate = diarydelegate;
    }

    public String getMarkdelegate() {
        return markdelegate;
    }

    public void setMarkdelegate(String markdelegate) {
        this.markdelegate = markdelegate;
    }

    public String getFromdate() {
        return fromdate;
    }

    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeValue(this.delegated);
        dest.writeString(this.delegatedTo);
        dest.writeString(this.delegationId);
        dest.writeString(this.delegatedFacultyId);
        dest.writeString(this.attdelegate);
        dest.writeString(this.hwdelegate);
        dest.writeString(this.diarydelegate);
        dest.writeString(this.markdelegate);
        dest.writeString(this.fromdate);
        dest.writeString(this.todate);
    }

    public DelegationItem() {
    }

    protected DelegationItem(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
        this.delegated = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.delegatedTo = in.readString();
        this.delegationId = in.readString();
        this.delegatedFacultyId = in.readString();
        this.attdelegate = in.readString();
        this.hwdelegate = in.readString();
        this.diarydelegate = in.readString();
        this.markdelegate = in.readString();
        this.fromdate = in.readString();
        this.todate = in.readString();
    }

    public static final Parcelable.Creator<DelegationItem> CREATOR = new Parcelable.Creator<DelegationItem>() {
        @Override
        public DelegationItem createFromParcel(Parcel source) {
            return new DelegationItem(source);
        }

        @Override
        public DelegationItem[] newArray(int size) {
            return new DelegationItem[size];
        }
    };
}

