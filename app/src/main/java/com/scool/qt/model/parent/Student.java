
package com.scool.qt.model.parent;

import com.google.gson.annotations.SerializedName;

public class Student {

    @SerializedName("class_id")
    private Long mClassId;
    @SerializedName("class_name")
    private String mClassName;
    @SerializedName("image")
    private String mImage;
    @SerializedName("name")
    private String mName;
    @SerializedName("parent_id")
    private Long mParentId;
    @SerializedName("student_id")
    private Long mStudentId;

    public Long getClassId() {
        return mClassId;
    }

    public void setClassId(Long classId) {
        mClassId = classId;
    }

    public String getClassName() {
        return mClassName;
    }

    public void setClassName(String className) {
        mClassName = className;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Long getParentId() {
        return mParentId;
    }

    public void setParentId(Long parentId) {
        mParentId = parentId;
    }

    public Long getStudentId() {
        return mStudentId;
    }

    public void setStudentId(Long studentId) {
        mStudentId = studentId;
    }

}
