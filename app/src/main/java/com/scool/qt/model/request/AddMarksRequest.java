package com.scool.qt.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class AddMarksRequest {

    @SerializedName("school_id")
    String schoolID;

    @SerializedName("student_id")
    int studentID;

    @SerializedName("subject_id")
    int subjectID;

    @SerializedName("exam_id")
    int examID;

    @SerializedName("marks")
    float marks;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddMarksRequest that = (AddMarksRequest) o;
        return studentID == that.studentID &&
                subjectID == that.subjectID &&
                examID == that.examID &&
                Float.compare(that.marks, marks) == 0 &&
                Objects.equals(schoolID, that.schoolID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(schoolID, studentID, subjectID, examID, marks);
    }



    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public int getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(int subjectID) {
        this.subjectID = subjectID;
    }

    public int getExamID() {
        return examID;
    }

    public void setExamID(int examID) {
        this.examID = examID;
    }

    public float getMarks() {
        return marks;
    }

    public void setMarks(float marks) {
        this.marks = marks;
    }
}
