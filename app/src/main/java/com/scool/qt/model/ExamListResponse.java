
package com.scool.qt.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ExamListResponse {

    @SerializedName("exams")
    private List<Exam> mExams;

    public List<Exam> getExams() {
        return mExams;
    }

    public void setExams(List<Exam> exams) {
        mExams = exams;
    }

}
