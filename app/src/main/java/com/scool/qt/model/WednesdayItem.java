package com.scool.qt.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WednesdayItem{

	@SerializedName("school_id")
	private String schoolId;

	@SerializedName("subject_name")
	private String subjectName;
	@SerializedName("class_name")
	@Expose
	private String className;

	@SerializedName("id")
	private int id;

	@SerializedName("time")
	private String time;

	@SerializedName("class_list_id")
	private int classListId;

	@SerializedName("day")
	private String day;

	public void setSchoolId(String schoolId){
		this.schoolId = schoolId;
	}

	public String getSchoolId(){
		return schoolId;
	}

	public void setSubjectName(String subjectName){
		this.subjectName = subjectName;
	}

	public String getSubjectName(){
		return subjectName;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTime(String time){
		this.time = time;
	}

	public String getTime(){
		return time;
	}

	public void setClassListId(int classListId){
		this.classListId = classListId;
	}

	public int getClassListId(){
		return classListId;
	}

	public void setDay(String day){
		this.day = day;
	}

	public String getDay(){
		return day;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	@Override
 	public String toString(){
		return 
			"WednesdayItem{" + 
			"school_id = '" + schoolId + '\'' + 
			",subject_name = '" + subjectName + '\'' + 
			",id = '" + id + '\'' + 
			",time = '" + time + '\'' + 
			",class_list_id = '" + classListId + '\'' + 
			",day = '" + day + '\'' + 
			"}";
		}
}