package com.scool.qt.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ParentDiaryReplyResponse {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("diary")
    @Expose
    private String diary;
    @SerializedName("parent_reply")
    @Expose
    private String parentReply;
    @SerializedName("notified")
    @Expose
    private Boolean notified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDiary() {
        return diary;
    }

    public void setDiary(String diary) {
        this.diary = diary;
    }

    public String getParentReply() {
        return parentReply;
    }

    public void setParentReply(String parentReply) {
        this.parentReply = parentReply;
    }

    public Boolean getNotified() {
        return notified;
    }

    public void setNotified(Boolean notified) {
        this.notified = notified;
    }
}
