package com.scool.qt.model;
import com.google.gson.annotations.SerializedName;

public class ParentLoginResponse{

	@SerializedName("code")
	private int code;

	@SerializedName("parent_id")
	private int parentId;

	@SerializedName("message")
	private String message;

	@SerializedName("token")
	private String token;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setParentId(int parentId){
		this.parentId = parentId;
	}

	public int getParentId(){
		return parentId;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

	@Override
 	public String toString(){
		return
			"ParentLoginResponse{" +
			"code = '" + code + '\'' +
			",parent_id = '" + parentId + '\'' +
			",message = '" + message + '\'' +
			",token = '" + token + '\'' +
			"}";
		}
}