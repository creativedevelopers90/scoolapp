package com.scool.qt.model.request;
import com.google.gson.annotations.SerializedName;
public class LoginUserRequest {

    @SerializedName("school_id")
    private String school_id;

    @SerializedName("username")
    private String username;

    @SerializedName("password")
    private String password;

    @SerializedName("android_device_id")
    private String android_device_id;


    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAndroid_device_id() {
        return android_device_id;
    }

    public void setAndroid_device_id(String android_device_id) {
        this.android_device_id = android_device_id;
    }
}
