package com.scool.qt.model;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FacultyLoginResponse implements Serializable {

	@SerializedName("code")
	private int code;

	@SerializedName("faculty_id")
	private int facultyId;

	@SerializedName("message")
	private String message;

	@SerializedName("token")
	private String token;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setFacultyId(int facultyId){
		this.facultyId = facultyId;
	}

	public int getFacultyId(){
		return facultyId;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

	@Override
 	public String toString(){
		return 
			"FacultyLoginResponse{" + 
			"code = '" + code + '\'' + 
			",faculty_id = '" + facultyId + '\'' + 
			",message = '" + message + '\'' + 
			",token = '" + token + '\'' + 
			"}";
		}
}