package com.scool.qt.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TimetableList {
    @SerializedName("time_table")
    @Expose
    private List<TimeTableObject> timeTable = null;

    public List<TimeTableObject> getTimeTable() {
        return timeTable;
    }

    public void setTimeTable(List<TimeTableObject> timeTable) {
        this.timeTable = timeTable;
    }
}
