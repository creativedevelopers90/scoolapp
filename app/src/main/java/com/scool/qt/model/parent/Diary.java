
package com.scool.qt.model.parent;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Diary implements Parcelable {

    @SerializedName("class_list_id")
    private Long mClassListId;
    @SerializedName("date")
    private String mDate;
    @SerializedName("diary")
    private String mDiary;
    @SerializedName("faculty_id")
    private Long mFacultyId;
    @SerializedName("faculty_name")
    private String mFacultyName;
    @SerializedName("id")
    private Long mId;
    @SerializedName("notified")
    private Boolean mNotified;
    @SerializedName("parent_reply")
    private String mParentReply;
    @SerializedName("student_id")
    private Long mStudentId;

    public Long getClassListId() {
        return mClassListId;
    }

    public void setClassListId(Long classListId) {
        mClassListId = classListId;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDiary() {
        return mDiary;
    }

    public void setDiary(String diary) {
        mDiary = diary;
    }

    public Long getFacultyId() {
        return mFacultyId;
    }

    public void setFacultyId(Long facultyId) {
        mFacultyId = facultyId;
    }

    public String getFacultyName() {
        return mFacultyName;
    }

    public void setFacultyName(String facultyName) {
        mFacultyName = facultyName;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Boolean getNotified() {
        return mNotified;
    }

    public void setNotified(Boolean notified) {
        mNotified = notified;
    }

    public String getParentReply() {
        return mParentReply;
    }

    public void setParentReply(String parentReply) {
        mParentReply = parentReply;
    }

    public Long getStudentId() {
        return mStudentId;
    }

    public void setStudentId(Long studentId) {
        mStudentId = studentId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.mClassListId);
        dest.writeString(this.mDate);
        dest.writeString(this.mDiary);
        dest.writeValue(this.mFacultyId);
        dest.writeString(this.mFacultyName);
        dest.writeValue(this.mId);
        dest.writeValue(this.mNotified);
        dest.writeString(this.mParentReply);
        dest.writeValue(this.mStudentId);
    }

    public Diary() {
    }

    protected Diary(Parcel in) {
        this.mClassListId = (Long) in.readValue(Long.class.getClassLoader());
        this.mDate = in.readString();
        this.mDiary = in.readString();
        this.mFacultyId = (Long) in.readValue(Long.class.getClassLoader());
        this.mFacultyName = in.readString();
        this.mId = (Long) in.readValue(Long.class.getClassLoader());
        this.mNotified = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mParentReply = in.readString();
        this.mStudentId = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Creator<Diary> CREATOR = new Creator<Diary>() {
        @Override
        public Diary createFromParcel(Parcel source) {
            return new Diary(source);
        }

        @Override
        public Diary[] newArray(int size) {
            return new Diary[size];
        }
    };
}
