
package com.scool.qt.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class FacultyClassResponse {

    @SerializedName("class_list")
    private List<ClassList> mClassList;

    public List<ClassList> getClassList() {
        return mClassList;
    }

    public void setClassList(List<ClassList> classList) {
        mClassList = classList;
    }

}
