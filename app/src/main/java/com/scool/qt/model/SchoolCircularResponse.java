package com.scool.qt.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SchoolCircularResponse{

	@SerializedName("circulars")
	private List<CircularsItem> circulars;

	public void setCirculars(List<CircularsItem> circulars){
		this.circulars = circulars;
	}

	public List<CircularsItem> getCirculars(){
		return circulars;
	}

	@Override
 	public String toString(){
		return 
			"SchoolCircularResponse{" + 
			"circulars = '" + circulars + '\'' + 
			"}";
		}
}