package com.scool.qt.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ClassListFrDelegation implements Parcelable {

    @SerializedName("class_lists_for_Delegation")
    @Expose
    private List<DelegationItem> classListsForDelegation = null;

    public List<DelegationItem> getClassListsForDelegation() {
        return classListsForDelegation;
    }

    public void setClassListsForDelegation(List<DelegationItem> classListsForDelegation) {
        this.classListsForDelegation = classListsForDelegation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.classListsForDelegation);
    }

    public ClassListFrDelegation() {
    }

    protected ClassListFrDelegation(Parcel in) {
        this.classListsForDelegation = in.createTypedArrayList(DelegationItem.CREATOR);
    }

    public static final Parcelable.Creator<ClassListFrDelegation> CREATOR = new Parcelable.Creator<ClassListFrDelegation>() {
        @Override
        public ClassListFrDelegation createFromParcel(Parcel source) {
            return new ClassListFrDelegation(source);
        }

        @Override
        public ClassListFrDelegation[] newArray(int size) {
            return new ClassListFrDelegation[size];
        }
    };
}
