
package com.scool.qt.model.parent;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ParentTimeTableResponse {

    @SerializedName("time_table")
    private List<TimeTable> mTimeTable;

    public List<TimeTable> getTimeTable() {
        return mTimeTable;
    }

    public void setTimeTable(List<TimeTable> timeTable) {
        mTimeTable = timeTable;
    }

}
