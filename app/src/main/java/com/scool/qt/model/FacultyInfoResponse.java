
package com.scool.qt.model;

import com.google.gson.annotations.SerializedName;

public class FacultyInfoResponse {

    @SerializedName("class_id1")
    private Long mClassId1;
    @SerializedName("class_id2")
    private Long mClassId2;
    @SerializedName("class_id3")
    private Long mClassId3;
    @SerializedName("class_id4")
    private Long mClassId4;
    @SerializedName("class_id5")
    private Long mClassId5;
    @SerializedName("class_id6")
    private Long mClassId6;
    @SerializedName("class_id7")
    private Long mClassId7;
    @SerializedName("class_id8")
    private Long mClassId8;
    @SerializedName("class_id9")
    private Long mClassId9;
    @SerializedName("department")
    private String mDepartment;
    @SerializedName("id")
    private Long mId;
    @SerializedName("is_admin")
    private Boolean mIsAdmin;
    @SerializedName("name")
    private String mName;
    @SerializedName("photo")
    private String mPhoto;
    @SerializedName("school_id")
    private String mSchoolId;
    @SerializedName("username")
    private String mUsername;

    public Long getClassId1() {
        return mClassId1;
    }

    public void setClassId1(Long classId1) {
        mClassId1 = classId1;
    }

    public Long getClassId2() {
        return mClassId2;
    }

    public void setClassId2(Long classId2) {
        mClassId2 = classId2;
    }

    public Long getClassId3() {
        return mClassId3;
    }

    public void setClassId3(Long classId3) {
        mClassId3 = classId3;
    }

    public Long getClassId4() {
        return mClassId4;
    }

    public void setClassId4(Long classId4) {
        mClassId4 = classId4;
    }

    public Long getClassId5() {
        return mClassId5;
    }

    public void setClassId5(Long classId5) {
        mClassId5 = classId5;
    }

    public Long getClassId6() {
        return mClassId6;
    }

    public void setClassId6(Long classId6) {
        mClassId6 = classId6;
    }

    public Long getClassId7() {
        return mClassId7;
    }

    public void setClassId7(Long classId7) {
        mClassId7 = classId7;
    }

    public Long getClassId8() {
        return mClassId8;
    }

    public void setClassId8(Long classId8) {
        mClassId8 = classId8;
    }

    public Long getClassId9() {
        return mClassId9;
    }

    public void setClassId9(Long classId9) {
        mClassId9 = classId9;
    }

    public String getDepartment() {
        return mDepartment;
    }

    public void setDepartment(String department) {
        mDepartment = department;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Boolean getIsAdmin() {
        return mIsAdmin;
    }

    public void setIsAdmin(Boolean isAdmin) {
        mIsAdmin = isAdmin;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPhoto() {
        return mPhoto;
    }

    public void setPhoto(String photo) {
        mPhoto = photo;
    }

    public String getSchoolId() {
        return mSchoolId;
    }

    public void setSchoolId(String schoolId) {
        mSchoolId = schoolId;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

}
