package com.scool.qt.model;
import com.google.gson.annotations.SerializedName;

public class ContactUsResp{

	@SerializedName("contact_us")
	private String contactUs;

	public void setContactUs(String contactUs){
		this.contactUs = contactUs;
	}

	public String getContactUs(){
		return contactUs;
	}

	@Override
 	public String toString(){
		return 
			"ContactUsResp{" + 
			"contact_us = '" + contactUs + '\'' + 
			"}";
		}
}