
package com.scool.qt.model;

import com.google.gson.annotations.SerializedName;

public class UpdateMarksResponse {

    @SerializedName("marks")
    private Marks mMarks;

    public Marks getMarks() {
        return mMarks;
    }

    public void setMarks(Marks marks) {
        mMarks = marks;
    }

}
