
package com.scool.qt.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class SubjectListResponse {

    @SerializedName("faculty_id")
    private Long mFacultyId;
    @SerializedName("school_id")
    private String mSchoolId;
    @SerializedName("subjects")
    private List<Subject> mSubjects;

    public Long getFacultyId() {
        return mFacultyId;
    }

    public void setFacultyId(Long facultyId) {
        mFacultyId = facultyId;
    }

    public String getSchoolId() {
        return mSchoolId;
    }

    public void setSchoolId(String schoolId) {
        mSchoolId = schoolId;
    }

    public List<Subject> getSubjects() {
        return mSubjects;
    }

    public void setSubjects(List<Subject> subjects) {
        mSubjects = subjects;
    }

}
