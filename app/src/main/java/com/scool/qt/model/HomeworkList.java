
package com.scool.qt.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class HomeworkList implements Serializable {

    @SerializedName("class_list_id")
    private Long mClassListId;
    @SerializedName("created_at")
    private String mCreatedAt;
    @SerializedName("date")
    private String mDate;
    @SerializedName("faculty_id")
    private Long mFacultyId;
    @SerializedName("home_work")
    private String mHomeWork;
    @SerializedName("id")
    private Long mId;
    @SerializedName("notified")
    private Boolean mNotified;
    @SerializedName("student_id")
    private Long mStudentId;
    @SerializedName("subject_id")
    private Long mSubjectId;
    @SerializedName("subject_name")
    private String mSubjectName;
    @SerializedName("target_date")
    private String mTargetDate;
    @SerializedName("updated_at")
    private String mUpdatedAt;

    public Long getClassListId() {
        return mClassListId;
    }

    public void setClassListId(Long classListId) {
        mClassListId = classListId;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public Long getFacultyId() {
        return mFacultyId;
    }

    public void setFacultyId(Long facultyId) {
        mFacultyId = facultyId;
    }

    public String getHomeWork() {
        return mHomeWork;
    }

    public void setHomeWork(String homeWork) {
        mHomeWork = homeWork;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Boolean getNotified() {
        return mNotified;
    }

    public void setNotified(Boolean notified) {
        mNotified = notified;
    }

    public Long getStudentId() {
        return mStudentId;
    }

    public void setStudentId(Long studentId) {
        mStudentId = studentId;
    }

    public Long getSubjectId() {
        return mSubjectId;
    }

    public void setSubjectId(Long subjectId) {
        mSubjectId = subjectId;
    }

    public String getSubjectName() {
        return mSubjectName;
    }

    public void setSubjectName(String subjectName) {
        mSubjectName = subjectName;
    }

    public String getTargetDate() {
        return mTargetDate;
    }

    public void setTargetDate(String targetDate) {
        mTargetDate = targetDate;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

}
