
package com.scool.qt.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class FacultyDiaryListResponse {

    @SerializedName("diary_list")
    private List<DiaryList> mDiaryList;

    public List<DiaryList> getDiaryList() {
        return mDiaryList;
    }

    public void setDiaryList(List<DiaryList> diaryList) {
        mDiaryList = diaryList;
    }

}
