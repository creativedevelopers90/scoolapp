package com.scool.qt.model.room;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Homework {
    @PrimaryKey
    public Long homework_id;

    @ColumnInfo(name = "date")
    public String mDate;

    @ColumnInfo(name = "home_work")
    public String mHomeWork;

    public Homework(Long homework_id, String mDate, String mHomeWork) {
        this.homework_id = homework_id;
        this.mDate = mDate;
        this.mHomeWork = mHomeWork;
    }

    public Long getHomework_id() {
        return homework_id;
    }

    public void setHomework_id(Long homework_id) {
        this.homework_id = homework_id;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public String getmHomeWork() {
        return mHomeWork;
    }

    public void setmHomeWork(String mHomeWork) {
        this.mHomeWork = mHomeWork;
    }

    @Override
    public String toString() {
        return "Homework{" +
                "homework_id=" + homework_id +
                ", mDate='" + mDate + '\'' +
                ", mHomeWork='" + mHomeWork + '\'' +
                '}';
    }
}
