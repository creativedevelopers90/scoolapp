
package com.scool.qt.model.request;

import com.google.gson.annotations.SerializedName;

public class AttendanceRequest {

    @SerializedName("date")
    private String mDate;
    @SerializedName("status")
    private Long mStatus;
    @SerializedName("student_id")
    private Long mStudentId;
    @SerializedName("time_table_id")
    private Long mTimeTableId;

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public Long getStatus() {
        return mStatus;
    }

    public void setStatus(Long status) {
        mStatus = status;
    }

    public Long getStudentId() {
        return mStudentId;
    }

    public void setStudentId(Long studentId) {
        mStudentId = studentId;
    }

    public Long getTimeTableId() {
        return mTimeTableId;
    }

    public void setTimeTableId(Long timeTableId) {
        mTimeTableId = timeTableId;
    }

}
