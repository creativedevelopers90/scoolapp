package com.scool.qt.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class StartResponse{
	@SerializedName("images")
	private List<String> images;

	@SerializedName("name")
	private String name;

	@SerializedName("logo")
	private String logo;

	@SerializedName("id")
	private String id;

	@SerializedName("slogan")
	private String slogan;

	public void setImages(List<String> images){
		this.images = images;
	}

	public List<String> getImages(){
		return images;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setLogo(String logo){
		this.logo = logo;
	}

	public String getLogo(){
		return logo;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setSlogan(String slogan){
		this.slogan = slogan;
	}

	public String getSlogan(){
		return slogan;
	}

	@Override
 	public String toString(){
		return 
			"StartResponse{" + 
			"images = '" + images + '\'' + 
			",name = '" + name + '\'' + 
			",logo = '" + logo + '\'' + 
			",id = '" + id + '\'' + 
			",slogan = '" + slogan + '\'' + 
			"}";
		}
}