
package com.scool.qt.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AddHomeworkResponse {

    @SerializedName("home_work")
    private HomeWork mHomeWork;

    public HomeWork getHomeWork() {
        return mHomeWork;
    }

    public void setHomeWork(HomeWork homeWork) {
        mHomeWork = homeWork;
    }

}
