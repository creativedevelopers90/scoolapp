package com.scool.qt.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SurveyRequest {

    @SerializedName("answers")
    @Expose
    private List<AnswerDataObject> answers = null;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("parent_id")
    @Expose
    private Integer parentId;
    @SerializedName("survey_id")
    @Expose
    private Integer surveyId;

    public List<AnswerDataObject> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerDataObject> answers) {
        this.answers = answers;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }
}
