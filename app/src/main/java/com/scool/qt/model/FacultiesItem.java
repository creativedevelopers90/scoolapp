package com.scool.qt.model;
import com.google.gson.annotations.SerializedName;

public class FacultiesItem{

	@SerializedName("class_id2")
	private int classId2;

	@SerializedName("class_id3")
	private int classId3;

	@SerializedName("class_id4")
	private int classId4;

	@SerializedName("class_id5")
	private int classId5;

	@SerializedName("class_id6")
	private int classId6;

	@SerializedName("class_id7")
	private int classId7;

	@SerializedName("class_id8")
	private int classId8;

	@SerializedName("class_id9")
	private int classId9;

	@SerializedName("photo")
	private String photo;

	@SerializedName("is_admin")
	private boolean isAdmin;

	@SerializedName("school_id")
	private String schoolId;

	@SerializedName("deparment")
	private String deparment;

	@SerializedName("qualification_education")
	private String qualificationEducation;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("username")
	private String username;

	@SerializedName("class_id1")
	private int classId1;

	@SerializedName("info")
	private String info;

	public void setClassId2(int classId2){
		this.classId2 = classId2;
	}

	public int getClassId2(){
		return classId2;
	}

	public void setClassId3(int classId3){
		this.classId3 = classId3;
	}

	public int getClassId3(){
		return classId3;
	}

	public void setClassId4(int classId4){
		this.classId4 = classId4;
	}

	public int getClassId4(){
		return classId4;
	}

	public void setClassId5(int classId5){
		this.classId5 = classId5;
	}

	public int getClassId5(){
		return classId5;
	}

	public void setClassId6(int classId6){
		this.classId6 = classId6;
	}

	public int getClassId6(){
		return classId6;
	}

	public void setClassId7(int classId7){
		this.classId7 = classId7;
	}

	public int getClassId7(){
		return classId7;
	}

	public void setClassId8(int classId8){
		this.classId8 = classId8;
	}

	public int getClassId8(){
		return classId8;
	}

	public void setClassId9(int classId9){
		this.classId9 = classId9;
	}

	public int getClassId9(){
		return classId9;
	}

	public void setPhoto(String photo){
		this.photo = photo;
	}

	public String getPhoto(){
		return photo;
	}

	public void setIsAdmin(boolean isAdmin){
		this.isAdmin = isAdmin;
	}

	public boolean isIsAdmin(){
		return isAdmin;
	}

	public void setSchoolId(String schoolId){
		this.schoolId = schoolId;
	}

	public String getSchoolId(){
		return schoolId;
	}

	public void setDeparment(String deparment){
		this.deparment = deparment;
	}

	public String getDeparment(){
		return deparment;
	}

	public void setQualificationEducation(String qualificationEducation){
		this.qualificationEducation = qualificationEducation;
	}

	public String getQualificationEducation(){
		return qualificationEducation;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	public void setClassId1(int classId1){
		this.classId1 = classId1;
	}

	public int getClassId1(){
		return classId1;
	}

	public void setInfo(String info){
		this.info = info;
	}

	public String getInfo(){
		return info;
	}

	@Override
 	public String toString(){
		return 
			"FacultiesItem{" + 
			"class_id2 = '" + classId2 + '\'' + 
			",class_id3 = '" + classId3 + '\'' + 
			",class_id4 = '" + classId4 + '\'' + 
			",class_id5 = '" + classId5 + '\'' + 
			",class_id6 = '" + classId6 + '\'' + 
			",class_id7 = '" + classId7 + '\'' + 
			",class_id8 = '" + classId8 + '\'' + 
			",class_id9 = '" + classId9 + '\'' + 
			",photo = '" + photo + '\'' + 
			",is_admin = '" + isAdmin + '\'' + 
			",school_id = '" + schoolId + '\'' + 
			",deparment = '" + deparment + '\'' + 
			",qualification_education = '" + qualificationEducation + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",username = '" + username + '\'' + 
			",class_id1 = '" + classId1 + '\'' + 
			",info = '" + info + '\'' + 
			"}";
		}
}