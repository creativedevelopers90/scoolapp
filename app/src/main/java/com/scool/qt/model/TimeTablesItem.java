package com.scool.qt.model;
import com.google.gson.annotations.SerializedName;

import java.util.List;
public class TimeTablesItem{

	@SerializedName("Monday")
	private List<MondayItem> monday;

	@SerializedName("Tuesday")
	private List<TuesdayItem> tuesday;

	@SerializedName("Wednesday")
	private List<WednesdayItem> wednesday;

	@SerializedName("Thursday")
	private List<ThursdayItem> thursday;

	@SerializedName("Friday")
	private List<WednesdayItem> friday;

	@SerializedName("Saturday")
	private List<WednesdayItem> saturday;

	@SerializedName("Sunday")
	private List<WednesdayItem> sunday;



	public List<ThursdayItem> getThursday() {
		return thursday;
	}

	public void setThursday(List<ThursdayItem> thursday) {
		this.thursday = thursday;
	}

	public void setMonday(List<MondayItem> monday){
		this.monday = monday;
	}

	public List<MondayItem> getMonday(){
		return monday;
	}

	public void setWednesday(List<WednesdayItem> wednesday){
		this.wednesday = wednesday;
	}

	public List<WednesdayItem> getWednesday(){
		return wednesday;
	}

	public void setTuesday(List<TuesdayItem> tuesday){
		this.tuesday = tuesday;
	}

	public List<TuesdayItem> getTuesday(){
		return tuesday;
	}

	public List<WednesdayItem> getFriday() {
		return friday;
	}

	public void setFriday(List<WednesdayItem> friday) {
		this.friday = friday;
	}

	public List<WednesdayItem> getSaturday() {
		return saturday;
	}

	public void setSaturday(List<WednesdayItem> saturday) {
		this.saturday = saturday;
	}

	public List<WednesdayItem> getSunday() {
		return sunday;
	}

	public void setSunday(List<WednesdayItem> sunday) {
		this.sunday = sunday;
	}

	@Override
 	public String toString(){
		return 
			"TimeTablesItem{" + 
			"monday = '" + monday + '\'' + 
			",wednesday = '" + wednesday + '\'' + 
			",tuesday = '" + tuesday + '\'' + 
			"}";
		}
}