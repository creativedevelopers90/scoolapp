package com.scool.qt.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class UpdateDiaryRequest {
    @SerializedName("school_id")
    private String schoolID;
    @SerializedName("id")
    private int diaryID;
    @SerializedName("diary")
    private String diaryContent;
    @SerializedName("date")
    private Date date;
    @SerializedName("notified")
    private Boolean notified;

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public int getDiaryID() {
        return diaryID;
    }

    public void setDiaryID(int diaryID) {
        this.diaryID = diaryID;
    }

    public String getDiaryContent() {
        return diaryContent;
    }

    public void setDiaryContent(String diaryContent) {
        this.diaryContent = diaryContent;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getNotified() {
        return notified;
    }

    public void setNotified(Boolean notified) {
        this.notified = notified;
    }
}
