
package com.scool.qt.model.parent;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ParentHomeworkResponse {

    @SerializedName("home_work")
    private List<HomeWork> mHomeWork;

    private String homeworkDate;


    public List<HomeWork> getHomeWork() {
        return mHomeWork;
    }

    public void setHomeWork(List<HomeWork> homeWork) {
        mHomeWork = homeWork;
    }

    public String getHomeworkDate() {
        return homeworkDate;
    }

    public void setHomeworkDate(String homeworkDate) {
        this.homeworkDate = homeworkDate;
    }
}
