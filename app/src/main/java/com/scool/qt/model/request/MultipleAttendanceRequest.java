
package com.scool.qt.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MultipleAttendanceRequest {

    @SerializedName("attendances")
    private List<AttendanceRequest> mAttendances;
    @SerializedName("school_id")
    private String mSchoolId;

    @SerializedName("time_table_id")
    private String time_table_id;

    public List<AttendanceRequest> getAttendances() {
        return mAttendances;
    }

    public void setAttendances(List<AttendanceRequest> attendances) {
        mAttendances = attendances;
    }

    public String getSchoolId() {
        return mSchoolId;
    }

    public void setSchoolId(String schoolId) {
        mSchoolId = schoolId;
    }

    public String getTime_table_id() {
        return time_table_id;
    }

    public void setTime_table_id(String time_table_id) {
        this.time_table_id = time_table_id;
    }
}
