
package com.scool.qt.model.parent;

import com.google.gson.annotations.SerializedName;

public class TimeTable {

    @SerializedName("class_list_id")
    private Long mClassListId;
    @SerializedName("day")
    private String mDay;
    @SerializedName("faculty_id")
    private Long mFacultyId;
    @SerializedName("faculty_name")
    private String mFacultyName;
    @SerializedName("id")
    private Long mId;
    @SerializedName("school_id")
    private String mSchoolId;
    @SerializedName("subject_name")
    private String mSubjectName;
    @SerializedName("time")
    private String mTime;

    public Long getClassListId() {
        return mClassListId;
    }

    public void setClassListId(Long classListId) {
        mClassListId = classListId;
    }

    public String getDay() {
        return mDay;
    }

    public void setDay(String day) {
        mDay = day;
    }

    public Long getFacultyId() {
        return mFacultyId;
    }

    public void setFacultyId(Long facultyId) {
        mFacultyId = facultyId;
    }

    public String getFacultyName() {
        return mFacultyName;
    }

    public void setFacultyName(String facultyName) {
        mFacultyName = facultyName;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getSchoolId() {
        return mSchoolId;
    }

    public void setSchoolId(String schoolId) {
        mSchoolId = schoolId;
    }

    public String getSubjectName() {
        return mSubjectName;
    }

    public void setSubjectName(String subjectName) {
        mSubjectName = subjectName;
    }

    public String getTime() {
        return mTime;
    }

    public void setTime(String time) {
        mTime = time;
    }

}
