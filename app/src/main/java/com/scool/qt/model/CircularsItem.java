package com.scool.qt.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CircularsItem implements Serializable {

	@SerializedName("date")
	private String date;

	@SerializedName("target_date")
	private String targetDate;

	@SerializedName("school_id")
	private String schoolId;

	@SerializedName("photo")
	private String photo;

	@SerializedName("circular")
	private String circular;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setTargetDate(String targetDate){
		this.targetDate = targetDate;
	}

	public String getTargetDate(){
		return targetDate;
	}

	public void setSchoolId(String schoolId){
		this.schoolId = schoolId;
	}

	public String getSchoolId(){
		return schoolId;
	}

	public void setPhoto(String photo){
		this.photo = photo;
	}

	public String getPhoto(){
		return photo;
	}

	public void setCircular(String circular){
		this.circular = circular;
	}

	public String getCircular(){
		return circular;
	}

	@Override
 	public String toString(){
		return 
			"CircularsItem{" + 
			"date = '" + date + '\'' + 
			",target_date = '" + targetDate + '\'' + 
			",school_id = '" + schoolId + '\'' + 
			",photo = '" + photo + '\'' + 
			",circular = '" + circular + '\'' + 
			"}";
		}
}