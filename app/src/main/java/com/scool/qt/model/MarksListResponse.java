
package com.scool.qt.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class MarksListResponse {

    @SerializedName("marks_list")
    private List<MarksList> mMarksList;

    public List<MarksList> getMarksList() {
        return mMarksList;
    }

    public void setMarksList(List<MarksList> marksList) {
        mMarksList = marksList;
    }

}
