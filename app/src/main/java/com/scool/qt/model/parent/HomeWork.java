
package com.scool.qt.model.parent;

import com.google.gson.annotations.SerializedName;

public class HomeWork   {

    @SerializedName("class_list_id")
    private Long mClassListId;
    @SerializedName("date")
    private String mDate;
    @SerializedName("faculty_name")
    private String mFacultyName;
    @SerializedName("home_work")
    private String mHomeWork;
    @SerializedName("homework_id")
    private Long mHomeworkId;
    @SerializedName("notified")
    private Boolean mNotified;
    @SerializedName("student_id")
    private Long mStudentId;
    @SerializedName("subject_name")
    private String mSubjectName;
    @SerializedName("target_date")
    private String mTargetDate;

    private boolean isChecked;

    public Long getClassListId() {
        return mClassListId;
    }

    public void setClassListId(Long classListId) {
        mClassListId = classListId;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getFacultyName() {
        return mFacultyName;
    }

    public void setFacultyName(String facultyName) {
        mFacultyName = facultyName;
    }

    public String getHomeWork() {
        return mHomeWork;
    }

    public void setHomeWork(String homeWork) {
        mHomeWork = homeWork;
    }

    public Long getHomeworkId() {
        return mHomeworkId;
    }

    public void setHomeworkId(Long homeworkId) {
        mHomeworkId = homeworkId;
    }

    public Boolean getNotified() {
        return mNotified;
    }

    public void setNotified(Boolean notified) {
        mNotified = notified;
    }

    public Long getStudentId() {
        return mStudentId;
    }

    public void setStudentId(Long studentId) {
        mStudentId = studentId;
    }

    public String getSubjectName() {
        return mSubjectName;
    }

    public void setSubjectName(String subjectName) {
        mSubjectName = subjectName;
    }

    public String getTargetDate() {
        return mTargetDate;
    }

    public void setTargetDate(String targetDate) {
        mTargetDate = targetDate;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

}
