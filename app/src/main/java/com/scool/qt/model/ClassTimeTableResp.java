package com.scool.qt.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;
public class ClassTimeTableResp{

	@SerializedName("time_tables")
	private List<TimeTablesByClassId> timeTables;

	public void setTimeTables(List<TimeTablesByClassId> timeTables){
		this.timeTables = timeTables;
	}

	public List<TimeTablesByClassId> getTimeTables(){
		return timeTables;
	}

	@Override
 	public String toString(){
		return 
			"ClassTimeTableResp{" + 
			"time_tables = '" + timeTables + '\'' + 
			"}";
		}
}