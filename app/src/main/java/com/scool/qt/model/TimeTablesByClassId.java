package com.scool.qt.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeTablesByClassId implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("faculty_id")
    @Expose
    private Integer facultyId;
    @SerializedName("class_list_id")
    @Expose
    private Integer classListId;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("day")
    @Expose
    private String day;

    @SerializedName("subject_name")
    @Expose
    private String subject_name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public Integer getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Integer facultyId) {
        this.facultyId = facultyId;
    }

    public Integer getClassListId() {
        return classListId;
    }

    public void setClassListId(Integer classListId) {
        this.classListId = classListId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.schoolId);
        dest.writeValue(this.facultyId);
        dest.writeValue(this.classListId);
        dest.writeString(this.time);
        dest.writeString(this.day);
        dest.writeString(this.subject_name);
    }

    public TimeTablesByClassId() {
    }

    protected TimeTablesByClassId(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.schoolId = in.readString();
        this.facultyId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.classListId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.time = in.readString();
        this.day = in.readString();
        this.subject_name = in.readString();
    }

    public static final Parcelable.Creator<TimeTablesByClassId> CREATOR = new Parcelable.Creator<TimeTablesByClassId>() {
        @Override
        public TimeTablesByClassId createFromParcel(Parcel source) {
            return new TimeTablesByClassId(source);
        }

        @Override
        public TimeTablesByClassId[] newArray(int size) {
            return new TimeTablesByClassId[size];
        }
    };
}
