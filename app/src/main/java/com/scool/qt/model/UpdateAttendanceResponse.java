package com.scool.qt.model;

import com.google.gson.annotations.SerializedName;

public class UpdateAttendanceResponse {
    @SerializedName("id")
    private int id;

    @SerializedName("student_id")
    private int student_id;

    @SerializedName("date")
    private String date;

    @SerializedName("status")
    private String status;

    @SerializedName("notified")
    private boolean notified;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isNotified() {
        return notified;
    }

    public void setNotified(boolean notified) {
        this.notified = notified;
    }

    @Override
    public String toString() {
        return "UpdateAttendanceResponse{" +
                "id=" + id +
                ", student_id=" + student_id +
                ", date='" + date + '\'' +
                ", status='" + status + '\'' +
                ", notified=" + notified +
                '}';
    }
}
