package com.scool.qt.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class AddDiaryRequest {
    @SerializedName("school_id")
    private String schoolID;
    @SerializedName("faculty_id")
    private int facultyID;
    @SerializedName("class_list_id")
    private int classListID;
    @SerializedName("student_id")
    private int studentID;
    @SerializedName("diary")
    private String diary;
    @SerializedName("date")
    private Date date;
    @SerializedName("notified")
    private Boolean notified;

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public int getFacultyID() {
        return facultyID;
    }

    public void setFacultyID(int facultyID) {
        this.facultyID = facultyID;
    }

    public int getClassListID() {
        return classListID;
    }

    public void setClassListID(int classListID) {
        this.classListID = classListID;
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public String getDiary() {
        return diary;
    }

    public void setDiary(String diary) {
        this.diary = diary;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getNotified() {
        return notified;
    }

    public void setNotified(Boolean notified) {
        this.notified = notified;
    }
}
