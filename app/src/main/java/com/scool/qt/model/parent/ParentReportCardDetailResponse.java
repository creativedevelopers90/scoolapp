
package com.scool.qt.model.parent;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ParentReportCardDetailResponse {

    @SerializedName("mark")
    private List<Mark> mMark;

    public List<Mark> getMark() {
        return mMark;
    }

    public void setMark(List<Mark> mark) {
        mMark = mark;
    }

}
