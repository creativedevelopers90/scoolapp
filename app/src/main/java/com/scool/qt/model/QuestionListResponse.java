package com.scool.qt.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuestionListResponse {

    @SerializedName("questions")
    @Expose
    private List<QuestionObject> questions = null;

    public List<QuestionObject> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionObject> questions) {
        this.questions = questions;
    }
}
