
package com.scool.qt.model;

import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("message")
    private String mMessage;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

}
