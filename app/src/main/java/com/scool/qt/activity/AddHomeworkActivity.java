package com.scool.qt.activity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.StudentDetailsResponse;
import com.scool.qt.model.Subject;
import com.scool.qt.model.SubjectListResponse;
import com.scool.qt.model.request.AddHomeworkRequest;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddHomeworkActivity extends BaseActivity implements NetworkView {
    @BindView(R.id.add_homework_toolbar)
    Toolbar toolbar;
    @BindView(R.id.add_homework_toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.homework_target_date_tv)
    TextView targetDate;
    @BindView(R.id.homework_subject_tv)
    TextView subject;
    @BindView(R.id.homework_content_et)
    EditText homeworkContent;
    private NetworkPresenter mNetworkPresenter;
    private Bundle bundle;
    private int mDay, mMonth, mYear;
    private String mSelectedSubject;
    private List<Subject> subjectList;
    List<String> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_homework);
        ButterKnife.bind(this);

        bundle = getIntent().getBundleExtra("select_record");
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        toolbarTitle.setText(R.string.kAddHomeworkText);
        targetDate.setText("Target Date: " + mDay + "-" + mMonth + "-" + mYear);

        setActionBar();

        fetchSubjectsList(loadFacultyData());
    }


    /**
     *
     * @param facultyInfo
     */
    private void fetchSubjectsList(FacultyLoginResponse facultyInfo) {
        mNetworkPresenter.methodToGetSubjectListByFaculty(AddHomeworkActivity.this, facultyInfo.getFacultyId(), Constant.UNIQUE_SCHOOL_ID, facultyInfo.getToken());
    }

    /**
     * Method to set the action bar
     */
    private void setActionBar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> finish());
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @OnClick(R.id.homework_target_date_tv)
    public void onTargetDatePressed() {
        Utils.hideSoftKeyboard(AddHomeworkActivity.this);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> {
                    targetDate.setText("Target Date: " + dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    mYear = year;
                    mMonth = monthOfYear;
                    mDay = dayOfMonth;
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.homework_subject_tv)
    public void tvHomeWorkClick() {
        homeworkAlertDialog();
    }

    private void homeworkAlertDialog() {
        String[] listItems = list.toArray(new String[list.size()]);
        AlertDialog.Builder builder = new AlertDialog.Builder(AddHomeworkActivity.this, R.style.MyDialogTheme);
        builder.setTitle("Choose item");
        builder.setItems(list.toArray(new String[list.size()]), (dialog, which) -> subject.setText("Subject name: "+ listItems[which]));
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @OnClick(R.id.add_homework_done)
    void onDoneClicked() {
        if (!homeworkContent.getText().toString().isEmpty())
            mNetworkPresenter.methodToAddHomeworkByFaculty(AddHomeworkActivity.this, generateRequestData(loadFacultyData()), loadFacultyData().getToken());
        else
            Utils.showSnackBar(this, "Please enter the homework.");
    }

    @OnClick(R.id.add_homework_cancel)
    void onCancelClicked() {
        onBackPressed();
    }

    /**
     * Method to load the faculty data
     *
     * @return
     */
    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", this);
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }

    /**
     * Method to generate request data to add homework
     *
     * @param facultyInfo
     * @return
     */
    private AddHomeworkRequest generateRequestData(FacultyLoginResponse facultyInfo) {
        AddHomeworkRequest homeworkRequest = new AddHomeworkRequest();
        homeworkRequest.setSchoolID(Constant.UNIQUE_SCHOOL_ID);
        homeworkRequest.setFacultyID(facultyInfo.getFacultyId());
        if (bundle.getString("header").contains("All Students")) {
            homeworkRequest.setClassListID(Constant.ACTIVE_CLASS.getId().intValue()); //42
            homeworkRequest.setStudentID(0);
        } else {
            StudentDetailsResponse selectedStudent = (StudentDetailsResponse) bundle.getSerializable("selected_student");
            homeworkRequest.setClassListID(selectedStudent.getClassListId().intValue());
            homeworkRequest.setStudentID(selectedStudent.getId().intValue());
        }
        homeworkRequest.setSubjectID(getSubjectID());
        homeworkRequest.setDate(new Date());
        Calendar cal = Calendar.getInstance();
        cal.set(mYear, mMonth, mDay);
        homeworkRequest.setTargetDate(cal.getTime());
        homeworkRequest.setHomeworkContent(homeworkContent.getText().toString().trim());

        return homeworkRequest;
    }

    /**
     * Method to get the subject list
     *
     * @param subjectList
     * @return
     */
    public List<String> getSubjectList(List<Subject> subjectList) {
        List<String> subjectNameList = new ArrayList<>();
        subjectList.stream().forEach(subjectItem -> {
            subjectNameList.add(subjectItem.getName());
        });
        return subjectNameList;
    }

    /**
     * Method to get the selected subject id
     *
     * @return
     */
    public int getSubjectID() {
        for (int i = 0; i < subjectList.size(); i++) {
            if (subjectList.get(i).getName().equals(mSelectedSubject)) {
                return subjectList.get(i).getId().intValue();
            }
        }
        return 0;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        if (endpointType == EndpointEnums.faculty_subject_list) {
            subjectList = ((SubjectListResponse) object).getSubjects();
            List<String> subjectStringList = getSubjectList(subjectList);
            subject.setText("Subject name: "+subjectStringList.get(0));
            mSelectedSubject = subjectStringList.get(0);
            list.addAll(subjectStringList);


        } else if (endpointType == EndpointEnums.faculty_add_homework) {
            onBackPressed();
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        if (endpointType == EndpointEnums.faculty_subject_list)
            Log.e("== FACULTY SUB RES ==>", "onEndpointFailure: " + endpointType + " : " + message);
        else if (endpointType == EndpointEnums.faculty_add_homework)
            Log.e("== ADD HOMEWORK ==>", "onEndpointFailure: " + endpointType + " : " + message);
        Utils.showSnackBar(this, message);
    }
}
