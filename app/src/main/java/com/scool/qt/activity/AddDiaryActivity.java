package com.scool.qt.activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.StudentDetailsResponse;
import com.scool.qt.model.request.AddDiaryRequest;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
public class AddDiaryActivity extends BaseActivity implements NetworkView {
    @BindView(R.id.add_diary_toolbar)
    Toolbar toolbar;
    @BindView(R.id.add_diary_toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.add_diary_content)
    EditText diaryContent;
    private NetworkPresenter mNetworkPresenter;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_diary);
        ButterKnife.bind(this);

        bundle = getIntent().getBundleExtra("select_record");

        toolbarTitle.setText(getResources().getText(R.string.kDiaryText));

        setActionBar();
    }

    /**
     * Method to set the action bar
     */
    private void setActionBar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> finish());
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @OnClick(R.id.add_diary_cancel)
    void onCancelAddDiaryPressed() {
        onBackPressed();
    }

    @OnClick(R.id.add_diary_done)
    void onDoneAddDiaryPressed() {
        if(!TextUtils.isEmpty(diaryContent.getText())) {
            mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
            FacultyLoginResponse facultyLoginResponse = loadFacultyData();
            mNetworkPresenter.methodToAddDiaryByFaculty(this, generateRequest(facultyLoginResponse), facultyLoginResponse.getToken());
        }
    }

    /**
     * Method to load the faculty data
     *
     * @return
     */
    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", this);
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }

    /**
     * Method to generate add diary request object
     *
     * @return
     */
    private AddDiaryRequest generateRequest(FacultyLoginResponse facultyData) {
        // TODO: 17/11/18 validate fields
        AddDiaryRequest mRequest = new AddDiaryRequest();
        mRequest.setSchoolID(Constant.UNIQUE_SCHOOL_ID);
        mRequest.setFacultyID(facultyData.getFacultyId());
        if (bundle.getString("header").contains("All Students")) {
            mRequest.setClassListID(Constant.ACTIVE_CLASS.getId().intValue());
            mRequest.setStudentID(0);
        } else {
            StudentDetailsResponse selectedStudent = (StudentDetailsResponse) bundle.getSerializable("selected_student");
            mRequest.setClassListID(selectedStudent.getClassListId().intValue());
            mRequest.setStudentID(selectedStudent.getId().intValue());
        }
        mRequest.setDiary(diaryContent.getText().toString());
        mRequest.setDate(new Date());
        mRequest.setNotified(true);
        return mRequest;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        if (message.equalsIgnoreCase("Created")) {
            onBackPressed();
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        Utils.showSnackBar(this, message);
    }
}
