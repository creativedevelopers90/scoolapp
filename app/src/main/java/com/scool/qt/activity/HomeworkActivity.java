package com.scool.qt.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.faculty.activity.FacultyHomeActivity;
import com.scool.qt.model.DeleteHomeworkResponse;
import com.scool.qt.model.FacultyHomeWorkListResponse;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.HomeworkList;
import com.scool.qt.model.StudentDetailsResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;
import com.scool.qt.widgets.FacultyHomeworkDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeworkActivity extends BaseActivity implements NetworkView, FacultyHomeworkDialog.FacultyHomeworkDialogListener {
    @BindView(R.id.homework_toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.selected_student_header_tv)
    TextView selectedStudentHeader;
    @BindView(R.id.homework_toolbar)
    Toolbar toolbar;
    @BindView(R.id.homework_list_rv)
    RecyclerView homeworkListRV;
    @BindView(R.id.ll_addhomework)
    LinearLayout llAddhomework;
    @BindView(R.id.cl_maincontent)
    ConstraintLayout clMainContent;
    @BindView(R.id.rl_nointernet)
    RelativeLayout rlNoInternet;

    private Bundle bundle;
    private NetworkPresenter mNetworkPresenter;
    private LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework);
        ButterKnife.bind(this);
        setActionBar();
        bundle = getIntent().getBundleExtra("select_record");
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        toolbarTitle.setText(getResources().getText(R.string.kHomeworkText));
        selectedStudentHeader.setText(bundle.getString("header"));
        homeworkListRV.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        homeworkListRV.setLayoutManager(mLayoutManager);

        if (Constant.ACTIVE_CLASS != null)
            fetchFacultyHomeworkList();
        else {
            Intent intent = new Intent(this, FacultyHomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchFacultyHomeworkList();
    }

    @OnClick(R.id.ll_addhomework)
    void onAddHomeworkPressed() {
        Intent intent = new Intent(this, AddHomeworkActivity.class);
        intent.putExtra("select_record", bundle);
        startActivity(intent);
    }

    /**
     * Method to set the action bar
     */
    private void setActionBar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> finish());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }


    public void fetchFacultyHomeworkList() {
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        FacultyLoginResponse facultyLoginResponse = loadFacultyData();
        if (bundle.getString("header").contains("All Students")) {
            mNetworkPresenter.methodToFetchFacultyHomeworkList(this, Constant.UNIQUE_SCHOOL_ID,
                    facultyLoginResponse.getFacultyId(), Constant.ACTIVE_CLASS.getId().intValue(), 0, facultyLoginResponse.getToken());
        } else {
            StudentDetailsResponse student = (StudentDetailsResponse) bundle.getSerializable("selected_student");
            mNetworkPresenter.methodToFetchFacultyHomeworkList(this, Constant.UNIQUE_SCHOOL_ID,
                    facultyLoginResponse.getFacultyId(), Constant.ACTIVE_CLASS.getId().intValue(), student.getId().intValue(), facultyLoginResponse.getToken());
        }
    }

    /**
     * Method to load the faculty data
     *
     * @return
     */
    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", this);
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
        rlNoInternet.setVisibility(View.VISIBLE);
        clMainContent.setVisibility(View.GONE);
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        if (endpointType == EndpointEnums.faculty_homework_list) {
            HomeWorkRecyclerAdapter mAdapter = new HomeWorkRecyclerAdapter(((FacultyHomeWorkListResponse) object).getHomeworkList());
            homeworkListRV.setAdapter(mAdapter);
        } else if (endpointType == EndpointEnums.faculty_delete_homework) {
            Utils.showSnackBar(this, ((DeleteHomeworkResponse) object).getMessage());
            fetchFacultyHomeworkList();
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        Log.e("== DIARYLIST RES ==>", "onEndpointFailure: " + endpointType + " : " + message);
    }

    @Override
    public void onEditHomeworkClicked(HomeworkList homeworkList) {
        Intent intent = new Intent(HomeworkActivity.this, UpdateHomeworkActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("selected_homework", homeworkList);
        intent.putExtra("homework_bundle", bundle);
        startActivity(intent);
    }

    @Override
    public void onDeleteHomeworkClicked(HomeworkList homeworkList) {
        mNetworkPresenter.methodToDeleteHomeworkByFaculty(this, Constant.UNIQUE_SCHOOL_ID, homeworkList.getId().intValue(), loadFacultyData().getToken());

    }

    /**
     * Adapter class for the recycler view to inflate diary items
     */
    class HomeWorkRecyclerAdapter extends RecyclerView.Adapter<MyHomeWorkViewHolder> {

        private final List<HomeworkList> homeworkList;

        public HomeWorkRecyclerAdapter(List<HomeworkList> homeworkList) {
            this.homeworkList = homeworkList;
        }

        @Override
        public MyHomeWorkViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyHomeWorkViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.faculty_homework_list_item, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyHomeWorkViewHolder holder, int position) {
            holder.homeworkDate.setText(Utils.convertDtFormat(homeworkList.get(position).getDate().trim()));
            holder.homeworkTargetDate.setText(Utils.convertDtFormat(homeworkList.get(position).getTargetDate().trim()));
            holder.homeworkSubject.setText(homeworkList.get(position).getSubjectName().trim());
            holder.homeworkContent.setText(homeworkList.get(position).getHomeWork().trim());

            holder.root.setOnLongClickListener(v -> {
                FacultyHomeworkDialog mDialog = new FacultyHomeworkDialog(HomeworkActivity.this);
                mDialog.setListener(HomeworkActivity.this);
                mDialog.setSelectedDiary(homeworkList.get(position));
                mDialog.show();
                return false;
            });
        }

        @Override
        public int getItemCount() {
            return homeworkList.size();
        }
    }

    private class MyHomeWorkViewHolder extends RecyclerView.ViewHolder {
        View root;
        TextView homeworkDate, homeworkTargetDate, homeworkSubject, homeworkContent;

        public MyHomeWorkViewHolder(View itemView) {
            super(itemView);

            root = itemView.findViewById(R.id.homework_item_root);
            homeworkDate = (TextView) itemView.findViewById(R.id.homework_item_date);
            homeworkTargetDate = (TextView) itemView.findViewById(R.id.homework_item_target_date);
            homeworkSubject = (TextView) itemView.findViewById(R.id.homework_item_subject_name);
            homeworkContent = (TextView) itemView.findViewById(R.id.homework_item_content);
        }
    }
}
