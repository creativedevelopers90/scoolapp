package com.scool.qt.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.google.gson.Gson;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.model.StudentLoginResponse;
import com.scool.qt.model.parent.AttendanceDateObject;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
public class ParentAttendanceCalendarActivity extends BaseActivity implements NetworkView {
    NetworkPresenter mPresenter;
    @BindView(R.id.attendance_calendar_toolbar)
    Toolbar toolbar;
    @BindView(R.id.attendance_calendar_toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.attendance_calendar)
    CalendarView attendanceCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_calendar);

        ButterKnife.bind(this);
        mPresenter = new NetworkPresenter(this, new NetworkInteractor());
        setActionBar();
        initView();
    }

    /**
     * Method to initialize view
     */
    private void initView() {
        if(Constant.ACTIVE_STUDENT != null){
            toolbarTitle.setText(Constant.ACTIVE_STUDENT.getName() + ", " + Constant.ACTIVE_STUDENT.getClassName());
        }
        setTheme();
        attendanceCalendar.setMaximumDate(Calendar.getInstance());
        attendanceCalendar.setOnForwardPageChangeListener(() -> {
            Calendar currentPageDate = attendanceCalendar.getCurrentPageDate();
            Calendar maxDate = Calendar.getInstance();
            maxDate.set(currentPageDate.get(Calendar.YEAR), currentPageDate.get(Calendar.MONTH),
                    currentPageDate.getActualMaximum(Calendar.DAY_OF_MONTH));
            fetchAttendanceDetails(currentPageDate.getTime(), maxDate.getTime());
        });

        attendanceCalendar.setOnPreviousPageChangeListener(() -> {
            Calendar currentPageDate = attendanceCalendar.getCurrentPageDate();
            Calendar maxDate = Calendar.getInstance();
            maxDate.set(currentPageDate.get(Calendar.YEAR), currentPageDate.get(Calendar.MONTH),
                    currentPageDate.getActualMaximum(Calendar.DAY_OF_MONTH));
            fetchAttendanceDetails(currentPageDate.getTime(), maxDate.getTime());
        });

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        fetchAttendanceDetails(calendar.getTime(), Calendar.getInstance().getTime());
    }

    /**
     * Method to fetch attendance details
     */
    private void fetchAttendanceDetails(Date startTime, Date endTime) {
        if (mPresenter != null) {
            if(Constant.ACTIVE_STUDENT != null) {
                mPresenter.methodToFetchAttendanceDetailsBetweenDates(this, Constant.UNIQUE_SCHOOL_ID,
                        Constant.ACTIVE_STUDENT.getStudentId().intValue(), startTime, endTime,
                        loadParentData().getToken());
            }else {
                mPresenter.methodToFetchAttendanceDetailsBetweenDates(this, Constant.UNIQUE_SCHOOL_ID,
                        loadStudentResponse().getStudentId().intValue(), startTime, endTime,
                        loadStudentResponse().getToken());
            }

        }
    }



    /**
     * Method to load the parent data
     *
     * @return
     */
    private StudentLoginResponse loadStudentResponse() {
        Gson gson = new Gson();
        String json = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", this);
        return gson.fromJson(json, StudentLoginResponse.class);
    }

    /**
     * Method to set the action bar
     */
    private void setActionBar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> finish());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    /**
     * Method to update attendance events in calendar
     *
     * @param attendanceList
     */
    private void updateEventsInCalendar(AttendanceDateObject attendanceList) {
        List<EventDay> events = new ArrayList<>();
        attendanceList.getAttendanceList().forEach((k, v) -> v.stream().forEach(attendance -> {
            Calendar eventCal;
            switch (attendance.getStatus().intValue()){
                case Constant.ATTENDANCE_TYPE_PRESENT:
                    eventCal = Calendar.getInstance();
                    Log.d("DAY OF MONTH {}", String.valueOf(eventCal.get(Calendar.DAY_OF_WEEK)));
                    try {
                        eventCal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(k));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(eventCal.get(Calendar.DAY_OF_WEEK) == 6 || eventCal.get(Calendar.DAY_OF_WEEK) == 7 ) {
                        events.add(new EventDay(eventCal, R.drawable.attendance_cyan_circle));
                    } else {
                        events.add(new EventDay(eventCal, R.drawable.attendance_green_circle));
                    }
                    break;
                      case Constant.ATTENDANCE_TYPE_SCHOOL_LEAVE:
                          eventCal = Calendar.getInstance();
                          try {
                              eventCal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(k));
                          } catch (ParseException e) {
                              e.printStackTrace();
                          }
                              events.add(new EventDay(eventCal, R.drawable.attendance_cyan_circle));
                          break;
                case Constant.ATTENDANCE_TYPE_LEAVE:
                    eventCal = Calendar.getInstance();
                    try {
                        eventCal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(k));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(eventCal.get(Calendar.DAY_OF_WEEK) == 6 || eventCal.get(Calendar.DAY_OF_WEEK) == 7 ) {
                        events.add(new EventDay(eventCal, R.drawable.attendance_cyan_circle));
                    } else {
                        events.add(new EventDay(eventCal, R.drawable.attendance_yellow_circle));
                    }
                    break;
                case Constant.ATTENDANCE_TYPE_ABSENT:
                    eventCal = Calendar.getInstance();
                    try {
                        eventCal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(k));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(eventCal.get(Calendar.DAY_OF_WEEK) == 6 || eventCal.get(Calendar.DAY_OF_WEEK) == 7 ) {
                        events.add(new EventDay(eventCal, R.drawable.attendance_cyan_circle));
                    } else {
                        events.add(new EventDay(eventCal, R.drawable.attendance_red_circle));
                    }
                    break;
                    case Constant.ATTENDANCE_TYPE_NO_STATUS_MARKED:
                        eventCal = Calendar.getInstance();
                        try {
                            eventCal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(k));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if(eventCal.get(Calendar.DAY_OF_WEEK) == 6 || eventCal.get(Calendar.DAY_OF_WEEK) == 7 ) {
                            events.add(new EventDay(eventCal, R.drawable.attendance_cyan_circle));
                        } else {
                            events.add(new EventDay(eventCal, R.drawable.attendance_grey_circle));
                        }
                        break;
            }
        }));
        attendanceCalendar.setEvents(events);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case parent_attendance_details:
                updateEventsInCalendar(((AttendanceDateObject) object));
                break;
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        Log.e("== " + endpointType + " ==>", message);
    }

    private void setTheme() {
        if(theme.equalsIgnoreCase("blue")) {
            attendanceCalendar.setHeaderColor(R.color.color_blue);
        } else if(theme.equalsIgnoreCase("red")) {
            attendanceCalendar.setHeaderColor(R.color.color_red_accent);
        } else if(theme.equalsIgnoreCase("purple")) {
            attendanceCalendar.setHeaderColor(R.color.color_purple_accent);
        } else if(theme.equalsIgnoreCase("black")) {
            attendanceCalendar.setHeaderColor(R.color.color_black_accent);
        } else if(theme.equalsIgnoreCase("brown")) {
            attendanceCalendar.setHeaderColor(R.color.color_brown_primary_accent);
        } else if(theme.equalsIgnoreCase("green")) {
            attendanceCalendar.setHeaderColor(R.color.color_green);
        }
    }
}
