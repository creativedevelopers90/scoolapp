package com.scool.qt.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.faculty.activity.FacultyHomeActivity;
import com.scool.qt.model.DeleteDiaryResponse;
import com.scool.qt.model.DiaryList;
import com.scool.qt.model.FacultyDiaryListResponse;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.StudentDetailsResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;
import com.scool.qt.widgets.FacultyDiaryDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
public class DiaryActivity extends BaseActivity implements NetworkView, FacultyDiaryDialog.FacultyDiaryDialogListener {
    @BindView(R.id.diary_toolbar_title)
    TextView diaryToolbarTitle;
    @BindView(R.id.diary_toolbar_add)
    ImageView diaryToolbarAdd;
    @BindView(R.id.diary_toolbar)
    Toolbar diaryToolbar;
    @BindView(R.id.applayout)
    AppBarLayout applayout;
    @BindView(R.id.selected_student_header_tv)
    TextView selectedStudentHeaderTv;
    @BindView(R.id.ll_adddiary)
    LinearLayout llAdddiary;
    @BindView(R.id.diary_header)
    RelativeLayout diaryHeader;
    @BindView(R.id.diary_list_rv)
    RecyclerView diaryListRv;
    @BindView(R.id.cl_maincontent)
    RelativeLayout clMaincontent;
    @BindView(R.id.iv_nointernet)
    ImageView ivNointernet;
    @BindView(R.id.rl_nointernet)
    RelativeLayout rlNointernet;
    private Bundle bundle;
    private NetworkPresenter mNetworkPresenter;
    private LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary);
        ButterKnife.bind(this);
        setActionBar();
        bundle = getIntent().getBundleExtra("select_record");
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        diaryToolbarTitle.setText(getResources().getText(R.string.kDiaryText));
        selectedStudentHeaderTv.setText(bundle.getString("header"));

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        diaryListRv.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        diaryListRv.setLayoutManager(mLayoutManager);
        if (Constant.ACTIVE_CLASS != null)
            fetchFacultyDiaryList();
        else {
            Intent intent = new Intent(this, FacultyHomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchFacultyDiaryList();
    }

    @OnClick(R.id.ll_adddiary)
    void onAddDiaryPressed() {
        Intent intent = new Intent(this, AddDiaryActivity.class);
        intent.putExtra("select_record", bundle);
        startActivity(intent);
    }

    /**
     * Method to set the action bar
     */
    private void setActionBar() {
        setSupportActionBar(diaryToolbar);
        diaryToolbar.setNavigationOnClickListener(view -> finish());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }


    public void fetchFacultyDiaryList() {
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        FacultyLoginResponse facultyLoginResponse = loadFacultyData();
        if (bundle.getString("header").contains("All Students")) {
            mNetworkPresenter.methodToFetchFacultyDiaryList(this, Constant.UNIQUE_SCHOOL_ID,
                    facultyLoginResponse.getFacultyId(), Constant.ACTIVE_CLASS.getId().intValue(), 0, facultyLoginResponse.getToken());
        } else {
            StudentDetailsResponse student = (StudentDetailsResponse) bundle.getSerializable("selected_student");
            mNetworkPresenter.methodToFetchFacultyDiaryList(this, Constant.UNIQUE_SCHOOL_ID,
                    facultyLoginResponse.getFacultyId(), Constant.ACTIVE_CLASS.getId().intValue(), student.getId().intValue(), facultyLoginResponse.getToken());
        }
    }

    /**
     * Method to load the faculty data
     *
     * @return
     */
    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", this);
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
        rlNointernet.setVisibility(View.VISIBLE);
        clMaincontent.setVisibility(View.GONE);
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        if (endpointType == EndpointEnums.faculty_diary_list) {
            DiaryRecyclerAdapter mAdapter = new DiaryRecyclerAdapter(((FacultyDiaryListResponse) object).getDiaryList());
            diaryListRv.setAdapter(mAdapter);
        } else if (endpointType == EndpointEnums.faculty_delete_diary) {
            Utils.showSnackBar(this, ((DeleteDiaryResponse) object).getMessage());
            fetchFacultyDiaryList();
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
    }

    @Override
    public void onEditDiaryClicked(DiaryList diary) {
        Intent intent = new Intent(DiaryActivity.this, UpdateDiaryActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("selected_diary", diary);
        intent.putExtra("diary_bundle", bundle);
        startActivity(intent);
    }

    @Override
    public void onDeleteDiaryClicked(DiaryList diary) {
        mNetworkPresenter.methodToDeleteDiaryByFaculty(this, Constant.UNIQUE_SCHOOL_ID, diary.getId().intValue(), loadFacultyData().getToken());
    }

    /**
     * Adapter class for the recycler view to inflate diary items
     */
    class DiaryRecyclerAdapter extends RecyclerView.Adapter<MyDiaryViewHolder> {

        private final List<DiaryList> diaryList;

        public DiaryRecyclerAdapter(List<DiaryList> diaryList) {
            this.diaryList = diaryList;
        }

        @Override
        public MyDiaryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyDiaryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.faculty_diary_list_item, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyDiaryViewHolder holder, int position) {
            holder.diaryDate.setText(diaryList.get(position).getDate().trim());
            holder.diaryContent.setText(diaryList.get(position).getDiary().trim());
            String parentsReply = (String) diaryList.get(position).getParentReply();
            if (parentsReply != null) {
                holder.diaryFeedback.setVisibility(View.VISIBLE);
                holder.diaryFeedback.setText(parentsReply.trim());
            } else
                holder.diaryFeedback.setVisibility(View.GONE);

            holder.root.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    FacultyDiaryDialog mDialog = new FacultyDiaryDialog(DiaryActivity.this);
                    mDialog.setListener(DiaryActivity.this);
                    mDialog.setSelectedDiary(diaryList.get(position));
                    mDialog.show();
                    return false;
                }
            });
        }

        @Override
        public int getItemCount() {
            return diaryList.size();
        }
    }

    private class MyDiaryViewHolder extends RecyclerView.ViewHolder {
        View root;
        TextView diaryDate, diaryContent, diaryFeedback;

        public MyDiaryViewHolder(View itemView) {
            super(itemView);
            root = itemView.findViewById(R.id.diary_item_root);
            diaryDate = (TextView) itemView.findViewById(R.id.diary_item_date);
            diaryContent = (TextView) itemView.findViewById(R.id.diary_item_content);
            diaryFeedback = (TextView) itemView.findViewById(R.id.diary_item_feedback);
        }
    }
}
