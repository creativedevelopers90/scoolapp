package com.scool.qt.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.scool.qt.BaseActivity;
import com.scool.qt.R;
import com.scool.qt.ScoolQtApplication;
import com.scool.qt.faculty.fragments.FacultyAttendanceFragment;
import com.scool.qt.helper.PrefData;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.parent.fragment.ParentMenuFragment;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ParentHomeActivity extends BaseActivity {
    @BindView(R.id.parent_toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.parent_toolbar)
    Toolbar toolbar;
    @BindView(R.id.content_frame)
    FrameLayout contentFrame;
    @BindView(R.id.logout_btn_tv)
    TextView logoutBtn;
    @BindView(R.id.month_btn_tv)
    TextView attendanceMonthBtn;
    public static Boolean isAppRunning = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ScoolQtApplication.getInstance().initAppLanguage(this);
        setContentView(R.layout.activity_parent_home);
        Fragment home = new ParentMenuFragment();
        Log.d("FROM NOTIFICATION", "" +  getIntent().getBooleanExtra("fromNotification", false));
        if (getIntent().hasExtra("fromNotification") && getIntent().getBooleanExtra("fromNotification", false) == true) {
            Bundle bundle = new Bundle();
            bundle.putString("hwbadge", getIntent().getStringExtra("hwbadge"));
            bundle.putString("sbadge", getIntent().getStringExtra("sbadge"));
            bundle.putString("dbadge", getIntent().getStringExtra("dbadge"));
            bundle.putString("abadge", getIntent().getStringExtra("abadge"));
            bundle.putString("ebadge", getIntent().getStringExtra("ebadge"));
            bundle.putString("mbadge", getIntent().getStringExtra("mbadge"));
            home.setArguments(bundle);
        }
        ButterKnife.bind(this);
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        setToolbarTitle();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, home).commit();
        fragmentManager.addOnBackStackChangedListener(() -> {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);// show back button
                logoutBtn.setVisibility(View.GONE);
                toolbar.setNavigationOnClickListener(v -> onBackPressed());
            } else {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                logoutBtn.setVisibility(View.VISIBLE);
                updateToolbar(getString(R.string.kParentHomeText), false);
            }
        });
    }


    private void setToolbarTitle() {
        if(new PrefData(ParentHomeActivity.this).getLoginType() == Constant.LOGIN_TYPE_PARENT){
            if(this.getSupportFragmentManager().findFragmentByTag(getString(R.string.ksurveyText)) != null) {
                Fragment fragment = this.getSupportFragmentManager().findFragmentByTag(getString(R.string.ksurveyText));
                if (fragment instanceof FacultyAttendanceFragment) {
                    toolbarTitle.setText(getString(R.string.ksurveyText));
                }
            } else {
                toolbarTitle.setText(getString(R.string.kParentHomeText));
            }
        }else {
            toolbarTitle.setText(getString(R.string.kStudentHomeText));
        }
    }

    @Override
    public void onBackPressed() {
        setToolbarTitle();
        Utils.hideSoftKeyboard(this);
        super.onBackPressed();
    }



    @Override
    protected void onPause() {
        super.onPause();
    }

    @OnClick(R.id.logout_btn_tv)
    public void onLogoutClicked() {
        logoutAlertDialog(getResources().getString(R.string.kLogout), getResources().getString(R.string.kLogOutConfirmationText));
    }

    @OnClick(R.id.month_btn_tv)
    public void onMonthClicked() {
        Intent intent = new Intent(this, ParentAttendanceCalendarActivity.class);
        startActivity(intent);
    }

    /**
     * Method to add a fragment
     *
     * @param fragment
     */
    public void addFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment,
                fragment.getTag()).addToBackStack(tag).commit();
    }

    /**
     * Method to update toolbar
     *
     * @param title
     * @param showMonthBtn
     */
    public void updateToolbar(String title, boolean showMonthBtn) {
        if (showMonthBtn)
            attendanceMonthBtn.setVisibility(View.VISIBLE);
        else
            attendanceMonthBtn.setVisibility(View.GONE);

        if (title != null && !title.isEmpty()) {
                toolbarTitle.setText(title);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAppRunning = false;
    }

}
