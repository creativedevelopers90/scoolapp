package com.scool.qt.activity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.google.android.material.appbar.AppBarLayout;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.model.parent.Diary;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
public class ParentReplyDiaryActivity extends BaseActivity implements NetworkView {
    @BindView(R.id.add_diary_cancel)
    TextView addDiaryCancel;
    @BindView(R.id.add_diary_toolbar_title)
    TextView addDiaryToolbarTitle;
    @BindView(R.id.add_diary_done)
    TextView addDiaryDone;
    @BindView(R.id.add_diary_toolbar)
    Toolbar addDiaryToolbar;
    @BindView(R.id.applayout)
    AppBarLayout applayout;
    @BindView(R.id.add_diary_content)
    EditText addDiaryContent;
    private Diary diary;
    private NetworkPresenter mNetworkPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_diary);
        ButterKnife.bind(this);
        addDiaryToolbarTitle.setText(getString(R.string.kParent) + " Reply");
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        diary = (Diary) getIntent().getExtras().getParcelable("diary");
    }

    @OnClick(R.id.add_diary_done)
    public void onAddReplyClicked() {
        if (Utils.isNetworkAvailable(ParentReplyDiaryActivity.this)) {
            if (addDiaryContent.getText().length() > 0) {
                mNetworkPresenter.parentDiaryReplyRequest(this, Constant.UNIQUE_SCHOOL_ID,
                        diary.getStudentId().intValue(), diary.getDate(),
                        loadParentData().getToken(), diary.getFacultyId().intValue(), diary.getId().intValue(),
                        addDiaryContent.getText().toString());
            }
        }
    }


    @OnClick(R.id.add_diary_cancel)
    public void onCancelReplyClicked() {
        onBackPressed();
    }


    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
        Utils.showSnackBar(this, getString(R.string.check_internet));
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case parent_diary_reply:
              //  iNotifyFragment.updateFragment();
                Utils.showSnackBar(this, "Parent Reply successfully");
                finish();
                break;
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case parent_diary_reply:
                break;
        }
    }



}
