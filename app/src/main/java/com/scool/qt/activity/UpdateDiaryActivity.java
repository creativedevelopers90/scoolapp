package com.scool.qt.activity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.model.DiaryList;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.request.UpdateDiaryRequest;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateDiaryActivity extends BaseActivity implements NetworkView {

    @BindView(R.id.update_diary_toolbar)
    Toolbar toolbar;
    @BindView(R.id.update_diary_toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.update_diary_content)
    EditText diaryContent;
    private NetworkPresenter mNetworkPresenter;
    private Bundle bundle;
    private DiaryList diary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_diary);
        ButterKnife.bind(this);

        bundle = getIntent().getBundleExtra("diary_bundle");
        diary = (DiaryList) bundle.getSerializable("selected_diary");

        toolbarTitle.setText(R.string.kUpdateDiaryText);
        diaryContent.setText(diary.getDiary());

        setActionBar();
    }

    /**
     * Method to set the action bar
     */
    private void setActionBar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> finish());
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @OnClick(R.id.update_diary_cancel)
    void onCancelAddDiaryPressed() {
        onBackPressed();
    }

    @OnClick(R.id.update_diary_done)
    void onDoneAddDiaryPressed() {
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        FacultyLoginResponse facultyLoginResponse = loadFacultyData();
        mNetworkPresenter.methodToUpdateDiaryByFaculty(this, generateRequest(), facultyLoginResponse.getToken());
    }

    /**
     * Method to load the faculty data
     *
     * @return
     */
    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", this);
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }

    /**
     * Method to generate update diary request object
     *
     * @return
     */
    private UpdateDiaryRequest generateRequest() {
        UpdateDiaryRequest mRequest = new UpdateDiaryRequest();
        mRequest.setSchoolID(Constant.UNIQUE_SCHOOL_ID);
        mRequest.setDiaryID(diary.getId().intValue());
        mRequest.setDiaryContent(diaryContent.getText().toString());
        mRequest.setDate(getDateFromString(diary.getDate()));
        mRequest.setNotified(true);
        return mRequest;
    }

    private Date getDateFromString(String date) {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date formatedDate = null;
        try {
            formatedDate = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatedDate;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        if (message.equalsIgnoreCase("OK")) {
            onBackPressed();
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        Utils.showSnackBar(this, message);
    }
}
