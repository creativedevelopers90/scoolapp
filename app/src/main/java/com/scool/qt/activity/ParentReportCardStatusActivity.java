package com.scool.qt.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.ParentReportCardRecyclerAdapter;
import com.scool.qt.model.Exam;
import com.scool.qt.model.parent.ParentReportCardDetailResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.widgets.ProgressDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.scool.qt.EndpointEnums.parent_report_card_status;

public class ParentReportCardStatusActivity extends BaseActivity implements NetworkView {
    @BindView(R.id.report_status_toolbar)
    Toolbar toolbar;
    @BindView(R.id.report_status_toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.exam_name_tv)
    TextView examNameTV;
    @BindView(R.id.subject_marks_list_rv)
    RecyclerView subjectMarksListRV;
    @BindView(R.id.applayout)
    AppBarLayout applayout;
    @BindView(R.id.subject_column_header)
    TextView subjectColumnHeader;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.marks_column_header)
    TextView marksColumnHeader;
    @BindView(R.id.constraintLayout2)
    RelativeLayout constraintLayout2;
    @BindView(R.id.parent_report_root)
    RelativeLayout parentReportRoot;
    @BindView(R.id.iv_nointernet)
    ImageView ivNointernet;
    @BindView(R.id.rl_nointernet)
    RelativeLayout rlNointernet;
    private NetworkPresenter mPresenter;
    private Bundle bundle;
    ParentReportCardRecyclerAdapter mAdapter;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_report_card_status);
        mPresenter = new NetworkPresenter(this, new NetworkInteractor());
        bundle = getIntent().getExtras();

        ButterKnife.bind(this);
        setActionBar();
        initView();
    }

    /**
     * Method to set the action bar
     */
    private void setActionBar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> finish());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    /**
     * Method to initialize view
     */
    private void initView() {
        toolbarTitle.setText(Constant.ACTIVE_STUDENT.getName() + ", " + Constant.ACTIVE_STUDENT.getClassName());
        examNameTV.setText(((Exam) bundle.getSerializable("selected_exam")).getName());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        subjectMarksListRV.setLayoutManager(mLayoutManager);
        subjectMarksListRV.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new ParentReportCardRecyclerAdapter(this);
        subjectMarksListRV.setAdapter(mAdapter);
        fetchReportCardStatus();
    }

    /**
     * Method to fetch report card status from the server
     */
    private void fetchReportCardStatus() {
        mPresenter.methodToFetchParentReportStatusDetails(this, Constant.UNIQUE_SCHOOL_ID,
                Constant.ACTIVE_STUDENT.getStudentId().intValue(), ((Exam) bundle.getSerializable("selected_exam")).getName(), loadParentData().getToken());
    }


    @Override
    public void showProgress() {
        if (dialog == null) {
            dialog = new ProgressDialog(this, true, "Progress");
            dialog.show();
        }
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
    }

    @Override
    public void noInternetConnection() {
        rlNointernet.setVisibility(View.VISIBLE);
        parentReportRoot.setVisibility(View.GONE);
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case parent_report_card_status:
                mAdapter.addItemsInList(((ParentReportCardDetailResponse) object).getMark());
                break;
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        if (endpointType == parent_report_card_status)
            Log.e("== " + endpointType + " ==>", "onEndpointFailure: " + message);
    }
}
