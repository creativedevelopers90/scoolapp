package com.scool.qt.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.FacultyAllTimeTableAdapter;
import com.scool.qt.model.AllClassTimeTableResp;
import com.scool.qt.model.AllClassTimetableList;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.DayOfWeeks;
import com.scool.qt.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FacultyAllClassTimeTableActivity extends BaseActivity implements NetworkView {

    NetworkPresenter mApiPresenter;
    FacultyAllTimeTableAdapter mAdapter;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.rv_faculty)
    RecyclerView rvFaculty;
    private List<AllClassTimetableList> mAllTimeTableList = new ArrayList<AllClassTimetableList>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faculty_all_timetable_list);
        ButterKnife.bind(this);
        mApiPresenter = new NetworkPresenter(this, new NetworkInteractor());
       // constraintLayout2.setVisibility(View.GONE);
        //tvClassId.setVisibility(View.GONE);
        appBarLayout.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        toolbarTitle.setText(getString(R.string.kAllClassText));
        toolbar.setNavigationOnClickListener(view -> finish());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getAllTimeTableList();
        rvFaculty.setHasFixedSize(true);
        mAdapter = new FacultyAllTimeTableAdapter(this,mAllTimeTableList);
        rvFaculty.setLayoutManager(new LinearLayoutManager(FacultyAllClassTimeTableActivity.this,
                LinearLayoutManager.VERTICAL, false));
        rvFaculty.getLayoutManager().scrollToPosition(0);
        rvFaculty.setAdapter(mAdapter);
    }

    private void getAllTimeTableList() {
        FacultyLoginResponse facultyLoginResponse = loadFacultyData();
        mApiPresenter.methodToFetchAllTimeTable(this, Constant.UNIQUE_SCHOOL_ID, facultyLoginResponse.getFacultyId(), facultyLoginResponse.getToken());
    }

    /**
     * Method to load the faculty data
     *
     * @return
     */
    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", this);
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }


    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
        Utils.showSnackBar(this, getString(R.string.check_internet));
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case faculy_get_class_time_table:
                AllClassTimeTableResp mTimeTableItem = (AllClassTimeTableResp) object;
                mTimeTableItem.getTimeTables().stream().forEach(obj -> {
                    if (obj.getMonday() != null){
                        mAllTimeTableList.add(new AllClassTimetableList("", "",
                                0, "",0,"", true, "Monday", ""));
                        obj.getMonday().stream().forEach(mondayItem -> {
                            mAllTimeTableList.add(new AllClassTimetableList(mondayItem.getSchoolId(),
                                    mondayItem.getSubjectName(),
                                    mondayItem.getId(), mondayItem.getTime(),
                                    mondayItem.getClassListId(),
                                    mondayItem.getDay(), false, "", mondayItem.getClassName()));
                        });
                    }
                    if (obj.getTuesday() != null){
                        mAllTimeTableList.add(new AllClassTimetableList("", "",
                                0, "",0,"", true, "Tuesday", ""));
                        obj.getTuesday().stream().forEach(tuesdayItem -> {
                            mAllTimeTableList.add(new AllClassTimetableList(tuesdayItem.getSchoolId(),
                                    tuesdayItem.getSubjectName(),
                                    tuesdayItem.getId(), tuesdayItem.getTime(),
                                    tuesdayItem.getClassListId(),
                                    tuesdayItem.getDay(), false, "", tuesdayItem.getClassName()));
                        });
                    }
                    if (obj.getWednesday() != null) {
                        mAllTimeTableList.add(new AllClassTimetableList("", "",
                                0, "",0,"", true, "Wednesday", ""));
                        obj.getWednesday().forEach(wednesdayItem -> mAllTimeTableList.add(new AllClassTimetableList(wednesdayItem.getSchoolId(),
                                wednesdayItem.getSubjectName(),
                                wednesdayItem.getId(), wednesdayItem.getTime(),
                                wednesdayItem.getClassListId(),
                                wednesdayItem.getDay(), false, "", wednesdayItem.getClassName())));
                    }

                    if(obj.getThursday() != null){
                        mAllTimeTableList.add(new AllClassTimetableList("", "",
                                0, "",0,"", true, "Thursday", ""));
                        obj.getThursday().forEach(thursdayItem -> mAllTimeTableList.add(new AllClassTimetableList(thursdayItem.getSchoolId(),
                                thursdayItem.getSubjectName(),
                                thursdayItem.getId(), thursdayItem.getTime(),
                                thursdayItem.getClassListId(),
                                thursdayItem.getDay(), false, "", thursdayItem.getClassName())));
                    }

                    if(obj.getFriday() != null){
                        mAllTimeTableList.add(new AllClassTimetableList("", "",
                                0, "",0,"", true, "Friday", ""));

                        obj.getFriday().forEach(fridayItem -> mAllTimeTableList.add(new AllClassTimetableList(fridayItem.getSchoolId(),
                                fridayItem.getSubjectName(),
                                fridayItem.getId(), fridayItem.getTime(),
                                fridayItem.getClassListId(),
                                fridayItem.getDay(), false, "", fridayItem.getClassName())));
                    }

                    if(obj.getSaturday() != null){
                        mAllTimeTableList.add(new AllClassTimetableList("", "",
                                0, "",0,"", true, "Saturday", ""));
                        obj.getSaturday().forEach(satItem -> mAllTimeTableList.add(new AllClassTimetableList(satItem.getSchoolId(),
                                satItem.getSubjectName(),
                                satItem.getId(), satItem.getTime(),
                                satItem.getClassListId(),
                                satItem.getDay(), false, "", satItem.getClassName())));
                    }

                     if(obj.getSunday() != null){
                     mAllTimeTableList.add(new AllClassTimetableList("", "",
                            0, "",0,"", true, "Sunday", ""));
                         obj.getSunday().forEach(sundayItem -> mAllTimeTableList.add(new AllClassTimetableList(sundayItem.getSchoolId(),
                                 sundayItem.getSubjectName(),
                                 sundayItem.getId(), sundayItem.getTime(),
                                 sundayItem.getClassListId(),
                                 sundayItem.getDay(), false, "", sundayItem.getClassName())));
                       }
                });
                List<AllClassTimetableList> finalList = new ArrayList<>();
                for(DayOfWeeks day: DayOfWeeks.values()){
                    switch (day) {
                        case Sunday:
                            finalList.addAll(mAllTimeTableList.stream().filter(p -> p.getDay().equalsIgnoreCase(DayOfWeeks.Sunday.toString()) || p.getHeaderName().equalsIgnoreCase(DayOfWeeks.Sunday.toString())).collect(Collectors.toList()));
                            break;
                        case Monday:
                            finalList.addAll(mAllTimeTableList.stream().filter(p -> p.getDay().equalsIgnoreCase(DayOfWeeks.Monday.toString()) || p.getHeaderName().equalsIgnoreCase(DayOfWeeks.Monday.toString())).collect(Collectors.toList()));
                            break;
                        case Tuesday:
                            finalList.addAll(mAllTimeTableList.stream().filter(p -> p.getDay().equalsIgnoreCase(DayOfWeeks.Tuesday.toString()) || p.getHeaderName().equalsIgnoreCase(DayOfWeeks.Tuesday.toString())).collect(Collectors.toList()));
                            break;
                        case Wednesday:
                            finalList.addAll(mAllTimeTableList.stream().filter(p -> p.getDay().equalsIgnoreCase(DayOfWeeks.Wednesday.toString()) || p.getHeaderName().equalsIgnoreCase(DayOfWeeks.Wednesday.toString())).collect(Collectors.toList()));
                            break;
                        case Thursday:
                            finalList.addAll(mAllTimeTableList.stream().filter(p -> p.getDay().equalsIgnoreCase(DayOfWeeks.Thursday.toString()) || p.getHeaderName().equalsIgnoreCase(DayOfWeeks.Thursday.toString())).collect(Collectors.toList()));
                            break;
                        case Friday:
                            finalList.addAll(mAllTimeTableList.stream().filter(p -> p.getDay().equalsIgnoreCase(DayOfWeeks.Friday.toString()) || p.getHeaderName().equalsIgnoreCase(DayOfWeeks.Friday.toString())).collect(Collectors.toList()));
                            break;
                        case Saturday:
                            finalList.addAll(mAllTimeTableList.stream().filter(p -> p.getDay().equalsIgnoreCase(DayOfWeeks.Saturday.toString()) || p.getHeaderName().equalsIgnoreCase(DayOfWeeks.Saturday.toString())).collect(Collectors.toList()));
                            break;
                    }
                }
                mAdapter.updateList(finalList);
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {

    }
}
