package com.scool.qt.activity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.HomeworkList;
import com.scool.qt.model.request.UpdateHomeworkRequest;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateHomeworkActivity extends BaseActivity implements NetworkView {

    @BindView(R.id.update_homework_toolbar)
    Toolbar toolbar;
    @BindView(R.id.update_homework_toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.update_homework_content)
    EditText homeworkContentET;
    @BindView(R.id.homework_target_date_tv)
    TextView targetDateTV;
    private int mYear, mMonth, mDay;
    private HomeworkList homework;
    private Bundle bundle;
    private NetworkPresenter mNetworkPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_homework);
        ButterKnife.bind(this);

        bundle = getIntent().getBundleExtra("homework_bundle");
        homework = (HomeworkList) bundle.getSerializable("selected_homework");

        toolbarTitle.setText(R.string.kUpdateHomeworkText);
        homeworkContentET.setText(homework.getHomeWork());
        targetDateTV.setText("Target Date: " + homework.getTargetDate());
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            cal.setTime(df.parse(homework.getTargetDate()));
            mYear = cal.get(Calendar.YEAR);
            mMonth = cal.get(Calendar.MONTH);
            mDay = cal.get(Calendar.DAY_OF_MONTH);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        setActionBar();
    }

    /**
     * Method to set the action bar
     */
    private void setActionBar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> finish());
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @OnClick(R.id.update_homework_cancel)
    void onCancelAddHomeworkPressed() {
        onBackPressed();
    }

    @OnClick(R.id.update_homework_done)
    void onDoneAddHomeworkPressed() {
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        mNetworkPresenter.methodToUpdateHomeworkByFaculty(this, generateRequest(), loadFacultyData().getToken());
    }

    @OnClick(R.id.homework_target_date_tv)
    public void onTargetDatePressed() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        targetDateTV.setText("Target Date: " + dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        mYear = year;
                        mMonth = monthOfYear;
                        mDay = dayOfMonth;
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    /**
     * Method to load the faculty data
     *
     * @return
     */
    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", this);
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }

    /**
     * Method to generate request
     *
     * @return
     */
    private UpdateHomeworkRequest generateRequest() {
        UpdateHomeworkRequest mRequest = new UpdateHomeworkRequest();
        mRequest.setSchoolID(Constant.UNIQUE_SCHOOL_ID);
        mRequest.setHomeworkID(homework.getId().intValue());
        mRequest.setHomeworkContent(homeworkContentET.getText().toString());
        Calendar cal = Calendar.getInstance();
        cal.set(mYear, mMonth, mDay);
        mRequest.setTargetDate(cal.getTime());
        mRequest.setNotified(true);

        return mRequest;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        if (message.equalsIgnoreCase("OK")) {
            onBackPressed();
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        Log.e("== UPDATE HOMEWORK ==>", "onEndpointFailure: " + endpointType + " : " + message);
        Utils.showSnackBar(this, message);
    }
}
