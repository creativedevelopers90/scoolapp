package com.scool.qt.activity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.ReportCardMarksListAdapter;
import com.scool.qt.model.Exam;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.MarksList;
import com.scool.qt.model.MarksListResponse;
import com.scool.qt.model.request.AddMarksRequest;
import com.scool.qt.model.request.UpdateMarksRequest;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;
import com.scool.qt.widgets.ProgressDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FacultyMarksListReportActivity extends BaseActivity implements NetworkView, ReportCardMarksListAdapter.AdapterButtonClickListener {

    @BindView(R.id.report_card_toolbar)
    Toolbar toolbar;
    @BindView(R.id.report_card_toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.marks_list_rv)
    RecyclerView marksListRV;
    private Bundle mBundle;
    private NetworkPresenter mPresenter;
    private LinearLayoutManager mLayoutManager;
    ReportCardMarksListAdapter mAdapter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_marks_list_report);
        ButterKnife.bind(this);
        setActionBar();

        mBundle = getIntent().getExtras();

        toolbarTitle.setText(Constant.ACTIVE_CLASS.getName());
        mAdapter = new ReportCardMarksListAdapter(FacultyMarksListReportActivity.this, FacultyMarksListReportActivity.this);
        marksListRV.setAdapter(mAdapter);
        marksListRV.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        marksListRV.setLayoutManager(mLayoutManager);

        mPresenter = new NetworkPresenter(this, new NetworkInteractor());
        mPresenter.methodToFetchMarksListForFaculty(this, Constant.UNIQUE_SCHOOL_ID, Constant.ACTIVE_CLASS.getId().intValue(),
                ((Exam) mBundle.getSerializable("selected_exam")).getId().intValue(), loadFacultyData().getFacultyId(), loadFacultyData().getToken());
    }

    /**
     * Method to set the action bar
     */
    private void setActionBar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> finish());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    /**
     * Method to load the faculty data
     *
     * @return
     */
    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", this);
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }


    private AddMarksRequest generateAddMarksRequest(MarksList marks, float updatedMarks) {
        AddMarksRequest addMarksRequest = new AddMarksRequest();
        addMarksRequest.setSchoolID(Constant.UNIQUE_SCHOOL_ID);
        addMarksRequest.setStudentID(marks.getStudentId().intValue());
        addMarksRequest.setSubjectID(((Exam)mBundle.getSerializable("selected_exam")).getSubjectId().intValue());
        addMarksRequest.setExamID(((Exam)mBundle.getSerializable("selected_exam")).getId().intValue());
        addMarksRequest.setMarks(updatedMarks);
        return addMarksRequest;
    }

    /**
     * Method to generate form object to change the marks of the student
     *
     * @param marks
     * @return
     */
    private UpdateMarksRequest generateUpdateMarksRequest(MarksList marks, float updatedMarks) {
        UpdateMarksRequest updateMarksRequest = new UpdateMarksRequest();
        updateMarksRequest.setSchoolID(Constant.UNIQUE_SCHOOL_ID);
        updateMarksRequest.setMarksID(marks.getMarkId().intValue());
        updateMarksRequest.setMarks(updatedMarks);
        return updateMarksRequest;
    }

    @Override
    public void showProgress() {
        if(progressDialog == null) {
            progressDialog = new ProgressDialog(this, false, "");
            progressDialog.show();
        }

    }

    @Override
    public void hideProgress() {
            progressDialog.dismiss();
    }

    @Override
    public void noInternetConnection() {

    }

    /**
     *
     * @param object
     * @param message
     * @param endpointType
     */
    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        if (endpointType == EndpointEnums.marks_list) {
            mAdapter.addItemsInList(((MarksListResponse) object).getMarksList());
        } else if (endpointType == EndpointEnums.faculty_update_marks || endpointType == EndpointEnums.faculty_save_marks) {
            Utils.showSnackBar(this, message);
        }
    }

    /**
     *
     * @param message
     * @param endpointType
     */
    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        if (endpointType == EndpointEnums.marks_list)
            Log.e("== FACULTY EXAM RES ==>", "onEndpointFailure: " + endpointType + " : " + message);
        else if (endpointType == EndpointEnums.faculty_update_marks || endpointType == EndpointEnums.faculty_save_marks)  {
                Log.e("== FACULTY SAVE RES ==>", "onEndpointFailure: " + endpointType + " : " + message);
                Utils.showSnackBar(this, message);
            }
    }

    @Override
    public void onSaveMarksSaveClicked(MarksList marks, float updatedMarks) {
        if(marks.getMarks().isEmpty()) {
            mPresenter.methodToAddMarksByFaculty(FacultyMarksListReportActivity.this, generateAddMarksRequest(marks, updatedMarks), loadFacultyData().getToken());
        } else {
            mPresenter.methodToUpdateMarksByFaculty(FacultyMarksListReportActivity.this, generateUpdateMarksRequest(marks, updatedMarks), loadFacultyData().getToken());
        }
    }

    @Override
    public void unknownMarks() {
        Utils.showSnackBar(this, getString(R.string.str_valid_marks).toString());
    }
}
