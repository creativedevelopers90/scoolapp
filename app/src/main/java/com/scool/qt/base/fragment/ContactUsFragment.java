package com.scool.qt.base.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.model.ContactUsResp;
import com.scool.qt.model.StartResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ContactUsFragment extends Fragment implements NetworkView {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    Unbinder unbinder;
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;
    @BindView(R.id.tv_school_slogan)
    TextView tvSchoolSlogan;
    @BindView(R.id.layout_top)
    ConstraintLayout layoutTop;
    @BindView(R.id.tv_contactus)
    TextView tvContactus;
    @BindView(R.id.sv_text)
    NestedScrollView svText;
    @BindView(R.id.iv_nointernet)
    ImageView ivNointernet;
    @BindView(R.id.rl_nointernet)
    RelativeLayout rlNointernet;
    @BindView(R.id.rl_maincontent)
    RelativeLayout rlMaincontent;
    @BindView(R.id.img_contactus)
    RecyclingImageView imgContactus;

    private NetworkPresenter mApiPresenter;


    private OnFragmentInteractionListener mListener;

    public ContactUsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ContactUsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ContactUsFragment newInstance(String param1, String param2) {
        ContactUsFragment fragment = new ContactUsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mApiPresenter = new NetworkPresenter(this, new NetworkInteractor());
        getContactUsSchoolData();
    }

    private void getContactUsSchoolData() {
        mApiPresenter.methodToGetContactUsData(getActivity(), Constant.UNIQUE_SCHOOL_ID);
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
        rlNointernet.setVisibility(View.VISIBLE);
        rlMaincontent.setVisibility(View.GONE);
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        if (endpointType == EndpointEnums.contact_us) {
            svText.setVisibility(View.VISIBLE);
            imgContactus.setVisibility(View.VISIBLE);
            ContactUsResp mContactUsResp = (ContactUsResp) object;
            tvContactus.setText(mContactUsResp.getContactUs());
            StartResponse mStartResp = Utils.loadSchoolData(getActivity());
            tvSchoolname.setText(mStartResp.getName());
            tvSchoolSlogan.setText(mStartResp.getSlogan());
            uploadImageFromServer(Constant.IMAGE_BASE_URL + mStartResp.getLogo());
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case contact_us:
                Utils.showSnackBar(getActivity(), message);
                svText.setVisibility(View.GONE);
                imgContactus.setVisibility(View.GONE);
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void uploadImageFromServer(String logo) {
        Glide.with(getActivity()).load(logo)
                .thumbnail(0.5f)
                .into(imgSchool);
    }
}

