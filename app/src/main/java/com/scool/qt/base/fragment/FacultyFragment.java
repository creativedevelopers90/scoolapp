package com.scool.qt.base.fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.FacultyFragmentRecyclerAdapter;
import com.scool.qt.model.FacultiesItem;
import com.scool.qt.model.SchoolFacultiesResp;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
public class FacultyFragment extends Fragment implements NetworkView, FacultyFragmentRecyclerAdapter.IItemClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.feed_listview)
    RecyclerView feedListview;
    @BindView(R.id.feed_frame_layout)
    RelativeLayout feedFrameLayout;
    Unbinder unbinder;
    List<FacultiesItem> mFacultiesItem = new ArrayList<>();

    FacultyFragmentRecyclerAdapter mAdapter;
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;
    @BindView(R.id.rl_maincontent)
    RelativeLayout rlMaincontent;
    @BindView(R.id.iv_nointernet)
    ImageView ivNointernet;
    @BindView(R.id.rl_nointernet)
    RelativeLayout rlNointernet;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private NetworkPresenter mApiPresenter;

    public FacultyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FacultyFragment.
     */
    public static FacultyFragment newInstance(String param1, String param2) {
        FacultyFragment fragment = new FacultyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_faculty, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mApiPresenter = new NetworkPresenter(this, new NetworkInteractor());
        if (mFacultiesItem != null)
            mAdapter = new FacultyFragmentRecyclerAdapter(getActivity(), "withoutLogin", FacultyFragment.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        feedListview.setLayoutManager(mLayoutManager);
        feedListview.setItemAnimator(new DefaultItemAnimator());
        feedListview.setAdapter(mAdapter);
        tvSchoolname.setText(Utils.loadSchoolData(getActivity()).getName());
        Glide.with(getActivity())
                .load(Constant.IMAGE_BASE_URL + Utils.loadSchoolData(getActivity()).getLogo())
                .thumbnail(0.5f)
                .into(imgSchool);
        schoolFaultyData();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
        rlNointernet.setVisibility(View.VISIBLE);
        rlMaincontent.setVisibility(View.GONE);
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case school_faculty:
                progressBar.setVisibility(View.GONE);
                SchoolFacultiesResp schoolFacultiesResp = (SchoolFacultiesResp) object;
                mFacultiesItem = schoolFacultiesResp.getFaculties();
                mAdapter.updateList(mFacultiesItem);
                break;
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }




    private void schoolFaultyData() {
        progressBar.setVisibility(View.VISIBLE);
        mApiPresenter.methodToGetSchoolFacultiesData(getActivity(), Constant.UNIQUE_SCHOOL_ID);
    }

    @Override
    public void itemClick(View view, int position, FacultiesItem mFacultiesItem) {

    }
}
