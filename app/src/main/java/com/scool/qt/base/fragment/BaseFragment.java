package com.scool.qt.base.fragment;

import android.app.AlertDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.scool.qt.R;
import com.scool.qt.helper.PrefData;
import com.scool.qt.helper.ThemeHelper;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;
import com.scool.qt.widgets.ProgressDialog;

public class BaseFragment extends Fragment {
    public int buttonThemeId;
    public String theme;
    public PrefData prefData;
    protected Resources resources;
    public ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        theme = ThemeHelper.getPrefCurrentTheme(getActivity());
        prefData = new PrefData(getActivity());
        if(theme.equalsIgnoreCase("blue")){
            getActivity().setTheme(R.style.Theme_App_Blue);
            buttonThemeId = R.drawable.btn_rounded_cornor_less;
        } else if(theme.equalsIgnoreCase("red")) {
            getActivity().setTheme(R.style.Theme_App_Red);
            buttonThemeId = R.drawable.btn_rounded_red_cornor;
        } else if(theme.equalsIgnoreCase("green")) {
            getActivity().setTheme(R.style.Theme_App_Green);
            buttonThemeId = R.drawable.btn_rounded_green_cornor;
        } else if(theme.equalsIgnoreCase("purple")) {
            getActivity().setTheme(R.style.Theme_App_Purple);
            buttonThemeId = R.drawable.btn_rounded_purple_cornor;
        }else if(theme.equalsIgnoreCase("black")) {
            getActivity().setTheme(R.style.Theme_App_Black);
            buttonThemeId = R.drawable.btn_rounded_black_cornor;
        }else if(theme.equalsIgnoreCase("brown")) {
            getActivity().setTheme(R.style.Theme_App_Brown);
            buttonThemeId = R.drawable.btn_rounded_brown_cornor;
        }
    }

    public void alertDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        builder.setTitle(title);
        builder.setMessage(message);
        String positiveText = getString(R.string.kokay);
        builder.setPositiveButton(positiveText,
                (dialog, which) -> dialog.dismiss());
        AlertDialog dialog = builder.create();
        dialog.show();
    }


}
