package com.scool.qt.base.fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.model.AboutUsResp;
import com.scool.qt.model.StartResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
/**
 *
 */
public class AboutUsFragment extends Fragment implements NetworkView {
    @BindView(R.id.tv_aboutus)
    TextView tvAboutus;
    Unbinder unbinder;
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;
    @BindView(R.id.tv_school_slogan)
    TextView tvSchoolSlogan;
    @BindView(R.id.layout_top)
    ConstraintLayout layoutTop;
    private NetworkPresenter mApiPresenter;

    public AboutUsFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about_us, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mApiPresenter = new NetworkPresenter(this, new NetworkInteractor());
        getAboutUsSchoolData();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
        Utils.showSnackBar(getActivity(),getString(R.string.check_internet));
    }

    /**
     *
     * @param object
     * @param message
     * @param endpointType
     * overide Method in Case of Success
     */
    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case about_us:
                if(getView() != null){
                    AboutUsResp aboutUsResp = (AboutUsResp) object;
                    StartResponse mStartResp = Utils.loadSchoolData(getActivity());
                    tvAboutus.setText(aboutUsResp.getAbout_us());
                    tvSchoolname.setText(mStartResp.getName());
                    tvSchoolSlogan.setText(mStartResp.getSlogan());
                    uploadImageFromServer(Constant.IMAGE_BASE_URL+mStartResp.getLogo());
                }
                break;
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case about_us:
                Utils.showSnackBar(getActivity(), message);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void getAboutUsSchoolData() {
        mApiPresenter.methodToGetAboutUsSchool(getActivity(), Constant.UNIQUE_SCHOOL_ID);
    }

    public void uploadImageFromServer(String logo){
        Glide.with(getActivity()).load(logo)
                .thumbnail(0.5f)
                .into(imgSchool);
    }
}
