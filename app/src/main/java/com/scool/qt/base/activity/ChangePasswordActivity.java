package com.scool.qt.base.activity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.helper.PrefData;
import com.scool.qt.model.ChangePasswordResponse;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.request.ChangePasswordRequest;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;
import com.scool.qt.widgets.ThemeButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordActivity extends BaseActivity implements NetworkView {
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.applayout)
    AppBarLayout applayout;
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;
    @BindView(R.id.current_pass_img)
    ImageView currentPassImg;
    @BindView(R.id.current_pass_et)
    EditText currentPassEt;
    @BindView(R.id.new_pass_img)
    ImageView newPassImg;
    @BindView(R.id.new_pass_et)
    EditText newPassEt;
    @BindView(R.id.verify_pass_img)
    ImageView verifyPassImg;
    @BindView(R.id.verify_pass_et)
    EditText verifyPassEt;
    @BindView(R.id.faculty_change_pwd_btn)
    ThemeButton facultyChangePwdBtn;
    private NetworkPresenter mNetworkPresenter;
    private PrefData prefData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());
        prefData = new PrefData(this);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> finish());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText(getResources().getText(R.string.kChangePassword));
        tvSchoolname.setText(Utils.loadSchoolData(ChangePasswordActivity.this).getName());
        Glide.with(ChangePasswordActivity.this)
                .load(Constant.IMAGE_BASE_URL + Utils.loadSchoolData(ChangePasswordActivity.this).getLogo())
                .thumbnail(0.5f)
                .into(imgSchool);
        facultyChangePwdBtn.setBackground(getDrawable(buttonThemeId));
    }

    @OnClick(R.id.faculty_change_pwd_btn)
    public void changePasswordBtnClicked() {
        Utils.hideKeyboard(this);
        if (!currentPassEt.getText().toString().isEmpty() &&
                !newPassEt.getText().toString().isEmpty() &&
                !verifyPassEt.getText().toString().isEmpty()) {
            if (verifyPassEt.getText().toString().equals(newPassEt.getText().toString())) {
                ChangePasswordRequest mRequest = new ChangePasswordRequest();
                mRequest.setSchoolID(Integer.parseInt(Constant.UNIQUE_SCHOOL_ID));
                mRequest.setCurrentPassword(currentPassEt.getText().toString());
                mRequest.setNewPassword(newPassEt.getText().toString());
                switch (prefData.getLoginType()) {
                    case Constant.LOGIN_TYPE_FACULTY:
                        if (Constant.facultyData != null)
                            mRequest.setUserName(Constant.facultyData.getUsername());
                        FacultyLoginResponse facultyLoginResponse = loadFacultyData();
                        mNetworkPresenter.methodToChangePasswordForFaculty(this, mRequest, facultyLoginResponse.getToken());
                        break;
                    case Constant.LOGIN_TYPE_PARENT:
                        if (Constant.parentData != null)
                            mRequest.setUserName(Constant.parentData.getParent().getUsername());
                        mNetworkPresenter.methodToChangePasswordForParent(this, mRequest, loadParentData().getToken());
                        break;
                }
            } else {
                Utils.showSnackBar(this, "New password mismatch");
                verifyPassEt.setText("");
                newPassEt.setText("");
            }
        } else
            Utils.showSnackBar(this, "Please fill all the fields");
    }

    private FacultyLoginResponse loadFacultyData() {
        Gson gson1 = new Gson();
        String json1 = Utils.loadPrefrence(Constant.PREF_LOGGED_IN_DATA,
                "", ChangePasswordActivity.this);
        return gson1.fromJson(json1, FacultyLoginResponse.class);
    }


    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {

    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        Utils.showSnackBar(this, ((ChangePasswordResponse) object).getMessage());
        verifyPassEt.setText("");
        newPassEt.setText("");
        currentPassEt.setText("");
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        Utils.showSnackBar(this, message);
    }
}
