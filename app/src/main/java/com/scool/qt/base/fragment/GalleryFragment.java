package com.scool.qt.base.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.base.activity.ImageActivity;
import com.scool.qt.base.adapters.GalleryAdapter;
import com.scool.qt.model.StartResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FacultyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GalleryFragment extends Fragment implements NetworkView, GalleryAdapter.GalleryAdapterListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.feed_listview)
    RecyclerView feedListview;
    @BindView(R.id.feed_frame_layout)
    RelativeLayout feedFrameLayout;
    Unbinder unbinder;
    List<String> mCircularList = new ArrayList<>();

    GalleryAdapter mAdapter;
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;
    @BindView(R.id.rl_maincontent)
    RelativeLayout rlMaincontent;
    @BindView(R.id.iv_nointernet)
    ImageView ivNointernet;
    @BindView(R.id.rl_nointernet)
    RelativeLayout rlNointernet;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private NetworkPresenter mApiPresenter;

    private OnFragmentInteractionListener mListener;

    public GalleryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FacultyFragment.
     */
    public static GalleryFragment newInstance(String param1, String param2) {
        GalleryFragment fragment = new GalleryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_faculty, container, false);
        unbinder = ButterKnife.bind(this, view);
        progressBar.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mApiPresenter = new NetworkPresenter(this, new NetworkInteractor());
        if (Utils.isNetworkAvailable(getActivity())) {
            rlNointernet.setVisibility(View.GONE);
            rlMaincontent.setVisibility(View.VISIBLE);
            extractDatafromPref();
        } else {
            rlNointernet.setVisibility(View.VISIBLE);
            rlMaincontent.setVisibility(View.GONE);
        }

    }

    private void extractDatafromPref() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        tvSchoolname.setText(Utils.loadSchoolData(getActivity()).getName());
        if (mCircularList != null)
            mAdapter = new GalleryAdapter(Utils.loadSchoolData(getActivity()).getImages(), getActivity(), this);
        Glide.with(getActivity())
                .load(Constant.IMAGE_BASE_URL + Utils.loadSchoolData(getActivity()).getLogo())
                .thumbnail(0.5f)
                .into(imgSchool);
        feedListview.setLayoutManager(mLayoutManager);
        feedListview.setItemAnimator(new DefaultItemAnimator());
        feedListview.setAdapter(mAdapter);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
        Utils.showSnackBar(getActivity(), getString(R.string.no_internet_connection));
    }


    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case start:
                if (getView() == null) return;
                progressBar.setVisibility(View.GONE);
                StartResponse schoolFacultiesResp = (StartResponse) object;
                mCircularList = schoolFacultiesResp.getImages();
                mAdapter = new GalleryAdapter(mCircularList, getActivity(), this);
                feedListview.setAdapter(mAdapter);
                break;
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case start:
                if (getView() == null) return;
                progressBar.setVisibility(View.GONE);
                Utils.showSnackBar(getActivity(), message);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * Method to listen the gallery selected item callback
     *
     * @param mGalleryImagePath
     */
    @Override
    public void onGalleryItemSelected(String mGalleryImagePath) {
        Bundle bundle = new Bundle();
        bundle.putString("type", "gallery");
        bundle.putSerializable("gallery_image_path", mGalleryImagePath);
        Intent intent = new Intent(getActivity(), ImageActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void schoolGalleryData() {
        mApiPresenter.methodToGetSchoolHomeData(getActivity(), Constant.UNIQUE_SCHOOL_ID);
    }

}
