package com.scool.qt.base.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.scool.qt.R;
import com.scool.qt.base.activity.SignInActivity;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class LoginFragment extends Fragment {
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;
    @BindView(R.id.img_parent)
    RecyclingImageView imgParent;
    @BindView(R.id.tv_parent)
    TextView tvParent;
    @BindView(R.id.view_parent)
    View viewParent;
    @BindView(R.id.parent_layout)
    ConstraintLayout parentLayout;
    @BindView(R.id.img_student)
    RecyclingImageView imgStudent;
    @BindView(R.id.tv_student)
    TextView tvStudent;
    @BindView(R.id.view_student)
    View viewStudent;
    @BindView(R.id.student_layout)
    ConstraintLayout studentLayout;
    @BindView(R.id.img_faculty)
    RecyclingImageView imgFaculty;
    @BindView(R.id.tv_faculty)
    TextView tvFaculty;
    @BindView(R.id.view_faculty)
    View viewFaculty;
    @BindView(R.id.faculty_layout)
    ConstraintLayout facultyLayout;
    Unbinder unbinder;
    @BindView(R.id.login_footer)
    AppCompatTextView loginFooter;

    public LoginFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvSchoolname.setText(Utils.loadSchoolData(getActivity()).getName());
        loginFooter.setText(getString(R.string.kFooterNotesTextBeforeLogin) + " " + Utils.loadSchoolData(getActivity()).getName());
        Glide.with(getActivity())
                .load(Constant.IMAGE_BASE_URL + Utils.loadSchoolData(getActivity()).getLogo())
                .thumbnail(0.5f)
                .into(imgSchool);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.parent_layout)
    void onParentLayoutClick() {
        Intent i = new Intent(getActivity(), SignInActivity.class);
        i.putExtra(Constant.LOGIN_TYPE, Constant.LOGIN_TYPE_PARENT);
        startActivity(i);
    }

    @OnClick(R.id.student_layout)
    void onStudentLayoutClick() {
        Intent i = new Intent(getActivity(), SignInActivity.class);
        i.putExtra(Constant.LOGIN_TYPE, Constant.LOGIN_TYPE_STUDENT);
        startActivity(i);
    }

    @OnClick(R.id.faculty_layout)
    void onFacultyLayoutClick() {
        Intent i = new Intent(getActivity(), SignInActivity.class);
        i.putExtra(Constant.LOGIN_TYPE, Constant.LOGIN_TYPE_FACULTY);
        startActivity(i);
    }
}
