package com.scool.qt.base.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.scool.qt.R;
import com.scool.qt.base.activity.ChangeLanguageActivity;
import com.scool.qt.base.activity.ChangeThemeActivity;
import com.scool.qt.helper.PrefData;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SettingsFragment extends Fragment {
    Unbinder unbinder;
    PrefData prefData;
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;
    @BindView(R.id.img_changelang)
    RecyclingImageView imgChangelang;
    @BindView(R.id.tv_changelang)
    TextView tvChangelang;
    @BindView(R.id.view_changelang)
    View viewChangelang;
    @BindView(R.id.change_language)
    ConstraintLayout changeLanguage;
    @BindView(R.id.img_chngethmem)
    RecyclingImageView imgChngethmem;
    @BindView(R.id.tv_chngtheme)
    TextView tvChngtheme;
    @BindView(R.id.view_thme)
    View viewThme;
    @BindView(R.id.change_theme)
    ConstraintLayout changeTheme;
    @BindView(R.id.setting_footer)
    AppCompatTextView settingFooter;

    public SettingsFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        unbinder = ButterKnife.bind(this, view);
        prefData = new PrefData(getActivity());
        tvSchoolname.setText(Utils.loadSchoolData(getActivity()).getName());
        Glide.with(getActivity())
                .load(Constant.IMAGE_BASE_URL + Utils.loadSchoolData(getActivity()).getLogo())
                .thumbnail(0.5f)
                .into(imgSchool);
        settingFooter.setText(getActivity().getString(R.string.kCopyrightText) + "\n App Version - 1.0");
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.change_language)
    void onLanguageBtnClick(){
        Intent i = new Intent(getActivity(), ChangeLanguageActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.change_theme)
    void onChangeTheme(){
        Intent i = new Intent(getActivity(), ChangeThemeActivity.class);
        startActivity(i);
    }
}
