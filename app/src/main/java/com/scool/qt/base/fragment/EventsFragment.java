package com.scool.qt.base.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.adapter.EventsFragmentAdapter;
import com.scool.qt.base.activity.ImageActivity;
import com.scool.qt.model.CircularsItem;
import com.scool.qt.model.SchoolCircularResponse;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class EventsFragment extends Fragment implements NetworkView, EventsFragmentAdapter.EventFragmentAdapterListener {
    @BindView(R.id.feed_listview)
    RecyclerView feedListview;
    @BindView(R.id.feed_frame_layout)
    RelativeLayout feedFrameLayout;
    Unbinder unbinder;
    List<CircularsItem> mCircularList = new ArrayList<>();

    EventsFragmentAdapter mAdapter;
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;
    @BindView(R.id.iv_nointernet)
    ImageView ivNointernet;
    @BindView(R.id.rl_nointernet)
    RelativeLayout rlNointernet;
    @BindView(R.id.rl_maincontent)
    RelativeLayout rlMaincontent;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private NetworkPresenter mApiPresenter;


    public EventsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_faculty, container, false);
        unbinder = ButterKnife.bind(this, view);
        rlMaincontent.setVisibility(View.VISIBLE);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mApiPresenter = new NetworkPresenter(this, new NetworkInteractor());
        if (mCircularList != null)
            mAdapter = new EventsFragmentAdapter(mCircularList, getActivity(), this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        feedListview.setLayoutManager(mLayoutManager);
        feedListview.setItemAnimator(new DefaultItemAnimator());
        feedListview.setAdapter(mAdapter);
        tvSchoolname.setText(Utils.loadSchoolData(getActivity()).getName());
        Glide.with(getActivity())
                .load(Constant.IMAGE_BASE_URL + Utils.loadSchoolData(getActivity()).getLogo())
                .thumbnail(0.5f)
                .into(imgSchool);
        schoolFaultyData();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
        progressBar.setVisibility(View.GONE);
        rlNointernet.setVisibility(View.VISIBLE);
        rlMaincontent.setVisibility(View.GONE);
    }


    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case school_circular:
                if (getView() == null) return;
                progressBar.setVisibility(View.GONE);
                SchoolCircularResponse schoolFacultiesResp = (SchoolCircularResponse) object;
                mCircularList = schoolFacultiesResp.getCirculars();
                mAdapter.updateList(mCircularList);
                break;
        }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        switch (endpointType) {
            case school_circular:
                if (getView() == null) return;
                progressBar.setVisibility(View.GONE);
                Utils.showSnackBar(getActivity(), message);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * Listener to listen the selected item from the list of events
     *
     * @param mItem
     */
    @Override
    public void onItemSelected(CircularsItem mItem) {
        Bundle bundle = new Bundle();
        bundle.putString("type", "event");
        bundle.putSerializable("event_item", mItem);
        Intent intent = new Intent(getActivity(), ImageActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }


    private void schoolFaultyData() {
        progressBar.setVisibility(View.VISIBLE);
        mApiPresenter.methodToGetSchoolCircularData(getActivity(), Constant.UNIQUE_SCHOOL_ID);
    }

}
