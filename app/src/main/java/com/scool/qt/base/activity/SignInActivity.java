package com.scool.qt.base.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.scool.qt.BaseActivity;
import com.scool.qt.EndpointEnums;
import com.scool.qt.R;
import com.scool.qt.activity.ParentHomeActivity;
import com.scool.qt.faculty.activity.FacultyHomeActivity;
import com.scool.qt.model.FacultyLoginResponse;
import com.scool.qt.model.ParentLoginResponse;
import com.scool.qt.model.StudentLoginResponse;
import com.scool.qt.model.request.LoginUserRequest;
import com.scool.qt.network.NetworkInteractor;
import com.scool.qt.network.NetworkPresenter;
import com.scool.qt.network.NetworkView;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A login screen that offers login via email/password.
 */
public class SignInActivity extends BaseActivity implements NetworkView {
    @BindView(R.id.parent_toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.parent_toolbar)
    Toolbar toolbar;
    @BindView(R.id.email)
    AutoCompleteTextView email_edit;
    @BindView(R.id.password)
    EditText password_Edit;
    @BindView(R.id.email_sign_in_button)
    CircularProgressButton emailSignInButton;
    @BindView(R.id.email_login_form)
    LinearLayout emailLoginForm;
    @BindView(R.id.login_form)
    ScrollView loginForm;
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;
    @BindView(R.id.login_footer)
    AppCompatTextView loginFooter;
    // UI references.
    private NetworkPresenter mNetworkPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> finish());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        tvSchoolname.setText(Utils.loadSchoolData(SignInActivity.this).getName());
        Glide.with(SignInActivity.this)
                .load(Constant.IMAGE_BASE_URL + Utils.loadSchoolData(SignInActivity.this).getLogo())
                .thumbnail(0.5f)
                .into(imgSchool);

        if (getIntent() != null) {
            if (getIntent().getExtras().getInt(Constant.LOGIN_TYPE) == Constant.LOGIN_TYPE_PARENT) {
                toolbarTitle.setText(getString(R.string.kParent) + " " + getString(R.string.kLoginText));
            } else if (getIntent().getExtras().getInt(Constant.LOGIN_TYPE) == Constant.LOGIN_TYPE_STUDENT) {
                toolbarTitle.setText(getString(R.string.kStudent) + " " + getString(R.string.kLoginText));
            } else if (getIntent().getExtras().getInt(Constant.LOGIN_TYPE) == Constant.LOGIN_TYPE_FACULTY) {
                toolbarTitle.setText(getString(R.string.kFacultyText) + " " + getString(R.string.kLoginText));
            }
        }
        loginFooter.setText(getString(R.string.kFooterNotesTextBeforeLogin) + Utils.loadSchoolData(SignInActivity.this).getName());
        mNetworkPresenter = new NetworkPresenter(this, new NetworkInteractor());

        emailSignInButton.setBackground(getDrawable(buttonThemeId));
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private boolean attemptLogin() {
        email_edit.setError(null);
        password_Edit.setError(null);
        String email = email_edit.getText().toString();
        String password = password_Edit.getText().toString();
        boolean cancel = false;
        View focusView = null;
        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            password_Edit.setError(getString(R.string.error_invalid_password));
            focusView = password_Edit;
            cancel = true;
            return cancel;
        }
        if (TextUtils.isEmpty(email)) {
            email_edit.setError(getString(R.string.error_field_required));
            focusView = email_edit;
            cancel = true;
            return cancel;
        }

        if (TextUtils.isEmpty(password)) {
            password_Edit.setError(getString(R.string.error_field_required));
            focusView = password_Edit;
            cancel = true;
            return cancel;
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            return false;
        } else {
            return false;
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    @OnClick(R.id.email_sign_in_button)
    void setEmailSignInButton() {
        if (!attemptLogin()) {
            if (getIntent() != null) {
                LoginUserRequest mloginReq = new LoginUserRequest();
                mloginReq.setSchool_id(Constant.UNIQUE_SCHOOL_ID);
                mloginReq.setUsername(email_edit.getText().toString().trim());
                mloginReq.setPassword(password_Edit.getText().toString());
                mloginReq.setAndroid_device_id( prefData.getFcmToken());
                emailSignInButton.startAnimation();
                if (getIntent().getExtras().getInt(Constant.LOGIN_TYPE) == Constant.LOGIN_TYPE_PARENT) {
                    mNetworkPresenter.methodToLoginAsParent(SignInActivity.this, mloginReq);
                } else if (getIntent().getExtras().getInt(Constant.LOGIN_TYPE) == Constant.LOGIN_TYPE_STUDENT) {
                    mNetworkPresenter.methodToLoginAsStudent(SignInActivity.this, mloginReq);
                } else if (getIntent().getExtras().getInt(Constant.LOGIN_TYPE) == Constant.LOGIN_TYPE_FACULTY) {
                    mNetworkPresenter.methodToLoginAsFaculty(SignInActivity.this, mloginReq);
                }
            }
        }
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void noInternetConnection() {
     Utils.showSnackBar(SignInActivity.this, getString(R.string.check_internet));
    }

    @Override
    public void onEndpointSuccess(Object object, String message, EndpointEnums endpointType) {
    switch (endpointType){
        case  login:
            Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.ic_done_white_48dp);
            emailSignInButton.doneLoadingAnimation(R.color.white, icon);
            final Handler handler = new Handler();
            handler.postDelayed(() -> emailSignInButton.revertAnimation(), 1000);
            switch (getIntent().getExtras().getInt(Constant.LOGIN_TYPE)) {
                case Constant.LOGIN_TYPE_PARENT:
                    ParentLoginResponse parentLoginResponse = (ParentLoginResponse) object;
                    prefData.setLoginStatus(true);
                    prefData.setLoginType(Constant.LOGIN_TYPE_PARENT);
                    Utils.savePreferences(Constant.PREF_LOGGED_IN_DATA, new Gson().toJson(parentLoginResponse), SignInActivity.this);
                    Intent intent = new Intent(this, ParentHomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    break;
                case Constant.LOGIN_TYPE_STUDENT:
                    StudentLoginResponse studentLoginResponse = (StudentLoginResponse) object;
                    prefData.setLoginStatus(true);
                    prefData.setLoginType(Constant.LOGIN_TYPE_STUDENT);
                    Utils.savePreferences(Constant.PREF_LOGGED_IN_DATA, new Gson().toJson(studentLoginResponse),
                            SignInActivity.this);
                    Intent studenIntent = new Intent(this, ParentHomeActivity.class);
                    studenIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(studenIntent);
                    break;
                case Constant.LOGIN_TYPE_FACULTY:
                    FacultyLoginResponse facultyLoginResponse = (FacultyLoginResponse)object;
                    prefData.setLoginStatus(true);
                    prefData.setLoginType(Constant.LOGIN_TYPE_FACULTY);
                    Utils.savePreferences(Constant.PREF_LOGGED_IN_DATA, new Gson().toJson(facultyLoginResponse), SignInActivity.this);
                    intent = new Intent(this, FacultyHomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    break;
            }
            break;
    }
    }

    @Override
    public void onEndpointFailure(String message, EndpointEnums endpointType) {
        switch (endpointType){
            case login:
                    Bitmap icon = BitmapFactory.decodeResource(getResources(),R.drawable.ic_clear_white_24dp);
                    emailSignInButton.doneLoadingAnimation(R.color.white, icon);
                    Utils.showSnackBar(SignInActivity.this, message);

                final Handler handler = new Handler();
                handler.postDelayed(() -> emailSignInButton.revertAnimation(), 2000);

                break;
        }
    }
}

