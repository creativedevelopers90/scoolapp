package com.scool.qt.base.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.scool.qt.R;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder> {


    private GalleryAdapterListener mListener;
    private List<String> mGalleryList;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.gallery_img_row)
        RecyclingImageView galleryImgRow;

        @BindView(R.id.layout_for_pick_a_beat_item)
        RelativeLayout rootView;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bindItems(String mFacultiesItem) {
            Log.d("URL", Constant.IMAGE_BASE_URL + mFacultiesItem);
            Glide.with(mContext).load(Constant.IMAGE_BASE_URL + mFacultiesItem)
                    .thumbnail(0.5f)
                    .into(galleryImgRow);

            rootView.setOnClickListener(v -> mListener.onGalleryItemSelected(mFacultiesItem));
        }
    }
    public GalleryAdapter(List<String> mGalleryList, Context mContext, GalleryAdapterListener mListener) {
        this.mGalleryList = mGalleryList;
        this.mContext = mContext;
        this.mListener = mListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_item_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String mFacultiesItem = mGalleryList.get(position);
        holder.bindItems(mFacultiesItem);
    }

    @Override
    public int getItemCount() {
        return mGalleryList.size();
    }

    /**
     * Gallery callback listener
     */
    public interface GalleryAdapterListener{
        public void onGalleryItemSelected(String mGalleryImagePath);
    }


}

