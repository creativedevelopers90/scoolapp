package com.scool.qt.base.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;

import com.google.firebase.iid.FirebaseInstanceId;
import com.scool.qt.BaseActivity;
import com.scool.qt.R;
import com.scool.qt.activity.ParentHomeActivity;
import com.scool.qt.faculty.activity.FacultyHomeActivity;
import com.scool.qt.utils.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class SplashActivity extends BaseActivity {
    @BindView(R.id.imageView5)
    ImageView imageView5;
    @BindView(R.id.progressBar2)
    ProgressBar progressBar2;
    Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        final Handler handler = new Handler();
        handler.postDelayed(() -> checkUserStatus(), 2000);
    }


    private void checkUserStatus() {
        // Get FCM token using Firebase InstanceId
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult ->  {
            Timber.d("FIREBASE TOKEN ==>" +  instanceIdResult.getToken());
            Log.d("TOKEN ===>", instanceIdResult.getToken());
            if (prefData.getLoginStatus()) {
                switch (prefData.getLoginType()) {
                    case Constant.LOGIN_TYPE_FACULTY:
                        intent = new Intent(this, FacultyHomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                        break;
                    case Constant.LOGIN_TYPE_PARENT:
                    case Constant.LOGIN_TYPE_STUDENT:
                        intent = new Intent(this, ParentHomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                        break;
                }
            }else {
                intent = new Intent(this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

    }
}
