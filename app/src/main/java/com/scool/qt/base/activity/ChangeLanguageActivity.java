package com.scool.qt.base.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.scool.qt.BaseActivity;
import com.scool.qt.R;
import com.scool.qt.activity.ParentHomeActivity;
import com.scool.qt.faculty.activity.FacultyHomeActivity;
import com.scool.qt.helper.LocaleHelper;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangeLanguageActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;

    @BindView(R.id.top_layout)
    ConstraintLayout topLayout;
    @BindView(R.id.img_changelang)
    RecyclingImageView imgChangelang;
    @BindView(R.id.tv_english)
    TextView tvEnglish;
    @BindView(R.id.iv_lang_eng)
    ImageView ivLangEng;
    @BindView(R.id.view_changelang)
    View viewChangelang;
    @BindView(R.id.change_lang_eng)
    ConstraintLayout changeLangEng;
    @BindView(R.id.img_chngethmem)
    RecyclingImageView imgChngethmem;
    @BindView(R.id.tv_arabic)
    TextView tvArabic;
    @BindView(R.id.view_thme)
    View viewThme;
    @BindView(R.id.iv_lang_arabic)
    ImageView ivLangArabic;
    @BindView(R.id.change_lang_arabic)
    ConstraintLayout changeLangArabic;
    @BindView(R.id.setting_footer)
    AppCompatTextView settingFooter;
    @BindView(R.id.btn_done)
    Button btnDone;
    private String langSelect = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changelang);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> finish());
        toolbarTitle.setText(getString(R.string.kLanguageSelectionText));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        btnDone.setClickable(false);
        btnDone.setEnabled(false);
        tvSchoolname.setText(Utils.loadSchoolData(ChangeLanguageActivity.this).getName());
        Glide.with(ChangeLanguageActivity.this)
                .load(Constant.IMAGE_BASE_URL + Utils.loadSchoolData(ChangeLanguageActivity.this).getLogo())
                .thumbnail(0.5f)
                .into(imgSchool);
        langSelect = LocaleHelper.getLanguage(ChangeLanguageActivity.this);
        if (langSelect.equalsIgnoreCase("en")) {
           ivLangEng.setBackgroundResource(R.drawable.iv_mark_select);
           ivLangArabic.setBackgroundResource(R.drawable.iv_mark_unselected);
        } else {
            ivLangArabic.setBackgroundResource(R.drawable.iv_mark_select);
            ivLangEng.setBackgroundResource(R.drawable.iv_mark_unselected);
        }
        btnDone.setBackground(getDrawable(buttonThemeId));
    }

    @OnClick(R.id.change_lang_eng)
    void setChangeLangEng() {
        langSelect = "en";
        btnDone.setClickable(true);
        btnDone.setEnabled(true);
        if (!langSelect.equalsIgnoreCase("en")) {
            ivLangEng.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivLangArabic.setBackgroundResource(R.drawable.iv_mark_select);
        } else {
            ivLangArabic.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivLangEng.setBackgroundResource(R.drawable.iv_mark_select);
        }
    }

    @OnClick(R.id.change_lang_arabic)
    void setChangeLangArabic() {
        langSelect = "ar";
        btnDone.setClickable(true);
        btnDone.setEnabled(true);
        if (!langSelect.equalsIgnoreCase("ar")) {
            ivLangEng.setBackgroundResource(R.drawable.iv_mark_select);
            ivLangArabic.setBackgroundResource(R.drawable.iv_mark_unselected);
        } else {
            ivLangArabic.setBackgroundResource(R.drawable.iv_mark_select);
            ivLangEng.setBackgroundResource(R.drawable.iv_mark_unselected);
        }

    }

    private void updateViews(String languageCode) {
        LocaleHelper.setLocale(ChangeLanguageActivity.this, languageCode);
        Context context = LocaleHelper.setLocale(this, languageCode);
        Resources resources = context.getResources();
        tvArabic.setText(resources.getString(R.string.kArabic));
        tvEnglish.setText(resources.getString(R.string.kEnglish));
        toolbarTitle.setText(resources.getString(R.string.kLanguageSelectionText));
        if (prefData.getLoginStatus()) {
            switch (prefData.getLoginType()) {
                case Constant.LOGIN_TYPE_FACULTY:
                    Intent intent = new Intent(this, FacultyHomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                    break;
                case Constant.LOGIN_TYPE_PARENT:
                case Constant.LOGIN_TYPE_STUDENT:
                    Intent intent1 = new Intent(this, ParentHomeActivity.class);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent1);
                    finish();
                    break;
            }
        }else {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

    }

    @OnClick(R.id.btn_done)
    void clickBtnDone() {
        updateViews(langSelect);
    }

}

