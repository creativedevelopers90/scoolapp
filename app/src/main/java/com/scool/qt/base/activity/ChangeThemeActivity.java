package com.scool.qt.base.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.scool.qt.BaseActivity;
import com.scool.qt.R;
import com.scool.qt.activity.ParentHomeActivity;
import com.scool.qt.faculty.activity.FacultyHomeActivity;
import com.scool.qt.helper.ThemeHelper;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.RecyclingImageView;
import com.scool.qt.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangeThemeActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.img_school)
    RecyclingImageView imgSchool;
    @BindView(R.id.tv_schoolname)
    TextView tvSchoolname;
    @BindView(R.id.top_layout)
    ConstraintLayout topLayout;
    @BindView(R.id.img_changelang)
    CardView imgChangelang;
    @BindView(R.id.tv_english)
    TextView tvEnglish;
    @BindView(R.id.iv_lang_eng)
    ImageView ivLangEng;
    @BindView(R.id.view_changelang)
    View viewChangelang;
    @BindView(R.id.layout_blue)
    ConstraintLayout layoutBlue;
    @BindView(R.id.cv_red)
    CardView cvRed;
    @BindView(R.id.tv_red)
    TextView tvRed;
    @BindView(R.id.red_view_theme)
    View redViewTheme;
    @BindView(R.id.iv_lang_red)
    ImageView ivLangRed;
    @BindView(R.id.layout_red)
    ConstraintLayout layoutRed;
    @BindView(R.id.cv_green)
    CardView cvGreen;
    @BindView(R.id.tv_green)
    TextView tvGreen;
    @BindView(R.id.green_view_thme)
    View greenViewThme;
    @BindView(R.id.iv_green)
    ImageView ivGreen;
    @BindView(R.id.layout_green)
    ConstraintLayout layoutGreen;
    @BindView(R.id.cv_purple)
    CardView cvPurple;
    @BindView(R.id.tv_purple)
    TextView tvPurple;
    @BindView(R.id.purple_view_theme)
    View purpleViewTheme;
    @BindView(R.id.iv_purple)
    ImageView ivPurple;
    @BindView(R.id.layout_purple)
    ConstraintLayout layoutPurple;
    @BindView(R.id.cv_black)
    CardView cvBlack;
    @BindView(R.id.tv_black)
    TextView tvBlack;
    @BindView(R.id.black_view_theme)
    View blackViewTheme;
    @BindView(R.id.iv_black)
    ImageView ivBlack;
    @BindView(R.id.layout_black)
    ConstraintLayout layoutBlack;
    @BindView(R.id.cv_brown)
    CardView cvBrown;
    @BindView(R.id.tv_brown)
    TextView tvBrown;
    @BindView(R.id.brown_view_theme)
    View brownViewTheme;
    @BindView(R.id.iv_brown)
    ImageView ivBrown;
    @BindView(R.id.layout_brown)
    ConstraintLayout layoutBrown;
    @BindView(R.id.setting_footer)
    AppCompatTextView settingFooter;
    @BindView(R.id.constraintLayout)
    ConstraintLayout constraintLayout;
    @BindView(R.id.btn_done)
    Button btnDone;
    private String themeSelect = "";
    String theme;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_theme);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> finish());
        toolbarTitle.setText(getString(R.string.kChangeTheme));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        btnDone.setClickable(false);
        btnDone.setEnabled(false);
        tvSchoolname.setText(Utils.loadSchoolData(ChangeThemeActivity.this).getName());
        Glide.with(ChangeThemeActivity.this)
                .load(Constant.IMAGE_BASE_URL + Utils.loadSchoolData(ChangeThemeActivity.this).getLogo())
                .thumbnail(0.5f)
                .into(imgSchool);
    }

    @OnClick(R.id.btn_done)
    void clickBtnDone() {
        ThemeHelper.setTheme(this, "green");
    }



    @OnClick(R.id.layout_blue)
    void setBlueButton(){
        ThemeHelper.setTheme(this, "blue");
        adjustCheckItem("blue");
        routeToMainActivity();
    }



    @OnClick(R.id.layout_red)
    void setRedButton(){
        ThemeHelper.setTheme(this, "red");
        adjustCheckItem("red");
        routeToMainActivity();
    }

    @OnClick(R.id.layout_green)
    void setGreenItem(){
        ThemeHelper.setTheme(this, "green");
        adjustCheckItem("green");
        routeToMainActivity();
    }

    @OnClick(R.id.layout_purple)
    void setPurpleItem(){
        ThemeHelper.setTheme(this, "purple");
        adjustCheckItem("purple");
        routeToMainActivity();
    }

    @OnClick(R.id.layout_black)
    void setBlackItem(){
        ThemeHelper.setTheme(this, "black");
        adjustCheckItem("black");
        routeToMainActivity();
    }

    @OnClick(R.id.layout_brown)
    void setBrownItem(){
        ThemeHelper.setTheme(this, "brown");
        adjustCheckItem("brown");
        routeToMainActivity();
    }

    @Override
    protected void onResume() {
        super.onResume();
        themeSelect = ThemeHelper.getPrefCurrentTheme(this);
        if (themeSelect.equalsIgnoreCase("blue")) {
            adjustCheckItem("blue");
        } else if(themeSelect.equalsIgnoreCase("red")) {
            adjustCheckItem("red");
        }else if(themeSelect.equalsIgnoreCase("green")) {
            adjustCheckItem("green");
        } else if(themeSelect.equalsIgnoreCase("purple")) {
            adjustCheckItem("purple");
        } else if(themeSelect.equalsIgnoreCase("black")) {
            adjustCheckItem("black");
        } else if(themeSelect.equalsIgnoreCase("brown")) {
            adjustCheckItem("brown");
        }
    }

    void adjustCheckItem(String themeSelect) {
        if(themeSelect.equalsIgnoreCase("blue")){
            ivLangEng.setBackgroundResource(R.drawable.iv_mark_select);
            ivLangRed.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivGreen.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivPurple.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivBlack.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivBrown.setBackgroundResource(R.drawable.iv_mark_unselected);
        } else if(themeSelect.equalsIgnoreCase("red")){
            ivLangEng.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivLangRed.setBackgroundResource(R.drawable.iv_mark_select);
            ivGreen.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivPurple.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivBlack.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivBrown.setBackgroundResource(R.drawable.iv_mark_unselected);
        } else if(themeSelect.equalsIgnoreCase("green")) {
            ivLangEng.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivLangRed.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivGreen.setBackgroundResource(R.drawable.iv_mark_select);
            ivPurple.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivBlack.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivBrown.setBackgroundResource(R.drawable.iv_mark_unselected);
        }else if(themeSelect.equalsIgnoreCase("purple")) {
            ivLangEng.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivLangRed.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivGreen.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivPurple.setBackgroundResource(R.drawable.iv_mark_select);
            ivBlack.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivBrown.setBackgroundResource(R.drawable.iv_mark_unselected);
        }else if(themeSelect.equalsIgnoreCase("black")) {
            ivLangEng.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivLangRed.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivGreen.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivPurple.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivBlack.setBackgroundResource(R.drawable.iv_mark_select);
            ivBrown.setBackgroundResource(R.drawable.iv_mark_unselected);
        }else if(themeSelect.equalsIgnoreCase("brown")) {
            ivLangEng.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivLangRed.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivGreen.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivPurple.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivBlack.setBackgroundResource(R.drawable.iv_mark_unselected);
            ivBrown.setBackgroundResource(R.drawable.iv_mark_select);
        }

    }

    private void routeToMainActivity() {
        if (prefData.getLoginStatus()) {
            switch (prefData.getLoginType()) {
                case Constant.LOGIN_TYPE_FACULTY:
                    intent = new Intent(this, FacultyHomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    break;
                case Constant.LOGIN_TYPE_PARENT:

                case Constant.LOGIN_TYPE_STUDENT:
                    intent = new Intent(this, ParentHomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    break;
            }
        }else {
            intent = new Intent(this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
    }
}
