package com.scool.qt.base.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.scool.qt.BaseActivity;
import com.scool.qt.R;
import com.scool.qt.base.fragment.AboutUsFragment;
import com.scool.qt.base.fragment.ContactUsFragment;
import com.scool.qt.base.fragment.EventsFragment;
import com.scool.qt.base.fragment.FacultyFragment;
import com.scool.qt.base.fragment.GalleryFragment;
import com.scool.qt.base.fragment.LoginFragment;
import com.scool.qt.base.fragment.SettingsFragment;
import com.scool.qt.fragments.HomeFragment;
import com.scool.qt.helper.LocaleHelper;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.content_frame)
    FrameLayout contentFrame;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_home);
            ButterKnife.bind(this);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawerLayout.addDrawerListener(toggle);
            navView.getHeaderView(0).setBackground(getDrawable(sideNavId));
            toggle.syncState();
            navView.setNavigationItemSelectedListener(this);
            update("update");
            toolbarTitle.setText(getString(R.string.kHomeText));
            Fragment home = new HomeFragment();
            FragmentManager FM = getSupportFragmentManager();
            FM.beginTransaction().replace(R.id.content_frame, home).commit();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        drawerLayout.closeDrawer(GravityCompat.START);
        openSelectedFragment(item);
        return true;
    }

    private void openSelectedFragment(MenuItem item) {
        Fragment fragment = null;
        Class fragmentClass = null;
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_home:
                fragmentClass = HomeFragment.class;
                toolbarTitle.setText(getString(R.string.kHomeText));
                break;
            case R.id.nav_aboutus:
                fragmentClass = AboutUsFragment.class;
                toolbarTitle.setText(getString(R.string.kAboutUsText));
                break;
            case R.id.nav_contact_us:
                fragmentClass = ContactUsFragment.class;
                toolbarTitle.setText(getString(R.string.kContactUsText));
                break;
            case R.id.nav_faculty:
                fragmentClass = FacultyFragment.class;
                toolbarTitle.setText(getString(R.string.kFacultyText));
                break;
            case R.id.nav_events:
                fragmentClass = EventsFragment.class;
                toolbarTitle.setText(getString(R.string.kEventsText));
                break;
            case R.id.nav_gallery:
                fragmentClass = GalleryFragment.class;
                toolbarTitle.setText(getString(R.string.kGalleryText));
                break;
            case R.id.nav_login:
                fragmentClass = LoginFragment.class;
                toolbarTitle.setText(getString(R.string.kLoginText));
                break;
            case R.id.nav_settings:
                fragmentClass = SettingsFragment.class;
                toolbarTitle.setText(getString(R.string.kSettingsText));
                break;
            default:
                fragmentClass = HomeFragment.class;
                toolbarTitle.setText(getString(R.string.kHomeText));
                break;
        }
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));

    }


    public void update(String msg) {
     if(msg.equalsIgnoreCase("updateActivity")){
         View hView =  navView.getHeaderView(0);
         TextView tv_schoolname = hView.findViewById(R.id.tv_schoolname);
         TextView tv_school_slogan = hView.findViewById(R.id.tv_school_slogan);
         ImageView imageView = hView.findViewById(R.id.imageView);
         if(Utils.loadSchoolData(HomeActivity.this) != null){
             tv_schoolname.setText(Utils.loadSchoolData(HomeActivity.this) != null ?
                     Utils.loadSchoolData(HomeActivity.this).getName(): "school" );

             tv_school_slogan.setText(Utils.loadSchoolData(HomeActivity.this) != null ?
                     Utils.loadSchoolData(HomeActivity.this).getSlogan(): "school" );

             Glide.with(HomeActivity.this)
                     .load(Constant.IMAGE_BASE_URL+ Utils.loadSchoolData(HomeActivity.this).getLogo())
                     .thumbnail(0.5f)
                     .into(imageView);
         }
     }
    }
}
