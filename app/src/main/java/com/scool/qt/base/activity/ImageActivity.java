package com.scool.qt.base.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.appbar.AppBarLayout;
import com.scool.qt.BaseActivity;
import com.scool.qt.R;
import com.scool.qt.model.CircularsItem;
import com.scool.qt.utils.Constant;
import com.scool.qt.utils.ZoomableImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageActivity extends BaseActivity {

    @BindView(R.id.image_preview_iv)
    ZoomableImageView imagePreviewIV;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitleTV;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.applayout)
    AppBarLayout applayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        String type = bundle.getString("type");
        setSptActionBar();

        if (type.equals("event")) {
            CircularsItem mItem = (CircularsItem) bundle.getSerializable("event_item");
            toolbarTitleTV.setText(mItem.getDate());
            Glide.with(this)
                    .asBitmap()
                    .load(Constant.IMAGE_BASE_URL + mItem.getPhoto())
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            imagePreviewIV.setImageBitmap(resource);
                        }
                    });
        } else if (type.equals("gallery")) {
            toolbarTitleTV.setText(R.string.gallery_header);
            String imagePath = bundle.getString("gallery_image_path");
            Glide.with(this)
                    .asBitmap()
                    .load(Constant.IMAGE_BASE_URL + imagePath)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            imagePreviewIV.setImageBitmap(resource);
                        }
                    });
        }
    }

    private void setSptActionBar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> finish());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
